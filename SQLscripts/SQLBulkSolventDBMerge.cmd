del mycmd.sql


echo CREATE TABLE BulkSolventTbl (TargetPDBid text, Fullmodel text, SpaceGroup text, SpaceGroupNumber int, Hires real, Lowres real, TwinningLtest real, NumberofReflections int, UsedReflections int, NumberofResiduesinTarget int, NumberofResiduesinModel int, ASUvolume real, MatthewsCoefficient real, CCmap real, Bfactor real, VRMS real, LLgvrms real, Fsol real, Bsol real); >> mycmd.sql

echo attach "NoSolParam.db" as NoSolParam_db; >> mycmd.sql
echo INSERT INTO BulkSolventTbl >> mycmd.sql
echo SELECT t.TargetPDBid, m.Fullmodel, t.Spacegroup, t.Spacegroupnumber, t.hires, t.lowres, t.twinLtest AS TwinningLtest, t.Allreflections AS NumberofReflections, m.UsedReflections, t.NumberofResiduesinTarget, m.NumberofResiduesinModel,  t.ASUvolume, t.MatthewsCoefficient, s.CCmap0 AS CCmap, s.Bfac0 AS Bfactor, s.VRMS0 AS VRMS, s.LLgvrms AS LLGvrms, NULL AS Fsol, NULL AS Bsol >> mycmd.sql
echo FROM NoSolParam_db.MRModelTbl m, NoSolParam_db.MRTargetTbl t, NoSolParam_db.TemplateRMSTbl s WHERE m.MRTarget_id = t.p_id AND s.MRModel_id = m.p_id; >> mycmd.sql
echo detach database NoSolParam_db; >> mycmd.sql

@FOR %%b IN (20, 30, 50, 100, 200) DO @(
	@FOR %%k IN (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7) DO @(
		echo attach "Fsol%%k_Bsol_%%b.db" as F%%k_B%%b_db; >> mycmd.sql
		echo INSERT INTO BulkSolventTbl >> mycmd.sql
		echo SELECT t.TargetPDBid, m.Fullmodel, t.Spacegroup, t.Spacegroupnumber, t.hires, t.lowres, t.twinLtest AS TwinningLtest, t.Allreflections AS NumberofReflections, m.UsedReflections, t.NumberofResiduesinTarget, m.NumberofResiduesinModel,  t.ASUvolume, t.MatthewsCoefficient, s.CCmap0 AS CCmap, s.Bfac0 AS Bfactor, s.VRMS0 AS VRMS, s.LLgvrms AS LLGvrms, %%k AS Fsol, %%b AS Bsol >> mycmd.sql
		echo FROM F%%k_B%%b_db.MRModelTbl m, F%%k_B%%b_db.MRTargetTbl t, F%%k_B%%b_db.TemplateRMSTbl s WHERE m.MRTarget_id = t.p_id AND s.MRModel_id = m.p_id;  >> mycmd.sql
		echo detach database F%%k_B%%b_db; >> mycmd.sql
	)
)

echo attach "B:\Users\oeffner\HomologySearch\Completeresults.db" AS completeDB; >> mycmd.sql
echo UPDATE BulkSolventTbl SET MatthewsCoefficient = (SELECT t.MatthewsCoefficient FROM completeDB.MRTargetTbl t WHERE t.TargetPDBid=BulkSolventTbl.TargetPDBid); >> mycmd.sql
echo detach database completeDB; >> mycmd.sql

