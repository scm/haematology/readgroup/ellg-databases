

attach "Sculpt1.db" as tomerge;

ALTER TABLE MRModelTbl ADD iLLGtemplateSculpt1 real;
UPDATE MRModelTbl SET iLLGtemplateSculpt1 = Null;
UPDATE MRModelTbl SET iLLGtemplateSculpt1 = 
					(
						SELECT m.iLLGtemplate 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om 
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

ALTER TABLE MRModelTbl ADD ModelCompletenessSculpt1 real;
UPDATE MRModelTbl SET ModelCompletenessSculpt1 = Null;
UPDATE MRModelTbl SET ModelCompletenessSculpt1 = 
					(
						SELECT m.ModelCompleteness 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om 
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

ALTER TABLE MRModelTbl ADD SSMseqidSculpt1 real;
UPDATE MRModelTbl SET SSMseqidSculpt1 = Null;
UPDATE MRModelTbl SET SSMseqidSculpt1 = 
					(
						SELECT m.SSMseqid 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om 
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

ALTER TABLE MRModelTbl ADD SSMlengthSculpt1 int;
UPDATE MRModelTbl SET SSMlengthSculpt1 = Null;
UPDATE MRModelTbl SET SSMlengthSculpt1 = 
					(
						SELECT m.SSMlength 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om 
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

ALTER TABLE MRModelTbl ADD NumberofResiduesinModelSculpt1 int;
UPDATE MRModelTbl SET NumberofResiduesinModelSculpt1 = Null;
UPDATE MRModelTbl SET NumberofResiduesinModelSculpt1 = 
					(
						SELECT m.NumberofResiduesinModel 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om 
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

ALTER TABLE MRModelTbl ADD TmplVRMSSculpt1 real;
UPDATE MRModelTbl SET TmplVRMSSculpt1 = Null;
UPDATE MRModelTbl SET TmplVRMSSculpt1 = 
					(
						SELECT tmpl.VRMS 
						FROM tomerge.MRModelTbl m, tomerge.MRTargetTbl t, MRTargetTbl om, tomerge.TemplateRMSTbl tmpl
						WHERE m.Fullmodel = MRModelTbl.Fullmodel
						AND tmpl.MRModel_id = m.p_id
						AND m.MRTarget_id =  t.p_id
						AND t.TargetPDBid = om.TargetPDBid
						AND om.p_id = MRModelTbl.MRTarget_id
                    );

detach database toMerge;


ALTER TABLE MRModelTbl ADD TmplVRMSSculpt1 real;
UPDATE MRModelTbl SET TmplVRMSSculpt1 = Null;
UPDATE MRModelTbl SET TmplVRMSSculpt1 = (SELECT s.VRMS FROM TemplateRMSTbl s WHERE s.MRModel_id = MRModelTbl.p_id);

CREATE VIEW DistinctMRTargets AS SELECT * FROM (SELECT ROWID, * FROM MRTargetTbl GROUP BY TargetPDBid, SCOPid) ORDER BY ROWID;
.read AllSculptprtclMLAD.sql


CREATE VIEW UniqueMRresultsTbl AS SELECT
t.TargetPDBid,
t.SCOPid,
t.NumberofUniqueSCOPids,
t.TargetClass,
t.TargetFold,
t.TargetSuperfamily,
t.TargetFamily,
t.Spacegroup,
t.Spacegroupnumber,
t.IsPolarSpacegroup,
t.Crystalsystem,
t.MatthewsCoefficient,
t.NumberofResiduesinTarget,
t.Resolution,
t.Unitcell,
t.WilsonB,
t.WilsonScale,
t.TargetScattering,
t.UsedReflections,
t.Allreflections,
t.ASUvolume,
t.Robs,
t.Rall,
t.Rwork,
t.Rfree,
t.EDSentry,
m.p_id,
m.Fullmodel,
m.NewRMSD,
m.SSMrms,
m.Comment,
m.ModelOrder,
m.Foundit,
m.SolutionRank,
m.PartialModel,
m.ModelResolution,
m.SequenceIdentity,
m.ClustalWlength,
m.ModelSCOPid,
m.NumberofUniqueModelSCOPids,
m.ModelClass,
m.ModelFold,
m.ModelSuperfamily,
m.ModelFamily,
m.C_L_rms,

m.iLLGtemplateSculpt1,
m.ModelCompletenessSculpt1,
m.SSMseqidSculpt1,
m.SSMlengthSculpt1,
m.TmplVRMSSculpt1,
m.NumberofResiduesinModelSculpt1,
m.Sculpt1templMLAD,

m.iLLGtemplateSculpt2,
m.ModelCompletenessSculpt2,
m.SSMseqidSculpt2,
m.SSMlengthSculpt2,
m.TmplVRMSSculpt2,
m.NumberofResiduesinModelSculpt2,
m.Sculpt2templMLAD,

m.iLLGtemplateSculpt3,
m.ModelCompletenessSculpt3,
m.SSMseqidSculpt3,
m.SSMlengthSculpt3,
m.TmplVRMSSculpt3,
m.NumberofResiduesinModelSculpt3,
m.Sculpt3templMLAD,

m.iLLGtemplateSculpt4,
m.ModelCompletenessSculpt4,
m.SSMseqidSculpt4,
m.SSMlengthSculpt4,
m.TmplVRMSSculpt4,
m.NumberofResiduesinModelSculpt4,
m.Sculpt4templMLAD,

m.iLLGtemplateSculpt5,
m.ModelCompletenessSculpt5,
m.SSMseqidSculpt5,
m.SSMlengthSculpt5,
m.TmplVRMSSculpt5,
m.NumberofResiduesinModelSculpt5,
m.Sculpt5templMLAD,

m.iLLGtemplateSculpt6,
m.ModelCompletenessSculpt6,
m.SSMseqidSculpt6,
m.SSMlengthSculpt6,
m.TmplVRMSSculpt6,
m.NumberofResiduesinModelSculpt6,
m.Sculpt6templMLAD,

m.iLLGtemplateSculpt7,
m.ModelCompletenessSculpt7,
m.SSMseqidSculpt7,
m.SSMlengthSculpt7,
m.TmplVRMSSculpt7,
m.NumberofResiduesinModelSculpt7,
m.Sculpt7templMLAD,

m.iLLGtemplateSculpt8,
m.ModelCompletenessSculpt8,
m.SSMseqidSculpt8,
m.SSMlengthSculpt8,
m.TmplVRMSSculpt8,
m.NumberofResiduesinModelSculpt8,
m.Sculpt8templMLAD,

m.iLLGtemplateSculpt9,
m.ModelCompletenessSculpt9,
m.SSMseqidSculpt9,
m.SSMlengthSculpt9,
m.TmplVRMSSculpt9,
m.NumberofResiduesinModelSculpt9,
m.Sculpt9templMLAD,

m.iLLGtemplateSculpt10,
m.ModelCompletenessSculpt10,
m.SSMseqidSculpt10,
m.SSMlengthSculpt10,
m.TmplVRMSSculpt10,
m.NumberofResiduesinModelSculpt10,
m.Sculpt10templMLAD,

m.iLLGtemplateSculpt11,
m.ModelCompletenessSculpt11,
m.SSMseqidSculpt11,
m.SSMlengthSculpt11,
m.TmplVRMSSculpt11,
m.NumberofResiduesinModelSculpt11,
m.Sculpt11templMLAD,

m.iLLGtemplateSculpt12,
m.ModelCompletenessSculpt12,
m.SSMseqidSculpt12,
m.SSMlengthSculpt12,
m.TmplVRMSSculpt12,
m.NumberofResiduesinModelSculpt12,
m.Sculpt12templMLAD,

m.iLLGtemplateSculpt13,
m.ModelCompletenessSculpt13,
m.SSMseqidSculpt13,
m.SSMlengthSculpt13,
m.TmplVRMSSculpt13,
m.NumberofResiduesinModelSculpt13,
m.Sculpt13templMLAD

FROM MRModelTbl m, DistinctMRTargets t WHERE m.MRTarget_id = t.p_id;


CREATE VIEW GoodTemplates AS SELECT * 
FROM UniqueMRresultsTbl 
WHERE Sculpt1templMLAD < 1.5 
AND Sculpt2templMLAD < 1.5
AND Sculpt3templMLAD < 1.5
AND Sculpt4templMLAD < 1.5
AND Sculpt5templMLAD < 1.5
AND Sculpt6templMLAD < 1.5
AND Sculpt7templMLAD < 1.5 
AND Sculpt8templMLAD < 1.5 
AND Sculpt9templMLAD < 1.5 
AND Sculpt10templMLAD < 1.5 
AND Sculpt11templMLAD < 1.5 
AND Sculpt12templMLAD < 1.5 
AND Sculpt13templMLAD < 1.5;


CREATE VIEW SolvedTemplates AS SELECT *, 
(
AVG(iLLGtemplateSculpt1) +
AVG(iLLGtemplateSculpt2) +
AVG(iLLGtemplateSculpt3) +
AVG(iLLGtemplateSculpt4) +
AVG(iLLGtemplateSculpt5) +
AVG(iLLGtemplateSculpt6) +
AVG(iLLGtemplateSculpt7) +
AVG(iLLGtemplateSculpt8) +
AVG(iLLGtemplateSculpt9) +
AVG(iLLGtemplateSculpt10) +
AVG(iLLGtemplateSculpt11) +
AVG(iLLGtemplateSculpt12) +
AVG(iLLGtemplateSculpt13)
)/13.0 AS AvgLLG,
(
AVG(TmplVRMSSculpt1) +
AVG(TmplVRMSSculpt2) +
AVG(TmplVRMSSculpt3) +
AVG(TmplVRMSSculpt4) +
AVG(TmplVRMSSculpt5) +
AVG(TmplVRMSSculpt6) +
AVG(TmplVRMSSculpt7) +
AVG(TmplVRMSSculpt8) +
AVG(TmplVRMSSculpt9) +
AVG(TmplVRMSSculpt10) +
AVG(TmplVRMSSculpt11) +
AVG(TmplVRMSSculpt12) +
AVG(TmplVRMSSculpt13)
)/13.0 AS AvgVRMS
FROM UniqueMRresultsTbl 
WHERE Sculpt1templMLAD < 1.5 
OR Sculpt2templMLAD < 1.5
OR Sculpt3templMLAD < 1.5 
OR Sculpt4templMLAD < 1.5 
OR Sculpt5templMLAD < 1.5 
OR Sculpt6templMLAD < 1.5 
OR Sculpt7templMLAD < 1.5 
OR Sculpt8templMLAD < 1.5 
OR Sculpt9templMLAD < 1.5 
OR Sculpt10templMLAD < 1.5 
OR Sculpt11templMLAD < 1.5 
OR Sculpt12templMLAD < 1.5 
OR Sculpt13templMLAD < 1.5;




CREATE VIEW AvgDminval AS SELECT Resiter.i as Resolution, COUNT(*),
(
AVG(TmplVRMSSculpt1) +
AVG(TmplVRMSSculpt2) +
AVG(TmplVRMSSculpt3) +
AVG(TmplVRMSSculpt4) +
AVG(TmplVRMSSculpt5) +
AVG(TmplVRMSSculpt6) +
AVG(TmplVRMSSculpt7) +
AVG(TmplVRMSSculpt8) +
AVG(TmplVRMSSculpt9) +
AVG(TmplVRMSSculpt10) +
AVG(TmplVRMSSculpt11) +
AVG(TmplVRMSSculpt12) +
AVG(TmplVRMSSculpt13)
)/13.0 AS AvgVRMSResol,
(
AVG(iLLGtemplateSculpt1) +
AVG(iLLGtemplateSculpt2) +
AVG(iLLGtemplateSculpt3) +
AVG(iLLGtemplateSculpt4) +
AVG(iLLGtemplateSculpt5) +
AVG(iLLGtemplateSculpt6) +
AVG(iLLGtemplateSculpt7) +
AVG(iLLGtemplateSculpt8) +
AVG(iLLGtemplateSculpt9) +
AVG(iLLGtemplateSculpt10) +
AVG(iLLGtemplateSculpt11) +
AVG(iLLGtemplateSculpt12) +
AVG(iLLGtemplateSculpt13)
)/13.0 AS AvgLLGResol
FROM GoodTemplates m, Resiter 
WHERE m.Resolution <= (Resiter.i + 0.1) 
	AND m.Resolution > (Resiter.i - 0.1) 
	GROUP BY Resiter.i;



CREATE VIEW AvgResidval AS SELECT Residuesiter4.i as Residues, COUNT(*),
(
AVG(TmplVRMSSculpt1) +
AVG(TmplVRMSSculpt2) +
AVG(TmplVRMSSculpt3) +
AVG(TmplVRMSSculpt4) +
AVG(TmplVRMSSculpt5) +
AVG(TmplVRMSSculpt6) +
AVG(TmplVRMSSculpt7) +
AVG(TmplVRMSSculpt8) +
AVG(TmplVRMSSculpt9) +
AVG(TmplVRMSSculpt10) +
AVG(TmplVRMSSculpt11) +
AVG(TmplVRMSSculpt12) +
AVG(TmplVRMSSculpt13)
)/13.0 AS AvgVRMSResid,
(
AVG(iLLGtemplateSculpt1) +
AVG(iLLGtemplateSculpt2) +
AVG(iLLGtemplateSculpt3) +
AVG(iLLGtemplateSculpt4) +
AVG(iLLGtemplateSculpt5) +
AVG(iLLGtemplateSculpt6) +
AVG(iLLGtemplateSculpt7) +
AVG(iLLGtemplateSculpt8) +
AVG(iLLGtemplateSculpt9) +
AVG(iLLGtemplateSculpt10) +
AVG(iLLGtemplateSculpt11) +
AVG(iLLGtemplateSculpt12) +
AVG(iLLGtemplateSculpt13)
)/13.0 AS AvgLLGResid
FROM GoodTemplates m, Residuesiter4 
WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
	AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25) 
	GROUP BY Residuesiter4.i;



CREATE VIEW AvgSeqidval AS SELECT SEQiditer.i AS Seqid, COUNT(*),
(
AVG(iLLGtemplateSculpt1) +
AVG(iLLGtemplateSculpt2) +
AVG(iLLGtemplateSculpt3) +
AVG(iLLGtemplateSculpt4) +
AVG(iLLGtemplateSculpt5) +
AVG(iLLGtemplateSculpt6) +
AVG(iLLGtemplateSculpt7) +
AVG(iLLGtemplateSculpt8) +
AVG(iLLGtemplateSculpt9) +
AVG(iLLGtemplateSculpt10) +
AVG(iLLGtemplateSculpt11) +
AVG(iLLGtemplateSculpt12) +
AVG(iLLGtemplateSculpt13)
)/13.0 AS AvgLLGseqid
FROM GoodTemplates m, SEQiditer 
WHERE m.SequenceIdentity <= (SEQiditer.i + 1.5) 
	AND m.SequenceIdentity > (SEQiditer.i - 1.5) 
	GROUP BY SEQiditer.i;




SELECT r.i as Resid, COUNT(*), AvgLLGResid,
(AVG(iLLGtemplateSculpt1) - s.AvgLLGResid) AS dLLG1, 
(AVG(iLLGtemplateSculpt2) - s.AvgLLGResid) AS dLLG2, 
(AVG(iLLGtemplateSculpt3) - s.AvgLLGResid) AS dLLG3, 
(AVG(iLLGtemplateSculpt4) - s.AvgLLGResid) AS dLLG4, 
(AVG(iLLGtemplateSculpt5) - s.AvgLLGResid) AS dLLG5, 
(AVG(iLLGtemplateSculpt6) - s.AvgLLGResid) AS dLLG6, 
(AVG(iLLGtemplateSculpt7) - s.AvgLLGResid) AS dLLG7, 
(AVG(iLLGtemplateSculpt8) - s.AvgLLGResid) AS dLLG8, 
(AVG(iLLGtemplateSculpt9) - s.AvgLLGResid) AS dLLG9, 
(AVG(iLLGtemplateSculpt10) - s.AvgLLGResid) AS dLLG10, 
(AVG(iLLGtemplateSculpt11) - s.AvgLLGResid) AS dLLG11, 
(AVG(iLLGtemplateSculpt12) - s.AvgLLGResid) AS dLLG12, 
(AVG(iLLGtemplateSculpt13) - s.AvgLLGResid) AS dLLG13, 
AvgVRMSResid,
(AVG(TmplVRMSSculpt1) - s.AvgVRMSResid) AS dVRMS1, 
(AVG(TmplVRMSSculpt2) - s.AvgVRMSResid) AS dVRMS2, 
(AVG(TmplVRMSSculpt3) - s.AvgVRMSResid) AS dVRMS3, 
(AVG(TmplVRMSSculpt4) - s.AvgVRMSResid) AS dVRMS4, 
(AVG(TmplVRMSSculpt5) - s.AvgVRMSResid) AS dVRMS5, 
(AVG(TmplVRMSSculpt6) - s.AvgVRMSResid) AS dVRMS6, 
(AVG(TmplVRMSSculpt7) - s.AvgVRMSResid) AS dVRMS7, 
(AVG(TmplVRMSSculpt8) - s.AvgVRMSResid) AS dVRMS8, 
(AVG(TmplVRMSSculpt9) - s.AvgVRMSResid) AS dVRMS9, 
(AVG(TmplVRMSSculpt10) - s.AvgVRMSResid) AS dVRMS10, 
(AVG(TmplVRMSSculpt11) - s.AvgVRMSResid) AS dVRMS11, 
(AVG(TmplVRMSSculpt12) - s.AvgVRMSResid) AS dVRMS12, 
(AVG(TmplVRMSSculpt13) - s.AvgVRMSResid) AS dVRMS13 
FROM GoodTemplates m, Residuesiter4 r, AvgResidval s 
WHERE m.NumberofResiduesinTarget <= (r.i + 25) 
	AND m.NumberofResiduesinTarget > (r.i - 25) 
	AND Resid = s.Residues
	GROUP BY Resid;




SELECT r.i as Resol, COUNT(*), AvgLLGResol,
(AVG(iLLGtemplateSculpt1) - s.AvgLLGResol) AS dLLG1, 
(AVG(iLLGtemplateSculpt2) - s.AvgLLGResol) AS dLLG2, 
(AVG(iLLGtemplateSculpt3) - s.AvgLLGResol) AS dLLG3, 
(AVG(iLLGtemplateSculpt4) - s.AvgLLGResol) AS dLLG4, 
(AVG(iLLGtemplateSculpt5) - s.AvgLLGResol) AS dLLG5, 
(AVG(iLLGtemplateSculpt6) - s.AvgLLGResol) AS dLLG6, 
(AVG(iLLGtemplateSculpt7) - s.AvgLLGResol) AS dLLG7, 
(AVG(iLLGtemplateSculpt8) - s.AvgLLGResol) AS dLLG8, 
(AVG(iLLGtemplateSculpt9) - s.AvgLLGResol) AS dLLG9, 
(AVG(iLLGtemplateSculpt10) - s.AvgLLGResol) AS dLLG10, 
(AVG(iLLGtemplateSculpt11) - s.AvgLLGResol) AS dLLG11, 
(AVG(iLLGtemplateSculpt12) - s.AvgLLGResol) AS dLLG12, 
(AVG(iLLGtemplateSculpt13) - s.AvgLLGResol) AS dLLG13, 
AvgVRMSResol,
(AVG(TmplVRMSSculpt1) - s.AvgVRMSResol) AS dVRMS1, 
(AVG(TmplVRMSSculpt2) - s.AvgVRMSResol) AS dVRMS2, 
(AVG(TmplVRMSSculpt3) - s.AvgVRMSResol) AS dVRMS3, 
(AVG(TmplVRMSSculpt4) - s.AvgVRMSResol) AS dVRMS4, 
(AVG(TmplVRMSSculpt5) - s.AvgVRMSResol) AS dVRMS5, 
(AVG(TmplVRMSSculpt6) - s.AvgVRMSResol) AS dVRMS6, 
(AVG(TmplVRMSSculpt7) - s.AvgVRMSResol) AS dVRMS7, 
(AVG(TmplVRMSSculpt8) - s.AvgVRMSResol) AS dVRMS8, 
(AVG(TmplVRMSSculpt9) - s.AvgVRMSResol) AS dVRMS9, 
(AVG(TmplVRMSSculpt10) - s.AvgVRMSResol) AS dVRMS10, 
(AVG(TmplVRMSSculpt11) - s.AvgVRMSResol) AS dVRMS11, 
(AVG(TmplVRMSSculpt12) - s.AvgVRMSResol) AS dVRMS12, 
(AVG(TmplVRMSSculpt13) - s.AvgVRMSResol) AS dVRMS13 
FROM GoodTemplates m, Resiter r, AvgDminval s
WHERE m.Resolution <= (r.i + 0.1) 
	AND m.Resolution > (r.i - 0.1) 
	AND Resol = s.Resolution
	GROUP BY Resol;



SELECT r.i AS sequenceid, COUNT(*), AvgLLGseqid,
(AVG(iLLGtemplateSculpt1) - s.AvgLLGseqid) AS dLLG1, 
(AVG(iLLGtemplateSculpt2) - s.AvgLLGseqid) AS dLLG2, 
(AVG(iLLGtemplateSculpt3) - s.AvgLLGseqid) AS dLLG3, 
(AVG(iLLGtemplateSculpt4) - s.AvgLLGseqid) AS dLLG4, 
(AVG(iLLGtemplateSculpt5) - s.AvgLLGseqid) AS dLLG5, 
(AVG(iLLGtemplateSculpt6) - s.AvgLLGseqid) AS dLLG6, 
(AVG(iLLGtemplateSculpt7) - s.AvgLLGseqid) AS dLLG7, 
(AVG(iLLGtemplateSculpt8) - s.AvgLLGseqid) AS dLLG8, 
(AVG(iLLGtemplateSculpt9) - s.AvgLLGseqid) AS dLLG9, 
(AVG(iLLGtemplateSculpt10) - s.AvgLLGseqid) AS dLLG10, 
(AVG(iLLGtemplateSculpt11) - s.AvgLLGseqid) AS dLLG11, 
(AVG(iLLGtemplateSculpt12) - s.AvgLLGseqid) AS dLLG12, 
(AVG(iLLGtemplateSculpt13) - s.AvgLLGseqid) AS dLLG13 
FROM GoodTemplates m, SEQiditer r, AvgSeqidval s
WHERE m.SequenceIdentity <= (r.i + 1.5) 
	AND m.SequenceIdentity > (r.i - 1.5) 
	AND sequenceid = s.Seqid
	GROUP BY sequenceid;





SELECT FUllmodel, 
MAX(ModelCompletenessSculpt1, 
    ModelCompletenessSculpt2, 
    ModelCompletenessSculpt3, 
    ModelCompletenessSculpt4,
    ModelCompletenessSculpt5, 
    ModelCompletenessSculpt6, 
    ModelCompletenessSculpt7, 
    ModelCompletenessSculpt8, 
    ModelCompletenessSculpt9, 
    ModelCompletenessSculpt10, 
    ModelCompletenessSculpt11, 
    ModelCompletenessSculpt12, 
    ModelCompletenessSculpt13) 
AS Maxmodelcompleteness, 
MIN(ModelCompletenessSculpt1, 
	ModelCompletenessSculpt2, 
	ModelCompletenessSculpt3, 
	ModelCompletenessSculpt4, 
	ModelCompletenessSculpt5, 
	ModelCompletenessSculpt6, 
	ModelCompletenessSculpt7, 
	ModelCompletenessSculpt8, 
	ModelCompletenessSculpt9, 
	ModelCompletenessSculpt10, 
	ModelCompletenessSculpt11, 
	ModelCompletenessSculpt12, 
	ModelCompletenessSculpt13) 
AS Minmodelcompleteness 
FROM GoodTemplates 
WHERE (Maxmodelcompleteness - Minmodelcompleteness) < 0.03*Maxmodelcompleteness;



CREATE VIEW BestM1 AS SELECT COUNT(*) AS b1
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM2 AS SELECT COUNT(*) AS b2
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt2 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM3 AS SELECT COUNT(*) AS b3
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt3 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM4 AS SELECT COUNT(*) AS b4
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt4 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM5 AS SELECT COUNT(*) AS b5
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt5 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM6 AS SELECT COUNT(*) AS b6
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt6 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM7 AS SELECT COUNT(*) AS b7
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt7 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM8 AS SELECT COUNT(*) AS b8
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt1 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt8 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM9 AS SELECT COUNT(*) AS b9
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt9 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM10 AS SELECT COUNT(*) AS b10
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt10 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM11 AS SELECT COUNT(*) AS b11
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt11 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM12 AS SELECT COUNT(*) AS b12
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt1
AND m.iLLGtemplateSculpt12 >= m.iLLGtemplateSculpt13;


CREATE VIEW BestM13 AS SELECT COUNT(*) AS b13
FROM GoodTemplates m
WHERE m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt2
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt3
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt4
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt5
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt6
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt7
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt8
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt9
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt10
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt11
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt12
AND m.iLLGtemplateSculpt13 >= m.iLLGtemplateSculpt1;



SELECT BestM1.b1, BestM2.b2, BestM3.b3, BestM4.b4, BestM5.b5, BestM6.b6, BestM7.b7, 
   BestM8.b8, BestM9.b9, BestM10.b10, BestM11.b11, BestM12.b12, BestM13.b13
   
FROM BestM1, BestM2, BestM3, BestM4, BestM5, BestM6, BestM7, 
  BestM8, BestM9, BestM10, BestM11, BestM12, BestM13;
  
