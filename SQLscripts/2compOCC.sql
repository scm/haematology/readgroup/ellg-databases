

SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0;



SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0 
AND CCrnp0rnp1 < 0.2;


SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0 
AND CCocc0rnp1 > 0.2 
AND CCrnp0rnp1 < 0.2;


SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0 
AND (LLGocc1 - LLGrnp1) > 40 
AND CCocc0rnp1 > 0.2 
AND CCrnp0rnp1 < 0.2;



SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0 
AND (LLGocc1 - LLGrnp1) > 0 
AND CCocc0rnp1 > 0.2 
AND CCrnp0rnp1 < 0.2;


SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CCrnp0, CCocc0, 
CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE CCocc0 > CCrnp0 
AND (LLGocc1 - LLGrnp1) > 90 
AND CCrnp0rnp1 < 0.2
;




ATTACH "..\2CompPlainMR_svn6966.db" AS tomerge;

CREATE TABLE TwoComponentMROCCTbl AS SELECT 
m.TargetPDBid AS TargetPDBid, 
m.Fullmodel AS Fullmodel, 
m.PartialModel AS Partialmodel, 
t.RefinedModel AS Refinedmodel, 
f.CC1 AS CC2nd_MR_1stback, -- CC of 2nd component during RNP
m.CCmap1 AS CC2nd_MROCC_1stback, -- CC of 2nd component during RNP (from OCC run)
t.CCmap0 AS CC1st_template_alone,  -- CC of template of 1st component alone
f.CC1 AS CC_Rnp1, -- CC of 2nd component from MR solution with 1st in the background
f.CC0comb AS CC1st_templ_2ndback, -- CC of template of 1st component with 2nd in the background
f.CC1comb AS CC2nd_templ_1stback,-- CC of template of 2nd component with 1st in the background
f.LLG1 AS LLGrnp1,
t.LLGvrms AS LLGrnp0, 
m.MLAD0 AS MLADocc0, 
m.MLAD1 AS MLADocc1, 
m.PAK0 AS PAKocc1,
f.PAK0 AS PAKRnp1,
m.TFZ0 AS TFZ1occ, 
f.TFZ1 AS TFZ1rnp, 
m.LLGvrms AS LLGglobal_rnp,
m.CCglobal AS CCglobal_rnp, -- only ~CCoccglobal if OCC was necessary for solving
m.CCoccglobal AS CCoccglobal,
m.LLGocc AS LLGoccglobal,
f.LLGtotal AS LLGrnp01, 
f.CCglobal AS CCrnp01,
f.SolutionExists AS SolutionExists,
m.SolutionExists AS OccSolutionExists,
m.TriedOCCrefining AS TriedOCCrefining,
m.CPUtime AS CPUtime,
m.NumberofResiduesinTarget AS NumberofResiduesinTarget,
m.NumberofResiduesinModel AS NumberofResiduesinModel, 
m.UsedReflections AS UsedReflections, 
m.Allreflections AS Allreflections, 
m.Resolution AS Resolution,
--(m.LLGocc - m.LLGensmbl0) AS LLGocc1, 
(m.LLGvrms - m.LLGensmbl0) AS LLGocc1, 
t.LLGocc AS LLGocc0, 
t.CCoccglobal AS CC_Occ0
FROM AllProteins m, TemplateRMSTbl t, tomerge.TwoComponentRealMR f  
WHERE m.PartialModel != t.RefinedModel 
AND m.FullModel != t.RefinedModel 
AND instr(m.Fullmodel, t.RefinedModel) > 0 -- conditions for RNP of template for 1st component alone
AND t.MRTarget_id = m.MRTarget_id 
AND m.TargetPDBid = f.TargetPDBid 
AND m.Fullmodel = f.Fullmodel
AND m.modelorder = 2 
AND m.CCmap1 IS NOT NULL 
AND t.CCmap0 IS NOT NULL 
AND m.LLGvrms IS NOT NULL
AND t.LLGvrms IS NOT NULL
--AND t.LLGocc > t.LLGvrms
;

detach database toMerge;


SELECT TargetPDBid, Fullmodel, Refinedmodel, LLG0, CCrnp0, LLGboth, CCboth, LLGrnp1, CCrnp0rnp1, CCocc0rnp1, LLGocc0, CCocc0, LLGocc0rnp1, CCglobal, LLGoccboth, CCoccBoth 
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLG0
--AND LLG0 > 20

AND LLGoccboth >= (LLGocc0rnp1 + 2)
--AND LLGoccboth > 80
;


SELECT TargetPDBid, Fullmodel, Refinedmodel, LLGocc0, LLG0, CCocc0rnp1, CCrnp0rnp1 
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > (LLG0 + 1) 
AND CCocc0rnp1 > (CCrnp0rnp1+ 0.005) 
AND CCocc0rnp1 > 0.2;



SELECT TargetPDBid, Fullmodel, Refinedmodel, LLGrnp0, LLGocc0, LLGrnp1, LLGocc1, CC1st_template_alone, CC_Occ0, CC2nd_MROCC_1stback, CC2nd_MR_1stback 
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
AND CCocc0rnp1 > 0.2 
AND CCrnp0rnp1 < 0.2;





SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
;

SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
AND CC2nd_MROCC_1stback > 0.2 
;

SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 <= LLGrnp0 
AND CC2nd_MR_1stback > 0.2 
AND CC2nd_MROCC_1stback > 0.2 
;


SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
AND CC2nd_MR_1stback < 0.2 
AND CC2nd_MROCC_1stback > 0.2 
;


SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
AND CC2nd_MR_1stback > 0.2 
AND CC2nd_MROCC_1stback > 0.2 
;


SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE LLGocc0 > LLGrnp0 
AND CC2nd_MR_1stback > 0.2 
AND CC2nd_MROCC_1stback < 0.2 
;


SELECT COUNT(*), TargetPDBid, Fullmodel, Refinedmodel, TFZ1rnp, TFZ1occ
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback < 0.2 
AND CC2nd_MR_1stback > 0.2 
;


-- Total number of 2nd component MR cases with and without OCC
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl;


-- Number of solved 2nd component with plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MR_1stback > 0.2 
;

-- Number of solved 2nd component with MR OCC?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND OccSolutionExists
;

-- Number of solved 2nd component with MR OCC and with plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND CC2nd_MR_1stback > 0.2 
AND OccSolutionExists
;

-- Number of solved 2nd component with MR OCC but not with plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ), PAKRnp1, PAKocc1
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND CC2nd_MR_1stback < 0.2 
--AND CC1st_templ_2ndback > 0.2
AND CC1st_template_alone > 0.2
AND OccSolutionExists
;

-- Number of unsolved 2nd component with MR OCC and plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback < 0.2 
AND CC2nd_MR_1stback < 0.2 
;

-- Number of solved 2nd component with plain MR but not with MR OCC?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback <= 0.2
AND CC2nd_MR_1stback > 0.2 
--AND CC1st_templ_2ndback > 0.2
AND CC1st_template_alone > 0.2
-- AND OccSolutionExists
;



-- Total number of 2nd component MR cases with and without OCC
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE TriedOCCrefining=1
;

SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MR_1stback > 0.2 
AND TriedOCCrefining=1
;

-- Number of solved 2nd component with MR OCC?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND OccSolutionExists
AND TriedOCCrefining=1
;

-- Number of solved 2nd component with MR OCC and with plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND CC2nd_MR_1stback > 0.2 
AND OccSolutionExists
AND TriedOCCrefining=1
;

-- Number of solved 2nd component with MR OCC but not with plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ), PAKRnp1, PAKocc1
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback > 0.2 
AND CC2nd_MR_1stback < 0.2 
--AND CC1st_templ_2ndback > 0.2
AND CC1st_template_alone > 0.2
AND OccSolutionExists
AND TriedOCCrefining=1
;

-- Number of unsolved 2nd component with MR OCC and plain MR?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback < 0.2 
AND CC2nd_MR_1stback < 0.2 
AND TriedOCCrefining=1
;

-- Number of solved 2nd component with plain MR but not with MR OCC?
SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl 
WHERE CC2nd_MROCC_1stback <= 0.2
AND CC2nd_MR_1stback > 0.2 
--AND CC1st_templ_2ndback > 0.2
AND CC1st_template_alone > 0.2
AND TriedOCCrefining=1
-- AND OccSolutionExists
;

SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ)
FROM TwoComponentMROCCTbl
WHERE CC2nd_MROCC_1stback <= 0.2
AND CC2nd_MR_1stback > 0.2
AND TriedOCCrefining=1
-- AND OccSolutionExists
;

SELECT COUNT(*), AVG(TFZ1rnp), AVG(TFZ1occ), PAKRnp1, PAKocc1
FROM TwoComponentMROCCTbl
WHERE CC2nd_MROCC_1stback > 0.2
AND CC2nd_MR_1stback < 0.2
AND OccSolutionExists
AND TriedOCCrefining=1
;








SELECT TargetPDBid, Fullmodel, Partialmodel,  TFZ1rnp, TFZ1occ, CC1st_template_alone, CC1st_templ_2ndback, CC2nd_templ_1stback, CC2nd_MR_1stback, CC2nd_MROCC_1stback, PAKRnp1, PAKocc1, CPUtime
FROM TwoComponentMROCCTbl
WHERE CC2nd_MROCC_1stback < 0.2
AND CC2nd_MR_1stback > 0.2
AND CC1st_templ_2ndback > 0.2
AND CC1st_template_alone > 0.2
AND OccSolutionExists
;


SELECT TargetPDBid, NumberofResiduesinTarget, Fullmodel, NumberofResiduesinmodel, Partialmodel, LLGrnp0,  TFZ1rnp, TFZ1occ, PAKRnp1, PAKocc1, CPUtime FROM TwoComponentMROCCTbl WHERE CC2nd_MROCC_1stback > 0.2 AND CC2nd_MR_1stback < 0.2 AND OccSolutionExists AND CPUtime < 1000 AND NumberofResiduesinmodel > NumberofResiduesinTarget*0.3 AND NumberofResiduesinmodel < NumberofResiduesinTarget*0.7 AND LLGrnp0 > 50;

-- CCglobal CCocc scatterplot
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCglobal, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE LLGocc > LLGvrms
AND CCglobal >= 0.2
AND SolutionExists
AND Modelorder=2
;

-- CCglobal CCocc scatterplot, high res
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCglobal, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE LLGocc > LLGvrms
AND CCglobal >= 0.2
AND resolution < 1.5
AND SolutionExists
AND Modelorder=2
;

-- CCglobal CCocc scatterplot, medium res
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCglobal, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE LLGocc > LLGvrms
AND CCglobal >= 0.2
AND resolution >= 1.5
AND resolution < 2.5
AND SolutionExists
AND Modelorder=2
;

-- CCglobal CCocc scatterplot, low res
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCglobal, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE LLGocc > LLGvrms
AND CCglobal >= 0.2
AND resolution >= 2.5
AND SolutionExists
AND Modelorder=2
;


-- Delta(CCocc-CC) histogram
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE (m.CCoccglobal- m.CCglobal) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobal- m.CCglobal) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobal != ""
AND CCglobal >= 0.2
AND LLGocc > LLGvrms
AND SolutionExists
AND Modelorder=2
GROUP BY dCCiter2.i;

-- 2comp_svn6966_DeltaCCocc_CCmap_eLLGReswindow.txt
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCoccglobal, CCglobal, LLGocc, LLG0, dLLG, ELLGoccwnd, dCC 
FROM ELlgOccTbl 
WHERE CCglobal > 0.2;

-- DeltaCCocc_ELLGwnd0.4_0.5
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCglobal <= 0.5
AND CCglobal > 0.4
GROUP BY LLGiter6.i;

--DeltaCCocc_ELLGwnd0.2_0.3
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCglobal <= 0.3
AND CCglobal > 0.2
GROUP BY LLGiter6.i;

--DeltaCCocc_ELLGwnd0.5_0.6
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCglobal <= 0.6
AND CCglobal > 0.5
GROUP BY LLGiter6.i;


--DeltaCCocc_DeltaLLG30_50
SELECT LLGiter2.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter2 
WHERE 
m.dLLG <= (LLGiter2.i + 10) 
AND m.dLLG > (LLGiter2.i - 10) 
AND LLG0 <= 50
AND LLG0 > 30
GROUP BY LLGiter2.i;

--DeltaCCocc_DeltaLLG50_70
SELECT LLGiter2.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter2 
WHERE 
m.dLLG <= (LLGiter2.i + 10) 
AND m.dLLG > (LLGiter2.i - 10) 
AND LLG0 <= 70
AND LLG0 > 50
GROUP BY LLGiter2.i;

--DeltaCCocc_DeltaLLG70_90
SELECT LLGiter2.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter2 
WHERE 
m.dLLG <= (LLGiter2.i + 10) 
AND m.dLLG > (LLGiter2.i - 10) 
AND LLG0 <= 90
AND LLG0 > 70
GROUP BY LLGiter2.i;

--DeltaCCocc_DeltaLLG90_110
SELECT LLGiter2.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter2 
WHERE 
m.dLLG <= (LLGiter2.i + 10) 
AND m.dLLG > (LLGiter2.i - 10) 
AND LLG0 <= 110
AND LLG0 > 90
GROUP BY LLGiter2.i;

--DeltaCCocc_DeltaLLG150_200
SELECT LLGiter2.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter2 
WHERE 
m.dLLG <= (LLGiter2.i + 10) 
AND m.dLLG > (LLGiter2.i - 10) 
AND LLG0 <= 200
AND LLG0 > 150
GROUP BY LLGiter2.i;



