SELECT q.MRTarget_id, q.MRModel_id, q.shell, q.SigmaAshell, LOG(q.SigmaAshell), 
(LOG(q.SigmaAshell)-0.5*LOG(s.Modelcompleteness))*(q.avgresshell*q.avgresshell)/(3.14159*3.14159*3.14159*s.Vrms0*s.Vrms0) AS dfg, 
q.CCshell, q.avgresshell, 1.0/(q.avgresshell*q.avgresshell) AS invresshell2, s.VRMS0, 
s.FracvarVrms0, s.AvgSigmaaVrms0, s.Fracvarhilores0, s.AvgSigmaahilores0, s.CCfcglobal, (s.IsigIgrt3res+1.0) AS IsigI3res,
t.CCgoodSigmaA
FROM CCSigmaAtbl q, AllProteins s, TemplateRMSTbl t
WHERE q.MRTarget_id=s.MRTarget_id
AND q.MRModel_id=s.p_id
ANd t.MRTarget_id=s.MRTarget_id
AND t.MRModel_id=s.p_id
AND q.SigmaAshell <=1
AND s.CCglobal > 0.2
GROUP BY q.MRModel_id
--AND q.Loresshell < 5
--AND IsigI3res < q.avgresshell
--AND invresshell2 > 0.01
--AND invresshell2 < 0.08
--AND s.CCglobal > 0.3
--AND s.CCglobal < 0.308
--AND s.FracvarVrms0 > 0.25
--AND s.FracvarVrms0 < 0.252
--AND s.Vrms0 > 0.7
--AND s.Vrms0 < 0.72
--AND s.AvgSigmaaVrms0 > 0.5
--AND s.AvgSigmaaVrms0 < 0.505
--AND q.MRTarget_id=47
--AND q.MRModel_id=339