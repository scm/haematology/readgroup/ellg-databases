
{

"ActualELLGxtalratioHisto": ["""
CREATE TEMP VIEW ELLGxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS ActualLLGResratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter4 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGachievedres),0) AS pcount 
		FROM ELLGtbl restbl, LLGiter4 allbins
		WHERE restbl.LLGachievedres <= (allbins.i + 5) 
			AND restbl.LLGachievedres > (allbins.i-5) 
			AND restbl.foundit2 < 1
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter4 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGachievedres),0) AS pcount 
		FROM ELLGtbl restbl, LLGiter4 allbins
		WHERE restbl.LLGachievedres <= (allbins.i + 5) 
			AND restbl.LLGachievedres > (allbins.i-5) 
			AND restbl.foundit2 > 0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.ActualLLGResratio, m1.totalcount FROM ELLGxtalratio m1;
""",
"""
DROP VIEW ELLGxtalratio;
"""]
,

"ELLGxtalratioHisto": ["""
CREATE TEMP VIEW ELLGxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS AchievedELLGratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter4 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.achievedELLG),0) AS pcount 
		FROM ELLGtbl restbl, LLGiter4 allbins
		WHERE restbl.achievedELLG <= (allbins.i + 5) 
			AND restbl.achievedELLG > (allbins.i-5) 
			AND restbl.foundit2 < 1
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter4 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.achievedELLG),0) AS pcount 
		FROM ELLGtbl restbl, LLGiter4 allbins
		WHERE restbl.achievedELLG <= (allbins.i + 5) 
			AND restbl.achievedELLG > (allbins.i-5) 
			AND restbl.foundit2 > 0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.AchievedELLGratio, m1.totalcount FROM ELLGxtalratio m1;
""",
"""
DROP VIEW ELLGxtalratio;
"""]
,

"LLGSolvedRatioHisto": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 < 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 >= 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,

"LLG0SolvedRatioHisto": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLG0xtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLG0ratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG0),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLG0 <= (allbins.i + 2.5) 
			AND restbl.LLG0 > (allbins.i-2.5) 
			AND restbl.CCmap0 < 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG0),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLG0 <= (allbins.i + 2.5) 
			AND restbl.LLG0 > (allbins.i-2.5) 
			AND restbl.CCmap0 >= 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLG0ratio, m1.totalcount FROM LLG0xtalratio m1;
""",
"""
DROP VIEW LLG0xtalratio;
"""]
,

"LLGSolvedRatioHistoNonPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, XtalSysTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND IsPolar = 0  
			AND restbl.CCmap0 < 0.2
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, XtalSysTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND IsPolar = 0  
			AND restbl.CCmap0 >= 0.2
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.LLGVRMSratio AS HexagonalSpg, m2.totalcount AS Hexagonalcount, m3.LLGVRMSratio AS OrthorhombicSpg, m3.totalcount AS orthorhombiccount, m6.LLGVRMSratio AS CubicSpg, m6.totalcount AS cubiccount, m7.LLGVRMSratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2, LLGVRMSxtalratio m3, LLGVRMSxtalratio m6, LLGVRMSxtalratio m7 
WHERE m1.xtalsys ="Trigonal"
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Orthorhombic" 
	AND m6.xtalsys = "Cubic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,


"LLGSolvedRatioHistoPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, XtalSysTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 < 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, XtalSysTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 >= 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
--make combined ratiohistogram omitting cubic and orthorhombic AS these are always non polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.LLGVRMSratio AS HexagonalSpg, m2.totalcount AS hexagonalcount, m3.LLGVRMSratio AS MonoclinicSpg, m3.totalcount AS monocliniccount, m6.LLGVRMSratio AS TriclinicSpg, m6.totalcount AS tricliniccount, m7.LLGVRMSratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2, LLGVRMSxtalratio m3, LLGVRMSxtalratio m6, LLGVRMSxtalratio m7 
WHERE m1.xtalsys ="Trigonal" 
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Monoclinic" 
	AND m6.xtalsys = "Triclinic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,

"LLGSolvedRatioHistoPolar_NonPolar": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, columned by IsPolarSpacegroup
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.isfloating, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, IsPolarTbl 
		WHERE restbl.LLGvrms <= allbins.deltaup 
			AND restbl.LLGvrms > allbins.deltalow 
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
			AND restbl.SpaceGroup != "P 1"
			AND restbl.CCglobal != ""
			AND restbl.CCglobal < 0.2
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) u, 

(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, IsPolarTbl 
		WHERE restbl.LLGvrms <= allbins.deltaup 
			AND restbl.LLGvrms > allbins.deltalow 
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
			AND restbl.SpaceGroup != "P 1"
			AND restbl.CCglobal != ""
			AND restbl.CCglobal >= 0.2
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) s

WHERE u.bin = s.bin AND u.isfloating = s.isfloating;
""",
"""
--make combined ratiohistogram for polar and non polar crystals
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS PolarSpg, m1.totalcount AS polarcount, m2.LLGVRMSratio AS NonPolarSpg, m2.totalcount AS nonpolarcount 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2 
WHERE m1.isfloating =1 AND m2.isfloating = 0 AND m1.bin = m2.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,

"LLGSolvedP1RatioHisto": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, P1 crystals
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrms <= allbins.deltaup 
			AND restbl.LLGvrms > allbins.deltalow 
			AND restbl.CCglobal != ""
			AND restbl.CCglobal < 0.2
			AND restbl.SpaceGroup = "P 1"
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrms <= allbins.deltaup 
			AND restbl.LLGvrms > allbins.deltalow 
			AND restbl.CCglobal != ""
			AND restbl.CCglobal >= 0.2
			AND restbl.SpaceGroup = "P 1"
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS P1, m1.totalcount AS P1count FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,

"LLGSolvedRatioHistoResidueNumbers": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, columned by residuenumbers
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.residues, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(cbin.residbin,Residuesiter1.i) AS residues, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, Residuesiter1 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter1.i AS residbin, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, Residuesiter1 
		WHERE restbl.NumberofResiduesinTarget <= (Residuesiter1.i + 100) 
			AND restbl.NumberofResiduesinTarget > (Residuesiter1.i - 100) 
			AND restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.CCmap0 < 0.2
			AND restbl.LLGvrms > (allbins.i-2.5) 
		GROUP BY Residuesiter1.i, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.residbin = Residuesiter1.i
) u, 

(
	SELECT IFNULL(cbin.residbin,Residuesiter1.i) AS residues, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, Residuesiter1 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter1.i AS residbin, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, Residuesiter1 
		WHERE restbl.NumberofResiduesinTarget <= (Residuesiter1.i + 100) 
			AND restbl.NumberofResiduesinTarget > (Residuesiter1.i - 100) 
			AND restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 >= 0.2
		GROUP BY Residuesiter1.i, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.residbin = Residuesiter1.i
) s

WHERE u.bin = s.bin AND u.residues = s.residues;
""",
"""
--make combined ratiohistogram columned by residue size in steps of 200
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS residues00, m1.totalcount AS residues00count, m2.LLGVRMSratio AS residues200, m2.totalcount AS residues200count, m3.LLGVRMSratio AS residues400, m3.totalcount AS residues400count, m6.LLGVRMSratio AS residues600, m6.totalcount AS residues600count, m7.LLGVRMSratio AS residues800, m7.totalcount AS residues800count, m8.LLGVRMSratio AS residues1000, m7.totalcount AS residues1000count 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2, LLGVRMSxtalratio m3, LLGVRMSxtalratio m6, LLGVRMSxtalratio m7, LLGVRMSxtalratio m8 
WHERE m1.residues = 0 
	AND m2.residues = 200 
	AND m3.residues = 400 
	AND m6.residues = 600 
	AND m7.residues = 800 
	AND m8.residues = 1000 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin 
	AND m1.bin = m8.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,

"LLGSolvedRatioHistoBelow200Above200Residues": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, columned for proteins below and above 200 residues
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.residues, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(cbin.residbin,Residuesiter3.i) AS residues, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, Residuesiter3 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT r.i AS residbin, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, Residuesiter3 r
		WHERE restbl.NumberofResiduesinTarget <= (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = (r.ROWID+1)) 
			AND restbl.NumberofResiduesinTarget > (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = r.ROWID)
			AND restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 < 0.2
		GROUP BY r.i, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.residbin = Residuesiter3.i
) u, 

(
	SELECT IFNULL(cbin.residbin,Residuesiter3.i) AS residues, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, Residuesiter3 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT r.i AS residbin, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, Residuesiter3 r
		WHERE restbl.NumberofResiduesinTarget <= (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = (r.ROWID+1)) 
			AND restbl.NumberofResiduesinTarget > (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = r.ROWID)
			AND restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.CCmap0 >= 0.2
		GROUP BY r.i, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.residbin = Residuesiter3.i
) s

WHERE u.bin = s.bin AND u.residues = s.residues;
""",
"""
--make combined ratiohistogram for proteins below and above 200 residues, i.e. where Residuesiter3.ROWID = 2
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS residues00, m1.totalcount AS residues00count, m2.LLGVRMSratio AS residues200, m2.totalcount AS residues200count 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2
WHERE m1.residues = (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = 1) 
	AND m2.residues = (SELECT m.i FROM Residuesiter3 m WHERE m.ROWID = 2) 
	AND m1.bin = m2.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,


"LLGSolvedRatioHistoSCOPclass": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.scop, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(TargetClass,SCOPClasses.name) AS scop, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, SCOPClasses 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.TargetClass, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, SCOPClasses 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.TargetClass = SCOPClasses.name 
			AND restbl.CCmap0 < 0.2
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.TargetClass = SCOPClasses.name
) u, 

(
	SELECT IFNULL(TargetClass,SCOPClasses.name) AS scop, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, SCOPClasses 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.TargetClass, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, SCOPClasses 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.TargetClass = SCOPClasses.name 
			AND restbl.CCmap0 >= 0.2
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.TargetClass = SCOPClasses.name
) s

WHERE u.bin = s.bin AND u.scop = s.scop;
""",
"""
--make combined ratiohistogram omitting cubic and orthorhombic AS these are always non polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio AS Alpha, m1.totalcount AS alphacount, m2.LLGVRMSratio AS Beta, m2.totalcount AS betacount, m3.LLGVRMSratio AS Alpha_p_Beta, m3.totalcount AS a_p_bcount, m4.LLGVRMSratio AS Alpha_s_Beta, m4.totalcount AS a_s_bccount 
FROM LLGVRMSxtalratio m1, LLGVRMSxtalratio m2, LLGVRMSxtalratio m3, LLGVRMSxtalratio m4
WHERE m1.scop ="All alpha proteins" 
	AND m2.scop = "All beta proteins" 
	AND m3.scop ="Alpha and beta proteins (a+b)" 
	AND m4.scop = "Alpha and beta proteins (a/b)"
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m4.bin;
""",
"""
DROP VIEW LLGVRMSxtalratio;
---------------------------------------------------------------------------------------------------------
"""]
,

"TFZSolvedRatioHisto": ["""
-- create ratio histogram for the TFZ with Bin size 0.5
CREATE TEMP VIEW RatioHistogram AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZsolvedratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25)
			AND restbl.CCmap0 < 0.2
			AND restbl.Spacegroup != "P 1" 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.CCmap0 >= 0.2
			AND restbl.Spacegroup != "P 1" 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make ratio histogram for TFZ correct solutions
SELECT m1.bin AS TFZbin, m1.unsolved, m1.solved, m1.totalcount, m1.TFZsolvedratio
FROM RatioHistogram m1 
WHERE m1.bin <= 20;
""",
"""
DROP VIEW RatioHistogram;
"""]
,

"TFZSolvedRatioHistoPolar_NonPolar": ["""
-- create ratio histogram for the TFZ with Bin size 0.5, columned by IsPolarSpacegroup
CREATE TEMP VIEW RatioHistogram AS
SELECT u.isfloating, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZsolvedratio 
FROM
(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins, IsPolarTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 < 0.2
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) u, 

(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins, IsPolarTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 >= 0.2
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) s

WHERE u.bin = s.bin AND u.isfloating = s.isfloating;
""",
"""
--make combined ratiohistogram for polar and non polar crystals
SELECT m1.bin AS TFZbin, m1.TFZsolvedratio AS PolarSpg, m1.totalcount AS polarcount, m2.TFZsolvedratio AS NonPolarSpg, m2.totalcount AS nonpolarcount 
FROM RatioHistogram m1, RatioHistogram m2 
WHERE m1.isfloating =1 AND m2.isfloating = 0 AND m1.bin = m2.bin
AND m1.bin <= 20;
""",
"""
DROP VIEW RatioHistogram;
"""]
,


"TFZSolvedRatioHistoNonPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZ0) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND IsPolar = 0  
			AND restbl.CCmap0 < 0.2
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZ0) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND IsPolar = 0  
			AND restbl.CCmap0 >= 0.2
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS TFZbin, m1.TFZratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.TFZratio AS HexagonalSpg, m2.totalcount AS Hexagonalcount, m3.TFZratio AS OrthorhombicSpg, m3.totalcount AS orthorhombiccount, m6.TFZratio AS CubicSpg, m6.totalcount AS cubiccount, m7.TFZratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM TFZxtalratio m1, TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m6, TFZxtalratio m7 
WHERE m1.xtalsys ="Trigonal"
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Orthorhombic" 
	AND m6.xtalsys = "Cubic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin
	AND m1.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,



"TFZSolvedRatioHistoPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZ0) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 < 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZ0) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 >= 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
-- make combined ratiohistogram omitting cubic and orthorhombic AS these are always non polar spg
-- as well as triclinic as that is always P1 which hasn't got a TFZ value
SELECT m1.bin AS TFZbin, m1.TFZratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.TFZratio AS HexagonalSpg, m2.totalcount AS Hexagonalcount, m3.TFZratio AS MonoclinicSpg, m3.totalcount AS Monocliniccount, m7.TFZratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM TFZxtalratio m1, TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m7 
WHERE m1.xtalsys ="Trigonal" 
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Monoclinic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m7.bin
	AND m1.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
---------------------------------------------------------------------------------------------------------
"""]
,


"TFZequivSolvedRatioHisto": ["""
-- create ratio histogram for the TFZ with Bin size 0.5
CREATE TEMP VIEW RatioHistogram AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZsolvedratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZequiv),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25)
			AND restbl.CCmap0 < 0.2
			AND restbl.Spacegroup != "P 1" 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZequiv),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.CCmap0 >= 0.2
			AND restbl.Spacegroup != "P 1" 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make ratio histogram for TFZ correct solutions
SELECT m1.bin AS TFZbin, m1.unsolved, m1.solved, m1.totalcount, m1.TFZsolvedratio
FROM RatioHistogram m1
WHERE m1.bin <= 20;
""",
"""
DROP VIEW RatioHistogram;
"""]
,


"TFZequivSolvedRatioHistoPolar_NonPolar": ["""
-- create ratio histogram for the TFZ with Bin size 0.5, columned by IsPolarSpacegroup
CREATE TEMP VIEW RatioHistogram AS
SELECT u.isfloating, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZsolvedratio 
FROM
(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.TFZequiv),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins, IsPolarTbl 
		WHERE restbl.TFZequiv <= allbins.deltaup 
			AND restbl.TFZequiv > allbins.deltalow 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) u, 

(
	SELECT IFNULL(cbin.IsPolarSpacegroup, IsPolarTbl.ispolar) AS isfloating, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins, IsPolarTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup, allbins.i AS bin, IFNULL(COUNT(restbl.TFZequiv),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins, IsPolarTbl 
		WHERE restbl.TFZequiv <= allbins.deltaup 
			AND restbl.TFZequiv > allbins.deltalow 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.IsPolarSpacegroup = IsPolarTbl.ispolar 
		GROUP BY IsPolarSpacegroup, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.IsPolarSpacegroup = IsPolarTbl.ispolar
) s

WHERE u.bin = s.bin AND u.isfloating = s.isfloating;
""",
"""
--make combined ratiohistogram for polar and non polar crystals
SELECT m1.bin AS TFZbin, m1.TFZsolvedratio AS PolarSpg, m1.totalcount AS polarcount, m2.TFZsolvedratio AS NonPolarSpg, m2.totalcount AS nonpolarcount 
FROM RatioHistogram m1, RatioHistogram m2 
WHERE m1.isfloating =1 AND m2.isfloating = 0 AND m1.bin = m2.bin
AND m1.bin <= 20;
""",
"""
DROP VIEW RatioHistogram;
"""]
,


"TFZequivSolvedRatioHistoNonPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 < 0.2
			AND IsPolar = 0  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 >= 0.2
			AND IsPolar = 0  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS TFZbin, m1.TFZratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.TFZratio AS HexagonalSpg, m2.totalcount AS Hexagonalcount, m3.TFZratio AS OrthorhombicSpg, m3.totalcount AS orthorhombiccount, m6.TFZratio AS CubicSpg, m6.totalcount AS cubiccount, m7.TFZratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM TFZxtalratio m1, TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m6, TFZxtalratio m7 
WHERE m1.xtalsys ="Trigonal"
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Orthorhombic" 
	AND m6.xtalsys = "Cubic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin
	AND m1.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,



"TFZequivSolvedRatioHistoPolarXtalsys": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.xtalsys, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 < 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) u, 

(
	SELECT IFNULL(Crystalsystem,XtalSysTbl.name) AS xtalsys, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, XtalSysTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.IsPolarSpacegroup AS IsPolar, restbl.Crystalsystem, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, XtalSysTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCmap0 >= 0.2
			AND IsPolar = 1  
			AND restbl.Crystalsystem = XtalSysTbl.name 
		GROUP BY restbl.Crystalsystem, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Crystalsystem = XtalSysTbl.name
) s

WHERE u.bin = s.bin AND u.xtalsys = s.xtalsys;
""",
"""
-- make combined ratiohistogram omitting cubic and orthorhombic AS these are always non polar spg
-- as well as triclinic as that is always P1 which hasn't got a TFZ value
SELECT m1.bin AS TFZbin, m1.TFZratio AS TrigonalSpg, m1.totalcount AS trigonalcount, m2.TFZratio AS HexagonalSpg, m2.totalcount AS Hexagonalcount, m3.TFZratio AS MonoclinicSpg, m3.totalcount AS Monocliniccount, m7.TFZratio AS TetragonalSpg, m7.totalcount AS tetragonalcount 
FROM TFZxtalratio m1, TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m7 
WHERE m1.xtalsys ="Trigonal" 
	AND m2.xtalsys = "Hexagonal" 
	AND m3.xtalsys ="Monoclinic" 
	AND m7.xtalsys = "Tetragonal" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m7.bin
	AND m1.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
---------------------------------------------------------------------------------------------------------
"""]
,






"AlphaBeta-Hydrolases_LLG_Fracvarvrms": ["""
SELECT FracvarVrms, LLGrefl_vrms FROM AllProteins 
WHERE TargetFold = "alpha/beta-Hydrolases" 
AND LLGrefl_vrms != "";
"""]
,

"Concanavalin_LLG_Fracvarvrms": ["""
SELECT FracvarVrms, LLGrefl_vrms FROM AllProteins 
WHERE TargetFold = "Concanavalin A-like lectins/glucanases"
AND  LLGrefl_vrms != "";
"""]
,

"LLG_Fracvarvrms": ["""
-- scatter data for K, LLGvrms/refl, correlation coefficient
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLG_FracvarVrmsList.txt, outputfile =SMoothLLG_FracvarVrmsList.txt,
-- sigx= 0.002, sigy= 0.001, step= 200, xmin= 0.0, xmax= 0.85, ymin= 0.0 ymax= 0.45, xhisto= x 
SELECT FracvarVrms, LLGrefl_vrms, CCglobal 
FROM AllProteins 
WHERE LLGrefl_vrms != "" 
AND CCglobal IS NOT "";
"""]
,

"LLG_Fracvar": ["""
SELECT Fracvar, LLGreflection0, CCglobal 
FROM AllProteins 
WHERE LLGreflection0 != ""
AND CCglobal IS NOT "";
"""]
,

"LLG_FracvarVrmsAllSolutions": ["""
-- scatter data for K, LLGvrms/refl, correlation coefficient
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLG_FracvarVrmsAllSolutionsList.txt, outputfile =SmoothLLG_FracvarVrmsAllSolutionsList.txt,
-- sigx= 0.004, sigy= 0.002, step= 200, xmin= 0.0, xmax= 0.85, ymin= -0.0 ymax= 0.45, xhisto= x 
SELECT FracvarVrms, LLGrefl_vrms, CCglobal 
FROM AnnotationTbl 
WHERE FracvarVRMS IS NOT "" 
AND CCglobal IS NOT "";
"""]
,

"ResidueSizeHistogram": ["""
-- make histogram of residue numbers
SELECT Residuesiter.i as Residues, COUNT(m.NumberofResiduesinTarget) AS count 
FROM AllProteins m, Residuesiter 
WHERE m.NumberofResiduesinTarget <= (Residuesiter.i + 50) 
	AND m.NumberofResiduesinTarget > (Residuesiter.i-50) 
	GROUP BY Residuesiter.i;
"""]
,

"ResidueSizeHistogram50": ["""
-- make histogram of residue numbers
SELECT Residuesiter4.i as Residues, COUNT(m.NumberofResiduesinTarget) AS count 
FROM AllProteins m, Residuesiter4 
WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
	AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25) 
	GROUP BY Residuesiter4.i;
"""]
,

"SolvedResidueSizeHistogram50": ["""
-- make histogram of residue numbers
SELECT u.Residues AS Residues, u.count AS solved, v.count AS count, 
	(CAST(u.count AS REAL)/IFNULL(v.count, MAX(v.count, 1))) AS solvedratio, 
	v.avgfracvar as avgfracvar, v.avgfracvarvrms as avgfracvarvrms, 
	v.avgC_L_rms, v.avgResolution, v.avgModelCompleteness
FROM
(
	SELECT IFNULL(resbin.Residues, allbins.i) as Residues, IFNULL(resbin.count, 0) AS count
	FROM Residuesiter4 allbins
	LEFT OUTER JOIN
	(
		SELECT Residuesiter4.i as Residues, COUNT(m.NumberofResiduesinTarget) AS count
		FROM AllProteins m, Residuesiter4 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
			AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25) 
			AND m.CorrectSolution > 0
		GROUP BY Residuesiter4.i
	)
	resbin ON resbin.Residues = allbins.i
) u,
(
	SELECT IFNULL(resbin.Residues, allbins.i) as Residues, IFNULL(resbin.count, 0) AS count, 
		IFNULL(resbin.avgfracvar, 0) as avgfracvar, IFNULL(resbin.avgfracvarvrms, 0) as avgfracvarvrms, 
		IFNULL(resbin.avgC_L_rms, 0) as avgC_L_rms, IFNULL(resbin.avgResolution, 0) as avgResolution, 
		IFNULL(resbin.avgModelCompleteness, 0) as avgModelCompleteness
	FROM Residuesiter4 allbins
	LEFT OUTER JOIN
	(
		SELECT Residuesiter4.i as Residues, COUNT(m.NumberofResiduesinTarget) AS count, 
			AVG(m.Fracvar) as avgfracvar, AVG(m.FracvarVrms1) as avgfracvarvrms, AVG(C_L_rms) as avgC_L_rms, AVG(Resolution) AS avgResolution, 
			AVG(ModelCompleteness) AS avgModelCompleteness
		FROM AllProteins m, Residuesiter4 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
			AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25)
		GROUP BY Residuesiter4.i
	)
	resbin ON resbin.Residues = allbins.i
) v
WHERE u.Residues = v.Residues;
"""]
,

"SCOPClassResidueSizeHistogram": ["""
-- make ratio histograms for residue size for the first 4 SCOP classes and the others
CREATE TEMP VIEW SCOPClassResiduesizeHisto AS

SELECT v.bin AS bin, u.scopclass AS tclass, (CAST(u.Residues AS REAL)/IFNULL(v.Residues, MAX(u.Residues,1))) AS ratio 
FROM
(
-- the histogram for all proteins sorted by SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues, IFNULL(cbin.tclass, sclass.name) AS scopclass
	FROM Residuesiter allbins, SCOPClasses sclass  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter.i as bin, m.Targetclass AS tclass, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter.i + 50) 
			AND m.NumberofResiduesinTarget > (Residuesiter.i-50) 
		GROUP BY m.TargetClass, Residuesiter.i
	) 
	cbin ON cbin.bin = allbins.i AND cbin.tclass = sclass.name
) u,
(
-- the histogram for all proteins regardless of SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues
	FROM Residuesiter allbins  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter.i as bin, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter.i + 50) 
			AND m.NumberofResiduesinTarget > (Residuesiter.i-50) 
		GROUP BY Residuesiter.i
	) 
	cbin ON cbin.bin = allbins.i
) v
WHERE u.bin = v.bin;
""",
"""
SELECT a.bin AS residuebin, a.ratio AS alphas, b.ratio AS betas, apb.ratio AS alpha_p_betas, asb.ratio AS alpha_s_betas, o.ratio AS others,  (1 - (a.ratio + b.ratio + apb.ratio + asb.ratio + o.ratio)) AS unSCOPed
FROM SCOPClassResiduesizeHisto a, SCOPClassResiduesizeHisto b, SCOPClassResiduesizeHisto apb, SCOPClassResiduesizeHisto asb, SCOPClassResiduesizeHisto o
WHERE a.tclass = "All alpha proteins"
	AND b.tclass = "All beta proteins"
	AND apb.tclass = "Alpha and beta proteins (a+b)"
	AND asb.tclass = "Alpha and beta proteins (a/b)"
	AND 
	(
		o.tclass = "Coiled coil proteins" 
		OR o.tclass = "Designed proteins"
		OR o.tclass = "Membrane and cell surface proteins and peptides"
		OR o.tclass = "Multi-domain proteins (alpha and beta)"
		OR o.tclass = "Small proteins"
	)
	AND a.bin = b.bin
	AND a.bin = apb.bin
	AND a.bin = asb.bin
	AND a.bin = o.bin
-- use group to ensure we only sum o.ratio for each residuebin	
GROUP BY residuebin;
""",
"""
DROP VIEW SCOPClassResiduesizeHisto;
"""]
,

"SCOPClassResidueSizeHistogram50": ["""
-- make ratio histograms for residue size for the first 4 SCOP classes and the others
CREATE TEMP VIEW SCOPClassResiduesizeHisto AS

SELECT v.bin AS bin, u.scopclass AS tclass, (CAST(u.Residues AS REAL)/IFNULL(v.Residues, MAX(u.Residues,1))) AS ratio 
FROM
(
-- the histogram for all proteins sorted by SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues, IFNULL(cbin.tclass, sclass.name) AS scopclass
	FROM Residuesiter4 allbins, SCOPClasses sclass  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter4.i as bin, m.Targetclass AS tclass, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter4 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
			AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25) 
			AND m.Modelorder=1
		GROUP BY m.TargetClass, Residuesiter4.i
	) 
	cbin ON cbin.bin = allbins.i AND cbin.tclass = sclass.name
) u,
(
-- the histogram for all proteins regardless of SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues
	FROM Residuesiter4 allbins  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter4.i as bin, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter4 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter4.i + 25) 
			AND m.NumberofResiduesinTarget > (Residuesiter4.i - 25) 
			AND m.Modelorder=1
		GROUP BY Residuesiter4.i
	) 
	cbin ON cbin.bin = allbins.i
) v
WHERE u.bin = v.bin;
""",
"""
SELECT a.bin AS residuebin, a.ratio AS alphas, b.ratio AS betas, apb.ratio AS alpha_p_betas, asb.ratio AS alpha_s_betas, o.ratio AS others,  (1 - (a.ratio + b.ratio + apb.ratio + asb.ratio + o.ratio)) AS unSCOPed
FROM SCOPClassResiduesizeHisto a, SCOPClassResiduesizeHisto b, SCOPClassResiduesizeHisto apb, SCOPClassResiduesizeHisto asb, SCOPClassResiduesizeHisto o
WHERE a.tclass = "All alpha proteins"
	AND b.tclass = "All beta proteins"
	AND apb.tclass = "Alpha and beta proteins (a+b)"
	AND asb.tclass = "Alpha and beta proteins (a/b)"
	AND 
	(
		o.tclass = "Coiled coil proteins" 
		OR o.tclass = "Designed proteins"
		OR o.tclass = "Membrane and cell surface proteins and peptides"
		OR o.tclass = "Multi-domain proteins (alpha and beta)"
		OR o.tclass = "Small proteins"
	)
	AND a.bin = b.bin
	AND a.bin = apb.bin
	AND a.bin = asb.bin
	AND a.bin = o.bin
-- use group to ensure we only sum o.ratio for each residuebin	
GROUP BY residuebin;
""",
"""
DROP VIEW SCOPClassResiduesizeHisto;
"""]
,

"CorrelationCoefficient_FracvarUnsolved": ["""
-- make scatter plot of CC values against vrms values
-- Convert into python table and process using SmoothData.py wiht the arguments below
-- inputfile= CorrelationCoefficient_Fracvar.txt, outputfile =SmoothCorrelationCoefficient_Fracvar.txt,
-- sigx= 0.004, sigy= 0.004, step= 200, xmin= 0.0, xmax= 0.85, ymin= -0.13 ymax= 0.96, xhisto= x 
SELECT FracvarVrms0, AvgSigmaaVrms0, CCglobal FROM AllProteinsUnsolved 
WHERE CCglobal != "" 
AND FracvarVrms0 != "";
"""]
,

"CorrelationCoefficient_FracvarSolved": ["""
--make scatter plot of CC values against vrms values
SELECT FracvarVrms0, AvgSigmaaVrms0, CCglobal FROM AllProteinsSolved 
WHERE CCglobal != "" 
AND FracvarVrms0 != "";
"""]
,

"CorrelationCoefficient_Fracvar": ["""
--make scatter plot of CC values against vrms values
SELECT FracvarVrms0, AvgSigmaaVrms0, CCglobal FROM AllProteins 
WHERE CCglobal != "" 
AND FracvarVrms0 != "";
"""]
,

"CorrelationCoefficient_SigmaA": ["""
--make scatter plot of CC values against vrms values
SELECT FracvarVrms0, AvgSigmaaVrms0, CCfcglobal FROM AllProteins 
WHERE CCfcglobal != "" 
AND FracvarVrms0 != "";
"""]
,

"CC_SigmaAShellsolv": ["""
--make scatter plot of CC values against sigmaA values
SELECT s.SigmaAshell, s.CCshell, s.avgresshell, t.CCglobal FROM CCsigmaAtbl s, AllProteins t
WHERE s.MRTarget_id = t.MRTarget_id
AND s.MRModel_id = t.p_id 
AND t.CCglobal > 0.3
AND t.FracvarVrms0 != ""
-- LIMIT 5000
;
"""]
,

"CC_SigmaAShellsolvHires": ["""
--make scatter plot of CC values against sigmaA values
SELECT q.SigmaAshell, q.CCshell, s.CCglobal FROM CCSigmaAtbl q, AllProteins s
WHERE q.MRTarget_id=s.MRTarget_id
AND q.MRModel_id=s.p_id
AND s.CCglobal > 0.25
AND q.Loresshell < 4
;
"""]
,

"VRMS_CC_SeqId_Residues0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, CCglobal 
FROM AllProteins
WHERE NumberofResiduesinTarget < 200 
AND VRMS1 != "" 
AND CCglobal != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_CC_SeqId_SolvedResidues0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, CCglobal
FROM AllProteinsSolved
WHERE NumberofResiduesinTarget < 200 
AND VRMS1 != "" 
AND CCglobal != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_CC_SeqId_SolvedResidues200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, CCglobal
FROM AllProteinsSolved
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 200
AND VRMS1 != "" 
AND CCglobal != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_CC_SeqId_SolvedResidues400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, CCglobal
FROM AllProteinsSolved
WHERE NumberofResiduesinTarget < 600 
AND NumberofResiduesinTarget >= 400
AND VRMS1 != "" 
AND CCglobal != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_CC_SeqId_SolvedResidues600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, CCglobal 
FROM AllProteinsSolved
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND VRMS1 != "" 
AND CCglobal != ""
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,

"NewRMSD_VRMSratio_SeqId_SolvedHisto": ["""
-- make histograms and cumulative values for NewRMSD/VRMS, total and categorized in seqid bins.
-- cumulative histogram requires calling the same view twice, i.e. scales as N^2.
-- Storing view as a table saves time
CREATE TEMP VIEW histoview AS
SELECT n.f AS bin, COUNT(s.VRMS1) AS countval 
FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
	AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
	AND VRMS1 != ""
GROUP BY bin;
""",
"""
CREATE TABLE histotbl (bin real, countval int);
""",
"""
INSERT INTO histotbl (bin, countval)
SELECT m.bin, m.countval
FROM histoview m;
""",
"""
CREATE TEMP VIEW SSMhistoview AS
SELECT n.f AS bin, COUNT(s.VRMS1) AS countval 
FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
WHERE (CAST(s.VRMS1 AS REAL)/s.NewSSMRMSD) <= (n.f + 0.025)
	AND (CAST(s.VRMS1 AS REAL)/s.NewSSMRMSD) > (n.f - 0.025) 
	AND VRMS1 != ""
GROUP BY bin;
""",
"""
CREATE TABLE SSMhistotbl (bin real, countval int);
""",
"""
INSERT INTO SSMhistotbl (bin, countval)
SELECT m.bin, m.countval
FROM SSMhistoview m;
""",
"""
CREATE TEMP VIEW histoview20 AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND s.SequenceIdentity < 25
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histotbl20 (bin real, countval int);
""",
"""
INSERT INTO histotbl20 (bin, countval)
SELECT m.bin, m.countval
FROM histoview20 m;
""",
"""
CREATE TEMP VIEW histoview30 AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND s.SequenceIdentity > 25
		AND s.SequenceIdentity <= 35
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histotbl30 (bin real, countval int);
""",
"""
INSERT INTO histotbl30 (bin, countval)
SELECT m.bin, m.countval
FROM histoview30 m;
""",
"""
CREATE TEMP VIEW histoview40 AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND s.SequenceIdentity > 35
		AND s.SequenceIdentity <= 45
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histotbl40 (bin real, countval int);
""",
"""
INSERT INTO histotbl40 (bin, countval)
SELECT m.bin, m.countval
FROM histoview40 m;
""",
"""
CREATE TEMP VIEW histoview50 AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND s.SequenceIdentity > 45
		AND s.SequenceIdentity <= 55
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histotbl50 (bin real, countval int);
""",
"""
INSERT INTO histotbl50 (bin, countval)
SELECT m.bin, m.countval
FROM histoview50 m;
""",
"""
CREATE TEMP VIEW histoview60 AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND s.SequenceIdentity > 55
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histotbl60 (bin real, countval int);
""",
"""
INSERT INTO histotbl60 (bin, countval)
SELECT m.bin, m.countval
FROM histoview60 m;
""",
"""
SELECT ha.bin AS Bin, 
	ha.countval AS Histogram,
	( CAST(
		(SELECT SUM(hb.countval)
		 FROM histotbl hb
		 WHERE hb.bin <= ha.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved)
	) AS Cumulative,

	ha20.countval AS Histo20,
	( CAST(
		(SELECT SUM(hb20.countval)
		 FROM histotbl20 hb20
		 WHERE hb20.bin <= ha20.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
			WHERE SequenceIdentity < 25
		)
	) AS Cumulative20,

	ha30.countval AS Histo30,
	( CAST(
		(SELECT SUM(hb30.countval)
		 FROM histotbl30 hb30
		 WHERE hb30.bin <= ha30.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
			WHERE SequenceIdentity < 35
			AND SequenceIdentity >= 25
		)
	) AS Cumulative30,

	ha40.countval AS Histo40,
	( CAST(
		(SELECT SUM(hb40.countval)
		 FROM histotbl40 hb40
		 WHERE hb40.bin <= ha40.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
			WHERE SequenceIdentity < 45
			AND SequenceIdentity >= 35
		)
	) AS Cumulative40,

	ha50.countval AS Histo50,
	( CAST(
		(SELECT SUM(hb50.countval)
		 FROM histotbl50 hb50
		 WHERE hb50.bin <= ha50.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
			WHERE SequenceIdentity < 55
			AND SequenceIdentity >= 45
		)
	) AS Cumulative50,

	ha60.countval AS Histo60,
	( CAST(
		(SELECT SUM(hb60.countval)
		 FROM histotbl60 hb60
		 WHERE hb60.bin <= ha60.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
			WHERE SequenceIdentity >= 55
		)
	) AS Cumulative60,

	hssm.countval AS HistoSSM,
	( CAST(
		(SELECT SUM(h.countval)
		 FROM SSMhistotbl h
		 WHERE h.bin <= hssm.bin 
		) AS REAL
		)/
		(SELECT COUNT(*) 
			FROM AllProteinsSolved 
		)
	) AS CumulativeSSM

FROM histotbl ha, histotbl20 ha20, histotbl30 ha30, histotbl40 ha40, histotbl50 ha50, histotbl60 ha60, SSMhistotbl hssm
WHERE ha.bin = ha20.bin
	AND ha.bin = ha30.bin
	AND ha.bin = ha40.bin
	AND ha.bin = ha50.bin
	AND ha.bin = ha60.bin
	AND ha.bin = hssm.bin
;
""",
"""
DROP TABLE histotbl;
""",
"""
DROP VIEW histoview;
""",
"""
DROP TABLE histotbl20;
""",
"""
DROP VIEW histoview20;
""",
"""
DROP TABLE histotbl30;
""",
"""
DROP VIEW histoview30;
""",
"""
DROP TABLE histotbl40;
""",
"""
DROP VIEW histoview40;
""",
"""
DROP TABLE histotbl50;
""",
"""
DROP VIEW histoview50;
""",
"""
DROP TABLE histotbl60;
""",
"""
DROP VIEW histoview60;
""",
"""
DROP TABLE SSMhistotbl;
""",
"""
DROP VIEW SSMhistoview;
"""]
,


"BorderlineNewRMSD_VRMSratio_SeqId_SolvedHisto": ["""
-- histogram of (VRMS/newrmsfit) hopefully gaussian
CREATE TEMP VIEW histoview AS
SELECT n.f AS bin, COUNT(s.VRMS1) AS countval 
FROM BorderlineSolved s, RMSD_VRMSratioIter n 
WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
	AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
	AND VRMS1 != ""
	AND CCglobal != ""
GROUP BY bin;
""",
"""
CREATE TABLE histotbl (bin real, countval int);
""",
"""
INSERT INTO histotbl (bin, countval)
SELECT m.bin, m.countval
FROM histoview m;
""",
"""
CREATE TEMP VIEW Histo AS SELECT ha.bin AS Bin, 
	ha.countval AS hiscountval,
	( CAST(
		(SELECT SUM(hb.countval)
		 FROM histotbl hb
		 WHERE hb.bin <= ha.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM BorderlineSolved)
	) AS Cumulative,
	(ha.countval * ha.bin) as BinHisProd,
	(ha.countval * ha.bin * ha.bin) as BinSqrHisProd
FROM histotbl ha;
""",
"""
SELECT Bin, hiscountval, Cumulative FROM Histo;
""",
"""
-- calculate the standard deviation from sqrt(avg(f^2) - avg(f)^2)
SELECT SUM(BinHisProd)/SUM(hiscountval), POW(SUM(BinSqrHisProd)/SUM(hiscountval),2), 
SQRT(SUM(BinSqrHisProd)/SUM(hiscountval) - POW(SUM(BinHisProd)/SUM(hiscountval),2)) AS sigma FROM Histo;
""",
"""
DROP TABLE histotbl;
""",
"""
DROP VIEW Histo;
""",
"""
DROP VIEW histoview;
"""]
,

"TargetClassRMSD_VRMSratio_SolvedHisto": ["""
-- scatter plot of all vrms values against sequence identity
CREATE TEMP VIEW histoAlphaview AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND s.TargetClass = "All alpha proteins"
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histoAlphatbl (bin real, countval int);
""",
"""
INSERT INTO histoAlphatbl (bin, countval)
SELECT m.bin, m.countval
FROM histoAlphaview m;
""",
"""
CREATE TEMP VIEW histoBetaview AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND s.TargetClass = "All beta proteins" 
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histoBetatbl (bin real, countval int);
""",
"""
INSERT INTO histoBetatbl (bin, countval)
SELECT m.bin, m.countval
FROM histoBetaview m;
""",
"""
CREATE TEMP VIEW histoApBview AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND s.TargetClass = "Alpha and beta proteins (a+b)"
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histoApBtbl (bin real, countval int);
""",
"""
INSERT INTO histoApBtbl (bin, countval)
SELECT m.bin, m.countval
FROM histoApBview m;
""",
"""
CREATE TEMP VIEW histoAsBview AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND s.TargetClass = "Alpha and beta proteins (a/b)"
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histoAsBtbl (bin real, countval int);
""",
"""
INSERT INTO histoAsBtbl (bin, countval)
SELECT m.bin, m.countval
FROM histoAsBview m;
""",
"""
CREATE TEMP VIEW histoAllview AS
SELECT allbins.f AS bin, IFNULL(pcount,0) AS countval
FROM RMSD_VRMSratioIter allbins
LEFT OUTER JOIN
(
	SELECT n.f AS bin, IFNULL(COUNT(s.VRMS1),0) AS pcount 
	FROM AllProteinsSolved s, RMSD_VRMSratioIter n 
	WHERE (CAST(s.VRMS1 AS REAL)/s.NewRMSD) <= (n.f + 0.025)
		AND (CAST(s.VRMS1 AS REAL)/s.NewRMSD) > (n.f - 0.025) 
		AND VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND (s.TargetClass = "Alpha and beta proteins (a/b)"
			OR s.TargetClass = "Alpha and beta proteins (a+b)"
			OR s.TargetClass = "All beta proteins" 
			OR s.TargetClass = "All alpha proteins"
			)
	GROUP BY bin
)
cbin ON cbin.bin = allbins.f;
""",
"""
CREATE TABLE histoAlltbl (bin real, countval int);
""",
"""
INSERT INTO histoAlltbl (bin, countval)
SELECT m.bin, m.countval
FROM histoAllview m;
""",
"""
CREATE TEMP VIEW Histo AS SELECT ha.bin AS Bin, 
-- normalise the histogram
	(CAST(ha.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlphatbl htmp)) AS HistoAlpha,
	( CAST(
		(SELECT SUM(htmp.countval)
		 FROM histoAlphatbl htmp
		 WHERE htmp.bin <= ha.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved 
		WHERE VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND TargetClass = "All alpha proteins"
		)
	) AS CumulativeAlpha,
	(CAST(ha.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlphatbl htmp) * ha.bin) as BinHisAProd,
	(CAST(ha.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlphatbl htmp) * ha.bin * ha.bin) as BinSqrHisAProd,

-- normalise the histogram
	(CAST(hb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoBetatbl htmp)) AS HistoBeta,
	( CAST(
		(SELECT SUM(htmp.countval)
		 FROM histoBetatbl htmp
		 WHERE htmp.bin <= hb.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved 
		WHERE VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND TargetClass = "All beta proteins"
		)
	) AS CumulativeBeta,
	(CAST(hb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoBetatbl htmp) * hb.bin) as BinHisBProd,
	(CAST(hb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoBetatbl htmp) * hb.bin * hb.bin) as BinSqrHisBProd,

-- normalise the histogram
	(CAST(hapb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoApBtbl htmp)) AS histoApB,
	( CAST(
		(SELECT SUM(htmp.countval)
		 FROM histoApBtbl htmp
		 WHERE htmp.bin <= hapb.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved 
		WHERE VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND TargetClass = "Alpha and beta proteins (a+b)"
		)
	) AS CumulativeApB,
	(CAST(hapb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoApBtbl htmp) * hapb.bin) as BinHisApBProd,
	(CAST(hapb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoApBtbl htmp) * hapb.bin * hapb.bin) as BinSqrHisApBProd,

-- normalise the histogram
	( CAST(hasb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAsBtbl htmp)) AS histoAsB,
	( CAST(
		(SELECT SUM(htmp.countval)
		 FROM histoAsBtbl htmp
		 WHERE htmp.bin <= hasb.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved 
		WHERE VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND TargetClass = "Alpha and beta proteins (a/b)"
		)
	) AS CumulativeAsB,
	(CAST(hasb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAsBtbl htmp) * hasb.bin) as BinHisAsBProd,
	(CAST(hasb.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAsBtbl htmp) * hasb.bin * hasb.bin) as BinSqrHisAsBProd,

-- normalise the histogram
	( CAST(hall.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlltbl htmp)) AS histoAll,
	( CAST(
		(SELECT SUM(htmp.countval)
		 FROM histoAlltbl htmp
		 WHERE htmp.bin <= hall.bin 
		) AS REAL
		)/(SELECT COUNT(*) FROM AllProteinsSolved 
		WHERE VRMS1 != ""
		AND NumberofResiduesinModel < 300 
		AND NumberofResiduesinModel > 100 
		AND (TargetClass = "Alpha and beta proteins (a/b)"
			OR TargetClass = "Alpha and beta proteins (a+b)"
			OR TargetClass = "All beta proteins" 
			OR TargetClass = "All alpha proteins"
			)
		)
	) AS CumulativeAll,
	( CAST(hall.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlltbl htmp) * hall.bin) as BinHisAllProd,
	( CAST(hall.countval AS REAL)/(SELECT SUM(htmp.countval) FROM histoAlltbl htmp) * hall.bin * hall.bin) as BinSqrHisAllProd

FROM histoAlphatbl ha, histoBetatbl hb, histoApBtbl hapb, histoAsBtbl hasb, histoAlltbl hall
WHERE ha.bin = hb.bin
	AND ha.bin = hapb.bin
	AND ha.bin = hasb.bin
	AND ha.bin = hall.bin
;
""",
"""
SELECT Bin, HistoAlpha, CumulativeAlpha, HistoBeta, CumulativeBeta, HistoApB, CumulativeApB, HistoAsB, CumulativeAsB,  HistoAll, CumulativeAll
FROM Histo;
""",
"""
-- print average values of the histograms
SELECT SUM(BinHisAProd)/SUM(HistoAlpha) AS Alphaavg, SUM(BinHisBProd)/SUM(HistoBeta)Betaavg, SUM(BinHisApBProd)/SUM(HistoApB) AS ApBavg, SUM(BinHisAsBProd)/SUM(HistoAsB) AS AsBavg, SUM(BinHisAllProd)/SUM(HistoAll) AS Allavg
FROM Histo;
""",
"""
-- calculate the standard deviations from sqrt(avg(f^2) - avg(f)^2)
SELECT 
SQRT(SUM(BinSqrHisAProd)/SUM(HistoAlpha) - POW(SUM(BinHisAProd)/SUM(HistoAlpha),2)) AS sigmaAlpha, 
SQRT(SUM(BinSqrHisBProd)/SUM(HistoBeta) - POW(SUM(BinHisBProd)/SUM(HistoBeta),2)) AS sigmaBeta,  
SQRT(SUM(BinSqrHisApBProd)/SUM(HistoApB) - POW(SUM(BinHisApBProd)/SUM(HistoApB),2)) AS sigmaApB, 
SQRT(SUM(BinSqrHisAsBProd)/SUM(HistoAsB) - POW(SUM(BinHisAsBProd)/SUM(HistoAsB),2)) AS sigmaAsB,
SQRT(SUM(BinSqrHisAllProd)/SUM(HistoAll) - POW(SUM(BinHisAllProd)/SUM(HistoAll),2)) AS sigmaAll
FROM Histo;
""",
"""
DROP TABLE histoAlphatbl;
""",
"""
DROP VIEW histoAlphaview;
""",
"""
DROP TABLE histoBetatbl;
""",
"""
DROP VIEW histoBetaview;
""",
"""
DROP TABLE histoApBtbl;
""",
"""
DROP VIEW histoApBview;
""",
"""
DROP TABLE histoAsBtbl;
""",
"""
DROP VIEW histoAsBview;
""",
"""
DROP TABLE histoAlltbl;
""",
"""
DROP VIEW histoAllview;
""",
"""
DROP VIEW histo;
"""
]
,

"NewRMSD_VRMSratio_SeqId_Solved": ["""
-- scatter plot of all vrms values against sequence identity
SELECT SequenceIdentity, (CAST(NewRMSD AS REAL)/VRMS) as NewRMSD_VRMSratio, CCglobal
FROM AllProteinsSolved 
WHERE VRMS != ""
AND Bfac0 < 2.0 
AND Bfac0 > -2.0
AND Bfac1 < 2.0 
AND Bfac1 > -2.0
AND Modelorder=1
AND CCglobal != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_Solved": ["""
-- scatter plot of all vrms values against sequence identity
SELECT SequenceIdentity, NumberofResiduesinModel, NumberofResiduesinTarget, VRMS1, C_L_rms
FROM AllProteinsSolved 
WHERE VRMS1 != ""
AND Modelorder=1
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SSMseqId_Solved": ["""
-- scatter plot of all vrms values against sequence identity
SELECT SSMseqid, NumberofResiduesinModel, NumberofResiduesinTarget, VRMS1, C_L_rms
FROM AllProteinsSolved 
WHERE VRMS1 != ""
AND Modelorder=1
AND SSMseqid > 0
AND SSMrms < 2.8
ORDER BY SSMseqid;
"""]
,

"SeqId_VRMS_Solved": ["""
-- scatter plot of all vrms values against sequence identity
SELECT VRMS1, C_L_rms, SequenceIdentity, NumberofResiduesinModel, NumberofResiduesinTarget
FROM AllProteinsSolved 
WHERE VRMS1 != ""
AND Modelorder=1
ORDER BY VRMS1;
"""]
,

"VRMS_SeqId_SolvedResidues0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 200 
AND VRMS1 != ""
ORDER BY SequenceIdentity;

"""]
,

"VRMS_SeqId_SolvedResidues200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 200-400 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 200
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResidues400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 400-600 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 600 
AND NumberofResiduesinTarget >= 400
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResidues600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResidues800_1000": ["""
-- scatter plot of vrms values against sequence identity for targets within 800-1000 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 1000 
AND NumberofResiduesinTarget >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResidues800_2000": ["""
-- scatter plot of vrms values against sequence identity for targets greater than 800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResidues1000_2000": ["""
-- scatter plot of vrms values against sequence identity for targets greater than 1000 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget >= 1000
AND VRMS1 != ""
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,

"VRMS_SeqId_SolvedResiduesInModel0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 200 
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesInModel200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 200-400 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesInModel400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 400-600 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 400
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesInModel600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 800 
AND NumberofResiduesinModel >= 600
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesInModel800_1000": ["""
-- scatter plot of vrms values against sequence identity for targets within 800-1000 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 1000 
AND NumberofResiduesinModel >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesInModel800_2000": ["""
-- scatter plot of vrms values against sequence identity for targets within 800-1000 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,

"VRMS_SeqId_SolvedResiduesModel0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 200 
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 200-400 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 400-600 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 400
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 800 
AND NumberofResiduesinModel >= 600
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel800_1000": ["""
-- scatter plot of vrms values against sequence identity for targets within 800-1000 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 1000 
AND NumberofResiduesinModel >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,


"VRMS_SeqId_SolvedResiduesModel0_100": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 100 
AND NumberofResiduesinModel >= 0
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,


"VRMS_SeqId_SolvedResiduesModel100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 200 
AND NumberofResiduesinModel >= 100
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel200_300": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 300 
AND NumberofResiduesinModel >= 200
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 300
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel400_500": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 500 
AND NumberofResiduesinModel >= 400
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel500_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 500
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel600_700": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 700 
AND NumberofResiduesinModel >= 600
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel700_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 800 
AND NumberofResiduesinModel >= 700
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesModel800_900": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 900 
AND NumberofResiduesinModel >= 800
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,


"VRMS_SeqId_SolvedResiduesModel900_1000": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, VRMS, C_L_rms, NewRMSD, ABS(VRMS-C_L_rms) as VRMS_CL_diff, ABS(VRMS-NewRMSD) as VRMS_NewRMSD_diff 
FROM AllProteinsSolved 
WHERE NumberofResiduesinModel < 1000 
AND NumberofResiduesinModel >= 900
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,











"VRMS_SeqId_SolvedResiduesTarget0_200Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget >= 0
AND NumberofResiduesinModel < 200 
AND NumberofResiduesinModel >= 0 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget0_100Model0_100": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 100 
AND NumberofResiduesinTarget >= 0
AND NumberofResiduesinModel < 100 
AND NumberofResiduesinModel >= 0 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget100_200Model0_100": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget >= 100
AND NumberofResiduesinModel < 100 
AND NumberofResiduesinModel >= 0 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget100_200Model100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget >= 100
AND NumberofResiduesinModel < 200 
AND NumberofResiduesinModel >= 100 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget200_400Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 200-400 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 200
AND NumberofResiduesinModel < 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget400_600Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 400-600 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 600 
AND NumberofResiduesinTarget >= 400
AND NumberofResiduesinModel < 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget600_800Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND NumberofResiduesinModel < 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget800_1000Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 800-1000 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 1000 
AND NumberofResiduesinTarget >= 800
AND NumberofResiduesinModel < 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,


"VRMS_SeqId_SolvedResiduesTarget400_600Model200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 200-400 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 600 
AND NumberofResiduesinTarget >= 400
AND NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget400_600Model400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 400-600 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 600 
AND NumberofResiduesinTarget >= 400
AND NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 400 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,
"VRMS_SeqId_SolvedResiduesTarget200_400Model200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 200
AND NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget200_300Model200_300": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 300 
AND NumberofResiduesinTarget >= 200
AND NumberofResiduesinModel < 300 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget300_400Model200_300": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 300
AND NumberofResiduesinModel < 300 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget300_400Model300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget >= 300
AND NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 300 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget600_800Model200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget600_800Model400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 400 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget600_800Model600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 800 
AND NumberofResiduesinTarget >= 600
AND NumberofResiduesinModel < 800 
AND NumberofResiduesinModel >= 600 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget1000_2000Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 2000 
AND NumberofResiduesinTarget >= 1000
AND NumberofResiduesinModel < 200 
AND NumberofResiduesinModel >= 0 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget1000_2000Model200_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 2000 
AND NumberofResiduesinTarget >= 1000
AND NumberofResiduesinModel < 400 
AND NumberofResiduesinModel >= 200 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget800_1000Model0_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 1000 
AND NumberofResiduesinTarget >= 800
AND NumberofResiduesinModel < 200 
AND NumberofResiduesinModel >= 0 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget800_1000Model600_800": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 1000 
AND NumberofResiduesinTarget >= 800
AND NumberofResiduesinModel < 800 
AND NumberofResiduesinModel >= 600 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget800_1000Model400_600": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 1000 
AND NumberofResiduesinTarget >= 800
AND NumberofResiduesinModel < 600 
AND NumberofResiduesinModel >= 400 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesTarget800_2000Model600_2000": ["""
-- scatter plot of vrms values against sequence identity for targets within 600-800 residues
SELECT SequenceIdentity, MIN(TmplVRMS, VRMS) AS optRMS, C_L_rms, NewRMSD, ABS(MIN(TmplVRMS, VRMS)-C_L_rms) as VRMS_CL_diff, ABS(MIN(TmplVRMS, VRMS)-NewRMSD) as VRMS_NewRMSD_diff
FROM AllProteins 
WHERE NumberofResiduesinTarget < 2000 
AND NumberofResiduesinTarget >= 800
AND NumberofResiduesinModel < 2000 
AND NumberofResiduesinModel >= 600 
AND (TmplVRMS != "" 
	OR VRMS != "")
AND MAX(LLGtemplate, LLGvrms) > 40
ORDER BY SequenceIdentity;
----------------------------------------------------------------------------------------
"""]
,

"VRMS_SeqId_SolvedResiduesAlpha100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "All alpha proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesBeta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "All beta proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesAlpha_p_Beta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "Alpha and beta proteins (a+b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesAlpha_s_Beta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "Alpha and beta proteins (a/b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,


"VRMS_SeqId_SolvedResiduesAlpha300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "All alpha proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesBeta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "All beta proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesAlpha_p_Beta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "Alpha and beta proteins (a+b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_SolvedResiduesAlpha_s_Beta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM AllProteinsSolved 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "Alpha and beta proteins (a/b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,


"TFZequiv_LLGvrmsSolved": ["""
SELECT TFZequiv, LLGvrms FROM AllProteinsSolved;
"""]
,

"TargetClassFracvarRatioHistogram": ["""
-- create ratio histogram for fracvar with bin size 0.025 sorted by SCOP classes
CREATE TEMP VIEW RatioHistogram AS
SELECT u.bin, u.tclass, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS Fracvarratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(TargetClass,sclass.name) AS tclass, IFNULL(pcount,0) AS count 
	FROM FracVarbigiter allbins, SCOPClasses sclass 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, restbl.TargetClass AS TargetClass, IFNULL(COUNT(restbl.Fracvar),0) AS pcount 
		FROM ProteinsUnSolved restbl, FracVarbigiter allbins 
		WHERE restbl.Fracvar <= (allbins.i + 0.0125) 
			AND restbl.Fracvar > (allbins.i - 0.0125)
			AND restbl.Spacegroup != "P 1" 
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Targetclass = sclass.name
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(TargetClass,sclass.name) AS tclass, IFNULL(pcount,0) AS count 
	FROM FracVarbigiter allbins, SCOPClasses sclass 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, restbl.TargetClass AS TargetClass, IFNULL(COUNT(restbl.Fracvar),0) AS pcount 
		FROM ProteinsSolved restbl, FracVarbigiter allbins 
		WHERE restbl.Fracvar <= (allbins.i + 0.0125) 
			AND restbl.Fracvar > (allbins.i - 0.0125)
			AND restbl.Spacegroup != "P 1" 
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.Targetclass = sclass.name
) s

WHERE u.bin = s.bin AND u.tclass = s.tclass;
""",
"""
--make ratio histogram for fracvar, targetSCOPclass
SELECT m1.bin AS fracvarbin, m1.totalcount AS AllAlphacount, m1.Fracvarratio AS AllAlpha, m2.totalcount AS Allbetacount, m2.Fracvarratio AS Allbeta, m3.totalcount AS Alpha_p_Betacount, m3.Fracvarratio AS Alpha_p_Beta, m4.totalcount AS Alpha_s_Betacount, m4.Fracvarratio AS Alpha_s_Beta
FROM RatioHistogram m1, RatioHistogram m2, RatioHistogram m3, RatioHistogram m4
WHERE m1.tclass = "All alpha proteins" 
	AND m2.tclass = "All beta proteins" 
	AND m3.tclass = "Alpha and beta proteins (a+b)" 
	AND m4.tclass = "Alpha and beta proteins (a/b)" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m4.bin;
"""]
,

"LLGvrms_TFZequiv": ["""
-- scatter data for LLGvrms and TFZequiv
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv.txt, outputfile =SMoothLLGvrms_TFZequiv.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivPointGroupOrder6": ["""
-- scatter data for LLGvrms and TFZequiv for crystals with PointGroupOrder=6
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND PointGroupOrder = 6
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"LLGvrms_TFZequivPointGroupOrder12": ["""
-- scatter data for LLGvrms and TFZequiv for crystals with PointGroupOrder=6
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND PointGroupOrder = 12
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"LLGvrms_TFZequivPointGroupOrder2": ["""
-- scatter data for LLGvrms and TFZequiv for crystals with PointGroupOrder=2
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND PointGroupOrder = 2
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"LLGvrms_TFZequivPointGroupOrder3": ["""
-- scatter data for LLGvrms and TFZequiv for crystals with PointGroupOrder=3
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND PointGroupOrder = 3
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"LLGvrms_TFZequivPointGroupOrder4": ["""
-- scatter data for LLGvrms and TFZequiv for crystals with PointGroupOrder=4
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND PointGroupOrder = 4
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"LLGvrms_TFZequivSpg19": ["""
-- scatter data for LLGvrms and TFZequiv for P 21 21 21
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv19.txt, outputfile =SMoothLLGvrms_TFZequiv19.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 19
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivSpg5": ["""
-- scatter data for LLGvrms and TFZequiv for C 2
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 5
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivSpg152": ["""
-- scatter data for LLGvrms and TFZequiv for P 3(1)21
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv152.txt, outputfile =SMoothLLGvrms_TFZequiv152.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 152
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivSpg145": ["""
-- scatter data for LLGvrms and TFZequiv for P 3(2)
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv19.txt, outputfile =SMoothLLGvrms_TFZequiv19.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 145
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivSpg143": ["""
-- scatter data for LLGvrms and TFZequiv for P 3
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv5.txt, outputfile =SMoothLLGvrms_TFZequiv5.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 143
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,

"LLGvrms_TFZequivSpg144": ["""
-- scatter data for LLGvrms and TFZequiv for P 3(1)
-- turn output file into a python list and process it with SmoothData.py using the argumetns below.
-- inputfile= LLGvrms_TFZequiv152.txt, outputfile =SMoothLLGvrms_TFZequiv152.txt,
-- sigx= 3, sigy= 3, step= 200, xmin= 0.0, xmax= 800, ymin= 0.0 ymax= 800, xhisto= x 
SELECT LLGvrms, TFZequiv*TFZequiv, CCglobal
FROM AllProteins 
WHERE LLGvrms != "" 
AND TFZequiv != ""
AND Spacegroupnumber = 144
AND CCglobal > 0.2
AND CCglobal IS NOT "";
"""]
,


"TFZSolvedRatioHistoPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,


"TFZSolvedRatioHistoNonPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,


"TFZSolvedRatioHistoPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZequiv) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZequiv <= (allbins.i + 0.25) 
			AND restbl.TFZequiv > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,






"TFZcorrSolvedRatioHistoPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,


"TFZcorrSolvedRatioHistoNonPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,



"TFZcorrSolvedRatioHistoPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW TFZxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZsmalliter allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, COUNT(restbl.TFZcorr) AS pcount 
		FROM AllProteins restbl, TFZsmalliter allbins, PointgroupTbl 
		WHERE restbl.TFZcorr <= (allbins.i + 0.25) 
			AND restbl.TFZcorr > (allbins.i-0.25) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS TFZbin, m2.TFZratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.TFZratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.TFZratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.TFZratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.TFZratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.TFZratio AS Pointgroup12, m12.totalcount AS PG12count
FROM TFZxtalratio m2, TFZxtalratio m3, TFZxtalratio m4, TFZxtalratio m6, TFZxtalratio m8, TFZxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
	AND m2.bin <= 20;
""",
"""
DROP VIEW TFZxtalratio;
"""]
,










"LLGSolvedRatioHistoPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW LLGxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS LLGbin, m2.LLGratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.LLGratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.LLGratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.LLGratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.LLGratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.LLGratio AS Pointgroup12, m12.totalcount AS PG12count
FROM LLGxtalratio m2, LLGxtalratio m3, LLGxtalratio m4, LLGxtalratio m6, LLGxtalratio m8, LLGxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
""",
"""
DROP VIEW LLGxtalratio;
"""]
,


"LLGSolvedRatioHistoNonPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW LLGxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.IsPolarSpacegroup = 0
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS LLGbin, m2.LLGratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.LLGratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.LLGratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.LLGratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.LLGratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.LLGratio AS Pointgroup12, m12.totalcount AS PG12count
FROM LLGxtalratio m2, LLGxtalratio m3, LLGxtalratio m4, LLGxtalratio m6, LLGxtalratio m8, LLGxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
""",
"""
DROP VIEW LLGxtalratio;
"""]
,


"LLGSolvedRatioHistoPolarPointgroups": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 crystals columned by PointGroupOrder
CREATE TEMP VIEW LLGxtalratio AS
SELECT u.pointgroup, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGratio 
FROM
(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal < 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg 
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder = PointgroupTbl.pg
) u, 

(
	SELECT IFNULL(PointGroupOrder,PointgroupTbl.pg) AS pointgroup, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins, PointgroupTbl 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT restbl.PointGroupOrder, allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrms),0) AS pcount 
		FROM AllProteins restbl, LLGiter3 allbins, PointgroupTbl 
		WHERE restbl.LLGvrms <= (allbins.i + 2.5) 
			AND restbl.LLGvrms > (allbins.i-2.5) 
			AND restbl.IsPolarSpacegroup = 1
			AND restbl.Spacegroup != "P 1" 
			AND restbl.CCglobal >= 0.2
			AND restbl.PointGroupOrder = PointgroupTbl.pg  
		GROUP BY restbl.PointGroupOrder, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.PointGroupOrder= PointgroupTbl.pg
) s

WHERE u.bin = s.bin AND u.pointgroup = s.pointgroup;
""",
"""
--make combined ratiohistogram 
SELECT m2.bin AS LLGbin, m2.LLGratio AS Pointgroup2, m2.totalcount AS PG2count, 
						 m3.LLGratio AS Pointgroup3, m3.totalcount AS PG3count,
						 m4.LLGratio AS Pointgroup4, m4.totalcount AS PG4count, 
						 m6.LLGratio AS Pointgroup6, m6.totalcount AS PG6count,
						 m8.LLGratio AS Pointgroup8, m8.totalcount AS PG8count,
						 m12.LLGratio AS Pointgroup12, m12.totalcount AS PG12count
FROM LLGxtalratio m2, LLGxtalratio m3, LLGxtalratio m4, LLGxtalratio m6, LLGxtalratio m8, LLGxtalratio m12 
WHERE m2.pointgroup = 2
	AND m3.pointgroup = 3 
	AND m4.pointgroup = 4 
	AND m6.pointgroup = 6 
	AND m8.pointgroup = 8 
	AND m12.pointgroup = 12 
	AND m2.bin = m3.bin 
	AND m2.bin = m4.bin 
	AND m2.bin = m6.bin 
	AND m2.bin = m8.bin
	AND m2.bin = m12.bin
""",
"""
DROP VIEW LLGxtalratio;
"""]
,



"NumberAllSolutionUnsolvedAvg": ["""
-- average number of solutions when no correct solution is found versus TFZ
SELECT TFZiter1.i as TFZbin, COUNT(m.TFZ0) AS count, AVG(m.NumberofSolutions) AS average, SQRT(SUM( CAST(m.NumberofSolutions AS REAL) * CAST(m.NumberofSolutions AS REAL))/COUNT(m.NumberofSolutions) - AVG(m.NumberofSolutions)*AVG(m.NumberofSolutions) ) AS stdev 
FROM AllProteinsUnSolved m, TFZiter1 
WHERE m.TFZ0 <= (TFZiter1.i+0.5) 
	AND m.TFZ0 > (TFZiter1.i-0.5) 
	AND m.Spacegroup != "P 1" 
	GROUP BY TFZiter1.i;
"""]
,

"NumberAllSolutionSolvedAvg": ["""
-- average number of solutions when the correct solution is found versus TFZ
-- ratio of highest TFZ or LLG being the correct solution versus TFZ
SELECT TFZiter1.i as TFZbin, COUNT(m.TFZ0) AS count, AVG(m.NumberofSolutions) AS average, SQRT(SUM( CAST(m.NumberofSolutions AS REAL) * CAST(m.NumberofSolutions AS REAL))/COUNT(m.NumberofSolutions) - AVG(m.NumberofSolutions)*AVG(m.NumberofSolutions) ) AS stdev, AVG(m.CorrectTFZtopmost) AS solutionTFZtopmostavg, AVG(m.CorrectLLGtopmost) AS solutionLLGtopmostavg 
FROM AllProteinsSolved m, TFZiter1 
WHERE m.TFZ0 <= (TFZiter1.i+0.5) 
	AND m.TFZ0 > (TFZiter1.i-0.5) 
	AND m.Spacegroup != "P 1" 
	GROUP BY TFZiter1.i;
"""]
,

"Nsolutions_TFZ": ["""
-- average number of solutions when no correct solution is found versus TFZ
SELECT solvedtbl.TFZbin, IFNULL(solvedtbl.countval, 0) AS solvedTFZ, IFNULL(unsolvedtbl.countval, 0) AS unsolvedTFZ
FROM
(
	SELECT n.i as TFZbin, COUNT(s.TFZ0) AS countval 
	FROM AllProteinsSolved s, TFZiter n 
	WHERE s.TFZ0 <= n.i 
		AND s.TFZ0 > (n.i - 0.5) 
		AND s.Spacegroup != "P 1" 
	GROUP BY TFZbin
) 
solvedtbl
LEFT OUTER JOIN 
(
	SELECT m.i as TFZbin, COUNT(u.TFZ0) AS countval 
	FROM AllProteinsUnSolved u, TFZiter m
	WHERE u.TFZ0 <= m.i 
		AND u.TFZ0 > (m.i - 0.5) 
		AND u.Spacegroup != "P 1" 
	GROUP BY m.i
)
unsolvedtbl 
ON unsolvedtbl.TFZbin = solvedtbl.TFZbin;
"""]
,

"BetterRMS5results_0_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolution, CorrectSolutionRMS5 
FROM UniqueMRresultsTbl 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 0
AND CorrectSolutionRMS5 = 0
;
"""]
,

"BetterRMS5results_1_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolution, CorrectSolutionRMS5 
FROM UniqueMRresultsTbl 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 1
AND CorrectSolutionRMS5 = 1
;
"""]
,

"BetterRMS5results_1_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolution, CorrectSolutionRMS5 
FROM UniqueMRresultsTbl 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 1
AND CorrectSolutionRMS5 = 0
;
"""]
,

"BetterRMS5results_0_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolution, CorrectSolutionRMS5 
FROM UniqueMRresultsTbl 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 0
AND CorrectSolutionRMS5 = 1
;
"""]
,

"BorderlineBetterRMS5results_0_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolutionc_l, CorrectSolutionRMS5 
FROM BorderlineCases 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 0
AND CorrectSolutionRMS5 = 0
;
"""]
,

"BorderlineBetterRMS5results_1_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolutionc_l, CorrectSolutionRMS5 
FROM BorderlineCases 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 1
AND CorrectSolutionRMS5 = 1
;
"""]
,

"BorderlineBetterRMS5results_1_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolutionc_l, CorrectSolutionRMS5 
FROM BorderlineCases 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 1
AND CorrectSolutionRMS5 = 0
;
"""]
,

"BorderlineBetterRMS5results_0_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0RMS5, iLLG0, iLLGRMS5, Correctsolutionc_l, CorrectSolutionRMS5 
FROM BorderlineCases 
WHERE iLLGRMS5 != ""
AND iLLGRMSc_l != ""
AND Correctsolutionc_l = 0
AND CorrectSolutionRMS5 = 1
;
"""]
,

"BetterRMSresults_0_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0NewRMS, iLLG0, iLLG0NewRMS, Correctsolution, CorrectSolutionNewRMS 
FROM UniqueMRresultsTbl 
WHERE iLLG0NewRMS != ""
AND Correctsolution = 0
AND CorrectSolutionNewRMS = 0
;
"""]
,

"BetterRMSresults_1_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0NewRMS, iLLG0, iLLG0NewRMS, Correctsolution, CorrectSolutionNewRMS 
FROM UniqueMRresultsTbl 
WHERE iLLG0NewRMS != ""
AND Correctsolution = 1
AND CorrectSolutionNewRMS = 1
;
"""]
,

"BetterRMSresults_1_0": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0NewRMS, iLLG0, iLLG0NewRMS, Correctsolution, CorrectSolutionNewRMS 
FROM UniqueMRresultsTbl 
WHERE iLLG0NewRMS != ""
AND Correctsolution = 1
AND CorrectSolutionNewRMS = 0
;
"""]
,

"BetterRMSresults_0_1": ["""
-- scatter plots of TFZ and LLG values from the 400 calculations with the new RMS estimate
SELECT TFZ0, TFZ0NewRMS, iLLG0, iLLG0NewRMS, Correctsolution, CorrectSolutionNewRMS 
FROM UniqueMRresultsTbl 
WHERE iLLG0NewRMS != ""
AND Correctsolution = 0
AND CorrectSolutionNewRMS = 1
;
"""]
,

"SeqIdSolvedRatioHisto": ["""
-- create ratio histogram for the sequence identity with bin size 5%
CREATE TEMP VIEW RatioHistogram AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS Seqidsolvedratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM SEQidbigiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.SequenceIdentity),0) AS pcount 
		FROM AllProteinsUnSolved restbl, SEQidbigiter allbins 
		WHERE restbl.SequenceIdentity <= (allbins.i + 2.5) 
			AND restbl.SequenceIdentity > (allbins.i - 2.5)
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM SEQidbigiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.SequenceIdentity),0) AS pcount 
		FROM AllProteinsSolved restbl, SEQidbigiter allbins 
		WHERE restbl.SequenceIdentity <= (allbins.i + 2.5) 
			AND restbl.SequenceIdentity > (allbins.i - 2.5)
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make ratio histogram for correct solutions
SELECT m1.bin AS SeqIDbin, m1.unsolved, m1.solved, m1.totalcount, m1.Seqidsolvedratio
FROM RatioHistogram m1;
""",
"""
DROP VIEW RatioHistogram;
"""]
,


"SeqIdSCOPSolvedRatioHisto": ["""
-- create ratio histogram for the sequence identity with bin size 5% divided ito SCOP classes
CREATE TEMP VIEW RatioHistogram AS
SELECT u.scopclass, u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS solvedratio 
FROM
(
	SELECT sclass.name AS scopclass, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM SEQidbigiter allbins, SCOPClasses sclass 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, restbl.TargetClass, IFNULL(COUNT(restbl.SequenceIdentity),0) AS pcount 
		FROM AllProteinsUnSolved restbl, SEQidbigiter allbins 
		WHERE restbl.SequenceIdentity <= (allbins.i + 2.5) 
			AND restbl.SequenceIdentity > (allbins.i - 2.5)
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.TargetClass = sclass.name
) u, 

(
	SELECT sclass.name AS scopclass, IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM SEQidbigiter allbins, SCOPClasses sclass  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, restbl.TargetClass, IFNULL(COUNT(restbl.SequenceIdentity),0) AS pcount 
		FROM AllProteinsSolved restbl, SEQidbigiter allbins
		WHERE restbl.SequenceIdentity <= (allbins.i + 2.5) 
			AND restbl.SequenceIdentity > (allbins.i - 2.5)
		GROUP BY restbl.TargetClass, bin
	) 
	cbin ON cbin.Bin = allbins.i AND cbin.TargetClass = sclass.name
) s

WHERE u.bin = s.bin AND u.scopclass = s.scopclass;
""",
"""
--make ratio histogram for correct solutions
SELECT m1.bin AS SeqIDbin, m1.totalcount AS alphatotal, m1.solvedratio AS alpharatio, m2.totalcount AS betacount, m2.solvedratio AS betaratio, 
 m3.totalcount AS Alpha_s_Betacount, m3.solvedratio AS Alpha_s_Betaratio, m4.totalcount AS Alpha_p_Betacount, m4.solvedratio AS Alpha_p_Beta, 
 m5.totalcount AS CoiledCoilcount, m5.solvedratio AS CoiledCoilratio, m6.totalcount AS DesignedProteinscount, m6.solvedratio AS DesignedProteins,
 m7.totalcount AS Membranecount, m7.solvedratio as Membraneratio, m8.totalcount AS MultiDomaincount, m8.solvedratio AS MultiDomainratio,
 m9.totalcount AS SmallProteinscount, m9.solvedratio AS SmallProteinsratio
FROM RatioHistogram m1, RatioHistogram m2, RatioHistogram m3, RatioHistogram m4, RatioHistogram m5, RatioHistogram m6, RatioHistogram m7, RatioHistogram m8, RatioHistogram m9
WHERE m1.scopclass = "All alpha proteins"
	AND m2.scopclass = "All beta proteins" 
	AND m3.scopclass = "Alpha and beta proteins (a/b)" 
	AND m4.scopclass = "Alpha and beta proteins (a+b)" 
	AND m5.scopclass = "Coiled coil proteins" 
	AND m6.scopclass = "Designed proteins" 
	AND m7.scopclass = "Membrane and cell surface proteins and peptides" 
	AND m8.scopclass = "Multi-domain proteins (alpha and beta)" 
	AND m9.scopclass = "Small proteins" 
	AND m1.bin = m2.bin 
	AND m1.bin = m3.bin 
	AND m1.bin = m4.bin 
	AND m1.bin = m5.bin
	AND m1.bin = m6.bin 
	AND m1.bin = m7.bin
	AND m1.bin = m8.bin 
	AND m1.bin = m9.bin;

""",
"""
DROP VIEW RatioHistogram;
"""]
,

"FsolHisto": ["""
SELECT IFNULL(Bin,allbins.f) AS bin, IFNULL(pcount,0) AS count 
FROM RMSD_VRMSratioIter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
LEFT OUTER JOIN 
(
	SELECT allbins.f AS bin, IFNULL(COUNT(m.Fsol),0) AS pcount 
	FROM UniqueMRresultsTbl m, RMSD_VRMSratioIter allbins
	WHERE m.Fsol <= (allbins.f + 0.25) 
		AND m.Fsol > (allbins.f - 0.25) 
		AND  m.Fsol != ""
	GROUP BY bin
) 
cbin ON cbin.Bin = allbins.f;
"""]

,

"VRMS_SeqId_ProteinsResiduesBeta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "All beta proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]

,

"VRMS_SeqId_ProteinsResiduesAlpha100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "All alpha proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_ProteinsResiduesAlpha_p_Beta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "Alpha and beta proteins (a+b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]

,

"VRMS_SeqId_ProteinsResiduesAlpha_s_Beta100_200": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 200 
AND NumberofResiduesinTarget > 100 
AND TargetClass = "Alpha and beta proteins (a/b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_ProteinsResiduesBeta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "All beta proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]

,

"VRMS_SeqId_ProteinsResiduesAlpha300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "All alpha proteins"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,

"VRMS_SeqId_ProteinsResiduesAlpha_p_Beta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "Alpha and beta proteins (a+b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]
,
"VRMS_SeqId_ProteinsResiduesAlpha_s_Beta300_400": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT SequenceIdentity, VRMS, C_L_rms 
FROM Proteins 
WHERE NumberofResiduesinTarget < 400 
AND NumberofResiduesinTarget > 300 
AND TargetClass = "Alpha and beta proteins (a/b)"
AND VRMS1 != ""
ORDER BY SequenceIdentity;
"""]

,
"TwoComponents": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT TargetPDBid, FullModel, PartialModel, ModelOrder, ModelCompleteness, FullModelCompleteness, VRMS1, VRMS2, VRMSL2, VRMSCombined, FracvarVrmsCombined, FracvarVrms1, FracvarVrms2, FracvarVrmsL2, LLGrefl_vrms, LLGcomb_vrms_refl
FROM AllProteins
WHERE LLGrefl_vrms != ""
ORDER BY ModelOrder;
"""]

,
"TwoComponents1": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT TargetPDBid, FullModel, PartialModel, ModelCompleteness, FullModelCompleteness, VRMS1, FracvarVrms1, LLGrefl_vrms
FROM AllProteins
WHERE LLGrefl_vrms != ""
AND FullModel != PartialModel 
AND ModelOrder=1;
"""]

,
"TwoComponents2": ["""
-- scatter plot of vrms values against sequence identity for targets within 0-200 residues
SELECT TargetPDBid, FullModel, PartialModel, ModelCompleteness, FullModelCompleteness, VRMS1, VRMS2, VRMSL2, FracvarVrms1, FracvarVrms2, FracvarVrmsL2, FracvarVrmsSum, LLGrefl_vrms, VRMSCombined, FracvarVrmsCombined, LLGcomb_vrms_refl
FROM AllProteins
WHERE LLGrefl_vrms != ""
AND FullModel != PartialModel 
AND ModelOrder=2;
"""]
,

"TwoComponentsLLGsigma": ["""
SELECT m.TargetPDBid, m.FullModel, m.PartialModel AS Model1, m.LLGrefl_vrms AS LLGrefl_vrms1, m.LLGvrms AS LLGvrms1, m.Tstar AS Tstar1, s.PartialModel AS PartialModel2, s.LLGrefl_vrms AS LLGrefl_vrms2, s.LLGvrms AS LLGvrms2, s.Tstar AS Tstar2, s.TFZ0, (s.LLGvrms - m.LLGvrms) AS LLGgain, ((s.LLGvrms - m.LLGvrms)/SQRT(s.LLGvrms + m.LLGvrms)) AS LLGsigma 
FROM AllProteins m, AllProteins s 
WHERE m.TargetPDBid = s.TargetPDBid 
AND m.FullModel = s.FullModel 
AND m.ModelOrder = 1 
AND s.ModelOrder = 2 
ORDER BY LLGgain;
"""]
,

"HeteroDimerLLGrefl_fracvar": ["""
SELECT FracvarVrmsSum, LLG1refl FROM TwoComponentRealMR
WHERE Bfac0 < 2.0 
AND Bfac0 > -2.0
AND Bfac1 < 2.0 
AND Bfac1 > -2.0;
"""]
,

"TwoComponents9": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i-2.5) 
			AND CC2 < 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i-2.5) 
			AND CC2 > 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,


"TwoComponentsLLG1Tstar": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND Tstar1 < 1
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND Tstar1 > 0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,


"TwoComponentsLLG1CC1": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND (CC1 < 0.2)
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND CC1 > 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]
,


"TFZHeteroSolvedRatio": ["""
-- create ratio histogram for the TFZ with Bin size 0.5
CREATE TEMP VIEW RatioHistogram AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZsolvedratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25)
			AND Tstar < 1
			AND Modelorder=2 
			AND ModelCompleteness < 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM AllProteins restbl, TFZiter allbins 
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25)
			AND Tstar > 0
			AND Modelorder=2 
			AND ModelCompleteness < 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make ratio histogram for TFZ correct solutions
SELECT m1.bin AS TFZbin, m1.unsolved, m1.solved, m1.totalcount, m1.TFZsolvedratio
FROM RatioHistogram m1;
""",
"""
DROP VIEW RatioHistogram;
"""]
,


"TFZHeteroSolvedRatio2": ["""
-- with CC2 > 0.2
CREATE VIEW DimerSolved AS 
SELECT TargetPDBid, FullModel, LLG1, Tstar1, TFZ0, Tstar2
FROM TwoComponents 
-- WHERE CC2 > 0.2
WHERE Tstar2 > 0
ORDER BY TFZ0;
""",
"""
CREATE VIEW DimerUnSolved AS 
SELECT TargetPDBid, FullModel, LLG1, Tstar1, TFZ0, Tstar2
FROM TwoComponents 
-- WHERE CC2 < 0.2
WHERE Tstar2 < 1
ORDER BY TFZ0;
""",
"""
-- 
CREATE TEMP VIEW TFZHeteroDimerRatio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM DimerUnSolved restbl, TFZiter allbins
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25)
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM TFZiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.TFZ0),0) AS pcount 
		FROM DimerSolved restbl, TFZiter allbins
		WHERE restbl.TFZ0 <= (allbins.i + 0.25) 
			AND restbl.TFZ0 > (allbins.i-0.25)
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--
SELECT m1.bin AS TFZbin, m1.TFZratio, m1.totalcount FROM TFZHeteroDimerRatio m1;
""",
"""
DROP VIEW DimerUnSolved;
""",
"""
DROP VIEW DimerSolved;
""",
"""
DROP VIEW TFZHeteroDimerRatio;
"""]
,


"TwoComponentsLLG2ContrivedMR": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponentContrivedMR restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND LLG0 > 80 
			AND CC1 < 0.2 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponentContrivedMR restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
			AND LLG0 > 80 
			AND CC1 > 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,


"TwoComponentsLLG2RealMR": ["""
-- create ratio histogram for the LLGvrms with Bin size 5 
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponentRealMR restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
--			AND CC0 > 0.4 
			AND CC1 < 0.2 
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponentRealMR restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
--			AND CC0 > 0.4 
			AND CC1 > 0.2
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,

"TwoComponentsLLG1MLAD1": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
--			AND CC0 > 0.4 
			AND CC1 < 0.2 
			AND Bfac0 < 5.0 
			AND Bfac0 > -5.0
			AND Bfac1 < 5.0 
			AND Bfac1 > -5.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG1),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG1 <= (allbins.i + 2.5) 
			AND restbl.LLG1 > (allbins.i-2.5) 
--			AND CC0 > 0.4 
			AND CC1 > 0.2
			AND Bfac0 < 5.0 
			AND Bfac0 > -5.0
			AND Bfac1 < 5.0 
			AND Bfac1 > -5.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,

"TwoComponentsLLG2MLAD2": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND MLAD2 > 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND MLAD2 <= 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,


"TwoComponentsLLG2MLAD2noCC1": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND MLAD2 > 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND MLAD2 <= 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,

"TwoComponentsLLG2MLAD2unsolved1": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW LLGVRMSxtalratio AS
SELECT u.bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND CC1 < 0.2 
			AND MLAD0 > 1.5
			AND MLAD2 > 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLG2),0) AS pcount 
		FROM TwoComponents restbl, LLGiter3 allbins
		WHERE restbl.LLG2 <= (allbins.i + 2.5) 
			AND restbl.LLG2 > (allbins.i - 2.5) 
			AND CC1 < 0.2 
			AND MLAD0 > 1.5
			AND MLAD2 <= 1.5
			AND Bfac0 < 2.0 
			AND Bfac0 > -2.0
			AND Bfac1 < 2.0 
			AND Bfac1 > -2.0
			AND Bfac2 < 2.0 
			AND Bfac2 > -2.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
--make combined ratiohistogram omitting triclinic and monoclinic AS these are always polar spg
SELECT m1.bin AS LLGbin, m1.LLGVRMSratio, m1.totalcount FROM LLGVRMSxtalratio m1;
""",
"""
DROP VIEW LLGVRMSxtalratio;
"""]

,

"ModelsperTargetHisto": ["""
-- Histogram of number of models as a function of targetsize
-- Must use a token HAVING clause because countval is an aggregate and cannot be filtered in a WHERE clause
SELECT n.i AS MRmodels, COUNT(m.countval) AS MRcalcs, m.TargetPDBid 
FROM TFZiter1 n, (SELECT COUNT(s.TargetPDBid) AS countval, s.TargetPDBid 
					FROM AllProteins s 
					GROUP BY s.TargetPDBid 
					HAVING countval > 0 
					ORDER BY countval
					) m

WHERE m.countval = n.i
GROUP BY n.i
;
"""]

,

"SSMseqid_ClustalWseqidDifs": ["""
SELECT SequenceIdentity, (SequenceIdentity - SSMseqid) AS diffs, SSMseqid, ClustalWlength, SSMlength, NumberofResiduesinmodel, VRMS1, SSMrms, LLGvrms, TargetPDBid, Fullmodel, SCOPid
FROM AllProteinsSolved
WHERE Foundit > 0
AND SSMseqid != -1
AND SSMseqid != 0
AND SSMrms < 2.8
ORDER BY diffs;
"""]

,
"LLG_ELLG_rmsd-vrms_0.1": ["""
-- Use with TemplateRfnELLG-svn6448.db
SELECT TargetPDBid, Fullmodel, LLG0, ELLG0, VELLG0, VRMS0, NewRMSD 
FROM AllProteins 
WHERE LLG0 != "" 
AND (NewRMSD - VRMS0) < 0.1 
AND 0.0 < (NewRMSD - VRMS0)
ORDER BY LLG0 DESC
;
"""]

,
"LLG_ELLG_vrms-rmsd_0.1": ["""
-- Use with TemplateRfnELLG-svn6448.db
SELECT TargetPDBid, Fullmodel, LLG0, ELLG0, VELLG0, VRMS0, NewRMSD 
FROM AllProteins 
WHERE LLG0 != "" 
AND (VRMS0 - NewRMSD) < 0.1 
AND 0.0 < (VRMS0 - NewRMSD)
ORDER BY LLG0 DESC
;
"""]

,
"LLG_ELLG_abs_rmsd-vrms_0.1": ["""
-- Use with TemplateRfnELLG-svn6448.db
SELECT TargetPDBid, Fullmodel, LLG0, ELLG0, VELLG0, VRMS0, NewRMSD 
FROM AllProteins 
WHERE LLG0 != "" 
AND ABS(NewRMSD - VRMS0) < 0.1
ORDER BY LLG0 DESC
;
"""]

,
"LLG_ELLG": ["""
-- Use with TemplateRfnELLG-svn6448.db
SELECT TargetPDBid, Fullmodel, LLG0, ELLG0, VELLG0, VRMS0, NewRMSD 
FROM AllProteins 
WHERE LLG0 != "" 
ORDER BY LLG0 DESC
;
"""]

,
"DeltaCCocc_ELLGwnd0.2_0.3": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.3
AND CCmap0 > 0.2
GROUP BY LLGiter6.i;
"""]

,
"DeltaCCocc_ELLGwnd0.3_0.4": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.4
AND CCmap0 > 0.3
GROUP BY LLGiter6.i;
"""]

,
"DeltaCCocc_ELLGwnd0.4_0.5": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.5
AND CCmap0 > 0.4
GROUP BY LLGiter6.i;
"""]
,
"DeltaCCocc_ELLGwnd0.5_0.6": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.6
AND CCmap0 > 0.5
GROUP BY LLGiter6.i;
"""]

,
"DeltaCCocc_ELLGwnd0.6_0.7": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.7
AND CCmap0 > 0.6
GROUP BY LLGiter6.i;
"""]

,
"DeltaCCocc_ELLGwnd0.7_0.8": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.8
AND CCmap0 > 0.7
GROUP BY LLGiter6.i;
"""]

,
"DeltaCCocc_ELLGwnd0.8_0.9": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT LLGiter6.i as LLGbin, AVG(m.dCC) AS avcDcc, COUNT(m.dCC) AS count 
FROM ELlgOccTbl m, LLGiter6 
WHERE 
m.ELLGoccwnd <= (LLGiter6.i + 1.25) 
AND m.ELLGoccwnd > (LLGiter6.i - 1.25) 
AND CCmap0 <= 0.9
AND CCmap0 > 0.8
GROUP BY LLGiter6.i;
"""]

,
"OCCfachisto": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6433.db
SELECT OCCiter.i as OCCfacbin,  COUNT(m.OCCfac0) AS count 
FROM AllProteins m, OCCiter 
WHERE 
m.OCCfac0 <= (OCCiter.i + 0.025) 
AND m.OCCfac0 > (OCCiter.i - 0.025) 
GROUP BY OCCiter.i;
"""]

,
"dVRMS_Ofac-svn6447": ["""
-- Use with RNP_VRMS-svn6447.db and RNP_OFAC_VRMS-svn6447.db
attach "..\..\RNP_OFAC_VRMS-svn6447.db" AS Ofacdb;
""",
"""
SELECT v.TargetPDBid, v.Fullmodel, v.VRMS0 AS VRMS_RNP, o.VRMS0 AS VRMS_OCC, o.OCCfac0 AS Ofac, (o.VRMS0 - v.VRMS0) AS dVRMS 
FROM AllProteins v, Ofacdb.AllProteins o 
WHERE v.TargetPDBid = o.TargetPDBid 
AND v.Fullmodel = o.Fullmodel
AND o.OCCfac0 != "";
""",
"""
detach database Ofacdb;
"""]

,
"dCC_ELLGoccwnd-svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCoccglobal, CCmap0, LLGocc, LLG0, dLLG, ELLGoccwnd, dCC 
FROM ELlgOccTbl;
"""]
,

"dCC_ELLGoccwnd-svn6966": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCoccglobaltmpl, CCglobaltmpl, LLGocctmpl, LLGvrmstmpl, dLLG, dLLGaic, NrOccRefParamtmpl, ELLGoccwndtmpl, dCC 
FROM ELlgOccTbl 
WHERE LLGvrmstmpl>30
AND CCglobaltmpl>0.18
ORDER BY dLLGaic DESC;
"""]


,
"dCC_ELLGoccwnd-OCCtesting_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCoccglobal, CCmap0, LLGocc, LLG0, dLLG, ELLGoccwnd, dCC 
FROM ELlgOccTbl;
"""]


,
"dCC_ELLGoccwnd-OCC_testing_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
attach "..\TemplateOCCRfnNreswindow1svn6454.db" AS MRdb;
""",
"""
SELECT f.TargetPDBid, f.Fullmodel, f.CCoccglobal, f.CCmap0, f.LLGocc, f.LLG0, f.dLLG, 
f.ELLGoccwnd, f.dCC, s.CCoccglobal, s.CCmap0, s.LLGocc, s.LLG0, s.dLLG, s.ELLGoccwnd, s.dCC
FROM ELlgOccTbl f, MRdb.ELlgOccTbl s
WHERE f.TargetPDBid = s.TargetPDBid
AND f.Fullmodel = s.Fullmodel
ORDER BY s.dLLG
;
""",
"""
detach MRdb;
"""]


,
"CCocc_CCmap-svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db or TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCmap0, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE CCmap0 >= 0.2
"""]

,
"CCocc_CCmap-OCCtesting_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCmap0, CCoccglobal, LLG0, LLGocc, ELLGoccwnd
FROM AllPRoteins
WHERE CCmap0 >= 0.2
"""]

,
"dCChistodefreswind_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1-3svn6454.db
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobal- m.CCmap0) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobal- m.CCmap0) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobal != ""
AND CCmap0 >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"dCChistodefreswind-OCCtesting_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobal- m.CCmap0) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobal- m.CCmap0) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobal != ""
AND CCmap0 >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"dCChistodefreswind_svn6487": ["""
-- Use with OCCrfnNonAIC-svn6487.db
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobal- m.CCmap0) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobal- m.CCmap0) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobal != ""
AND CCmap0 >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"dCChistodefreswind-NonAIC_svn6487": ["""
-- Use with OCCrfnNonAIC-svn6487.db
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobalNonAIC- m.CCmap0) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobalNonAIC- m.CCmap0) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobalNonAIC != ""
AND CCmap0 >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"Nreswndsizehisto_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db
SELECT Nresoccwnditer.i as Nressizebin, COUNT(m.Nresoccwnd) AS count 
FROM AllProteins m, Nresoccwnditer 
WHERE 
-- Nresoccwnd assumes only odd valued integers
m.Nresoccwnd = Nresoccwnditer.i
AND m.LLG0 < m.LLGocc
AND m.CCmap0 >= 0.2
GROUP BY Nresoccwnditer.i;
"""]

,
"Nreswndsizehisto-OCCtesting_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT Nresoccwnditer.i as Nressizebin,  COUNT(m.Nresoccwnd) AS count 
FROM AllProteins m, Nresoccwnditer 
WHERE 
-- Nresoccwnd assumes only odd valued integers
m.Nresoccwnd = Nresoccwnditer.i
AND m.LLG0 < m.LLGocc
AND m.CCmap0 >= 0.2
GROUP BY Nresoccwnditer.i;
"""]


,
"NoOCCNreswndsizehisto_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db
SELECT Nresoccwnditer.i as Nressizebin,  COUNT(m.Nresoccwnd) AS count 
FROM AllProteins m, Nresoccwnditer 
WHERE 
-- Nresoccwnd assumes only odd valued integers
m.Nresoccwnd = Nresoccwnditer.i
AND m.LLG0 >= m.LLGocc
AND m.CCmap0 >= 0.2
GROUP BY Nresoccwnditer.i;
"""]

,
"NoOCCNreswndsizehisto-OCCtesting_svn6454": ["""
-- Use with TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT Nresoccwnditer.i as Nressizebin,  COUNT(m.Nresoccwnd) AS count 
FROM AllProteins m, Nresoccwnditer 
WHERE 
-- Nresoccwnd assumes only odd valued integers
m.Nresoccwnd = Nresoccwnditer.i
AND m.LLG0 >= m.LLGocc
AND m.CCmap0 >= 0.2
GROUP BY Nresoccwnditer.i;
"""]

,
"dCC_ELLGoccwnd-svn6583": ["""
-- Use with Completeresults_svn6583.db
SELECT TargetPDBid, Fullmodel, Nresoccwnd, CCoccglobal, CCmap0, LLGocc, LLG0, dLLG, ELLGoccwnd, dCC 
FROM ELlgOccTbl 
WHERE CCmap0 > 0.2 -- forgot to omit non-solutions in the calculations
"""]

,
"dCChistodefreswind-svn6583": ["""
-- Use with Completeresults_svn6583.db
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCmap0) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobal- m.CCmap0) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobal- m.CCmap0) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobal != ""
AND CCmap0 >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"CCocc_CCmap-svn6966": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db or TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL
ORDER BY dLLGaicthrshld DESC;
"""]


,
"CCocc_CCmap": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db or TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - LLGvrmstmpl) AS dLLG,
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL
ORDER BY dLLG DESC;
"""]


,
"dCChistodefreswind_AICthrshld-svn6966": ["""
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCglobaltmpl) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobaltmpl- m.CCglobaltmpl) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobaltmpl- m.CCglobaltmpl) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobaltmpl != ""
AND m.CCglobaltmpl >= 0.2
AND (m.LLGocctmpl - (m.LLGvrmstmpl + m.NrOccThresholdstmpl)) > 0.0
GROUP BY dCCiter2.i;
"""]


,
"dCChistodefreswind-svn6966": ["""
SELECT dCCiter2.i as dCCbin,  COUNT(m.CCglobaltmpl) AS count 
FROM AllProteins m, dCCiter2 
WHERE 
(m.CCoccglobaltmpl- m.CCglobaltmpl) <= (dCCiter2.i + 0.0025) 
AND (m.CCoccglobaltmpl- m.CCglobaltmpl) > (dCCiter2.i - 0.0025) 
AND m.CCoccglobaltmpl != ""
AND m.CCglobaltmpl >= 0.2
GROUP BY dCCiter2.i;
"""]

,
"DeltaCCocc_CCglobalAICthrshld": ["""
CREATE TEMP VIEW dCCTbl AS 
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
LLGoccfracmergedtmpl, (LLGoccfracmergedtmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGoccfracmerged,
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL;
""",
"""
SELECT CCiter.i as CCbin, AVG(m.dCC) AS avcDccAICthrshld, COUNT(m.dLLGaicthrshld) AS dCCcount 
FROM dCCTbl m, CCiter
WHERE 
m.CCglobaltmpl <= (CCiter.i + 0.0125) 
AND m.CCglobaltmpl > (CCiter.i - 0.0125) 
AND dLLGaicthrshld > 0.0
AND m.CCoccglobaltmpl != ""
AND m.CCglobaltmpl >= 0.2
GROUP BY CCiter.i;
"""]

,
"DeltaCCocc_CCglobal_underAICthrshld": ["""
CREATE TEMP VIEW dCCTbl AS 
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL;
""",
"""
SELECT CCiter.i as CCbin, AVG(m.dCC) AS avcDccAICthrshld, COUNT(m.dLLGaicthrshld) AS dCCcount 
FROM dCCTbl m, CCiter
WHERE 
m.CCglobaltmpl <= (CCiter.i + 0.0125) 
AND m.CCglobaltmpl > (CCiter.i - 0.0125) 
AND dLLGaicthrshld <= 0.0
AND m.CCoccglobaltmpl != ""
AND m.CCglobaltmpl >= 0.2
GROUP BY CCiter.i;
"""]


,
"DeltaCCocc_CCglobal": ["""
CREATE TEMP VIEW dCCTbl AS 
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL;
""",
"""
SELECT CCiter.i as CCbin, AVG(m.dCC) AS avcDccAICthrshld, COUNT(m.dCC) AS dCCcount 
FROM dCCTbl m, CCiter
WHERE 
m.CCglobaltmpl <= (CCiter.i + 0.0125) 
AND m.CCglobaltmpl > (CCiter.i - 0.0125) 
--AND dLLGaicthrshld > 0.0
AND m.CCoccglobaltmpl != ""
AND m.CCglobaltmpl >= 0.2
GROUP BY CCiter.i;
"""]


,
"CCocc_CCmap_AICrefparm-svn6966": ["""
-- Use with TemplateOCCRfnNreswindow1svn6454.db or TemplateOCCRfnNreswindow1_OCCtesting_svn6454.db
SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
LLGoccfracmergedtmpl, AvgLLGsoccfracunboundtmpl, AvgLLGsoccfracboundtmpl, NrOccOffsetstmpl,
(LLGoccfracmergedtmpl - (LLGvrmstmpl + NrOccRefParamtmpl + NrOccOffsetstmpl)) AS dLLGoccfracmerged,
(AvgLLGsoccfracunboundtmpl - (LLGvrmstmpl + NrOccRefParamtmpl + NrOccOffsetstmpl)) AS dAvgLLGsoccfracunboundtmpl,
(AvgLLGsoccfracboundtmpl - (LLGvrmstmpl + NrOccRefParamtmpl + NrOccOffsetstmpl)) AS dAvgLLGsoccfracboundtmpl,
(CCoccglobaltmpl - CCglobaltmpl) AS dCC,
ELLGoccwndtmpl
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL
ORDER BY dAvgLLGsoccfracunboundtmpl DESC;
"""]

,

"DeltaCCoccImprovedRatioHisto": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW dCCdLLG AS SELECT TargetPDBid, Fullmodel, Nresoccwndtmpl, CCglobaltmpl, CCoccglobaltmpl, LLGvrmstmpl, LLGocctmpl, 
(LLGocctmpl - LLGvrmstmpl) AS dLLGocc,
(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
LLGoccfracmergedtmpl, AvgLLGsoccfracunboundtmpl, AvgLLGsoccfracboundtmpl, NrOccOffsetstmpl,
(LLGoccfracmergedtmpl - (LLGvrmstmpl + NrOccRefParamtmpl + NrOccOffsetstmpl)) AS dLLGoccfracmerged,
(AvgLLGsoccfracunboundtmpl - LLGvrmstmpl) AS dAvgLLGsfracunboundtmpl,
(AvgLLGsoccfracboundtmpl - LLGvrmstmpl) AS dAvgLLGsfracboundtmpl,
(LLGoccfracmergedtmpl - LLGvrmstmpl) AS dLLGoccfracmergedtmpl,
(AvgLLGsoccfracunboundtmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dAvgLLGsoccfracunboundtmpl,
(AvgLLGsoccfracboundtmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dAvgLLGsoccfracboundtmpl,
(CCoccglobaltmpl - CCglobaltmpl) AS dCC
FROM AllPRoteins
WHERE CCglobaltmpl >= 0.2
AND CCoccglobaltmpl IS NOT NULL;
""",
"""
CREATE TEMP VIEW DCCoccimprovratio AS
SELECT u.bin, u.count AS worseCC, s.count AS betterCC, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS dCCratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM dLLGiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.dLLGocc),0) AS pcount 
		FROM dCCdLLG restbl, dLLGiter allbins
		WHERE restbl.dLLGoccfracmergedtmpl <= (allbins.i + 2.5) 
			AND restbl.dLLGoccfracmergedtmpl > (allbins.i-2.5) 
			AND restbl.dCC < 0.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM dLLGiter allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.dLLGocc),0) AS pcount 
		FROM dCCdLLG restbl, dLLGiter allbins
		WHERE restbl.dLLGoccfracmergedtmpl <= (allbins.i + 2.5) 
			AND restbl.dLLGoccfracmergedtmpl > (allbins.i-2.5) 
			AND restbl.dCC >= 0.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
SELECT m1.bin AS dLLGbin, m1.dCCratio, m1.totalcount FROM DCCoccimprovratio m1;
""",
"""
DROP VIEW DCCoccimprovratio;
""",
"""
DROP VIEW dCCdLLG;
"""]


,

"CCoccImprovedRatioHisto": ["""
-- create ratio histogram for the LLGvrms with Bin size 5, non-polar crystals columned by Crystalsystems
CREATE TEMP VIEW CCoccimprovratio AS
SELECT u.bin, u.count AS worseCC, s.count AS betterCC, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS dCCratio 
FROM
(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrmstmpl),0) AS pcount
		FROM AllPRoteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrmstmpl <= (allbins.i + 2.5) 
			AND restbl.LLGvrmstmpl > (allbins.i-2.5) 
			AND CCoccglobaltmpl IS NOT NULL
			--AND (restbl.AvgLLGsoccfracboundtmpl - (restbl.LLGvrmstmpl + restbl.NrOccOffsetstmpl)) < 0.0
			AND (restbl.CCoccglobaltmpl - restbl.CCglobaltmpl) < 0.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) u, 

(
	SELECT IFNULL(Bin,allbins.i) AS bin, IFNULL(pcount,0) AS count 
	FROM LLGiter3 allbins 
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT allbins.i AS bin, IFNULL(COUNT(restbl.LLGvrmstmpl),0) AS pcount
		FROM AllPRoteins restbl, LLGiter3 allbins
		WHERE restbl.LLGvrmstmpl <= (allbins.i + 2.5) 
			AND restbl.LLGvrmstmpl > (allbins.i-2.5) 
			AND CCoccglobaltmpl IS NOT NULL
			--AND (restbl.AvgLLGsoccfracboundtmpl - (restbl.LLGvrmstmpl + restbl.NrOccOffsetstmpl)) > 0.0
			AND (restbl.CCoccglobaltmpl - restbl.CCglobaltmpl) >= 0.0
		GROUP BY bin
	) 
	cbin ON cbin.Bin = allbins.i
) s

WHERE u.bin = s.bin;
""",
"""
SELECT m1.bin AS LLGbin, m1.dCCratio, m1.totalcount FROM CCoccimprovratio m1;
""",
"""
DROP VIEW CCoccimprovratio;
"""]



}




