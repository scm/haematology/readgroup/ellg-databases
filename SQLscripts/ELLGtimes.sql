/* List times, avg(times) and stdev(times) of calculations of groups of model-targets using the same number of reflections,
i.e. where more than 1 calculation uses the same number of reflections
*/
CREATE VIEW AvgTimes AS SELECT * FROM (SELECT a.UsedReflections, a.FullModel, a.MRTarget_id,
 ( SELECT COUNT(*) FROM MRModelTbl b WHERE a.MRTarget_id=b.MRTarget_id AND a.FullModel=b.FullModel AND a.UsedReflections=b.UsedReflections  ) AS SameNrRefl,
 ( SELECT AVG(b.Usertime) FROM MRModelTbl b WHERE a.MRTarget_id=b.MRTarget_id AND a.FullModel=b.FullModel AND a.UsedReflections=b.UsedReflections  ) AS AvgUsertime,
 ( SELECT STDEV(b.Usertime) FROM MRModelTbl b WHERE a.MRTarget_id=b.MRTarget_id AND a.FullModel=b.FullModel AND a.UsedReflections=b.UsedReflections  ) AS StdevUsertime
FROM MRModelTbl a WHERE SameNrRefl>1 AND AvgUsertime IS NOT NULL GROUP BY a.MRTarget_id, a.FullModel, a.UsedReflections) ORDER BY AvgUsertime;

 
  
CREATE TABLE ELLGtarget_iter(eLLG_target real);
INSERT INTO ELLGtarget_iter VALUES(64);
INSERT INTO ELLGtarget_iter VALUES(81);
INSERT INTO ELLGtarget_iter VALUES(100);
INSERT INTO ELLGtarget_iter VALUES(121);
INSERT INTO ELLGtarget_iter VALUES(144);
INSERT INTO ELLGtarget_iter VALUES(169);
INSERT INTO ELLGtarget_iter VALUES(196);
INSERT INTO ELLGtarget_iter VALUES(225);
INSERT INTO ELLGtarget_iter VALUES(256);
INSERT INTO ELLGtarget_iter VALUES(289);
INSERT INTO ELLGtarget_iter VALUES(324);
INSERT INTO ELLGtarget_iter VALUES(361);
INSERT INTO ELLGtarget_iter VALUES(400);
INSERT INTO ELLGtarget_iter VALUES(441);
INSERT INTO ELLGtarget_iter VALUES(484);
CREATE INDEX ELLGtarget_idx ON ELLGtarget_iter(eLLG_target);



/* make a naive average times table ignoring the fact that some data sets will have exhausted
their available number of reflections for increasing ELLG_TARGET
*/
CREATE Table TimesTbl AS SELECT e.eLLG_target,
 (SELECT SUM(f.Usertime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target) AS SumUsertime,
  (SELECT SUM(f.CPUtime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,  
  (SELECT AVG(f.Usertime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsertime,
  (SELECT AVG(f.CPUtime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT AVG(f.Usertime) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID) AS AvgSolvedUsertime,
  (SELECT AVG(f.Usertime) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID) AS AvgUnsolvedUsertime,
  (SELECT COUNT(*) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID) AS CountSolvedUsertime,
  (SELECT COUNT(*) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID) AS CountUnsolvedUsertime
  FROM ELLGtarget_iter e;
  
  

CREATE Table TimesnoRMSLoweringHighResTbl AS SELECT e.eLLG_target,
 (SELECT SUM(f.Usertime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target AND f.RMSLoweringHighRes IS NULL) AS SumUsertime,
  (SELECT SUM(f.CPUtime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target AND f.RMSLoweringHighRes IS NULL) AS SumCPUtime,  
  (SELECT AVG(f.Usertime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target AND f.RMSLoweringHighRes IS NULL) AS AvgUsertime,
  (SELECT AVG(f.CPUtime) FROM MRModelTbl f WHERE f.ELLG_TARG=e.eLLG_target AND f.RMSLoweringHighRes IS NULL) AS AvgCPUtime,
  (SELECT AVG(f.Usertime) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID AND f.RMSLoweringHighRes IS NULL) AS AvgSolvedUsertime,
  (SELECT AVG(f.Usertime) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID AND f.RMSLoweringHighRes IS NULL) AS AvgUnsolvedUsertime,
  (SELECT COUNT(*) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID AND f.RMSLoweringHighRes IS NULL) AS CountSolvedUsertime,
  (SELECT COUNT(*) FROM MRModelTbl f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.MRTarget_id=a.MRTarget_id AND a.MRModel_id=f.ROWID AND f.RMSLoweringHighRes IS NULL) AS CountUnsolvedUsertime
  FROM ELLGtarget_iter e;
  
  
  
ALTER TABLE MRModelTbl ADD MRLogFile text;  
ALTER TABLE MRModelTbl ADD MRResults text;  
ALTER TABLE MRModelTbl ADD MRModelInput text;  
ALTER TABLE MRModelTbl ADD MTZinput text;  
ALTER TABLE MRModelTbl ADD Sequenceinput text;  
UPDATE MRModelTbl SET MRLogFile= (SELECT "1component/" || TargetPDBid || "/" || TargetPDBid || "_rmsdmult2.0_ELLG169" || ELLG_TARG || "_2.8.1.7220RobsDevelop3_b545524.zip" || " : /MR/" || Fullmodel || "/Strider{" || Fullmodel || "}" || Fullmodel || ".log" ) ;  
UPDATE MRModelTbl SET MRResults= (SELECT "1component/" || TargetPDBid || "/" || TargetPDBid || "_rmsdmult2.0_ELLG169" || ELLG_TARG || "_2.8.1.7220RobsDevelop3_b545524.zip" || " : /MR/" || Fullmodel ) ;  
UPDATE MRModelTbl SET MRModelInput= (SELECT "1component/" || TargetPDBid || "/" || TargetPDBid || "_*/" || Fullmodel || "/sculpt_" || Fullmodel || "(?:_singlechain).pdb" ) ;  
UPDATE MRModelTbl SET MTZinput= (SELECT "1component/" || TargetPDBid || "/Phaserinput/" || TargetPDBid || ".mtz" ) ;  
UPDATE MRModelTbl SET Sequenceinput= (SELECT "1component/" || TargetPDBid || "/Phaserinput/" || TargetPDBid || "_Chain*.seq" ) ;  

/*
Prepare for making table taking into account the fact that some data sets will have exhausted
their available number of reflections for increasing ELLG_TARGET
*/ 
  
-- create separate tables for each ELLG_TARG as to speed up lookups
CREATE TABLE MRModel64Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=64;
CREATE TABLE MRModel81Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=81;
CREATE TABLE MRModel100Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=100;
CREATE TABLE MRModel121Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=121;
CREATE TABLE MRModel144Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=144;
CREATE TABLE MRModel169Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=169;
CREATE TABLE MRModel196Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=196;
CREATE TABLE MRModel225Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=225;
CREATE TABLE MRModel256Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=256;
CREATE TABLE MRModel289Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=289;
CREATE TABLE MRModel324Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=324;
CREATE TABLE MRModel361Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=361;
CREATE TABLE MRModel400Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=400;
CREATE TABLE MRModel441Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=441;
CREATE TABLE MRModel484Tbl AS SELECT * FROM MRModelTbl WHERE ELLG_TARG=484;

-- make indices for each table as to speed up lookups by many orders of magnitude
CREATE INDEX MRTarget_id_MRModel64Tbl_idx ON MRModel64Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel64Tbl_idx ON MRModel64Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel64Tbl_idx ON MRModel64Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel81Tbl_idx ON MRModel81Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel81Tbl_idx ON MRModel81Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel81Tbl_idx ON MRModel81Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel100Tbl_idx ON MRModel100Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel100Tbl_idx ON MRModel100Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel100Tbl_idx ON MRModel100Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel121Tbl_idx ON MRModel121Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel121Tbl_idx ON MRModel121Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel121Tbl_idx ON MRModel121Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel144Tbl_idx ON MRModel144Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel144Tbl_idx ON MRModel144Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel144Tbl_idx ON MRModel144Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel169Tbl_idx ON MRModel169Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel169Tbl_idx ON MRModel169Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel169Tbl_idx ON MRModel169Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel196Tbl_idx ON MRModel196Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel196Tbl_idx ON MRModel196Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel196Tbl_idx ON MRModel196Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel225Tbl_idx ON MRModel225Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel225Tbl_idx ON MRModel225Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel225Tbl_idx ON MRModel225Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel256Tbl_idx ON MRModel256Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel256Tbl_idx ON MRModel256Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel256Tbl_idx ON MRModel256Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel289Tbl_idx ON MRModel289Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel289Tbl_idx ON MRModel289Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel289Tbl_idx ON MRModel289Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel324Tbl_idx ON MRModel324Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel324Tbl_idx ON MRModel324Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel324Tbl_idx ON MRModel324Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel361Tbl_idx ON MRModel361Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel361Tbl_idx ON MRModel361Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel361Tbl_idx ON MRModel361Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel400Tbl_idx ON MRModel400Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel400Tbl_idx ON MRModel400Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel400Tbl_idx ON MRModel400Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel441Tbl_idx ON MRModel441Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel441Tbl_idx ON MRModel441Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel441Tbl_idx ON MRModel441Tbl(Fullmodel);

CREATE INDEX MRTarget_id_MRModel484Tbl_idx ON MRModel484Tbl(MRTarget_id);
CREATE INDEX TargetPDBid_MRModel484Tbl_idx ON MRModel484Tbl(TargetPDBid);
CREATE INDEX Fullmodel_MRModel484Tbl_idx ON MRModel484Tbl(Fullmodel);


 
-- make table for models where ELLG_TARG>100 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_81 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel81Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) < 
( SELECT g.UsedReflections FROM MRModel100Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))
AND (( SELECT g.UsedReflections FROM MRModel100Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>121 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_100 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel100Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) < 
( SELECT g.UsedReflections FROM MRModel121Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))
AND (( SELECT g.UsedReflections FROM MRModel121Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>144 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_121 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel121Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) < 
( SELECT g.UsedReflections FROM MRModel144Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))
AND (( SELECT g.UsedReflections FROM MRModel144Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>169 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_144 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel144Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) < 
( SELECT g.UsedReflections FROM MRModel169Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))
AND (( SELECT g.UsedReflections FROM MRModel169Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>196 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_169 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel169Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel196Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))
AND (( SELECT g.UsedReflections FROM MRModel196Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>225 exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_196 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG 
AND (( SELECT g.UsedReflections FROM MRModel196Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id) <
( SELECT g.UsedReflections FROM MRModel225Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel225Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>256 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_225 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel225Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel256Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel256Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>289 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_256 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel256Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel289Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel289Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>324 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_289 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel289Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel324Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel324Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;
-- make table for models where ELLG_TARG>361 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_324 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel324Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel361Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel361Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;

-- make table for models where ELLG_TARG>400 may have exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_361 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel361Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel400Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel400Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;

-- make table for models where ELLG_TARG>441 has exhausted the number of reflections available
CREATE TABLE IncrUsedReflecTbl_400 AS SELECT f.ROWID AS MRModel_id, f.CPUtime, f.TargetPDBid, f.Fullmodel, f.ELLG_TARG, f.MRTarget_id, f.NewRMSD, f.UsedReflections, f.LLG, f.ELLG0, a.vrms0 AS VRMS,
 f.MRLogFile, f.MRResults, f.MRModelInput, f.MTZinput, f.Sequenceinput
FROM MRModelTbl f, AnnotationTbl a 
WHERE f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.ROWID AND a.LLG=f.LLG  
AND (( SELECT g.UsedReflections FROM MRModel400Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) <
( SELECT g.UsedReflections FROM MRModel441Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND (( SELECT g.UsedReflections FROM MRModel441Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ) =
( SELECT g.UsedReflections FROM MRModel484Tbl g WHERE g.Fullmodel=f.Fullmodel AND g.MRTarget_id=f.MRTarget_id ))  
AND f.RMSLoweringHighRes IS NULL
;





CREATE TABLE UnbiasedTimesTbl_81 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_81 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_100 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_100 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_100 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_121 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_121 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_121 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_144 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_144 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_144 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_169 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_169 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_169 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_196 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_196 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_196 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


CREATE TABLE UnbiasedTimesTbl_225 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_225 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_225 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;

  
CREATE TABLE UnbiasedTimesTbl_256 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_256 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_256 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;

CREATE TABLE UnbiasedTimesTbl_289 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_289 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_289 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;

  
CREATE TABLE UnbiasedTimesTbl_324 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_324 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_324 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;

  
CREATE TABLE UnbiasedTimesTbl_361 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_361 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_361 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;

  
CREATE TABLE UnbiasedTimesTbl_400 AS SELECT e.eLLG_target,
  (SELECT SUM(f.CPUtime) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS SumCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS stdevCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgSolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevSolvedCPUtime,
  (SELECT AVG(f.CPUtime) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS AvgUnsolvedCPUtime,
  (SELECT STDEV(f.CPUtime) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS stdevUnsolvedCPUtime,
  (SELECT AVG(f.UsedReflections) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgUsedReflections,
  (SELECT AVG(f.ELLG0) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgELLG,
  (SELECT AVG(f.NewRMSD) FROM IncrUsedReflecTbl_81 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgRMSD,
  (SELECT AVG(f.VRMS) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS AvgVRMS,
  (SELECT MEDIAN(f.VRMS) FROM IncrUsedReflecTbl_400 f WHERE f.ELLG_TARG=e.eLLG_target) AS MedianVRMS,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 >= 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountSolvedCPUtime,
  (SELECT COUNT(*) FROM IncrUsedReflecTbl_400 f, AnnotationTbl a WHERE f.ELLG_TARG=e.eLLG_target AND a.LLG=f.LLG AND a.CCmap0 < 0.2 AND f.TargetPDBid=a.TargetPDBid AND a.MRModel_id=f.MRModel_id) AS CountUnsolvedCPUtime
  FROM ELLGtarget_iter e;


  
CREATE TABLE UnbiasedTbln AS SELECT e.eLLG_target, v81.AvgSolvedCPUtime AS AvgSolvedCPUtime81, v100.AvgSolvedCPUtime AS AvgSolvedCPUtime100, 
  v121.AvgSolvedCPUtime AS AvgSolvedCPUtime121, 
  v144.AvgSolvedCPUtime AS AvgSolvedCPUtime144, v169.AvgSolvedCPUtime AS AvgSolvedCPUtime169, v196.AvgSolvedCPUtime AS AvgSolvedCPUtime196, 
  v225.AvgSolvedCPUtime AS AvgSolvedCPUtime225, 
  v256.AvgSolvedCPUtime AS AvgSolvedCPUtime256, v289.AvgSolvedCPUtime AS AvgSolvedCPUtime289, v324.AvgSolvedCPUtime AS AvgSolvedCPUtime324, 
  v361.AvgSolvedCPUtime AS AvgSolvedCPUtime361, v400.AvgSolvedCPUtime AS AvgSolvedCPUtime400,
  
  v81.AvgVRMS AS AvgVRMS81, v100.AvgVRMS AS AvgVRMS100, v121.AvgVRMS AS AvgVRMS121, v144.AvgVRMS AS AvgVRMS144, 
  v169.AvgVRMS AS AvgVRMS169, v196.AvgVRMS AS AvgVRMS196, v225.AvgVRMS AS AvgVRMS225, 
  v256.AvgVRMS AS AvgVRMS256, v289.AvgVRMS AS AvgVRMS289, v324.AvgVRMS AS AvgVRMS324, 
  v361.AvgVRMS AS AvgVRMS361, v400.AvgVRMS AS AvgVRMS400,
  
  
  v81.AvgUsedReflections AS AvgUsedRefl81, v100.AvgUsedReflections AS AvgUsedRefl100, v121.AvgUsedReflections AS AvgUsedRefl121, v144.AvgUsedReflections AS AvgUsedRefl144, 
  v169.AvgUsedReflections AS AvgUsedRefl169, v196.AvgUsedReflections AS AvgUsedRefl196, v225.AvgUsedReflections AS AvgUsedRefl225, 
  v256.AvgUsedReflections AS AvgUsedRefl256, v289.AvgUsedReflections AS AvgUsedRefl289, v324.AvgUsedReflections AS AvgUsedRefl324, v361.AvgUsedReflections AS AvgUsedRefl361, 
  v400.AvgUsedReflections AS AvgUsedRefl400
    
FROM ELLGtarget_iter e, UnbiasedTimesTbl_81 v81, UnbiasedTimesTbl_100 v100, UnbiasedTimesTbl_121 v121, UnbiasedTimesTbl_144 v144, 
 UnbiasedTimesTbl_169 v169, UnbiasedTimesTbl_196 v196, UnbiasedTimesTbl_225 v225, UnbiasedTimesTbl_256 v256,
 UnbiasedTimesTbl_289 v289, UnbiasedTimesTbl_324 v324, UnbiasedTimesTbl_361 v361 , UnbiasedTimesTbl_400 v400 
WHERE e.eLLG_target=v81.eLLG_target 
AND e.eLLG_target=v100.eLLG_target 
AND e.eLLG_target=v121.eLLG_target 
AND e.eLLG_target=v144.eLLG_target 
AND e.eLLG_target=v169.eLLG_target 
AND e.eLLG_target=v196.eLLG_target 
AND e.eLLG_target=v225.eLLG_target
AND e.eLLG_target=v256.eLLG_target
AND e.eLLG_target=v289.eLLG_target
AND e.eLLG_target=v324.eLLG_target
AND e.eLLG_target=v361.eLLG_target
AND e.eLLG_target=v400.eLLG_target
;





