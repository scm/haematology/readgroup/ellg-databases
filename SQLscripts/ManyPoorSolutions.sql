SELECT TargetPDBid, Fullmodel, CCglobal, TFZ0, LLG0, Numberofsolutions, CPUtime FROM AllProteins 
WHERE Numberofsolutions > 10
AND CPUtime < 600
AND CCglobal > 0.2


attach "1CompBulksol_svn6966.db" as bulkdb;


SELECT m.TargetPDBid, m.Fullmodel, m.PAK0, m.CCglobal, b.CCglobal AS bulkCC, b.TFZ0,
m.TFZ0, b.LLG0, m.Numberofsolutions, m.CPUtime
FROM AllProteins m, bulkdb.AllProteins b
WHERE b.TargetPDBid=m.TargetPDBid
AND b.Fullmodel=m.Fullmodel
AND m.Numberofsolutions > 10
AND b.Numberofsolutions > 10
AND m.CCglobal > 0.2
AND  m.PAK0 < 3
;

SELECT m.TargetPDBid, m.Fullmodel, m.CCglobal, b.CCglobal AS bulkCC, b.TFZ0,
m.TFZ0, b.LLG0, b.Numberofsolutions, m.Numberofsolutions, b.CPUtime, m.CPUtime
FROM AllProteins m, bulkdb.AllProteins b
WHERE b.TargetPDBid=m.TargetPDBid
AND b.Fullmodel=m.Fullmodel
AND m.Numberofsolutions > 10
AND m.CCglobal > 0.2
AND b.CCglobal > 0.1
;

detach bulkdb;
