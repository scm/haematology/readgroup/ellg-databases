
-- check if some MR targets are duplicate and delete those with the fewest models.
SELECT s.p_id, 
       s.TargetPDBid, 
       (SELECT COUNT(*) FROM MRModelTbl u WHERE u.MRTarget_id=s.p_id) as nmodels, 
       (SELECT COUNT(*) FROM AnnotationTbl v WHERE v.MRModel_id=(
                                                                  SELECT u.p_id 
                                                                  FROM MRModelTbl u 
                                                                  WHERE u.MRTarget_id=s.p_id)
                                                                 ) as nannotations 
FROM MRTargetTbl s 
WHERE (
       SELECT COUNT(*) 
       FROM MRTargetTbl t 
       WHERE t.TargetPDBid=s.TargetPDBid
      ) > 1
ORDER BY s.TargetPDBid
;



-- DELETE FROM ELLGtbl WHERE MRModel_id IN (SELECT p_id FROM MRModelTbl WHERE MRTarget_id IN (175, 28, 61, 285, 299, 345, 368, 459, 143));
-- DELETE FROM TemplateRMSTbl WHERE MRModel_id IN (SELECT p_id FROM MRModelTbl WHERE MRTarget_id IN (175, 28, 61, 285, 299, 345, 368, 459, 143));
-- DELETE FROM AnnotationTbl WHERE MRModel_id IN (SELECT p_id FROM MRModelTbl WHERE MRTarget_id IN (175, 28, 61, 285, 299, 345, 368, 459, 143));
-- DELETE FROM MRModelTbl WHERE MRTarget_id IN (175, 28, 61, 285, 299, 345, 368, 459, 143);
-- DELETE FROM MRTargetTbl WHERE p_id IN (175, 28, 61, 285, 299, 345, 368, 459, 143);



SELECT t.TargetPDBid, t.Fullmodel, s.CCglobal, t.CCglobal, s.LLGvrms, t.LLGvrms 
FROM AllProteins t, AllProteins s 
WHERE t.TargetPDBid=s.TargetPDBid 
AND t.Fullmodel = s.Fullmodel 
AND t.ModelOrder = 2 
AND t.CCglobal > 0.2 
AND s.ModelOrder=1 
AND S.CCglobal > 0.2 
AND t.CCglobal != "" 
ORDER BY RANDOM() LIMIT 20;


