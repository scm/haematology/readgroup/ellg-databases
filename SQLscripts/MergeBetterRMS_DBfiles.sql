-- merging databases to BetterRMSresults.db which is empty aside from table layouts
-- BACKUP ORIGINAL DB FILES BEFORE MERGING AS THEIR P_ID WILL BE ALTERED PERMANTENTLY

attach "BetterRMSresults1.db" as tomerge;
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

-- Have to update our p_id and MRTarget_id so that crossreferencing MRTargetTbl from MRModel_id and MRModel_id from AnnotationTbl or TemplateRMSTbl remains valid
-- To avoid a "PRIMARY KEY must be unique" error when updating the p_id we use the trick of updating them by assigning them to their negative values and then changing sign again
attach "BetterRMSresults2.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults3.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults4.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults5.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults6.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults7.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "BetterRMSresults8.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

ALTER TABLE MRModelTbl ADD RFZ0NewRMS real;
ALTER TABLE MRModelTbl ADD TFZ0NewRMS real;
ALTER TABLE MRModelTbl ADD PAK0NewRMS real;
ALTER TABLE MRModelTbl ADD iLLG0NewRMS real;
ALTER TABLE MRModelTbl ADD FounditNewRMS int;


