
attach "FsolBsol0.4_20.db" as K04_B20_db;
attach "FsolBsol0.6_30.db" as K06_B30_db;

BEGIN TRANSACTION;

CREATE TABLE BulkSolventTbl AS 

SELECT t.TargetPDBid, m.Fullmodel, t.Spacegroup, s.CCmap0, s.LLgvrms AS LLGvrms, s.Ksol, s.Bsol 
FROM K04_B20_db.MRModelTbl m, K04_B20_db.MRTargetTbl t, K04_B20_db.TemplateRMSTbl s WHERE m.MRTarget_id = t.p_id AND s.MRModel_id = m.p_id
UNION ALL

SELECT t.TargetPDBid, m.Fullmodel, t.Spacegroup, s.CCmap0, 
   s.LLgvrms AS LLGvrms, s.Ksol, s.Bsol
FROM K06_B30_db.MRModelTbl m, K06_B30_db.MRTargetTbl t, K06_B30_db.TemplateRMSTbl s 
WHERE m.MRTarget_id = t.p_id 
AND s.MRModel_id = m.p_id
;


COMMIT;

detach database K04_B20_db;
detach database K06_B30_db;

