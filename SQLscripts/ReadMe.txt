To create database:

On an 8 core machine the gathering of MR results from the HomologySearch folder is conveniently 
split up into 8 cmd files and takes just over 1 hour.

Run all the cmd files GetBLASTMRresult*.cmd from 8 command prompts. These create 8 databases named
Completeresults1.db - Completeresults8.db. 

To merge the 8 databases first create the layout for the database structure by running the file
MakeDBLayout.cmd from a command prompt. Rename empty.db to Completeresults.db. Make 
backup copies of the 8 database files as the merger will alter them. From a command prompt
start SQLite with the commandline "sqlite3 completeresults.db". Merge the databases by copying and 
pasting all the content of MergeDBfiles.sql into the SQLite prompt.

Now bogus results need to be deleted. Delete these by copying and pasting the content of 
DeleteBogusResults.sql into the SQLite prompt.

To make the database more convenient to use some database views and tables need to be set up. 
Do this by copying and pasting the content of PostProcessDatabase.sql into the SQLite prompt.

To allow queries to contain statements with common maths functions such as srt, log, exp compile the
file SQLiteExt.c and follow the instructions in the file header comment of that file.

The database is now ready to be queried. Examples are stored in SQLstatements.txt

