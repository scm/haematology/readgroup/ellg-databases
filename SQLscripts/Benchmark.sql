

SELECT t.TargetPDBid, m.FullModel, m.CPUtime, a.TFZ, a.LLGvrms, a.CCglobal, a.CCmap0, a.CCmap1, a.MLAD0, a.MLAD1, a.Bfac0, a.Bfac1 
FROM MRTargetTbl t, MRModelTbl m, AnnotationTbl a 
WHERE m.MRTarget_id = t.p_id 
AND a.MRModel_id = m.p_id
AND a.LLGvrms = (SELECT MAX(IFNULL(mb.LLGvrms, -42)) 
                 FROM AnnotationTbl mb 
                 WHERE mb.MRModel_id = m.p_id)



SELECT t.TargetPDBid, m.FullModel, m.CPUtime, a.TFZ, a.LLGvrms, a.CCglobal, a.CCmap0, a.CCmap1, a.MLAD0, a.MLAD1, a.Bfac0, a.Bfac1
FROM MRTargetTbl t, MRModelTbl m 
LEFT OUTER JOIN AnnotationTbl a ON a.MRModel_id = m.p_id AND m.MRTarget_id = t.p_id 
AND a.LLGvrms = (SELECT MAX(IFNULL(mb.LLGvrms, -42)) 
                 FROM AnnotationTbl mb  
                 WHERE mb.MRModel_id = m.p_id);
                 