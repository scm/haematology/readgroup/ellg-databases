-- merging databases to 2CompOccMR_svn6966.db which is empty aside from table layouts
-- BACKUP ORIGINAL DB FILES BEFORE MERGING AS THEIR P_ID WILL BE ALTERED PERMANTENTLY

attach "2CompOccMR_svn6966_1.db" as tomerge;
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

-- Have to update our p_id and MRTarget_id so that crossreferencing MRTargetTbl from MRModel_id and MRModel_id from AnnotationTbl or TemplateRMSTbl remains valid
-- To avoid a "PRIMARY KEY must be unique" error when updating the p_id we use the trick of updating them by assigning them to their negative values and then changing sign again
attach "2CompOccMR_svn6966_2.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_3.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_4.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_5.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_6.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_7.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_8.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_9.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_10.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_11.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_12.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_13.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_14.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_15.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_16.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_17.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_18.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_19.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;


attach "2CompOccMR_svn6966_20.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;

attach "2CompOccMR_svn6966_5b.db" as tomerge;
UPDATE toMerge.MRTargetTbl SET p_id = -(SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRTargetTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET p_id = -(SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1) - p_id;
UPDATE toMerge.MRModelTbl SET p_id = -p_id;
UPDATE toMerge.MRModelTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.AnnotationTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.TemplateRMSTbl SET MRTarget_id = MRTarget_id + (SELECT p_id FROM MRTargetTbl ORDER BY ROWID DESC LIMIT 1);
UPDATE toMerge.ELLGtbl SET MRModel_id = MRModel_id + (SELECT p_id FROM MRModelTbl ORDER BY ROWID DESC LIMIT 1);
BEGIN TRANSACTION;
insert into MRTargetTbl select * from toMerge.MRTargetTbl;
insert into MRModelTbl select * from toMerge.MRModelTbl;
insert into AnnotationTbl select * from toMerge.AnnotationTbl;
insert into ELLGtbl select * from toMerge.ELLGtbl;
insert into TemplateRMSTbl select * from toMerge.TemplateRMSTbl;
COMMIT;
detach database toMerge;










