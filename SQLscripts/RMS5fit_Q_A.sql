-- define borderline cases to have LLG of template solutions between 20 and 90 and CCglobal > 0.2
CREATE VIEW BorderlineCases AS SELECT * FROM AllProteins WHERE (CCglobalTemplate > 0.2 AND iLLGtemplate > 20) AND iLLGtemplate < 90;

CREATE VIEW CommonBLcases AS SELECT * FROM AllProteins WHERE (CCglobalTemplate > 0.2 AND iLLGtemplate > 20) AND iLLGtemplate < 90 AND CorrectSolutionc_l != '' AND CorrectSolutionRMS5 != '' AND CorrectSolutionRMS5p1sig != '' AND CorrectSolutionRMS5m1sig != '' AND CorrectSolutionRMS5p05sig != '' AND CorrectSolutionRMS5m05sig != '' 

SELECT COUNT(*) FROM BorderlineCases
COUNT(*)
4198

SELECT COUNT(*) FROM CommonBLcases



-- get average TFZ for borderline cases for each of the different rms estimates
SELECT COUNT(*), AVG(TFZ0RMSc_l), AVG(TFZ0RMS5p1sig), AVG(TFZ0RMS5p05sig), AVG(TFZ0RMS5), AVG(TFZ0RMS5m05sig), AVG(TFZ0RMS5m1sig) FROM CommonBLcases 
COUNT(*)  AVG(TFZ0RMSc_l)  AVG(TFZ0RMS5p1sig)  AVG(TFZ0RMS5p05sig)  AVG(TFZ0RMS5)  AVG(TFZ0RMS5m05sig)  AVG(TFZ0RMS5m1sig)
3380  6.27535502958581  6.33695266272189  6.42723505032563  6.4812721893491  6.47177514792899  6.37360946745561
3375  6.27757037037038  6.33937777777777  6.42930370370369  6.48216296296296  6.47297777777778  6.3743111111111


-- get average LLG for borderline cases for each of the different rms estimates
SELECT COUNT(*), AVG(iLLGRMSc_l), AVG(iLLGRMS5p1sig), AVG(iLLGRMS5p05sig), AVG(iLLGRMS5), AVG(iLLGRMS5m05sig), AVG(iLLGRMS5m1sig) FROM CommonBLcases
COUNT(*)  AVG(iLLGRMSc_l)  AVG(iLLGRMS5p1sig)  AVG(iLLGRMS5p05sig)  AVG(iLLGRMS5)  AVG(iLLGRMS5m05sig)  AVG(iLLGRMS5m1sig)
3380      37.36               41.28               42.78               40.10           28.61                1.54
3375  37.3659259259259  41.3102222222222  42.8352592592593  40.1662222222222  28.6838518518519  1.62874074074074


-- get number of correct solutions for each of the different rms estimates
SELECT SUM(CorrectSolutionc_l), SUM(CorrectSolutionRMS5p1sig), SUM(CorrectSolutionRMS5p05sig), SUM(CorrectSolutionRMS5), SUM(CorrectSolutionRMS5m05sig), SUM(CorrectSolutionRMS5m1sig) FROM CommonBLcases
SUM(CorrectSolutionc_l)  SUM(CorrectSolutionRMS5p1sig)  SUM(CorrectSolutionRMS5p05sig)  SUM(CorrectSolutionRMS5)  SUM(CorrectSolutionRMS5m05sig)  SUM(CorrectSolutionRMS5m1sig)
2800                        2841                              2909                            2872                          2858                           2821
2798                        2840                              2863                            2871                          2857                           2820


-- How many cases solve with RMS5p1sig and not RMS5p0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p1sig > 0 AND CorrectSolutionRMS5p05sig < 1
6
57

-- How many cases solve with RMS5p1sig and not RMS5
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p1sig > 0 AND CorrectSolutionRMS5 < 1
92

-- How many cases solve with RMS5p1sig and not RMS5m0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p1sig > 0 AND CorrectSolutionRMS5m05sig < 1
122
122

-- How many cases solve with RMS5p1sig and not RMS5m1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p1sig > 0 AND CorrectSolutionRMS5m1sig < 1
171
171

-- How many cases solve with RMS5p1sig and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p1sig > 0 AND CorrectSolutionc_l < 1
105
105




-- How many cases solve with RMS5p0.5sig and not RMS5p1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p05sig > 0 AND CorrectSolutionRMS5p1sig < 1
74
80

-- How many cases solve with RMS5p0.5sig and not RMS5
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p05sig > 0 AND CorrectSolutionRMS5 < 1
95
66

-- How many cases solve with RMS5p0.5sig and not RMS5m0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p05sig > 0 AND CorrectSolutionRMS5m05sig < 1
133
101

-- How many cases solve with RMS5p0.5sig and not RMS5m1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p05sig > 0 AND CorrectSolutionRMS5m1sig < 1
182
154

-- How many cases solve with RMS5p0.5sig and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5p05sig > 0 AND CorrectSolutionc_l < 1
153
146



-- How many cases solve with RMS5 and not RMS5p1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5 > 0 AND CorrectSolutionRMS5p1sig < 1
123
123

-- How many cases solve with RMS5 and not RMS5p0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5 > 0 AND CorrectSolutionRMS5p05sig < 1
58
74

-- How many cases solve with RMS5 and not RMS5m0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5 > 0 AND CorrectSolutionRMS5m05sig < 1
78
78

-- How many cases solve with RMS5 and not RMS5m1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5 > 0 AND CorrectSolutionRMS5m1sig < 1
136
136

-- How many cases solve with RMS5 and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5 > 0 AND CorrectSolutionc_l < 1
155
155



-- How many cases solve with RMS5m0.5sig and not RMS5p1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m05sig > 0 AND CorrectSolutionRMS5p1sig < 1
139
139

-- How many cases solve with RMS5m0.5sig and not RMS5p0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m05sig > 0 AND CorrectSolutionRMS5p05sig < 1
82
95

-- How many cases solve with RMS5m0.5sig and not RMS5
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m05sig > 0 AND CorrectSolutionRMS5 < 1
64
64

-- How many cases solve with RMS5m0.5sig and not RMS5m1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m05sig > 0 AND CorrectSolutionRMS5m1sig < 1
82
82

-- How many cases solve with RMS5m0.5sig and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m05sig > 0 AND CorrectSolutionc_l < 1
192
192




-- How many cases solve with RMS5m1sig and not RMS5p1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m1sig > 0 AND CorrectSolutionRMS5p1sig < 1
151
151

-- How many cases solve with RMS5m1sig and not RMS5p0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m1sig > 0 AND CorrectSolutionRMS5p05sig < 1
94
111

-- How many cases solve with RMS5m1sig and not RMS5
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m1sig > 0 AND CorrectSolutionRMS5 < 1
85
85

-- How many cases solve with RMS5m1sig and not RMS5m0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m1sig > 0 AND CorrectSolutionRMS5m05sig < 1
45
45

-- How many cases solve with RMS5m1sig and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionRMS5m1sig > 0 AND CorrectSolutionc_l < 1
204
204



-- How many cases solve with Chothia&Lesk and not RMS5p1sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5p1sig < 1
64
63

-- How many cases solve with Chothia&Lesk and not RMS5p0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5p05sig < 1
44
81

-- How many cases solve with Chothia&Lesk and not RMS5
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5 < 1
83
82

-- How many cases solve with Chothia&Lesk and not RMS5m0.5sig
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5m05sig < 1
134
133

-- How many cases solve with Chothia&Lesk and not Chothia&Lesk
SELECT COUNT(*) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5m1sig < 1
183
182














-- Union of solved RMS5 RMS5p1sig RMS5m1sig RMS5p05sig RMS5m05sig calculations from borderline cases:
SELECT COUNT(*) FROM CommonBLcases WHERE (CorrectSolutionRMS5 > 0 OR CorrectSolutionRMS5p1sig > 0 OR CorrectSolutionRMS5m1sig > 0 OR CorrectSolutionRMS5p05sig > 0 OR CorrectSolutionRMS5m05sig > 0)
COUNT(*)
3036
3037

-- How many cases solves with either of the 3 estimates but not with Chothia & Lesk
SELECT COUNT(*), AVG(TFZ0RMS5p1sig), AVG(TFZ0RMS5p05sig), AVG(TFZ0RMS5), AVG(TFZ0RMS5m05sig), AVG(TFZ0RMS5m1sig) FROM CommonBLcases 
WHERE CorrectSolutionc_l < 1 AND (CorrectSolutionRMS5 > 0 OR CorrectSolutionRMS5p1sig > 0 OR CorrectSolutionRMS5m1sig > 0 OR CorrectSolutionRMS5p05sig > 0 OR CorrectSolutionRMS5m05sig > 0)
COUNT(*)  AVG(TFZ0RMS5p1sig)  AVG(TFZ0RMS5p05sig)  AVG(TFZ0RMS5)  AVG(TFZ0RMS5m05sig)  AVG(TFZ0RMS5m1sig)
257  5.20155642023347  5.5408560311284  5.71206225680934  5.86186770428016  5.88249027237354
259  5.20772200772201  5.53861003861003  5.70849420849421  5.85405405405406  5.87413127413127

-- How many cases solve with Chothia & Lesk and not with RMS5, RMS5p1sig, RMS5m1sig, RMS5p05sig, RMS5m05sig
SELECT COUNT(*), AVG(NewRMSD), AVG(C_L_rms), AVG(VRMS1) FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5 < 1 AND CorrectSolutionRMS5p1sig < 1 AND CorrectSolutionRMS5m1sig < 1 AND CorrectSolutionRMS5p05sig < 1 AND CorrectSolutionRMS5m05sig < 1;
COUNT(*)  AVG(NewRMSD)  AVG(C_L_rms)  AVG(VRMS1)
21
20  1.14716745061358  1.53316  1.5294521


-- How many cases solves with either of the 3 estimates as top solution but not with Chothia & Lesk
SELECT COUNT(*) FROM CommonBLcases 
WHERE CorrectSolutionc_l < 1 AND ((CorrectSolutionRMS5 > 0 AND SolutionrankRMS5 = 1) OR (CorrectSolutionRMS5p1sig > 0 AND SolutionrankRMS5p1sig = 1) OR (CorrectSolutionRMS5m1sig > 0 AND SolutionrankRMS5m1sig = 1) OR (CorrectSolutionRMS5p05sig > 0 AND SolutionrankRMS5p05sig = 1) OR (CorrectSolutionRMS5m05sig > 0 AND SolutionrankRMS5m05sig = 1))
COUNT(*)
195
195

-- How many cases solves with either of the 3 estimates as top solution and with the highest TFZ but not with Chothia & Lesk
SELECT COUNT(*) FROM CommonBLcases 
WHERE CorrectSolutionc_l < 1 AND ((CorrectSolutionRMS5 > 0 AND SolutionrankRMS5 = 1 AND TFZ0RMS5 > TFZ0RMS5p1sig AND TFZ0RMS5 > TFZ0RMS5p05sig AND TFZ0RMS5 > TFZ0RMS5m05sig AND TFZ0RMS5 > TFZ0RMS5m1sig) 
                               OR (CorrectSolutionRMS5p1sig > 0 AND SolutionrankRMS5p1sig = 1 AND TFZ0RMS5p1sig > TFZ0RMS5 AND TFZ0RMS5p1sig > TFZ0RMS5p05sig AND TFZ0RMS5p1sig > TFZ0RMS5m05sig AND TFZ0RMS5p1sig > TFZ0RMS5m1sig) 
                               OR (CorrectSolutionRMS5m1sig > 0 AND SolutionrankRMS5m1sig = 1 AND TFZ0RMS5m1sig > TFZ0RMS5 AND TFZ0RMS5m1sig > TFZ0RMS5p05sig AND TFZ0RMS5m1sig > TFZ0RMS5m05sig AND TFZ0RMS5m1sig > TFZ0RMS5p1sig) 
                               OR (CorrectSolutionRMS5p05sig > 0 AND SolutionrankRMS5p05sig = 1 AND TFZ0RMS5p05sig > TFZ0RMS5 AND TFZ0RMS5p05sig > TFZ0RMS5p1sig AND TFZ0RMS5p05sig > TFZ0RMS5m05sig AND TFZ0RMS5p05sig > TFZ0RMS5m1sig) 
                               OR (CorrectSolutionRMS5m05sig > 0 AND SolutionrankRMS5m05sig = 1 AND TFZ0RMS5m05sig > TFZ0RMS5 AND TFZ0RMS5m05sig > TFZ0RMS5p05sig AND TFZ0RMS5m05sig > TFZ0RMS5p1sig AND TFZ0RMS5m05sig > TFZ0RMS5m1sig))
COUNT(*)
120
120



SELECT COUNT(*) FROM CommonBLcases WHERE SolutionrankRMS5 < SolutionrankRMSc_l
COUNT(*)
37
37


SELECT COUNT(*) FROM CommonBLcases WHERE SolutionrankRMS5 > SolutionrankRMSc_l
COUNT(*)
49
49


-- from 21 database


351C  1B7V_A  1.3    1.4161  0.993   MLAD_RMSp2sig=2.524159  c_l solves as not topmost, LLG=20 solution by chance
1UBQ  2ZEQ_A  1.402  1.4379  1.013   MLADc_l=1.089786  MLAD_RMSp2sig=1.091124    partly solved with p2sigma 
1ZNZ  2J41_A  1.538  1.33    1.049   MLADc_l=0.913483  MLAD_RMSp2sig=2.960059   c_l solves one domain clearly, VRMS=1.53 but evrmsp2sigma=1.46 

-- Manually edit database to be solved with NewRMS5p1sig for 1U2K  1STQ_A 
UPDATE MRModelTbl SET CorrectSolutionRMS5p1sig = 1 WHERE MRTarget_id = (SELECT m.p_id FROM DistinctMRTargets m WHERE m.TargetPDBid="1U2K") AND Partialmodel="1STQ_A" AND Modelorder = 1;


SELECT TargetPDBid, Fullmodel, CorrectSolutionRMS5p15sig, CorrectSolutionRMS5p2sig, SCOPid FROM CommonBLcases WHERE CorrectSolutionc_l > 0 AND CorrectSolutionRMS5 < 1 AND CorrectSolutionRMS5p1sig < 1 AND CorrectSolutionRMS5m1sig < 1 AND CorrectSolutionRMS5p05sig < 1 AND CorrectSolutionRMS5m05sig < 1
TargetPDBid  Fullmodel  CorrectSolutionRMS5p15sig  CorrectSolutionRMS5p2sig  SCOPid
1BK7  3D3Z_A  1  1  d.124.1.1
1I71  2HPQ_P  1  1  g.14.1.1
2HNX  2F73_A  1  1  b.60.1.2
1LR8  2NU0_I  0  1  g.68.1.1 | g.3.11.3
2UCZ  2GMI_B  1  1  d.20.1.1
351C  1B7V_A  0  0  a.3.1.1
1AK2  1TEV_A  1  1  c.37.1.1 | g.41.2.1
1KL9  2AHO_B  1  0  a.60.14.1 | b.40.4.5
1DUC  2ZDC_A  0  1  b.85.4.1
2CA8  1PX6_A  1  1  a.45.1.1 | c.47.1.5
1B1B  2H09_A  1  1  a.76.1.1 | a.4.5.24
2BRG  1H01_A  0  1  d.144.1.7
1HH1  1OB8_A  1  1  c.52.1.18
3BYR  2QFI_A  0  1  d.52.9.1
2CXH  1W94_A  0  1  c.51.1.2
2I5D  1V7R_A  1  1  c.51.4.1
1IPA  2HA8_A  1  1  c.116.1.1 | d.79.3.3
2HBO  2CY9_A  1  1  d.38.1.5
1UBQ  2ZEQ_A  0  0  d.15.1.1
1ZNZ  2J41_A  0  0  c.37.1.1

