CREATE TEMP VIEW SCOPClassResiduesizeHisto AS

SELECT v.bin AS bin, u.scopclass AS tclass, (CAST(u.Residues AS REAL)/IFNULL(v.Residues, MAX(u.Residues,1))) AS ratio 
FROM
(
-- the histogram for all proteins sorted by SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues, IFNULL(cbin.tclass, "") AS scopclass
	FROM Residuesiter allbins  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter.i as bin, m.Targetclass AS tclass, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter.i + 50) 
			AND m.NumberofResiduesinTarget > (Residuesiter.i-50) 
		GROUP BY m.TargetClass, Residuesiter.i
	) 
	cbin ON cbin.bin = allbins.i GROUP BY cbin.tclass, cbin.bin
) u,
(
-- the histogram for all proteins regardless of SCOPclass
	SELECT IFNULL(cbin.bin, allbins.i) as bin, IFNULL(cbin.pcount, 0) AS Residues
	FROM Residuesiter allbins  
-- use an outer join to pad missing rows from the SELECT below with default values from the SELECT above
	LEFT OUTER JOIN 
	(
		SELECT Residuesiter.i as bin, COUNT(m.NumberofResiduesinTarget) AS pcount 
		FROM AllProteins m, Residuesiter 
		WHERE m.NumberofResiduesinTarget <= (Residuesiter.i + 50) 
			AND m.NumberofResiduesinTarget > (Residuesiter.i-50) 
		GROUP BY Residuesiter.i
	) 
	cbin ON cbin.bin = allbins.i
) v
WHERE u.bin = v.bin;

SELECT a.bin AS residuebin, a.ratio AS alpharatio, b.ratio AS betaratio, apb.ratio AS alpha_p_betaratio, asb.ratio AS alpha_s_betaratio, SUM(o.ratio) AS othersratio
FROM SCOPClassResiduesizeHisto a, SCOPClassResiduesizeHisto b, SCOPClassResiduesizeHisto apb, SCOPClassResiduesizeHisto asb, SCOPClassResiduesizeHisto o
WHERE a.tclass = "All alpha proteins"
	AND b.tclass = "All beta proteins"
	AND apb.tclass = "Alpha and beta proteins (a+b)"
	AND asb.tclass = "Alpha and beta proteins (a/b)"
	AND o.tclass != "All alpha proteins"
	AND o.tclass !=  "All beta proteins"
	AND o.tclass !=  "Alpha and beta proteins (a+b)"
	AND o.tclass !=  "Alpha and beta proteins (a/b)" 
	AND a.bin = b.bin
	AND a.bin = apb.bin
	AND a.bin = asb.bin
	AND a.bin = o.bin
-- use group to ensure we only sum o.ratio for each residuebin	
GROUP BY residuebin;
