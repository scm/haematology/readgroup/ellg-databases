-- get names of tables from database
SELECT name FROM sqlite_master;
-- get info about a specific table
PRAGMA table_info(newMRresults);

--Get column names on the first line of output
.header on 
.separator "  "

-- make a selection of 100 random MR calculations
CREATE TABLE Random100MRCalcs(TargetPDBid, Fullmodel, CorrectSolution, TFZ0, LLGvrms, CPUtime);
INSERT INTO Random100MRCalcs (TargetPDBid, Fullmodel, CorrectSolution, TFZ0, LLGvrms, CPUtime) SELECT TargetPDBid, FullModel, Correctsolution, TFZ0, LLGvrms, cputime FROM AllProteins ORDER BY RANDOM() LIMIT 100;



SELECT m.ROWID, m.p_id, m.TargetPDBid, m.Fullmodel,  m.Comment FROM UniqueMRresultsTbl m WHERE m.Comment != ""

-- get outlier results where phaser didn't find a solution, with TFZ > 7.0, with TFZ <> 100, sort by TFZ
SELECT p_id, TFZ0, LLGtemplate, iLLGfullres, SequenceIdentity, ModelCompleteness, SCOPid, TargetPDBid, Fullmodel, Spacegroup, Spacegroupnumber FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 0 AND t.TFZ0 > 7.0 AND t.TFZ0 <> 100 ORDER BY TFZ0;

-- get outlier results where phaser didn't find a solution, TFZ > 7.0, resolution  > 2.5
SELECT TargetPDBid, Fullmodel, TFZ0, LLGFullres, LLGtemplate, Resolution, Modelcompleteness, Spacegroup, SCOPid FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 0 AND t.TFZ0 > 7 AND t.Comment = "" AND Resolution > 2.5 ORDER BY TFZ0;


-- Select those outliers we consider as being solved by phaser, i.e. match rows with comments starting with "solved "
SELECT m.TargetPDBid, m.Fullmodel, m.Comment FROM UniqueMRresultsTbl m WHERE SUBSTR(m.Comment, 1 , 7) = "solved ";


-- get count of those targetfolds that only belong to one fold type
SELECT m.TargetFold, COUNT(*) FROM UniqueMRresultsTbl m, TargetFolds t WHERE t.TargetFold = m.TargetFold GROUP BY m.TargetFold;

-- get count of targetPDBs belonging to unique folds, for each fold
SELECT m.TargetFold, COUNT(DISTINCT m.TargetPDBid) FROM UniqueMRresultsTbl m, TargetFolds t WHERE t.TargetFold = m.TargetFold GROUP BY m.TargetFold;

-- get count of MR calculations for each targetPDBs belonging to unique folds, ordered by fold
SELECT m.TargetFold, m.TargetPDBid, COUNT(*) FROM UniqueMRresultsTbl m, TargetFolds t WHERE t.TargetFold = m.TargetFold GROUP BY m.TargetPDBid ORDER BY m.TargetFold;



-- get the pdbs of those targets that are uniquely Lysozyme-like and with MR solutions
SELECT DISTINCT m.TargetPDBid, m.WilsonB, m.Spacegroup FROM ProteinsSolved m WHERE m.TargetFold = "Lysozyme-like";

-- get those Target PDBs that have the word "hemoglobin" somewhere in their family name.
SELECT DISTINCT m.TargetPDBid, m.SCOPid, m.TargetFamily, m.Spacegroup FROM UniqueMRresultsTbl m WHERE m.Targetfamily LIKE "%hemoglobin%";

-- get those beta proteins that have more than 20 models for their MR calculations
SELECT SCOPid, TargetPDBid, (SELECT COUNT(m.Fullmodel) FROM UniqueMRresultsTbl m WHERE m.TargetPDBid = t.TargetPDBid) AS modelcount FROM UniqueMRresultsTbl t WHERE t.TargetClass = "All beta proteins" AND modelcount > 20 GROUP BY TargetPDBid;


SELECT * FROM AnnotationTbl  m WHERE m.MRtable_id IN ( SELECT p_id FROM UniqueMRresultsTbl t WHERE t.TargetPDBid = '1MNZ' );
SELECT * FROM AnnotationTbl  m WHERE m.MRtable_id = 7589 ORDER BY (-TFZ);
SELECT * FROM AnnotationTbl  m WHERE m.MRtable_id = ( SELECT p_id FROM UniqueMRresultsTbl t 
WHERE t.TargetPDBid = "1RNL" AND t.PartialModel = "1MVO_A") ORDER BY (-TFZ);


-- get results with 3 solutions one mathcing a template
-- first create an index to speed it up
CREATE INDEX idx_MRtable_id ON AnnotationTbl(MRtable_id);
SELECT m.TargetPDBid, m.p_id FROM UniqueMRresultsTbl m where (SELECT COUNT(*) FROM AnnotationTbl  u WHERE  u.MRtable_id = m.p_id) =3 AND m.CorrectSolution = 1;

-- get results where phaser didn't find a solution, with TFZ > 7.0, with TFZ <> 100, sort by TFZ
SELECT p_id, TFZ0, LLGtemplate, iLLGfullres, SequenceIdentity, ModelCompleteness, SCOPid, TargetPDBid, Fullmodel, Spacegroup, Spacegroupnumber FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 0 AND t.TFZ0 > 7.0 AND t.TFZ0 <> 100 ORDER BY TFZ0;


-- get difference between max TFZ and TFZ(solution) when phaser proposes multiple solutions
SELECT MRtable_id, (MAX(TFZ) - 
(SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 )
 ) FROM AnnotationTbl  m WHERE  m.MRtable_id IN (SELECT p_id FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 1) GROUP BY MRtable_id


-- get difference between max TFZ and TFZ(solution) when phaser proposes multiple solutions for monoclinic crystals
SELECT MRModel_id, (
  MAX(TFZ) - (SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRModel_id = m.MRModel_id AND u.Tstar = 1 )
) FROM AnnotationTbl  m WHERE  m.MRModel_id IN (
   SELECT p_id FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 1 AND t.Crystalsystem = "Monoclinic" 
) GROUP BY MRModel_id



# sort table first according to MRtable_id. Then according to -TFZ
SELECT * FROM AnnotationTbl WHERE MRtable_id IN ( 7592, 2800, 3718, 2459, 2352 )  ORDER  BY  MRtable_id, -TFZ


-- For Monoclinic crystals, TFZ(solution) = Max(TFZ), 7<TFZ<8  get either Max(TFZ)-TFZ(solution) or when Max(TFZ)=TFZ(solution) get TFZ-TFZ(next highest) 
SELECT p_id, MRtable_id, TFZ, 
CASE WHEN (SELECT COUNT(*) FROM AnnotationTbl f WHERE f.MRtable_id = m.MRtable_id)=1 THEN 
     0
 WHEN MAX(TFZ) <> ( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) THEN
   (( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) - MAX(TFZ)) 
  ELSE 
   ( MAX(TFZ) - (SELECT MAX( TFZ )  FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND
  TFZ < ( SELECT MAX( TFZ )  FROM AnnotationTbl   u WHERE  u.MRtable_id = m.MRtable_id) )  ) 
END
 FROM AnnotationTbl  m WHERE  m.MRtable_id IN 
( SELECT p_id FROM UniqueMRresultsTbl t WHERE t.Crystalsystem = "Monoclinic" AND t.CorrectSolution = 1  AND t.TFZ0 < 8 AND t.TFZ0 > 7) 
GROUP BY MRtable_id


-- For Monoclinic crystals, TFZ(solution) = Max(TFZ), more than one solution,  get either Max(TFZ)-TFZ(solution) or when Max(TFZ)=TFZ(solution) get TFZ-TFZ(next highest) 
CREATE TEMP VIEW MonoclinicTFZContrast AS 
SELECT p_id, MRtable_id, TFZ, 
CASE WHEN (SELECT COUNT(*) FROM AnnotationTbl f WHERE f.MRtable_id = m.MRtable_id)=1 THEN 
     0
 WHEN MAX(TFZ) <> ( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) THEN
   (( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) - MAX(TFZ)) 
  ELSE 
   ( MAX(TFZ) - (SELECT MAX( TFZ )  FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND
  TFZ < ( SELECT MAX( TFZ )  FROM AnnotationTbl   u WHERE  u.MRtable_id = m.MRtable_id) )  ) 
END
AS TFZcontrast FROM AnnotationTbl  m WHERE  m.MRtable_id IN 
( SELECT p_id FROM UniqueMRresultsTbl t WHERE t.Crystalsystem = "Monoclinic" AND t.CorrectSolution = 1 )
GROUP BY MRtable_id
;
SELECT * FROM MonoclinicTFZContrast WHERE TFZcontrast <> 0


-- For all but Monoclinic crystals, TFZ(solution) = Max(TFZ), more than one solution,  get either Max(TFZ)-TFZ(solution)
-- or when Max(TFZ)=TFZ(solution) get TFZ-TFZ(next highest) 
CREATE TEMP VIEW OtherTFZContrast AS 
SELECT p_id, MRtable_id, TFZ, 
CASE WHEN (SELECT COUNT(*) FROM AnnotationTbl f WHERE f.MRtable_id = m.MRtable_id)=1 THEN 
     0
 WHEN MAX(TFZ) <> ( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) THEN
   (( SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1 ) - MAX(TFZ)) 
  ELSE 
   ( MAX(TFZ) - (SELECT MAX( TFZ )  FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND
  TFZ < ( SELECT MAX( TFZ )  FROM AnnotationTbl   u WHERE  u.MRtable_id = m.MRtable_id) )  ) 
END
AS TFZcontrast FROM AnnotationTbl  m WHERE  m.MRtable_id IN 
( SELECT p_id FROM UniqueMRresultsTbl t WHERE t.Crystalsystem <> "Monoclinic" AND t.CorrectSolution = 1 )
GROUP BY MRtable_id
;
SELECT * FROM OtherTFZContrast WHERE TFZcontrast <> 0




-- Display max(TFZ), TFZ for the solution and number of solutions
CREATE TEMP VIEW MRSolutions AS 
SELECT MRtable_id, 
   MAX(TFZ) AS maxTFZ, 
  (SELECT TFZ FROM AnnotationTbl  u WHERE  u.MRtable_id = m.MRtable_id AND u.Tstar = 1) AS TFZsolution,
   COUNT(*) AS NumberOfSolutions, 
  (SELECT TARGETPDBid  FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS TrgtPDBid,
  (SELECT PartialModel FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS modelPDBid,
  (SELECT SequenceIdentity FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS seqid,
  (SELECT ModelCompleteness FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS mdlcompletness,
  (SELECT SCOPid FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS SCOP,
  (SELECT Spacegroup FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS spgrp,
  (SELECT Crystalsystem FROM UniqueMRresultsTbl m WHERE m.p_id = MRtable_id) AS crystsys
FROM AnnotationTbl  m WHERE  m.MRtable_id
IN ( SELECT p_id FROM UniqueMRresultsTbl t WHERE t.CorrectSolution = 1 )
-- the above has to be done on MRtable_id subsets so group according to that
GROUP BY MRtable_id
;

-- Get those results with more than one solution, one of which is correct.
SELECT * FROM MRSolutions WHERE NumberOfSolutions > 1;

-- Get those results with more than 3 solution, one of which is correct, model completeness >0.5 and maxTFZ < 6 
SELECT * FROM MRSolutions WHERE mdlcompletness >0.5 AND NumberOfSolutions  > 3 AND maxTFZ < 6 ORDER BY NumberOfSolutions ;

-- Get those results with TFZsolution less than TFZmax
SELECT * FROM MRSolutions WHERE maxTFZ > TFZsolution;

-- Get a specific entry for one MR calculation
SELECT * FROM MRSolutions WHERE TrgtPDBid = "2F1B" AND modelPDBid = "1O7D_D";



-- Count number of cases with only one SCOP id designated on the target 
SELECT  * FROM (SELECT COUNT(*), m.ROWID, m.p_id, m.TargetPDBid, m.TargetClass FROM UniqueMRresultsTbl m  where m.NumberofUniqueSCOPids =1 GROUP BY m.TargetClass);

-- or
SELECT COUNT( DISTINCT TargetPDBid) FROM UniqueMRresultsTbl  where NumberofUniqueSCOPids =1;

-- Count number of cases with only one SCOP id designated on target grouped according to foldnames
SELECT COUNT(*) FROM (SELECT m.ROWID, m.TargetPDBid, m.TargetClass FROM UniqueMRresultsTbl m  where m.NumberofUniqueSCOPids =1 GROUP BY m.TargetFold);

-- for each SCOP class and for targets with only one SCOP id list number of folds, targets and calculations
SELECT TargetClass, COUNT(DISTINCT TargetFold) AS Folds, COUNT(DISTINCT TargetPDBid) AS Targets, COUNT(FullModel) AS Calculations FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids =1 GROUP BY TargetClass;

-- get the sum of the above numbers
SELECT SUM(Folds), SUM(Targets), SUM(Calculations) FROM (SELECT TargetClass, COUNT(DISTINCT TargetFold) AS Folds, COUNT(DISTINCT TargetPDBid) AS Targets, COUNT(FullModel) AS Calculations FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids =1 GROUP BY TargetClass);

-- get LLG, TFZ, etc. for the "Small proteins" SCOP class that could be solved by Phaser
SELECT m.ROWID, m.p_id, m.TargetPDBid, m.TargetClass, m.Fullmodel, m.SequenceIdentity, m.ModelCompleteness, m.Fracvar, m.LLGreflection0, m.TFZ0  FROM UniqueMRresultsTbl m WHERE  m.TargetClass = "Small proteins" AND m.CorrectSolution=1;



-- get a view of the SCOP class termed "Small proteins" that phaser could solve
CREATE TEMP VIEW SmallProteinsSolved AS SELECT m.ROWID, m.p_id, m.TargetPDBid, m.TargetClass, m.Fullmodel, m.SequenceIdentity, m.ModelCompleteness, m.Fracvar, m.LLGreflection0, m.TFZ0  FROM UniqueMRresultsTbl m WHERE  m.TargetClass = "Small proteins" AND m.CorrectSolution=1;

-- display histogram bins for MR solved "Small proteins" only
SELECT TFZbigiter.i as TFZbin, COUNT(SmallProteinsSolved.TFZ0) AS count FROM SmallProteinsSolved,TFZbigiter WHERE SmallProteinsSolved.TFZ0 <= (TFZbigiter.i + 1.25) AND SmallProteinsSolved.TFZ0 > (TFZbigiter.i - 1.25) GROUP BY TFZbigiter.i;

-- show the solved MR TFZ histograms for all of the SCOP classes
SELECT TFZbigiter.i as TFZbin, COUNT(ProteinsSolved.TFZ0) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, TFZbigiter WHERE ProteinsSolved.TFZ0 <= (TFZbigiter.i + 1.25) AND ProteinsSolved.TFZ0 > (TFZbigiter.i - 1.25) GROUP BY ProteinsSolved.TargetClass, TFZbigiter.i;

-- show the unsolved MR TFZ histograms for all of the classes
SELECT TFZiter.i as TFZbin, COUNT(ProteinsUnSolved.TFZ0) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved,TFZiter WHERE ProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND ProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) GROUP BY ProteinsUnSolved.TargetClass, TFZiter.i;

CREATE TEMP VIEW TFZUnsolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(ProteinsUnSolved.TFZ0) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, TFZiter WHERE ProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND ProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND ProteinsUnSolved.Spacegroup != "P 1"  GROUP BY ProteinsUnSolved.TargetClass, TFZiter.i;

CREATE TEMP VIEW TFZSolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(ProteinsSolved.TFZ0) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, TFZiter WHERE ProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND ProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND ProteinsSolved.Spacegroup != "P 1"  GROUP BY ProteinsSolved.TargetClass, TFZiter.i;

-- now get the TFZ histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio FROM TFZUnsolvedHisto u, TFZSolvedHisto s WHERE u.TFZbin = s.TFZbin AND u.TargetClass = s.TargetClass;


-- 
-- and the same but grouped by crystal system regardless of SCOP classes
CREATE TEMP VIEW TFZUnsolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AllProteinsUnSolved.Crystalsystem AS Crystalsystem FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1"  GROUP BY AllProteinsUnSolved.Crystalsystem, TFZiter.i;

CREATE TEMP VIEW TFZSolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AllProteinsSolved.Crystalsystem AS Crystalsystem FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1"  GROUP BY AllProteinsSolved.Crystalsystem, TFZiter.i;

-- now get the TFZ histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.Crystalsystem, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio FROM TFZUnsolvedHisto u, TFZSolvedHisto s WHERE u.TFZbin = s.TFZbin AND u.Crystalsystem = s.Crystalsystem;

--
-- and do this for all proteins regardless of SCOP classes
SELECT u.count AS nunsolved, s.count AS nsolved, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS ratio FROM 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) u, 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) s 

WHERE u.TFZbin = s.TFZbin;



-- and do this for all proteins with floating origins
SELECT u.count AS nunsolved, s.count AS nsolved, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS ratio FROM 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.IsPolarSpacegroup = 1 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) u, 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.IsPolarSpacegroup = 1 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) s 

WHERE u.TFZbin = s.TFZbin;


-- and do this for all proteins without floating origins
SELECT u.count AS nunsolved, s.count AS nsolved, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS ratio FROM 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.IsPolarSpacegroup = 0 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) u, 

(SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.IsPolarSpacegroup = 0 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i) s 

WHERE u.TFZbin = s.TFZbin;



-- 
--
-- and the same but with floating origins, grouped by crystal system
CREATE TEMP VIEW TFZUnsolvedPolarHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AllProteinsUnSolved.Crystalsystem AS Crystalsystem FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.IsPolarSpacegroup = 1 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1"  GROUP BY AllProteinsUnSolved.Crystalsystem, TFZiter.i;

CREATE TEMP VIEW TFZSolvedPolarHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AllProteinsSolved.Crystalsystem AS Crystalsystem FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.IsPolarSpacegroup = 1 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1"  GROUP BY AllProteinsSolved.Crystalsystem, TFZiter.i;

-- now get the TFZ histogram ratios for each Crystalsystem. Also display the sum of solved and unsolved bin values as total count
SELECT u.Crystalsystem, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio FROM TFZUnsolvedPolarHisto u, TFZSolvedPolarHisto s WHERE u.TFZbin = s.TFZbin AND u.Crystalsystem = s.Crystalsystem;


-- and the same but without floating origins, grouped by crystal system
CREATE TEMP VIEW TFZUnsolvedNonpolarHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AllProteinsUnSolved.Crystalsystem AS Crystalsystem FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.IsPolarSpacegroup = 0 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1"  GROUP BY AllProteinsUnSolved.Crystalsystem, TFZiter.i;

CREATE TEMP VIEW TFZSolvedNonpolarHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AllProteinsSolved.Crystalsystem AS Crystalsystem FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.IsPolarSpacegroup = 0 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1"  GROUP BY AllProteinsSolved.Crystalsystem, TFZiter.i;

-- now get the TFZ histogram ratios for each Crystalsystem. Also display the sum of solved and unsolved bin values as total count
SELECT u.Crystalsystem, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS TFZratio FROM TFZUnsolvedNonpolarHisto u, TFZSolvedNonpolarHisto s WHERE u.TFZbin = s.TFZbin AND u.Crystalsystem = s.Crystalsystem;


-- and do this for all proteins regardless of SCOP classes residue size 0-200
CREATE TEMP VIEW AllTFZUnsolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <=200 AND AllProteinsUnSolved.NumberofResiduesinTarget > 0 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

CREATE TEMP VIEW AllTFZSolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.NumberofResiduesinTarget <=200 AND AllProteinsSolved.NumberofResiduesinTarget > 0 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

SELECT u.count AS nunsolved, s.count AS nsolved, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS ratio FROM AllTFZUnsolvedHisto u, AllTFZSolvedHisto s WHERE u.TFZbin = s.TFZbin;


-- and do this for all proteins regardless of SCOP classes residue size > 200
CREATE TEMP VIEW AllTFZUnsolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget > 200 AND AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

CREATE TEMP VIEW AllTFZSolvedHisto AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.NumberofResiduesinTarget > 200 AND AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

SELECT u.count AS nunsolved, s.count AS nsolved, (u.count + s.count) AS totalcount, u.TFZbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS ratio FROM AllTFZUnsolvedHisto u, AllTFZSolvedHisto s WHERE u.TFZbin = s.TFZbin;








-- show the solved MR TFZ histogram regardless of SCOP classes bin width 2.5
SELECT TFZbigiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZbigiter WHERE AllProteinsSolved.TFZ0 <= (TFZbigiter.i + 1.25) AND AllProteinsSolved.TFZ0 > (TFZbigiter.i - 1.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZbigiter.i;

SELECT TFZbigiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZbigiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZbigiter.i + 1.25) AND AllProteinsUnSolved.TFZ0 > (TFZbigiter.i - 1.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZbigiter.i;

-- show the solved MR TFZ histogram regardless of SCOP classes bin width 1
SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter1 WHERE AllProteinsSolved.TFZ0 <= (TFZiter1.i + 0.5) AND AllProteinsSolved.TFZ0 > (TFZiter1.i - 0.5) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter1.i;

SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter1 WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter1.i + 0.5) AND AllProteinsUnSolved.TFZ0 > (TFZiter1.i - 0.5) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter1.i;

-- show the solved MR TFZ histogram regardless of SCOP classes bin width 0.5
SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i - 0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter.i + 0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i - 0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;

-- show the solved MR TFZ histogram regardless of SCOP classes bin width 0.2
SELECT TFZsmalliter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count FROM AllProteinsSolved, TFZsmalliter WHERE AllProteinsSolved.TFZ0 <= (TFZsmalliter.i + 0.1) AND AllProteinsSolved.TFZ0 > (TFZsmalliter.i - 0.1) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZsmalliter.i;

SELECT TFZsmalliter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count FROM AllProteinsUnSolved, TFZsmalliter WHERE AllProteinsUnSolved.TFZ0 <= (TFZsmalliter.i + 0.1) AND AllProteinsUnSolved.TFZ0 > (TFZsmalliter.i - 0.1) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZsmalliter.i;







-- show the unsolved MR Fracvar histograms for all of the classes
SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsUnSolved.Fracvar) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, FracVariter WHERE ProteinsUnSolved.Fracvar <= (FracVariter.i + 0.005) AND ProteinsUnSolved.Fracvar > (FracVariter.i - 0.005) GROUP BY ProteinsUnSolved.TargetClass, FracVariter.i;

-- show the solved MR Fracvar histograms for all of the classes
SELECT FracVarbigiter.i as Fracvarbin, COUNT(ProteinsSolved.Fracvar) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVarbigiter WHERE ProteinsSolved.Fracvar <= FracVarbigiter.i AND ProteinsSolved.Fracvar > (FracVarbigiter.i-0.025) GROUP BY ProteinsSolved.TargetClass, FracVarbigiter.i;

-- show the solved MR Fracvar histograms for all of the classes but with the same bin size as the unsolved
SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsSolved.Fracvar) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVariter WHERE ProteinsSolved.Fracvar <= (FracVariter.i + 0.005) AND ProteinsSolved.Fracvar > (FracVariter.i - 0.005) GROUP BY ProteinsSolved.TargetClass, FracVariter.i;



-- for each histogram bin get the ratio between solved and unsolved fracvar, for each SCOP class
-- first create views of solved and unsolved histograms, Bin size: 0.01
CREATE TEMP VIEW FracvarUnsolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsUnSolved.Fracvar) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, FracVariter WHERE ProteinsUnSolved.Fracvar <= (FracVariter.i + 0.005) AND ProteinsUnSolved.Fracvar > (FracVariter.i - 0.005) GROUP BY ProteinsUnSolved.TargetClass, FracVariter.i;

CREATE TEMP VIEW FracvarSolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsSolved.Fracvar) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVariter WHERE ProteinsSolved.Fracvar <= (FracVariter.i + 0.005) AND ProteinsSolved.Fracvar > (FracVariter.i - 0.005) GROUP BY ProteinsSolved.TargetClass, FracVariter.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarratio FROM FracvarUnsolvedHisto001 u, FracvarSolvedHisto001 s WHERE u.Fracvarbin = s.Fracvarbin AND u.TargetClass = s.TargetClass;

-- Do the same for the FracvarVRMS, Bin size: 0.01
CREATE TEMP VIEW FracvarVRMSUnsolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsUnSolved.FracvarVRMS) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, FracVariter WHERE ProteinsUnSolved.FracvarVRMS <= (FracVariter.i + 0.005) AND ProteinsUnSolved.FracvarVRMS > (FracVariter.i - 0.005) GROUP BY ProteinsUnSolved.TargetClass, FracVariter.i;

CREATE TEMP VIEW FracvarVRMSSolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(ProteinsSolved.FracvarVRMS) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVariter WHERE ProteinsSolved.FracvarVRMS <= (FracVariter.i + 0.005) AND ProteinsSolved.FracvarVRMS > (FracVariter.i - 0.005) GROUP BY ProteinsSolved.TargetClass, FracVariter.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarVRMSratio FROM FracvarVRMSUnsolvedHisto001 u, FracvarVRMSSolvedHisto001 s WHERE u.Fracvarbin = s.Fracvarbin AND u.TargetClass = s.TargetClass;

-- And for the whole lot of proteins even if they belong to more than one SCOP class, Bin size: 0.01
CREATE TEMP VIEW FracvarVRMSAllUnsolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(AllProteinsUnSolved.FracvarVRMS) AS count FROM AllProteinsUnSolved, FracVariter WHERE AllProteinsUnSolved.FracvarVRMS <= (FracVariter.i + 0.005) AND AllProteinsUnSolved.FracvarVRMS > (FracVariter.i - 0.005) GROUP BY FracVariter.i;

CREATE TEMP VIEW FracvarVRMSAllSolvedHisto001 AS SELECT FracVariter.i as Fracvarbin, COUNT(AllProteinsSolved.FracvarVRMS) AS count FROM AllProteinsSolved, FracVariter WHERE AllProteinsSolved.FracvarVRMS <= (FracVariter.i + 0.005) AND AllProteinsSolved.FracvarVRMS > (FracVariter.i - 0.005) GROUP BY FracVariter.i;

-- now get the histogram ratios and display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarVRMSratio FROM FracvarVRMSAllUnsolvedHisto001 u, FracvarVRMSAllSolvedHisto001 s WHERE u.Fracvarbin = s.Fracvarbin;




-- Do the same for the Fracvar but with Bin size: 0.025
CREATE TEMP VIEW FracvarUnsolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(ProteinsUnSolved.Fracvar) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, FracVarbigiter WHERE ProteinsUnSolved.Fracvar <= (FracVarbigiter.i + 0.0125) AND ProteinsUnSolved.Fracvar > (FracVarbigiter.i-0.0125) GROUP BY ProteinsUnSolved.TargetClass, FracVarbigiter.i;

CREATE TEMP VIEW FracvarSolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(ProteinsSolved.Fracvar) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVarbigiter WHERE ProteinsSolved.Fracvar <= (FracVarbigiter.i + 0.0125) AND ProteinsSolved.Fracvar > (FracVarbigiter.i-0.0125) GROUP BY ProteinsSolved.TargetClass, FracVarbigiter.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS Fracvarratio FROM FracvarUnsolvedHisto0025 u, FracvarSolvedHisto0025 s WHERE u.Fracvarbin = s.Fracvarbin AND u.TargetClass = s.TargetClass;


-- And for the whole lot of proteins even if they belong to more than one SCOP class but with Bin size: 0.025
CREATE TEMP VIEW FracvarAllUnsolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsUnSolved.Fracvar) AS count FROM AllProteinsUnSolved, FracVarbigiter WHERE AllProteinsUnSolved.Fracvar <= (FracVarbigiter.i + 0.0125) AND AllProteinsUnSolved.Fracvar > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

CREATE TEMP VIEW FracvarAllSolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsSolved.Fracvar) AS count FROM AllProteinsSolved, FracVarbigiter WHERE AllProteinsSolved.Fracvar <= (FracVarbigiter.i + 0.0125) AND AllProteinsSolved.Fracvar > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

-- now get the histogram ratios and display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS Fracvarratio FROM FracvarAllUnsolvedHisto0025 u, FracvarAllSolvedHisto0025 s WHERE u.Fracvarbin = s.Fracvarbin;






-- Do the same for the FracvarVRMS but with Bin size: 0.025
CREATE TEMP VIEW FracvarVRMSUnsolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(ProteinsUnSolved.FracvarVRMS) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, FracVarbigiter WHERE ProteinsUnSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND ProteinsUnSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY ProteinsUnSolved.TargetClass, FracVarbigiter.i;

CREATE TEMP VIEW FracvarVRMSSolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(ProteinsSolved.FracvarVRMS) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, FracVarbigiter WHERE ProteinsSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND ProteinsSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY ProteinsSolved.TargetClass, FracVarbigiter.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarVRMSratio FROM FracvarVRMSUnsolvedHisto0025 u, FracvarVRMSSolvedHisto0025 s WHERE u.Fracvarbin = s.Fracvarbin AND u.TargetClass = s.TargetClass;



-- And for the whole lot of proteins even if they belong to more than one SCOP class but with Bin size: 0.025
CREATE TEMP VIEW FracvarVRMSAllUnsolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsUnSolved.FracvarVRMS) AS count FROM AllProteinsUnSolved, FracVarbigiter WHERE AllProteinsUnSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND AllProteinsUnSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

CREATE TEMP VIEW FracvarVRMSAllSolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsSolved.FracvarVRMS) AS count FROM AllProteinsSolved, FracVarbigiter WHERE AllProteinsSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND AllProteinsSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarVRMSratio FROM FracvarVRMSAllUnsolvedHisto0025 u, FracvarVRMSAllSolvedHisto0025 s WHERE u.Fracvarbin = s.Fracvarbin;



-- And for proteins with target number of residues greater 200 but smaller than 400 Bin size: 0.025
CREATE TEMP VIEW FracvarVRMSAllUnsolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsUnSolved.FracvarVRMS) AS count FROM AllProteinsUnSolved, FracVarbigiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <=800 AND AllProteinsUnSolved.NumberofResiduesinTarget > 600 AND AllProteinsUnSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND AllProteinsUnSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

CREATE TEMP VIEW FracvarVRMSAllSolvedHisto0025 AS SELECT FracVarbigiter.i as Fracvarbin, COUNT(AllProteinsSolved.FracvarVRMS) AS count FROM AllProteinsSolved, FracVarbigiter WHERE AllProteinsSolved.NumberofResiduesinTarget <=800 AND AllProteinsSolved.NumberofResiduesinTarget > 600 AND AllProteinsSolved.FracvarVRMS <= (FracVarbigiter.i + 0.0125) AND AllProteinsSolved.FracvarVRMS > (FracVarbigiter.i-0.0125) GROUP BY FracVarbigiter.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.Fracvarbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS fracvarVRMSratio FROM FracvarVRMSAllUnsolvedHisto0025 u, FracvarVRMSAllSolvedHisto0025 s WHERE u.Fracvarbin = s.Fracvarbin;



-- create ratio histogram for the LLGrefl_vrms with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedHisto0025 AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedHisto0025 AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.LLGbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedHisto0025 u, LLGReflVrmsAllSolvedHisto0025 s WHERE u.LLGbin = s.LLGbin;


-- create ratio histogram for the LLGrefl_vrms, seqID <=25 with Bin size=0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedSeqidHisto AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.SequenceIdentity <=25 AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedSeqidHisto AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.SequenceIdentity <=25 AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.LLGbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedSeqidHisto u, LLGReflVrmsAllSolvedSeqidHisto s WHERE u.LLGbin = s.LLGbin;


-- create ratio histogram for the LLGrefl_vrms, for each Crystalsystem, with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedHistoXtalsys AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count, AllProteinsUnSolved.Crystalsystem AS Crystalsystem FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY AllProteinsUnSolved.Crystalsystem, LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedHistoXtalsys AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count, AllProteinsSolved.Crystalsystem AS Crystalsystem FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY AllProteinsSolved.Crystalsystem, LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.Crystalsystem, (u.count + s.count) AS totalcount, u.LLGbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedHistoXtalsys u, LLGReflVrmsAllSolvedHistoXtalsys s WHERE u.LLGbin = s.LLGbin AND u.Crystalsystem = s.Crystalsystem;




-- create ratio histogram for the LLGrefl_vrms CCglobal>0.1 with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedCCHisto0025 AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteins.LLGrefl_vrms) AS count FROM AllProteins, LLG_vrmsiter3 WHERE AllProteins.CCglobal < 0.35 AND AllProteins.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteins.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedCCHisto0025 AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteins.LLGrefl_vrms) AS count FROM AllProteins, LLG_vrmsiter3 WHERE AllProteins.CCglobal >= 0.35 AND AllProteins.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteins.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, u.LLGbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedCCHisto0025 u, LLGReflVrmsAllSolvedCCHisto0025 s WHERE u.LLGbin = s.LLGbin;



-- create ratio histogram for the LLGrefl_vrms NumberofResiduesinTarget=100 with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, 
COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <=400 AND AllProteinsUnSolved.NumberofResiduesinTarget > 200 AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.NumberofResiduesinTarget <=400 AND AllProteinsSolved.NumberofResiduesinTarget > 200 AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedResSizeHisto u, LLGReflVrmsAllSolvedResSizeHisto s WHERE u.LLGbin = s.LLGbin;







-- create ratio histogram for the LLGvrms with Bin size 5, NumberofResiduesinTarget with bin size 200
SELECT u.residbin, u.Bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM 

(SELECT Residuesiter1.i as residbin, LLGiter3.i as Bin, COUNT(AllProteinsUnSolved.LLGvrms) AS count FROM AllProteinsUnSolved, LLGiter3, Residuesiter1 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsUnSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsUnSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY Residuesiter1.i, LLGiter3.i) u, 

(SELECT Residuesiter1.i as residbin, LLGiter3.i as Bin, COUNT(AllProteinsSolved.LLGvrms) AS count FROM AllProteinsSolved, LLGiter3, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY Residuesiter1.i, LLGiter3.i) s 

WHERE u.Bin = s.Bin AND u.residbin = s.residbin;



-- create ratio histogram for the LLGvrms with Bin size 5, grouped by Crystalsystem
SELECT u.Crystalsystem, u.Bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM 

(SELECT AllProteinsUnSolved.Crystalsystem AS Crystalsystem, LLGiter3.i as Bin, COUNT(AllProteinsUnSolved.LLGvrms) AS count FROM AllProteinsUnSolved, LLGiter3 WHERE AllProteinsUnSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsUnSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY Crystalsystem, LLGiter3.i) u, 

(SELECT AllProteinsSolved.Crystalsystem AS Crystalsystem, LLGiter3.i as Bin, COUNT(AllProteinsSolved.LLGvrms) AS count FROM AllProteinsSolved, LLGiter3 WHERE AllProteinsSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY Crystalsystem, LLGiter3.i) s 

WHERE u.Bin = s.Bin AND u.Crystalsystem = s.Crystalsystem;



-- create ratio histogram for the LLGvrms with Bin size 5, grouped by IsPolarSpacegroup
SELECT u.IsPolar, u.Bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM 

(SELECT AllProteinsUnSolved.IsPolarSpacegroup AS IsPolar, LLGiter3.i as Bin, COUNT(AllProteinsUnSolved.LLGvrms) AS count FROM AllProteinsUnSolved, LLGiter3 WHERE AllProteinsUnSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsUnSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY IsPolar, LLGiter3.i) u, 

(SELECT AllProteinsSolved.IsPolarSpacegroup AS IsPolar, LLGiter3.i as Bin, COUNT(AllProteinsSolved.LLGvrms) AS count FROM AllProteinsSolved, LLGiter3 WHERE AllProteinsSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY IsPolar, LLGiter3.i) s 

WHERE u.Bin = s.Bin AND u.IsPolar = s.IsPolar;


-- create ratio histogram for the LLGvrms with Bin size 5, grouped by IsPolarSpacegroup, Crystalsystems
SELECT u.IsPolar, u.Crystalsystem, u.Bin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM 

(SELECT AllProteinsUnSolved.IsPolarSpacegroup AS IsPolar, AllProteinsUnSolved.Crystalsystem AS Crystalsystem, LLGiter3.i as Bin, COUNT(AllProteinsUnSolved.LLGvrms) AS count FROM AllProteinsUnSolved, LLGiter3 WHERE AllProteinsUnSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsUnSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY IsPolar, Crystalsystem, LLGiter3.i) u, 

(SELECT AllProteinsSolved.IsPolarSpacegroup AS IsPolar, AllProteinsSolved.Crystalsystem AS Crystalsystem, LLGiter3.i as Bin, COUNT(AllProteinsSolved.LLGvrms) AS count FROM AllProteinsSolved, LLGiter3 WHERE AllProteinsSolved.LLGvrms <= (LLGiter3.i + 2.5) AND AllProteinsSolved.LLGvrms > (LLGiter3.i-2.5) GROUP BY IsPolar, Crystalsystem, LLGiter3.i) s 

WHERE u.Bin = s.Bin AND u.IsPolar = s.IsPolar AND u.Crystalsystem = s.Crystalsystem;








-- create ratio histogram for the LLGrefl_vrms NumberofResiduesinTarget=100 with Bin size 0.0025
-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count

SELECT u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM 

(SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <=400 AND AllProteinsUnSolved.NumberofResiduesinTarget > 200 AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i) u, 

(SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.NumberofResiduesinTarget <=400 AND AllProteinsSolved.NumberofResiduesinTarget > 200 AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i) s 

WHERE u.LLGbin = s.LLGbin;


-- Get the solved ratio at LLGrefl_vrms=0.005 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter (width=100) variables 
SELECT u.residbin, u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms005Solved FROM 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, Residuesiter.i) u, 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, Residuesiter.i) s

WHERE u.LLGbin = 0.005 AND s.LLGbin = 0.005 AND u.residbin = s.residbin;


-- Get the solved ratio at LLGrefl_vrms=0.0075 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter (width=100) variables 
SELECT u.residbin, u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms0075Solved FROM 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, Residuesiter.i) u, 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, Residuesiter.i) s

WHERE u.LLGbin = 0.0075 AND s.LLGbin = 0.0075 AND u.residbin = s.residbin;



-- Get the solved ratio at LLGrefl_vrms=0.005 as a function of ASUvolume
-- Do this by creating 2D histogram in LLG_vrmsiter3 and ASUVoliter (width=25000) variables 
SELECT u.ASUvolbin, u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms005Solved FROM 

(SELECT ASUVoliter.i as ASUvolbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, ASUVoliter WHERE AllProteinsUnSolved.ASUvolume <= (ASUVoliter.i + 12500) AND AllProteinsUnSolved.ASUvolume > (ASUVoliter.i - 12500) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, ASUVoliter.i) u, 

(SELECT ASUVoliter.i as ASUvolbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, ASUVoliter WHERE AllProteinsSolved.ASUvolume <= (ASUVoliter.i + 12500) AND AllProteinsSolved.ASUvolume > (ASUVoliter.i - 12500) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, ASUVoliter.i) s

WHERE u.LLGbin = 0.005 AND s.LLGbin = 0.005 AND u.ASUvolbin = s.ASUvolbin;



-- Get the solved ratio at LLGrefl_vrms=0.0075 as a function of ASUvolume
-- Do this by creating 2D histogram in LLG_vrmsiter3 and ASUVoliter variables 
SELECT u.ASUvolbin, u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms0075Solved FROM 

(SELECT ASUVoliter.i as ASUvolbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, ASUVoliter WHERE AllProteinsUnSolved.ASUvolume <= (ASUVoliter.i + 12500) AND AllProteinsUnSolved.ASUvolume > (ASUVoliter.i - 12500) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, ASUVoliter.i) u, 

(SELECT ASUVoliter.i as ASUvolbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, ASUVoliter WHERE AllProteinsSolved.ASUvolume <= (ASUVoliter.i + 12500) AND AllProteinsSolved.ASUvolume > (ASUVoliter.i - 12500) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i, ASUVoliter.i) s

WHERE u.LLGbin = 0.0075 AND s.LLGbin = 0.0075 AND u.ASUvolbin = s.ASUvolbin;



-- Get the LLGreflVrms for solved ratio=0.5 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter (width=100) variables 
SELECT u.residbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms05solvedRatio, u.LLGbin FROM 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter.i, LLG_vrmsiter3.i) u, 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter.i, LLG_vrmsiter3.i) s

WHERE u.LLGbin = s.LLGbin AND u.residbin = s.residbin AND LLGVrms05solvedRatio <0.6 AND LLGVrms05solvedRatio >= 0.4;


-- Get the LLGreflVrms for solved ratio=0.3 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter (width=100) variables 
SELECT u.residbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms03solvedRatio, u.LLGbin FROM 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter.i, LLG_vrmsiter3.i) u, 

(SELECT Residuesiter.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter.i - 50) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter.i, LLG_vrmsiter3.i) s

WHERE u.LLGbin = s.LLGbin AND u.residbin = s.residbin AND LLGVrms03solvedRatio <0.4 AND LLGVrms03solvedRatio >= 0.2;



-- Get the LLGreflVrms for solved ratio=0.5 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter1 (width=200) variables 
SELECT u.residbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms05solvedRatio, u.LLGbin FROM 

(SELECT Residuesiter1.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter1 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter1.i, LLG_vrmsiter3.i) u, 

(SELECT Residuesiter1.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter1.i, LLG_vrmsiter3.i) s

WHERE u.LLGbin = s.LLGbin AND u.residbin = s.residbin AND LLGVrms05solvedRatio <0.6 AND LLGVrms05solvedRatio >= 0.4;


-- Get the LLGreflVrms for solved ratio=0.3 as a function of NumberofResiduesinTarget
-- Do this by creating 2D histogram in LLG_vrmsiter3 and Residuesiter1 (width=200) variables 
SELECT u.residbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVrms03solvedRatio, u.LLGbin FROM 

(SELECT Residuesiter1.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3, Residuesiter1 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsUnSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter1.i, LLG_vrmsiter3.i) u, 

(SELECT Residuesiter1.i as residbin, LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY Residuesiter1.i, LLG_vrmsiter3.i) s

WHERE u.LLGbin = s.LLGbin AND u.residbin = s.residbin AND LLGVrms03solvedRatio <0.4 AND LLGVrms03solvedRatio >= 0.2;







-- create ratio histogram for the LLGrefl_vrms NumberofResiduesinTarget=100 with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, 
COUNT(AllProteinsUnSolved.LLGrefl_vrms) AS count FROM AllProteinsUnSolved, LLG_vrmsiter3 WHERE AllProteinsUnSolved.NumberofResiduesinTarget <=400 AND AllProteinsUnSolved.NumberofResiduesinTarget > 200 AND AllProteinsUnSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsUnSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteinsSolved.LLGrefl_vrms) AS count FROM AllProteinsSolved, LLG_vrmsiter3 WHERE AllProteinsSolved.NumberofResiduesinTarget <=400 AND AllProteinsSolved.NumberofResiduesinTarget > 200 AND AllProteinsSolved.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteinsSolved.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedResSizeHisto u, LLGReflVrmsAllSolvedResSizeHisto s WHERE u.LLGbin = s.LLGbin;







-- create ratio histogram for the LLGrefl_vrms with CCglobal below and above 0.1 with Bin size 0.0025
CREATE TEMP VIEW LLGReflVrmsAllUnsolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, 
COUNT(AllProteins.LLGrefl_vrms) AS count FROM AllProteins, LLG_vrmsiter3 WHERE AllProteins.CCglobal < 0.1 AND AllProteins.NumberofResiduesinTarget <= 800 AND AllProteins.NumberofResiduesinTarget > 600 AND AllProteins.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteins.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

CREATE TEMP VIEW LLGReflVrmsAllSolvedResSizeHisto AS SELECT LLG_vrmsiter3.i as LLGbin, COUNT(AllProteins.LLGrefl_vrms) AS count FROM AllProteins, LLG_vrmsiter3 WHERE AllProteins.CCglobal >= 0.1 AND AllProteins.NumberofResiduesinTarget <= 800 AND AllProteins.NumberofResiduesinTarget > 600 AND AllProteins.LLGrefl_vrms <= (LLG_vrmsiter3.i + 0.00125) AND AllProteins.LLGrefl_vrms > (LLG_vrmsiter3.i-0.00125) GROUP BY LLG_vrmsiter3.i;

-- now get the histogram ratios. Also display the sum of solved and unsolved bin values as total count
SELECT u.LLGbin, u.count AS unsolved, s.count AS solved, (u.count + s.count) AS totalcount, (CAST(s.count AS REAL)/(u.count + s.count)) AS LLGVRMSratio FROM LLGReflVrmsAllUnsolvedResSizeHisto u, LLGReflVrmsAllSolvedResSizeHisto s WHERE u.LLGbin = s.LLGbin;







-- create target residue number histogram 
SELECT Residuesiter.i as Residues, COUNT(MRTargetTbl.NumberofResiduesinTarget) AS count FROM MRTargetTbl, Residuesiter WHERE MRTargetTbl.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND MRTargetTbl.NumberofResiduesinTarget > (Residuesiter.i-50) GROUP BY Residuesiter.i;

SELECT Residuesiter1.i as Residues, COUNT(MRTargetTbl.NumberofResiduesinTarget) AS count FROM MRTargetTbl, Residuesiter1 WHERE MRTargetTbl.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND MRTargetTbl.NumberofResiduesinTarget > (Residuesiter1.i-100) GROUP BY Residuesiter1.i;

SELECT Residuesiter.i as Residues, COUNT(AllProteins.NumberofResiduesinTarget) AS count FROM AllProteins, Residuesiter WHERE AllProteins.NumberofResiduesinTarget <= (Residuesiter.i + 50) AND AllProteins.NumberofResiduesinTarget > (Residuesiter.i-50) GROUP BY Residuesiter.i;

SELECT Residuesiter2.i as Residues, COUNT(AllProteins.NumberofResiduesinTarget) AS count FROM AllProteins, Residuesiter2 WHERE AllProteins.NumberofResiduesinTarget <= (Residuesiter2.i + 25) AND AllProteins.NumberofResiduesinTarget > (Residuesiter2.i-25) GROUP BY Residuesiter2.i;



-- create ASUvolume histogram 
SELECT ASUVoliter.i as ASUvolume, COUNT(AllProteins.NumberofResiduesinTarget) AS count FROM AllProteins, ASUVoliter WHERE AllProteins.ASUvolume <= (ASUVoliter.i + 12500) AND AllProteins.ASUvolume > (ASUVoliter.i - 12500) GROUP BY ASUVoliter.i;


-- create ASUvolume histogram 
SELECT ASUVoliter.i as ASUvolume, COUNT(MRTargetTbl.NumberofResiduesinTarget) AS count FROM MRTargetTbl, ASUVoliter WHERE MRTargetTbl.ASUvolume <= (ASUVoliter.i + 12500) AND MRTargetTbl.ASUvolume > (ASUVoliter.i - 12500) GROUP BY ASUVoliter.i;





-- create histograms for LLG per reflection for the solved cases all unique SCOP classes, binwidth 0.01
SELECT LLG_vrmsbiggeriter.i as LLGbin, COUNT(ProteinsSolved.LLGreflection0) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, LLG_vrmsbiggeriter WHERE ProteinsSolved.LLGreflection0 <= (LLG_vrmsbiggeriter.i + 0.005) AND ProteinsSolved.LLGreflection0 > (LLG_vrmsbiggeriter.i - 0.005) GROUP BY ProteinsSolved.TargetClass, LLG_vrmsbiggeriter.i;

-- create histograms for LLG per reflection for the solved cases all unique SCOP classes, binwidth 0.005
SELECT LLG_vrmsbigiter.i as LLGbin, COUNT(ProteinsSolved.LLGreflection0) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, LLG_vrmsbigiter WHERE ProteinsSolved.LLGreflection0 <= (LLG_vrmsbigiter.i + 0.0025) AND ProteinsSolved.LLGreflection0 > (LLG_vrmsbigiter.i - 0.0025) GROUP BY ProteinsSolved.TargetClass, LLG_vrmsbigiter.i;

-- create histograms for LLG per reflection for the unsolved cases all unique SCOP classes, binwidth 0.005
SELECT LLG_vrmsbigiter.i as LLGbin, COUNT(ProteinsUnSolved.LLGreflection0) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, LLG_vrmsbigiter WHERE ProteinsUnSolved.LLGreflection0 <= (LLG_vrmsbigiter.i + 0.0025) AND ProteinsUnSolved.LLGreflection0 > (LLG_vrmsbigiter.i-0.0025) GROUP BY ProteinsUnSolved.TargetClass, LLG_vrmsbigiter.i;

-- create histograms for LLG per reflection for the unsolved cases all unique SCOP classes, binwidth 0.0005
SELECT LLG_vrmsiter.i as LLGbin, COUNT(ProteinsUnSolved.LLGreflection0) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, LLG_vrmsiter WHERE ProteinsUnSolved.LLGreflection0 <= (LLG_vrmsiter.i + 0.00025) AND ProteinsUnSolved.LLGreflection0 > (LLG_vrmsiter.i-0.00025) GROUP BY ProteinsUnSolved.TargetClass, LLG_vrmsiter.i;

-- create histograms for LLG per reflection for both solved and unsolved cases, all unique SCOP classes, binwidth 0.0005
SELECT LLG_vrmsiter.i as LLGbin, COUNT(Proteins.LLGreflection0) AS count, Proteins.TargetClass AS TargetClass FROM Proteins, LLG_vrmsiter WHERE Proteins.LLGreflection0 <= (LLG_vrmsiter.i + 0.00025) AND Proteins.LLGreflection0 > (LLG_vrmsiter.i-0.00025) GROUP BY Proteins.TargetClass, LLG_vrmsiter.i;

-- create histograms for LLG per reflection for models of 50 selected targets, binwidth 0.005
SELECT LLG_vrmsbigiter.i as LLGbin, COUNT(AllProteins.LLGreflection0) AS count FROM AllProteins, LLG_vrmsbigiter WHERE AllProteins.LLGreflection0 <= (LLG_vrmsbigiter.i + 0.0025) AND AllProteins.LLGreflection0 > (LLG_vrmsbigiter.i-0.0025) AND TargetPDBid IN ("1A53", "1BU2", "1BU8", "1DFQ", "1E1D", "1E7V", "1E7Z", "1EIL", "1EKG", "1F32", "1FHG", "1GK7", "1HH1", "1IV8", "1JK7", "1KCQ", "1KSW", "1LR8", "1LTU", "1LV7", "1M33", "1NJ4", "1NPC", "1NV9", "1NZU", "1O54", "1P7S", "1PKO", "1SU8", "1T6H", "1TGJ", "1UAM", "1WEF", "1WJ9", "1X06", "1X42", "1XI6", "1Y88", "1ZZM", "2A9Z", "2ASR", "2EUK", "2FEZ", "2FI1", "2FQ3", "2FTQ", "2NRQ", "2OYZ", "2P9B", "2REZ") GROUP BY LLG_vrmsbigiter.i;


-- get solved MR calculations, RMS values, targets belonging to unique SCOP classes
SELECT TargetClass, TargetPDBid, Fullmodel, SequenceIdentity, VRMS, C_L_rms, SSMrms FROM ProteinsSolved ORDER BY TargetClass;

-- get unsolved MR calculations, RMS values, targets belonging to unique SCOP classes
SELECT TargetClass, TargetPDBid, Fullmodel, SequenceIdentity, VRMS, C_L_rms, SSMrms FROM ProteinsUnSolved ORDER BY TargetClass;

-- get ids of those few calculations that lack a VRMS value
SELECT p_id, m.TargetPDBid, m.Fullmodel, m.LLGTemplate, m.iLLGfullres, m.VRMS, m.C_L_rms, m.SSMrms FROM UniqueMRresultsTbl m WHERE NOT EXISTS (SELECT * FROM TemplateRMSTbl s WHERE s.MRtable_id = m.p_id);




-- create histograms for sequenceID for the unsolved cases all unique SCOP classes, binwidth 3
SELECT SEQiditer.i as seqbin, COUNT(ProteinsUnSolved.SequenceIdentity) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, SEQiditer WHERE ProteinsUnSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND ProteinsUnSolved.SequenceIdentity > (SEQiditer.i - 1.5) GROUP BY ProteinsUnSolved.TargetClass, SEQiditer.i;

-- create histograms for sequenceID for the solved cases all unique SCOP classes, binwidth 3
SELECT SEQiditer.i as seqbin, COUNT(ProteinsSolved.SequenceIdentity) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, SEQiditer WHERE ProteinsSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND ProteinsSolved.SequenceIdentity > (SEQiditer.i- 1.5) GROUP BY ProteinsSolved.TargetClass, SEQiditer.i;

-- for each histogram bin get the ratio between solved and unsolved seqid, for each SCOP class
-- first create views of solved and unsolved histograms
CREATE TEMP VIEW SeqidUnsolvedHisto AS SELECT SEQiditer.i as Seqidbin, COUNT(ProteinsUnSolved.SequenceIdentity) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, SEQiditer WHERE ProteinsUnSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND ProteinsUnSolved.SequenceIdentity > (SEQiditer.i - 1.5) GROUP BY ProteinsUnSolved.TargetClass, SEQiditer.i;

CREATE TEMP VIEW SeqidSolvedHisto AS SELECT SEQiditer.i as Seqidbin, COUNT(ProteinsSolved.SequenceIdentity) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, SEQiditer WHERE ProteinsSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND ProteinsSolved.SequenceIdentity > (SEQiditer.i - 1.5) GROUP BY ProteinsSolved.TargetClass, SEQiditer.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Seqidbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS seqidratio FROM SeqidUnsolvedHisto u, SeqidSolvedHisto s WHERE u.Seqidbin = s.Seqidbin AND u.TargetClass = s.TargetClass;


-- ratiohistograms for VRMS values, grouped by targetresiduenumber
SELECT t.residbin, t.count AS totalcount, s.rmsdbin, (CAST(s.count AS REAL)/t.count) AS rmsdratio FROM 

(SELECT Residuesiter1.i as residbin, RMSDiter2.i as rmsdbin, COUNT(*) AS count FROM AllProteinsSolved, RMSDiter2, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsSolved.VRMS <= (RMSDiter2.i + 0.1) AND AllProteinsSolved.VRMS > (RMSDiter2.i - 0.1) GROUP BY Residuesiter1.i, RMSDiter2.i) s, 

(SELECT Residuesiter1.i as residbin, RMSDiter2.i as rmsdbin, COUNT(*) AS count FROM AllProteins, RMSDiter2, Residuesiter1 WHERE AllProteins.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteins.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteins.VRMS <= (RMSDiter2.i + 0.1) AND AllProteins.VRMS > (RMSDiter2.i - 0.1) GROUP BY Residuesiter1.i, RMSDiter2.i) t 

WHERE t.rmsdbin = s.rmsdbin AND t.residbin = s.residbin;



-- ratiohistograms for VRMS values, grouped by modelresiduenumber
SELECT t.residbin, t.count AS totalcount, s.rmsdbin, (CAST(s.count AS REAL)/t.count) AS rmsdratio FROM 

(SELECT Residuesiter1.i as residbin, RMSDiter2.i as rmsdbin, COUNT(*) AS count FROM AllProteinsSolved, RMSDiter2, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinModel <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinModel > (Residuesiter1.i - 100) AND AllProteinsSolved.VRMS <= (RMSDiter2.i + 0.1) AND AllProteinsSolved.VRMS > (RMSDiter2.i - 0.1) GROUP BY Residuesiter1.i, RMSDiter2.i) s, 

(SELECT Residuesiter1.i as residbin, RMSDiter2.i as rmsdbin, COUNT(*) AS count FROM AllProteins, RMSDiter2, Residuesiter1 WHERE AllProteins.NumberofResiduesinModel <= (Residuesiter1.i + 100) AND AllProteins.NumberofResiduesinModel > (Residuesiter1.i - 100) AND AllProteins.VRMS <= (RMSDiter2.i + 0.1) AND AllProteins.VRMS > (RMSDiter2.i - 0.1) GROUP BY Residuesiter1.i, RMSDiter2.i) t 

WHERE t.rmsdbin = s.rmsdbin AND t.residbin = s.residbin;



-- histograms for modelcompleteness values 
SELECT t.residbin, t.count AS totalcount, s.modelcompletebin, (CAST(s.count AS REAL)/t.count) AS modelcompleteratio FROM 

(SELECT Residuesiter1.i as residbin, ModelCompliter.i as modelcompletebin, COUNT(*) AS count FROM AllProteinsSolved, ModelCompliter, Residuesiter1 WHERE AllProteinsSolved.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteinsSolved.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteinsSolved.ModelCompleteness <= (ModelCompliter.i + 0.05) AND AllProteinsSolved.ModelCompleteness > (ModelCompliter.i - 0.05) GROUP BY Residuesiter1.i, ModelCompliter.i) s, 

(SELECT Residuesiter1.i as residbin, ModelCompliter.i as modelcompletebin, COUNT(*) AS count FROM AllProteins, ModelCompliter, Residuesiter1 WHERE AllProteins.NumberofResiduesinTarget <= (Residuesiter1.i + 100) AND AllProteins.NumberofResiduesinTarget > (Residuesiter1.i - 100) AND AllProteins.ModelCompleteness <= (ModelCompliter.i + 0.05) AND AllProteins.ModelCompleteness > (ModelCompliter.i - 0.05) GROUP BY Residuesiter1.i, ModelCompliter.i) t 

WHERE t.modelcompletebin = s.modelcompletebin AND t.residbin = s.residbin;






-- as above with bin width 5
CREATE TEMP VIEW SeqidbigUnsolvedHisto AS SELECT SEQidbigiter.i as Seqidbin, COUNT(ProteinsUnSolved.SequenceIdentity) AS count, ProteinsUnSolved.TargetClass AS TargetClass FROM ProteinsUnSolved, SEQidbigiter WHERE ProteinsUnSolved.SequenceIdentity <= (SEQidbigiter.i + 2.5) AND ProteinsUnSolved.SequenceIdentity > (SEQidbigiter.i - 2.5) GROUP BY ProteinsUnSolved.TargetClass, SEQidbigiter.i;

CREATE TEMP VIEW SeqidbigSolvedHisto AS SELECT SEQidbigiter.i as Seqidbin, COUNT(ProteinsSolved.SequenceIdentity) AS count, ProteinsSolved.TargetClass AS TargetClass FROM ProteinsSolved, SEQidbigiter WHERE ProteinsSolved.SequenceIdentity <= (SEQidbigiter.i + 2.5) AND ProteinsSolved.SequenceIdentity > (SEQidbigiter.i - 2.5) GROUP BY ProteinsSolved.TargetClass, SEQidbigiter.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT u.TargetClass, (u.count + s.count) AS totalcount, u.Seqidbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS seqidratio FROM SeqidbigUnsolvedHisto u, SeqidbigSolvedHisto s WHERE u.Seqidbin = s.Seqidbin AND u.TargetClass = s.TargetClass;

-- for each histogram bin get the ratio between solved and unsolved seqid. This time for all proteins, even those in multiple SCOP classes
-- first create views of solved and unsolved histograms
CREATE TEMP VIEW SeqidAllUnsolvedHisto AS SELECT SEQiditer.i as Seqidbin, COUNT(AllProteinsUnSolved.SequenceIdentity) AS count FROM AllProteinsUnSolved, SEQiditer WHERE AllProteinsUnSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND AllProteinsUnSolved.SequenceIdentity > (SEQiditer.i-1.5) GROUP BY SEQiditer.i;

CREATE TEMP VIEW SeqidAllSolvedHisto AS SELECT SEQiditer.i as Seqidbin, COUNT(AllProteinsSolved.SequenceIdentity) AS count FROM AllProteinsSolved, SEQiditer WHERE AllProteinsSolved.SequenceIdentity <= (SEQiditer.i + 1.5) AND AllProteinsSolved.SequenceIdentity > (SEQiditer.i-1.5) GROUP BY SEQiditer.i;

-- now get the histogram ratios for each SCOP class. Also display the sum of solved and unsolved bin values as total count
SELECT (u.count + s.count) AS totalcount, u.Seqidbin, (CAST(s.count AS REAL)/(u.count + s.count)) AS seqidratio FROM SeqidAllUnsolvedHisto u, SeqidAllSolvedHisto s WHERE u.Seqidbin = s.Seqidbin;


-- display histogram bins of resolutions of MR calculations
SELECT Resiter.i as Resbin, COUNT(*) AS count FROM AllProteins,Resiter WHERE AllProteins.Resolution <= (Resiter.i + 0.1) AND AllProteins.Resolution > (Resiter.i - 0.1) GROUP BY Resiter.i;



-- list the few cases where the LLGtemplate is negative in spite of RMS refinement being enabled
SELECT m.TargetPDBid, m.Fullmodel, m.Resolution, m.LLGTemplate, m.iLLGfullres, m.VRMS, m.C_L_rms, m.SCOPid FROM UniqueMRresultsTbl m WHERE  m.LLGTemplate < 0.0;

-- list LLGtemplateperreflection and LLGperreflection against VRMS for nearly constant modelcompletion values
SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.C_L_rms, m.LLGreflection0, m.LLGreflectiontemplate, m.ModelCompleteness FROM ProteinsSolved m WHERE m.ModelCompleteness > 0.6 AND m.ModelCompleteness < 0.7;

-- list RMS for the SCOP classes at constant model completion
SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.SequenceIdentity, m.VRMS, m.C_L_rms, m.ssmRMS FROM ProteinsSolved m WHERE m.ModelCompleteness > 0.6 AND m.ModelCompleteness < 0.7;


SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.C_L_rms, m.LLGreflection0, m.LLGreflectiontemplate, m.ModelCompleteness FROM ProteinsSolved m WHERE m.ModelCompleteness > 0.7 AND m.ModelCompleteness < 0.8;

-- list Fracvar and FracvarVrms for the SCOP classes
SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.C_L_rms, m.VRMS, m.ModelCompleteness, m.Fracvar, m.FracvarVrms, m.LLGreflection0, m.LLGreflectiontemplate FROM ProteinsSolved m ORDER BY m.TargetClass;

-- list those models that have been used for different targets at least 10 times in this database
SELECT * FROM (SELECT t.Fullmodel, COUNT(*) AS count FROM UniqueMRresultsTbl t GROUP BY t.Fullmodel) WHERE count > 10;

-- list data for targets using the model 1ALQ_A
SELECT m.TargetPDBid, m.Fullmodel, m.CorrectSolution, m.Modelcompleteness, TFZ0, m.VRMS, m.Fracvar, FracvarVrms, m.LLGreflectiontemplate, m.LLGreflection0, m.Sequenceidentity, m.SCOPid FROM UniqueMRresultsTbl m WHERE m.Fullmodel = "1ALQ_A";

-- list Fracvar, LLGreflection, RMS and VRMS for all targetclasses
SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.VRMS, m.C_L_rms, m.ModelCompleteness, m.WilsonB, m.Fracvar, m.FracvarVrms, m.LLGreflection0, m.LLGreflectiontemplate FROM ProteinsSolved m ORDER BY m.TargetClass;

-- list Fracvar, LLGreflection, RMS and VRMS for all proteins, not just targets belonging to a unique SCOPid
SELECT m.SCOPid, m.TargetPDBid, m.Fullmodel, m.VRMS, m.C_L_rms, m.ModelCompleteness, m.Fracvar, m.FracvarVrms, m.LLGreflection0, m.LLGreflectiontemplate FROM AllProteinsSolved m;

-- list Fracvar, LLGreflection, RMS and VRMS for targetfolds that are lysozyme like and spgrp P212121
SELECT m.TargetPDBid, m.Fullmodel, m.VRMS, m.C_L_rms, m.Unitcell, m.Spacegroup, m.WilsonB, m.Fracvar, m.FracvarVrms, m.LLGreflection0, m.LLGreflectiontemplate FROM ProteinsSolved m WHERE m.TargetFold = "Lysozyme-like" AND m.Spacegroup="P 21 21 21" ORDER BY m.FracvarVrms;

-- get fracvarvrms versus vrms for proteins belonging to only one SCOP class
SELECT m.TargetClass, m.TargetPDBid, m.Fullmodel, m.Resolution, m.SequenceIdentity, m.VRMS, m.C_L_rms, m.FracvarVrms, m.LLGreflectiontemplate FROM ProteinsSolved m ORDER BY m.TargetClass;

-- get fracvarvrms versus vrms for proteins belonging to any SCOP fold even if more than one
SELECT m.SCOPid, m.TargetPDBid, m.Fullmodel, m.Resolution, m.SequenceIdentity, m.VRMS, m.C_L_rms, m.FracvarVrms, m.LLGreflectiontemplate FROM AllProteinsSolved m;

-- display fracvarvrms and LLGs for targetPDBid 1XUH for those MR calculations that uses the same Fullmodel as for targetPDBid 2A31 and which found a solution for both targets
SELECT m.TargetPDBid, m.Fullmodel, m.Resolution, m.Modelcompleteness,m.VRMS, (SELECT FracvarVrms FROM TemplateRMSTbl s WHERE s.MRtable_id = m.p_id) AS FracvarVrms, m.UsedReflections, m.LLG0, m.LLGreflection0, m.iLLGfullres, m.LLGtemplate, m.LLGreflectiontemplate FROM UniqueMRresultsTbl m, UniqueMRresultsTbl u WHERE m.CorrectSolution > 0 AND u.CorrectSolution > 0 AND m.TargetPDBid ="1XUH" AND u.TargetPDBid ="2A31" AND u.Fullmodel = m.Fullmodel;

-- show LLG versus fracvar for folds with at least 20 TargetPDBid
SELECT m.TargetFold, m.TargetPDBid, m.Fullmodel, m.VRMS, m.C_L_rms, m.Spacegroup, m.Fracvar, m.FracvarVrms, m.LLGreflection0, m.LLGreflectiontemplate FROM ProteinsSolved m WHERE m.TargetFold IN (SELECT TargetFold FROM TargetPDBperFold WHERE nTargetPDBs > 19) ORDER BY TargetFold;

-- list folds and their targetpdbs with at least 20 targetpdbs belonging to each fold
SELECT DISTINCT m.TargetPDBid, m.TargetFold, m.Resolution, m.SCOPid, m.Spacegroup FROM ProteinsSolved m WHERE m.TargetFold IN (SELECT TargetFold FROM TargetPDBperFold WHERE nTargetPDBs > 19) ORDER BY TargetFold;

-- Get LLG(VRMS)/reflection versus Fracvar for different sized proteins
SELECT AllProteins.FracvarVRMS, AllProteins.LLGrefl_vrms FROM AllProteins WHERE AllProteins.NumberofResiduesinTarget <= 200 AND AllProteins.NumberofResiduesinTarget > 0;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that didn't solve the structure, sorted by crystal systems
CREATE TEMP VIEW NumberSolutionUnsolvedAvg AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AVG(AllProteinsUnSolved.NumberofSolutions) AS numavg, (
SUM( CAST(AllProteinsUnSolved.NumberofSolutions AS REAL) * CAST(AllProteinsUnSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsUnSolved.NumberofSolutions) - AVG(AllProteinsUnSolved.NumberofSolutions)*AVG(AllProteinsUnSolved.NumberofSolutions) 
) AS numvariance, AllProteinsUnSolved.Crystalsystem FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter.i+0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY AllProteinsUnSolved.Crystalsystem, TFZiter.i;

-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that solved the structure, sorted by crystal systems
CREATE TEMP VIEW NumberSolutionSolvedCrystAvg AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AVG(AllProteinsSolved.NumberofSolutions) AS numavg, (SUM( CAST(AllProteinsSolved.NumberofSolutions AS REAL) * CAST(AllProteinsSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsSolved.NumberofSolutions) - AVG(AllProteinsSolved.NumberofSolutions)*AVG(AllProteinsSolved.NumberofSolutions) ) AS numvariance, AVG(AllProteinsSolved.CorrectTFZtopmost) AS solutiontopmostavg, AllProteinsSolved.Crystalsystem FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.TFZ0 <= (TFZiter.i+0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY AllProteinsSolved.Crystalsystem, TFZiter.i;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that didn't solve the structure, sorted by IsPolarSpacegroup, binwidth=1
CREATE TEMP VIEW NumberSolutionUnsolvedPolarAvg AS SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AVG(AllProteinsUnSolved.NumberofSolutions) AS numavg, (
SUM( CAST(AllProteinsUnSolved.NumberofSolutions AS REAL) * CAST(AllProteinsUnSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsUnSolved.NumberofSolutions) - AVG(AllProteinsUnSolved.NumberofSolutions)*AVG(AllProteinsUnSolved.NumberofSolutions) 
) AS numvariance, AllProteinsUnSolved.IsPolarSpacegroup FROM AllProteinsUnSolved, TFZiter1 WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter1.i+0.5) AND AllProteinsUnSolved.TFZ0 > (TFZiter1.i-0.5) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY AllProteinsUnSolved.IsPolarSpacegroup, TFZiter1.i;

-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that solved the structure, IsPolarSpacegroup
CREATE TEMP VIEW NumberSolutionSolvedPolarAvg AS SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AVG(AllProteinsSolved.NumberofSolutions) AS numavg, (SUM( CAST(AllProteinsSolved.NumberofSolutions AS REAL) * CAST(AllProteinsSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsSolved.NumberofSolutions) - AVG(AllProteinsSolved.NumberofSolutions)*AVG(AllProteinsSolved.NumberofSolutions) ) AS numvariance, AVG(AllProteinsSolved.CorrectTFZtopmost) AS solutiontopmostavg, AllProteinsSolved.IsPolarSpacegroup FROM AllProteinsSolved, TFZiter1 WHERE AllProteinsSolved.TFZ0 <= (TFZiter1.i+0.5) AND AllProteinsSolved.TFZ0 > (TFZiter1.i-0.5) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY AllProteinsSolved.IsPolarSpacegroup, TFZiter1.i;




-- get number of solutions, the average and variance as a function of TFZ for all MR calculations both solved and unsolved. binsize=0.5
CREATE TEMP VIEW NumberSolutionAllAvg AS SELECT TFZiter.i as TFZbin, COUNT(AllProteins.TFZ0) AS count, AVG(AllProteins.NumberofSolutions) AS numavg, (
SUM( CAST(AllProteins.NumberofSolutions AS REAL) * CAST(AllProteins.NumberofSolutions AS REAL))/COUNT(AllProteins.NumberofSolutions) - AVG(AllProteins.NumberofSolutions)*AVG(AllProteins.NumberofSolutions) 
) AS numvariance FROM AllProteins, TFZiter WHERE AllProteins.TFZ0 <= (TFZiter.i+0.25) AND AllProteins.TFZ0 > (TFZiter.i-0.25) AND AllProteins.Spacegroup != "P 1" GROUP BY TFZiter.i;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that didn't solve the structure, binsize=0.5
CREATE TEMP VIEW NumberAllSolutionUnsolvedAvg AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AVG(AllProteinsUnSolved.NumberofSolutions) AS numavg, (
SUM( CAST(AllProteinsUnSolved.NumberofSolutions AS REAL) * CAST(AllProteinsUnSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsUnSolved.NumberofSolutions) - AVG(AllProteinsUnSolved.NumberofSolutions)*AVG(AllProteinsUnSolved.NumberofSolutions) 
) AS numvariance FROM AllProteinsUnSolved, TFZiter WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter.i+0.25) AND AllProteinsUnSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that solved the structure, binsize=0.5
CREATE TEMP VIEW NumberAllSolutionSolvedAvg AS SELECT TFZiter.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AVG(AllProteinsSolved.NumberofSolutions) AS numavg, (SUM( CAST(AllProteinsSolved.NumberofSolutions AS REAL) * CAST(AllProteinsSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsSolved.NumberofSolutions) - AVG(AllProteinsSolved.NumberofSolutions)*AVG(AllProteinsSolved.NumberofSolutions) ) AS numvariance, AVG(AllProteinsSolved.CorrectTFZtopmost) AS solutiontopmostavg FROM AllProteinsSolved, TFZiter WHERE AllProteinsSolved.TFZ0 <= (TFZiter.i+0.25) AND AllProteinsSolved.TFZ0 > (TFZiter.i-0.25) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter.i;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that didn't solve the structure, binsize=1
CREATE TEMP VIEW NumberAllSolutionUnsolvedAvg AS SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsUnSolved.TFZ0) AS count, AVG(AllProteinsUnSolved.NumberofSolutions) AS numavg, (
SUM( CAST(AllProteinsUnSolved.NumberofSolutions AS REAL) * CAST(AllProteinsUnSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsUnSolved.NumberofSolutions) - AVG(AllProteinsUnSolved.NumberofSolutions)*AVG(AllProteinsUnSolved.NumberofSolutions) 
) AS numvariance FROM AllProteinsUnSolved, TFZiter1 WHERE AllProteinsUnSolved.TFZ0 <= (TFZiter1.i+0.5) AND AllProteinsUnSolved.TFZ0 > (TFZiter1.i-0.5) AND AllProteinsUnSolved.Spacegroup != "P 1" GROUP BY TFZiter1.i;


-- get number of solutions, the average and variance as a function of TFZ for all those MR calculations that solved the structure, binsize=1
CREATE TEMP VIEW NumberAllSolutionSolvedAvg AS SELECT TFZiter1.i as TFZbin, COUNT(AllProteinsSolved.TFZ0) AS count, AVG(AllProteinsSolved.NumberofSolutions) AS numavg, (SUM( CAST(AllProteinsSolved.NumberofSolutions AS REAL) * CAST(AllProteinsSolved.NumberofSolutions AS REAL))/COUNT(AllProteinsSolved.NumberofSolutions) - AVG(AllProteinsSolved.NumberofSolutions)*AVG(AllProteinsSolved.NumberofSolutions) ) AS numvariance, AVG(AllProteinsSolved.CorrectTFZtopmost) AS solutionTFZtopmostavg, AVG(AllProteinsSolved.CorrectLLGtopmost) AS solutionLLGtopmostavg FROM AllProteinsSolved, TFZiter1 WHERE AllProteinsSolved.TFZ0 <= (TFZiter1.i+0.5) AND AllProteinsSolved.TFZ0 > (TFZiter1.i-0.5) AND AllProteinsSolved.Spacegroup != "P 1" GROUP BY TFZiter1.i;



-- show the modelcompleteness histogram
SELECT ModelCompliter.i as MdlCmplbin, COUNT(AllProteins.ModelCompleteness) AS count FROM AllProteins, ModelCompliter WHERE AllProteins.ModelCompleteness <= ModelCompliter.i AND AllProteins.ModelCompleteness > (ModelCompliter.i - 0.1) GROUP BY ModelCompliter.i;




-- list number of proposed solutions for MR calculations that solved their structures and tag them with 1 if the correct solution has the highest TFZ
SELECT m.TFZ0, m.NumberofSolutions, m.CorrectTFZtopmost FROM AllProteinsSolved m WHERE m.TFZ0 <= 6 AND m.TFZ0 > 5.5 AND m.Spacegroup != "P 1";

-- get average and variance of TFZ values for all calculations
SELECT AVG(TFZ0), (SUM(AllProteins.TFZ0 * AllProteins.TFZ0)/COUNT(AllProteins.TFZ0) - AVG(TFZ0)*AVG(TFZ0)) AS TFZvar FROM AllProteins WHERE Spacegroup != "P 1" ;

-- as above but only up to TFZ <9
SELECT AVG(TFZ0), (SUM(AllProteins.TFZ0 * AllProteins.TFZ0)/COUNT(AllProteins.TFZ0) - AVG(TFZ0)*AVG(TFZ0)) AS TFZvar FROM AllProteins WHERE Spacegroup != "P 1" AND TFZ0<9;

-- get numbers of targets and MR calculations belonging to only one SCOP class
CREATE TEMP VIEW NtargetNmodelsUniqueSCOP AS SELECT TargetClass,(SELECT COUNT( DISTINCT TargetPDBid) FROM PROTEINS t WHERE t.TargetClass = m.TargetClass ) AS Ntargetclass, COUNT(*) AS Nmodels FROM PROTEINS m WHERE m.TargetClass IN (SELECT * FROM TargetClasses) GROUP BY m.TargetClass;


-- get 3 random targetPDBid and all their models
SELECT m.TargetPDBid, m.Fullmodel FROM UniqueMRresultsTbl m WHERE m.TargetPDBid IN (SELECT DISTINCT u.TargetPDBid FROM UniqueMRresultsTbl u ORDER BY RANDOM() LIMIT 3);

-- get results for those 50 Targets where new MR calculations were done with new RMS values
SELECT TargetPDBid, Fullmodel, TFZ0, LLGreflection0, C_L_rms, CorrectSolution FROM UniqueMRresultsTbl WHERE TargetPDBid IN ("1A53", "1BU2", "1BU8", "1DFQ", "1E1D", "1E7V", "1E7Z", "1EIL", "1EKG", "1F32", "1FHG", "1GK7", "1HH1", "1IV8", "1JK7", "1KCQ", "1KSW", "1LR8", "1LTU", "1LV7", "1M33", "1NJ4", "1NPC", "1NV9", "1NZU", "1O54", "1P7S", "1PKO", "1SU8", "1T6H", "1TGJ", "1UAM", "1WEF", "1WJ9", "1X06", "1X42", "1XI6", "1Y88", "1ZZM", "2A9Z", "2ASR", "2EUK", "2FEZ", "2FI1", "2FQ3", "2FTQ", "2NRQ", "2OYZ", "2P9B", "2REZ");

-- get numbers of correct solutions for those 50 targets where new MR calculations were done with new RMS values
SELECT COUNT(*) FROM UniqueMRresultsTbl WHERE TargetPDBid IN ("1A53", "1BU2", "1BU8", "1DFQ", "1E1D", "1E7V", "1E7Z", "1EIL", "1EKG", "1F32", "1FHG", "1GK7", "1HH1", "1IV8", "1JK7", "1KCQ", "1KSW", "1LR8", "1LTU", "1LV7", "1M33", "1NJ4", "1NPC", "1NV9", "1NZU", "1O54", "1P7S", "1PKO", "1SU8", "1T6H", "1TGJ", "1UAM", "1WEF", "1WJ9", "1X06", "1X42", "1XI6", "1Y88", "1ZZM", "2A9Z", "2ASR", "2EUK", "2FEZ", "2FI1", "2FQ3", "2FTQ", "2NRQ", "2OYZ", "2P9B", "2REZ") AND CorrectSolution >0;

-- get LLG, fracvar, TFZ for those 50 selected targets
SELECT m.TargetPDBid, m.Fullmodel, m.CorrectSolution, m.NumberofSolutions, m.TFZ0, m.VRMS, m.C_L_rms, m.ModelCompleteness, m.Resolution, m.Fracvar, m.LLGreflection0 FROM UniqueMRresultsTbl m WHERE m.TargetPDBid IN ("1A53", "1BU2", "1BU8", "1DFQ", "1E1D", "1E7V", "1E7Z", "1EIL", "1EKG", "1F32", "1FHG", "1GK7", "1HH1", "1IV8", "1JK7", "1KCQ", "1KSW", "1LR8", "1LTU", "1LV7", "1M33", "1NJ4", "1NPC", "1NV9", "1NZU", "1O54", "1P7S", "1PKO", "1SU8", "1T6H", "1TGJ", "1UAM", "1WEF", "1WJ9", "1X06", "1X42", "1XI6", "1Y88", "1ZZM", "2A9Z", "2ASR", "2EUK", "2FEZ", "2FI1", "2FQ3", "2FTQ", "2NRQ", "2OYZ", "2P9B", "2REZ") ORDER BY TargetPDBid;

CREATE INDEX tmpTargetPDBid ON UniqueMRresultsTbl(TargetPDBid);
-- get number of models for each target in ascending order and the number of models that solved each target
SELECT m.TargetPDBid, COUNT (*) AS nmodels, Resolution, (SELECT SUM(t.CorrectSolution) FROM UniqueMRresultsTbl t WHERE t.TargetPDBid = m.TargetPDBid) AS numberofsolved, m.SCOPid FROM UniqueMRresultsTbl m GROUP BY m.TargetPDBid ORDER BY nmodels DESC LIMIT 40;

-- get cases that solved, tfz>8 but correct solution  isn't topmost
SELECT TargetPDBid, NumberofSolutions, Fullmodel, TFZ0, SCOPid, Comment FROM AllProteinsSolved WHERE SolutionRank > 1 AND TFZ0 > 8 ORDER BY TFZ0 DESC;

-- get cases that solved but aren't topmost and where TFZ for the correct solution is higher than for topmost
SELECT m.TargetPDBid, m.Fullmodel, m.TFZ0, (SELECT TFZ FROM AnnotationTbl u WHERE u.MRtable_id = m.p_id AND Tstar = 1) AS TFZanno, m.solutionRank FROM AllProteinsSolved m WHERE Spacegroup != "P 1" AND SolutionRank > 1 AND TFZanno > TFZ0 AND TFZ0 > 4;

-- get correct solutions that aren't topmost and indicate if the TFZ score for the correct solution is larger than for the topmost
SELECT m.TargetPDBid, m.Fullmodel, m.TFZ0, (SELECT TFZ FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id AND u.Tstar = 1) AS TFZanno, m.LLGreflection0, m.LLGrefl_vrms, (SELECT u.LLGrefl_vrms FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id AND u.Tstar) AS LLGreflvrmsanno, m.solutionRank, m.CorrectTFZtopmost FROM AllProteinsSolved m WHERE Spacegroup != "P 1" AND SolutionRank > 1 AND TFZ0 > 6;


-- get sequenceidentity v TFZ for unsolved cases
SELECT TFZ0, Sequenceidentity, Fracvar, VRMS, FracvarVrms FROM AllProteinsUnSolved WHERE Spacegroup != "P 1";

-- for solved but not topmost solutions
SELECT TFZ0, LLGreflection0, Sequenceidentity, Fracvar, VRMS, FracvarVrms FROM AllProteinsSolved WHERE Spacegroup != "P 1" AND SolutionRank > 1;

-- for solved and topmost solutions
SELECT TFZ0, Sequenceidentity, Fracvar, VRMS, FracvarVrms FROM AllProteinsSolved WHERE Spacegroup != "P 1" AND SolutionRank=1;

-- get TFZ, number of residues, cputimes for calculations finishing within 200 seconds
SELECT TargetPDBid, Fullmodel, TFZ0, NumberofSolutions, NumberofResiduesinTarget, NumberofResiduesinModel, Modelcompleteness, CPUtime, SCOPid FROM AllProteinsSolved WHERE TFZ0 <7 AND CPUtime < 200;

-- get all VRMs and LLG values for incorrect solutions found by phaser for "alpha/beta-Hydrolases", no more than 1000
SELECT (SELECT TargetPDBid FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS TargetPDBid, (SELECT Fullmodel FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id ) AS Fullmodel, Tstar, m.VRMS, m.FracvarVrms, (SELECT TmplVRMS FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) as TmplVRMS, (SELECT FracvarTmplVrms FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS FracvarTmplVrms, m.LLGreflection, m.LLGrefl_vrms, (SELECT LLGreflection0 FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS LLGreflTopSolu, (SELECT LLGreflectiontemplate FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS LLGTmplRefl FROM AnnotationTbl m WHERE m.MRtable_id IN (SELECT p_id FROM UniqueMRresultsTbl WHERE TargetFold = "beta-lactamase/transpeptidase-like") AND m.MRtable_id IN (SELECT p_id FROM AllProteins) AND Tstar < 1 ORDER BY RANDOM() LIMIT 1000;

-- get all VRMS and LLG values for unsolved solutions found by phaser with LLG/refl > 0.01;
SELECT (SELECT TargetPDBid FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS TargetPDBid, (SELECT Fullmodel FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id ) AS Fullmodel, TFZ, Tstar, m.VRMS, m.FracvarVrms, (SELECT TmplVRMS FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) as TmplVRMS, (SELECT FracvarTmplVrms FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS FracvarTmplVrms, m.LLGreflection, m.LLGrefl_vrms, (SELECT LLGreflection0 FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS LLGrefl, (SELECT LLGreflectiontemplate FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS LLGTmplRefl FROM AnnotationTbl m WHERE m.LLGrefl_vrms > 0.01 AND Tstar < 1 AND m.MRtable_id IN (SELECT p_id FROM AllProteins);


-- get all VRMS and LLG values for unsolved solutions found by phaser with TFZ>8 and not P1
SELECT (SELECT TargetPDBid FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS TargetPDBid, (SELECT Fullmodel FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id ) AS Fullmodel, TFZ, Tstar, m.VRMS, m.FracvarVrms, (SELECT TmplVRMS FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) as TmplVRMS, (SELECT FracvarTmplVrms FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS FracvarTmplVrms, m.LLG, m.LLGvrms, m.LLGreflection, m.LLGrefl_vrms, (SELECT LLGreflectiontemplate FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS LLGTmplRefl, (SELECT Comment FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) as Comment FROM AnnotationTbl m WHERE m.LLGrefl_vrms > 0.0 AND m.Tstar < 1 AND m.TFZ > 8 AND m.MRtable_id IN (SELECT p_id FROM AllProteins WHERE Spacegroup != "P 1");

-- Get the MR calculation with the smallest LLGrefl_vrms among its solution
SELECT TargetPDBid, Fullmodel, SCOPid, NumberofSolutions FROM UniqueMRresultsTbl WHERE p_id IN (SELECT MRtable_id FROM AnnotationTbl WHERE LLGrefl_vrms = (SELECT MIN(LLGrefl_vrms) FROM AnnotationTbl) );

-- Get number of MR solutions from all good calculations
SELECT COUNT(*)  FROM AnnotationTbl m where m.MRtable_id IN (SELECT p_id FROM AllProteins);

-- Get fracvar, LLG/reflection and correlation coefficient for all calculations, i.e. only topmost solutions
SELECT TargetPDBid, Fullmodel, SCOPid, Fracvar, LLGreflection0, FracvarVrms, LLGrefl_vrms, CCglobal, CorrectSolution FROM AllProteins ORDER BY CorrectSolution, LLGrefl_vrms;


-- get fracvarvrms, correlation coefficients for targets with more than 200 residues
SELECT FracvarVrms, CCglobal, CorrectSolution FROM AllProteins WHERE NumberofResiduesinTarget < 200 AND CCglobal != "" AND FracvarVrms != "";




-- get some values from a specific MR calculation
SELECT (SELECT TargetPDBid FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS TargetPDBid, (SELECT Fullmodel FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id ) AS Fullmodel, (SELECT Comment FROM UniqueMRresultsTbl WHERE p_id=m.MRtable_id) AS Comment, m.Tstar, m.LLG, m.iLLG, m.LLGvrms, m.iLLGvrms FROM AnnotationTbl m WHERE m.MRtable_id IN (SELECT p_id FROM AllProteins WHERE TargetPDBid="1ONJ" AND Fullmodel="2H7Z_B");



-- get values for non-topmost correct solutions 
SELECT u.TargetPDBid, u.FullModel, u.TFZ0, (SELECT m.TFZ FROM AnnotationTbl m WHERE m.Tstar = 1 AND m.MRModel_id=u.p_id) AS correctsolTFZ, u.LLGrefl_vrms, (SELECT m.LLGrefl_vrms FROM AnnotationTbl m WHERE m.Tstar = 1 AND m.MRModel_id=u.p_id) AS correctsolLLGrefl_vrms, u.CCglobal, (SELECT m.CCglobal FROM AnnotationTbl m WHERE m.Tstar = 1 AND m.MRModel_id=u.p_id) AS correctsolCCglobal, NumberofSolutions, SolutionRank, SCOPid FROM UniqueMRresultsTbl u WHERE TFZ0 < 6 AND NumberofSolutions > 1 AND CorrectSolution = 1 AND SolutionRank > 1 AND LLGrefl_vrms != "";



-- get FracvarVrms, LLGrefl_vrms for MR all solutions and ensure that empty records are dismissed
SELECT FracvarVrms, LLGrefl_vrms, Tstar FROM AnnotationTbl WHERE FracvarVRMS IS NOT "" AND LLGrefl_vrms IS NOT "" AND Tstar IS NOT "";

