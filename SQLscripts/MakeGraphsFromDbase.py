#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     12/01/2011
# Copyright:   (c) oeffner 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import sqlite3, sys, os
import traceback


def Maketables(DBfname, sqlstrfname, key, pythonarray):
    grsqlexprdict = eval(open(sqlstrfname,"r").read())
    grsqlexprkeys = set(grsqlexprdict)
    if key != "":
# then only make a table for that particular sql expression with this key name in the dictionary
        grsqlexprdict = {key: grsqlexprdict[key]}
        grsqlexprkeys = set(grsqlexprdict)

# open the database
    conn = sqlite3.connect(DBfname)
    conn.enable_load_extension(True)
    conn.load_extension("myfuncs.sqlext")

    mydb = conn.cursor()
    mydb.arraysize = 200
    mydb.execute('PRAGMA synchronous = OFF')
    mydb.execute('PRAGMA journal_mode = MEMORY')
    #mydb.execute("PRAGMA cache_size=-40000")
    #mydb.execute("PRAGMA page_size=65536")
    #mydb.execute("PRAGMA soft_heap_limit=40000000")
    #mydb.execute("PRAGMA query_only=1")
    try:
        for graphname in grsqlexprkeys:
            sqlxprsions = grsqlexprdict[graphname]
            outfname = graphname + "_" + os.path.basename(DBfname) +".txt"
            mstr = ""
            didprint = False

            for sqlxprs in sqlxprsions:
                print graphname + "\n" + sqlxprs
                mydb.execute(sqlxprs)
                rows = mydb.fetchall()
                lenrows = len(rows)-1
                #rows = mydb.fetchmany(size=200)

                if rows != []:
                    didprint = True
                    if pythonarray=="y":
                        print "printing as python array"
                        mstr = str(rows)
                        graphfile = open(outfname,"w")
                        graphfile.write(mstr)
                        graphfile.close()

                    else:
                        print "printing as table"
                        keys = mydb.description
                        for key in keys:
                            mstr += key[0] + "\t"

                        #print mstr
                        mstr += "\n"

                        for row in rows:
                            lenrow = len(row)-1
                            #print row
                            for e in row[:lenrow]: # don't add tab beyond the last column
                                s = str(e) if e != None else ""
                                mstr += s + "\t"

                            e = row[lenrow]
                            s = str(e) if e != None else ""
                            mstr += s
                            mstr += "\n"

                        graphfile = open(outfname,"w")
                        graphfile.write("# " + outfname + "\n# " + mstr) # using # to specify comment lines
                        graphfile.close()

                if didprint == False:
                    print "Warning: No rows printed to", outfname
                else:
                    print "Printed %d to %s" %(lenrows, outfname)

    except Exception, m:
        print traceback.format_exc()

    finally:
        mydb.close()




if __name__ == '__main__':
    key = ""
    if len(sys.argv) > 3:
        key = sys.argv[3]

    pythonarray = ""
    if len(sys.argv) > 4:
        pythonarray = sys.argv[4]

    Maketables(DBfname = sys.argv[1], sqlstrfname = sys.argv[2], key=key, pythonarray=pythonarray)


