--check failed CC calculations
--SELECT TargetPDBid, Fullmodel, LLG0, LLGvrms, TFZequiv, CCglobal FROM UniqueMRresultsTbl WHERE CCglobal = "";


-- to speed up searches some orders of magnitude
CREATE INDEX MRidx ON AnnotationTbl(MRModel_id);
CREATE INDEX modelpidx ON MRModelTbl(p_id);
CREATE INDEX sigmaapidx ON CCsigmaAtbl(MRModel_id);
CREATE INDEX TargetPDBidx ON MRTargetTbl(TargetPDBid);
CREATE INDEX tmplidx_MRtable_id ON TemplateRMSTbl(MRModel_id);
CREATE INDEX tmplRefinedModelidx ON TemplateRMSTbl(MRModel_id);
CREATE INDEX ellgidx_MRtable_id ON ELLGtbl(MRModel_id);
CREATE INDEX uniqueMRidx ON MRModelTbl(MRTarget_id);

ALTER TABLE MRModelTbl ADD CorrectSolution int;
UPDATE MRModelTbl SET CorrectSolution = Foundit;


-- ALTER TABLE MRModelTbl ADD NewRMSD real;
-- modified exponent with dependence of number of residues in model	
-- EXP((-0.014514 - 2.4499*10^-6 r) s) (1.50921 + 0.00153695 r)
-- UPDATE MRModelTbl SET NewRMSD = (1.50921 + 0.00153695 *NumberofResiduesinModel)
--	* EXP((-0.014514 - 0.0000024499 * NumberofResiduesinModel )* SequenceIdentity) 

ALTER TABLE MRModelTbl ADD SqLLGZratio0 real;
-- Make a column with the fit of VRMS values to the SSMseqids
ALTER TABLE MRModelTbl ADD NewSSMRMSD real;
ALTER TABLE MRModelTbl ADD SqLLGZratio1 real;
UPDATE MRModelTbl SET SqLLGZratio0 = SQRT(iLLG0)/TFZ0;
UPDATE MRModelTbl SET NewRMSD = 0.0568808 * EXP(1.52198 * (1.0 - SequenceIdentity/100.0)) * POW(172.53 + NumberofResiduesinModel, 1.0/3.0);
UPDATE MRModelTbl SET NewSSMRMSD = 0.0570187 * EXP(1.62696 * (1.0 - SSMseqid/100.0)) * POW(135.416 + NumberofResiduesinModel, 1.0/3.0);
-- UPDATE MRModelTbl SET SqLLGZratio1 = SQRT(iLLG1)/TFZ1;

ALTER TABLE MRModelTbl ADD TFZcorr real;
UPDATE MRModelTbl SET TFZcorr = TFZequiv/( SQRT(2.0) * (1.0-1.0/ ( SELECT t.PointGroupOrder FROM MRTargetTbl t WHERE MRTarget_id = t.p_id ) )  ) ;



-- Create a view with unique MR calculations pruned of BLAST runs calculated multiple times if the target PDB belongs to more than one SCOPid
-- Do this with the GROUP function rather than the DISTINCT function
CREATE VIEW DistinctMRTargets AS SELECT * FROM (SELECT ROWID, * FROM MRTargetTbl GROUP BY TargetPDBid, SCOPid) ORDER BY ROWID;

CREATE TABLE AnnotationTbl AS SELECT * FROM AnnotationTbl ORDER BY MRTarget_id, MRModel_id, CCglobal DESC;
CREATE INDEX MR2idx ON AnnotationTbl(MRModel_id);

CREATE VIEW UniqueMRresultsTbl AS SELECT 
t.TargetPDBid, 
t.SCOPid, 
t.NumberofUniqueSCOPids, 
t.TargetClass, 
t.TargetFold, 
t.TargetSuperfamily, 
t.TargetFamily, 
t.Spacegroup, 
t.Spacegroupnumber, 
t.IsPolarSpacegroup, 
t.Crystalsystem, 
t.MatthewsCoefficient, 
t.NumberofResiduesinTarget, 
t.Resolution, 
t.Unitcell, 
t.WilsonB, 
t.WilsonScale, 
t.TargetScattering, 
t.UsedReflections, 
t.Allreflections, 
t.ASUvolume,
t.Robs,
t.Rall,
t.Rwork,
t.Rfree,
t.EDSentry,
t.PointGroupOrder,
t.SpaceGroupOrder,

m.p_id, 
m.Fullmodel, 
m.NewRMSD,
m.NewSSMRMSD,
m.Comment, 
m.ModelOrder, 
m.Foundit, 
m.SolutionRank,
m.CorrectSolution,
m.SqLLGZratio,
(SELECT n.Tstar FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS Tstar,
(SELECT n.CCglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCglobal,
CCglobalTemplate,
(SELECT COUNT(*) FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS NumberofSolutions, 
m.PartialModel,
m.ModelResolution, 
m.SequenceIdentity, 
m.SSMseqid,
m.ModelCompleteness, 
m.FullModelCompleteness, 
m.CPUtime, 
m.Annotation, 
m.ClustalWlength,
m.SSMlength,
m.C_L_rms, 
m.SSMrms, 
(SELECT s.VRMS FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS TmplVRMS, 
(SELECT s.VRMSCombined FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMSCombined, 
(SELECT s.VRMSL2 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMSL2, 
(SELECT s.VRMS1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMS1, 
(SELECT s.VRMS2 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMS2, 
(SELECT s.VRMSfullres FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMSfullres, 
m.Fracvar, 
(SELECT s.FracvarVrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS FracvarTmplVrms, 
(SELECT s.FracvarVrmsCombined FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrmsCombined, 
(SELECT s.FracvarVrmsSum FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrmsSum, 
(SELECT s.FracvarVrmsL2 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrmsL2, 
(SELECT s.FracvarVrms1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrms1, 
(SELECT s.FracvarVrms2 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrms2, 
(SELECT s.FracvarVrmsfullres FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrmsfullres, 
(SELECT s.Bfac1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Bfac1, 
(SELECT s.Bfac2 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Bfac2 ,
m.ModelSCOPid, 
m.NumberofUniqueModelSCOPids, 
m.ModelClass, 
m.ModelFold, 
m.ModelSuperfamily, 
m.ModelFamily, 
m.NumberofResiduesinModel, 
m.LLGtemplate, 
m.LLGreflectiontemplate, 
m.RFZ0, 
m.TFZ0, 
m.TFZequiv,
m.TFZcorr,
m.PAK0, 
m.iLLGtemplate,
m.iLLG0,
m.LLG0, 
m.LLGreflection0, 

m.TFZ0RMSc_l,
m.TFZequivRMSc_l,
m.iLLGRMSc_l,
m.CorrectSolutionc_l,
m.SolutionRankRMSc_l,
m.CCglobalRMSc_l,
m.MLADc_l,

m.TFZ0RMS5,
m.TFZequivRMS5,
m.iLLGRMS5,
m.CorrectSolutionRMS5,
m.SolutionRankRMS5,
m.CCglobalRMS5, 
m.MLADRMS5,

m.TFZ0RMS5p1sig,
m.TFZequivRMS5p1sig,
m.iLLGRMS5p1sig,
m.CorrectSolutionRMS5p1sig,
m.SolutionRankRMS5p1sig,
m.CCglobalRMS5p1sig,
m.MLADRMS5p1sig, 

m.TFZ0RMS5m1sig,
m.TFZequivRMS5m1sig,
m.iLLGRMS5m1sig,
m.CorrectSolutionRMS5m1sig,
m.SolutionRankRMS5m1sig,
m.CCglobalRMS5m1sig,
m.MLADRMS5m1sig, 

m.TFZ0RMS5p05sig,
m.TFZequivRMS5p05sig,
m.iLLGRMS5p05sig,
m.CorrectSolutionRMS5p05sig,
m.SolutionRankRMS5p05sig,
m.CCglobalRMS5p05sig,
m.MLADRMS5p05sig, 

m.TFZ0RMS5m05sig,
m.TFZequivRMS5m05sig,
m.iLLGRMS5m05sig,
m.CorrectSolutionRMS5m05sig,
m.SolutionRankRMS5m05sig,
m.CCglobalRMS5m05sig,
m.MLADRMS5m05sig, 
(SELECT s.LLGcomb_vrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGcomb_vrms,
(SELECT s.LLGcomb_vrms_refl FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGcomb_vrms_refl,
(SELECT s.LLGfsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGfsol, 
(SELECT s.VRMSfsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMSfsol, 
(SELECT s.fracvarvrmsfsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS fracvarvrmsfsol, 
(SELECT s.Bsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Bsol, 
(SELECT s.TFZfsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS TFZfsol, 
(SELECT s.TFZfsolequiv FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS TFZfsolequiv, 
(SELECT s.Fsol FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Fsol, 
(SELECT s.LLGrefl_vrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGrefl_vrms, 
(SELECT s.LLGreflection FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflection, 
(SELECT s.LLGvrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGvrms, 
(SELECT s.LLGfullres_vrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGfullres_vrms, 
(SELECT s.LLGfullres_refl_vrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGfullres_refl_vrms, 
m.iLLGfullres0 
FROM MRModelTbl m, DistinctMRTargets t WHERE m.MRTarget_id = t.p_id
;


CREATE VIEW UniqueMRresultsTbl AS SELECT 
t.IsigIgrt3res,
t.hires,
t.lowres,
t.outliers,
t.icerings,
t.twinLtest,
t.TargetPDBid, 
t.SCOPid, 
t.NumberofUniqueSCOPids, 
t.TargetClass, 
t.TargetFold, 
t.TargetSuperfamily, 
t.TargetFamily, 
t.Spacegroup, 
t.Spacegroupnumber, 
t.IsPolarSpacegroup, 
t.Crystalsystem, 
t.MatthewsCoefficient, 
t.NumberofResiduesinTarget, 
t.Unitcell, 
t.WilsonB, 
t.WilsonScale, 
t.TargetScattering, 
m.UsedReflections, 
t.Allreflections, 
t.ASUvolume,
t.Robs,
t.Rall,
t.Rwork,
t.Rfree,
t.EDSentry,
m.SolutionExists,
m.PartialSolutionExists,
m.MRTarget_id,
m.p_id, 
m.Fullmodel, 
m.NewRMSD,
m.NewSSMRMSD,
m.Comment, 
m.ModelOrder, 
m.VRMSLLGmathstr,
m.Foundit, 
--m.SolutionRank,
m.Exitcode,
m.NumberofSolutions,
m.SqLLGZratio0,
t.PointGroupOrder,
t.SpaceGroupOrder,
m.TriedOCCrefining,
--m.NrOccRefParam,
--m.NrOccThresholds,
--m.NrOccOffsets, 
--m.LLGoccfracmerged, 
--m.LLGsoccfracrefbound,
--m.AvgLLGsoccfracunbound,
--m.AvgLLGsoccfracbound,

(SELECT n.Tstar FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS Tstar,
(SELECT n.LLGreflection FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS LLGreflection, 
(SELECT n.SolutionRank FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS SolutionRank,
--(SELECT COUNT(*) FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS NumberofSolutions, 
(SELECT MAX(n.TFZ) FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS MaxTFZ, 
(SELECT n.LLGensmbl0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS LLGensmbl0, 

(SELECT n.CCglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCglobal,
(SELECT n.CCglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id AND n.SolutionRank=1) AS CCglobalrank1,
(SELECT n.LLGvrms FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS LLGvrms, 
(SELECT n.LLGvrms FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id AND n.SolutionRank=1) AS LLGvrmsrank1, 

(SELECT n.LLGrefl_vrms FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS LLGreflvrms, 
(SELECT n.CCmap0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCmap0,
(SELECT n.CCpdb0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCpdb0,
(SELECT n.MLAD0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS MLAD0,
(SELECT n.Bfac0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS Bfac0, 
(SELECT n.FracvarVrms0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS FracvarVrms0, 
(SELECT n.AvgSigmaaVrms0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS AvgSigmaaVrms0, 
(SELECT n.VRMS0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS VRMS0, 
(SELECT n.Nresoccwnd FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS Nresoccwnd,
(SELECT n.ELLGoccwnd FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS ELLGoccwnd,
(SELECT n.LLGocc FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS LLGocc,
(SELECT n.CCocclocals FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCocclocals,
(SELECT n.CCoccglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCoccglobal,
(SELECT n.CCoccglobalpdb FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id AND n.MRTarget_id = t.p_id) AS CCoccglobalpdb,

m.RFZ0, 
m.TFZ0, 
m.TFZequiv,
m.TFZcorr,
m.PAK0, 
m.iLLG0,
m.LLG0, 
m.LLGreflection0, 
m.ELLG0, 
(SELECT s.CCglobal FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCglobaltmpl, 
(SELECT s.LLGvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGvrmstmpl, 
(SELECT s.LLGreflvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflvrmstmpl, 
(SELECT s.iLLGtemplate FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGtmpl, 
(SELECT s.CCmap0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCmap0tmpl, 
(SELECT s.CCpdb0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCpdb0tmpl, 
(SELECT s.MLAD0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS MLAD0tmpl, 
(SELECT s.Bfac0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Bfac0tmpl, 
(SELECT s.Fracvarvrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarvrms0tmpl, 
(SELECT s.AvgSigmaaVrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms0tmpl, 
(SELECT s.VRMS0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS VRMS0tmpl, 
(SELECT s.RefinedModel FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS RefinedModeltmpl, 
(SELECT s.LLGensmbl0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl0tmpl, 
(SELECT n.Nresoccwnd FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS Nresoccwndtmpl,
(SELECT n.ELLGoccwnd FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS ELLGoccwndtmpl,
(SELECT n.LLGocc FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS LLGocctmpl,
(SELECT n.CCocclocals FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCocclocalstmpl,
(SELECT n.CCoccglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobaltmpl,
(SELECT n.CCoccglobalpdb FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobalpdbtmpl,
--(SELECT n.NrOccRefParam FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS NrOccRefParamtmpl,
--(SELECT n.NrOccThresholds FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS NrOccThresholdstmpl,
--(SELECT n.NrOccOffsets FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS NrOccOffsetstmpl,
--(SELECT n.LLGoccfracmerged FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS LLGoccfracmergedtmpl,
--(SELECT n.LLGsoccfracrefbound FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS LLGsoccfracrefboundtmpl,
--(SELECT n.AvgLLGsoccfracunbound FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS AvgLLGsoccfracunboundtmpl,
--(SELECT n.AvgLLGsoccfracbound FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS AvgLLGsoccfracboundtmpl,


m.PartialModel,
m.ModelResolution, 
m.SequenceIdentity, 
m.SSMseqid,
m.ModelCompleteness, 
m.FullModelCompleteness, 
m.CPUtime, 
m.Annotation, 
m.ClustalWlength,
m.SSMlength,
m.C_L_rms, 
m.SSMrms, 
m.Fracvar, 
m.AvgSigmaA, 
m.ModelSCOPid, 
m.NumberofUniqueModelSCOPids, 
m.ModelClass, 
m.ModelFold, 
m.ModelSuperfamily, 
m.ModelFamily, 
m.NumberofResiduesinModel, 

m.iLLGfullres0 
FROM MRModelTbl m, DistinctMRTargets t WHERE m.MRTarget_id = t.p_id;


-- For RNP on template solutions only

CREATE VIEW UniqueMRresultsTbl AS SELECT 
t.IsigIgrt3res,
t.hires,
t.lowres,
t.outliers,
t.icerings,
t.twinLtest,
t.TargetPDBid, 
t.SCOPid, 
t.NumberofUniqueSCOPids, 
t.TargetClass, 
t.TargetFold, 
t.TargetSuperfamily, 
t.TargetFamily, 
t.Spacegroup, 
t.Spacegroupnumber, 
t.IsPolarSpacegroup, 
t.Crystalsystem, 
t.MatthewsCoefficient, 
t.NumberofResiduesinTarget, 
t.hires AS Resolution, 
t.Unitcell, 
t.WilsonB, 
t.WilsonScale, 
t.TargetScattering, 
m.UsedReflections, 
t.Allreflections, 
t.ASUvolume,
t.Robs,
t.Rall,
t.Rwork,
t.Rfree,
t.EDSentry,
m.SolutionExists,
m.PartialSolutionExists,
m.MRTarget_id,
m.p_id, 
m.Fullmodel, 
m.NewRMSD,
m.NewSSMRMSD,
m.Comment, 
m.ModelOrder, 
m.VRMSLLGmathstr,
m.Foundit, 
m.SolutionRank,
m.SqLLGZratio0,

m.SqLLGZratio1,

(SELECT n.CCglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCglobal,
(SELECT n.CCfcglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCfcglobal,
(SELECT s.LLGvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLG0, 
(SELECT s.LLGreflvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflvrms, 
(SELECT n.CCmap0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCmap0,
(SELECT n.CCfcmap0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCfcmap0,
(SELECT n.CCpdb0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCpdb0,
(SELECT n.MLAD0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS MLAD0,
(SELECT s.Bfac0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Bfac0, 
(SELECT s.FracvarVrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrms0, 
(SELECT s.AvgSigmaaVrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms0, 
(SELECT s.Fracvarhilores0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarhilores0, 
(SELECT s.AvgSigmaahilores0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaahilores0, 
(SELECT n.CCgoodSigmaA0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCgoodSigmaA0,
(SELECT s.VRMS0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS VRMS0, 
(SELECT n.Nresoccwnd FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS Nresoccwnd,
(SELECT n.ELLGoccwnd FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS ELLGoccwnd,
(SELECT n.LLGocc FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS LLGocc,
(SELECT n.CCocclocals FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCocclocals,
(SELECT n.CCoccglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobal,
(SELECT n.CCfcoccglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCfcoccglobal,
(SELECT n.CCoccglobalpdb FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobalpdb,
(SELECT s.OCCfac0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS OCCfac0, 

m.RFZ0, 
m.TFZ0, 
m.TFZequiv,
m.PAK0, 
m.iLLG0,
--m.LLG0, 
m.LLGreflection0, 
m.ELLG0, 
m.VELLG0, 
(SELECT s.CCglobal FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCglobaltmpl, 
(SELECT n.CCfcglobal FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCfcglobaltmpl,
(SELECT s.LLGvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGvrmstmpl, 
(SELECT s.LLGreflvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflvrmstmpl, 
(SELECT s.iLLGtemplate FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGtmpl, 
(SELECT s.CCmap0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCmap0tmpl, 
(SELECT n.CCfcmap0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCfcmap0tmpl,
(SELECT s.CCpdb0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCpdb0tmpl, 
(SELECT s.MLAD0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS MLAD0tmpl, 
(SELECT s.Bfac0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Bfac0tmpl, 
(SELECT s.Fracvarvrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarvrms0tmpl, 
(SELECT s.AvgSigmaaVrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms0tmpl, 
(SELECT s.Fracvarhilores0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarhilores0tmpl, 
(SELECT s.AvgSigmaahilores0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaahilores0tmpl, 
(SELECT n.CCgoodSigmaA0 FROM TemplateRMSTbl n WHERE n.MRModel_id = m.p_id) AS CCgoodSigmaA0tmpl,
(SELECT s.VRMS0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS VRMS0tmpl, 
(SELECT s.RefinedModel FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS RefinedModeltmpl, 
(SELECT s.LLGensmbl0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl0tmpl, 



m.PartialModel,
m.ModelResolution, 
m.SequenceIdentity, 
m.SSMseqid,
m.ModelCompleteness, 
m.FullModelCompleteness, 
m.CPUtime, 
m.Annotation, 
m.ClustalWlength,
m.SSMlength,
m.C_L_rms, 
m.SSMrms, 
m.Fracvar, 
m.AvgSigmaA, 
m.ModelSCOPid, 
m.NumberofUniqueModelSCOPids, 
m.ModelClass, 
m.ModelFold, 
m.ModelSuperfamily, 
m.ModelFamily, 
m.NumberofResiduesinModel, 

m.iLLGfullres0 
FROM MRModelTbl m, DistinctMRTargets t WHERE m.MRTarget_id = t.p_id;


-- For 2component systems

CREATE VIEW UniqueMRresultsTbl AS SELECT 
t.IsigIgrt3res,
t.hires,
t.lowres,
t.outliers,
t.icerings,
t.twinLtest,
t.TargetPDBid, 
t.SCOPid, 
t.NumberofUniqueSCOPids, 
t.TargetClass, 
t.TargetFold, 
t.TargetSuperfamily, 
t.TargetFamily, 
t.Spacegroup, 
t.Spacegroupnumber, 
t.IsPolarSpacegroup, 
t.Crystalsystem, 
t.MatthewsCoefficient, 
t.NumberofResiduesinTarget, 
t.hires AS Resolution, 
t.Unitcell, 
t.WilsonB, 
t.WilsonScale, 
t.TargetScattering, 
m.UsedReflections, 
t.Allreflections, 
t.ASUvolume,
t.Robs,
t.Rall,
t.Rwork,
t.Rfree,
t.EDSentry,
m.SolutionExists,
m.PartialSolutionExists,
m.TriedOCCrefining,
m.MRTarget_id,
m.p_id, 
m.Fullmodel, 
m.NewRMSD,
m.NewSSMRMSD,
m.Comment, 
m.ModelOrder, 
m.VRMSLLGmathstr,
m.Foundit, 
m.SolutionRank,
m.SqLLGZratio0,

m.SqLLGZratio1,

(SELECT n.Tstar FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS Tstar,
(SELECT s.LLGreflection FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflection, 
(SELECT COUNT(*) FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS NumberofSolutions, 
(SELECT MAX(s.TFZ) FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS MaxTFZ, 
(SELECT s.LLGensmbl0 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl0, 

(SELECT n.CCglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCglobal,
(SELECT s.LLGvrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGvrms, 
(SELECT s.LLGrefl_vrms FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflvrms, 
(SELECT n.CCmap0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCmap0,
(SELECT n.CCpdb0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCpdb0,
(SELECT n.MLAD0 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS MLAD0,
(SELECT s.Bfac0 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Bfac0, 
(SELECT s.FracvarVrms0 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrms0, 
(SELECT s.VRMS0 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMS0, 
(SELECT n.Nresoccwnd FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS Nresoccwnd,
(SELECT n.ELLGoccwnd FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS ELLGoccwnd,
(SELECT n.LLGocc FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS LLGocc,
(SELECT n.CCocclocals FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCocclocals,
(SELECT n.CCoccglobal FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobal,
(SELECT n.CCoccglobalpdb FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCoccglobalpdb,

m.RFZ0, 
m.TFZ0, 
m.TFZequiv,
m.PAK0, 
m.iLLG0,
m.LLG0, 
m.LLGreflection0, 
m.ELLG0, 
(SELECT s.CCglobal FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCglobaltmpl, 
(SELECT s.LLGvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGvrmstmpl, 
(SELECT s.LLGreflvrms FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGreflvrmstmpl, 
(SELECT s.iLLGtemplate FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGtmpl, 
-- CCmap for template solution of first model alone
(SELECT s.CCmap0 FROM TemplateRMSTbl s WHERE s.MRTarget_id=t.p_id AND instr(m.Fullmodel, s.RefinedModel)=1 AND m.Fullmodel != s.RefinedModel) AS CCmap0singltmpl,
-- CCmap for template solution of first model in presence of second model
(SELECT s.CCmap0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCmap0tmpl, 
(SELECT s.CCpdb0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCpdb0tmpl, 
(SELECT s.MLAD0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS MLAD0tmpl, 
(SELECT s.Bfac0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Bfac0tmpl, 
(SELECT s.Fracvarvrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarvrms0tmpl, 
(SELECT s.AvgSigmaaVrms0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms0tmpl, 
(SELECT s.VRMS0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS VRMS0tmpl, 
(SELECT s.RefinedModel FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS RefinedModeltmpl, 
(SELECT s.LLGensmbl0 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl0tmpl, 
(SELECT s.LLGocc FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGocctmpl, 
(SELECT s.CCocclocals FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCocclocalstmpl,
(SELECT s.CCoccglobal FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCoccglobaltmpl,
(SELECT s.CCoccglobalpdb FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCoccglobalpdbtmpl,


(SELECT s.CCmap1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCmap1tmpl, 
(SELECT s.CCpdb1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS CCpdb1tmpl, 
(SELECT s.MLAD1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS MLAD1tmpl, 
(SELECT s.Bfac1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Bfac1tmpl, 
(SELECT s.Fracvarvrms1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS Fracvarvrms1tmpl, 
(SELECT s.AvgSigmaaVrms1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms1tmpl, 
(SELECT s.VRMS1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS VRMS1tmpl, 
(SELECT s.LLGensmbl1 FROM TemplateRMSTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl1tmpl, 
(SELECT n.CCmap1 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCmap1,
(SELECT n.CCpdb1 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS CCpdb1,
(SELECT n.MLAD1 FROM AnnotationTbl n WHERE n.MRModel_id = m.p_id) AS MLAD1,
(SELECT s.Bfac1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS Bfac1, 
(SELECT s.FracvarVrms1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS FracvarVrms1, 
(SELECT s.AvgSigmaaVrms1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS AvgSigmaaVrms1, 
(SELECT s.VRMS1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS VRMS1, 
(SELECT s.LLGensmbl1 FROM AnnotationTbl s WHERE s.MRModel_id = m.p_id) AS LLGensmbl1, 
m.RFZ1, 
m.TFZ1, 
m.PAK1, 
m.iLLG1,
m.LLG1, 
m.LLGreflection1, 
m.ELLG1, 

m.PartialModel,
m.ModelResolution, 
m.SequenceIdentity, 
m.SSMseqid,
m.ModelCompleteness, 
m.FullModelCompleteness, 
m.CPUtime, 
m.Annotation, 
m.ClustalWlength,
m.SSMlength,
m.C_L_rms, 
m.SSMrms, 
m.Fracvar, 
m.AvgSigmaA, 
m.ModelSCOPid, 
m.NumberofUniqueModelSCOPids, 
m.ModelClass, 
m.ModelFold, 
m.ModelSuperfamily, 
m.ModelFamily, 
m.NumberofResiduesinModel, 

m.iLLGfullres0 
FROM MRModelTbl m, DistinctMRTargets t WHERE m.MRTarget_id = t.p_id;




-- make a view for the SCOP targetclasses and ditch those pdbs that belongs to more than one class
CREATE VIEW TargetClasses AS SELECT TargetClass 
FROM (SELECT m.TargetClass FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids < 2 GROUP BY m.TargetClass);

-- make a view for the SCOP targetfolds and ditch those pdbs that belongs to more than one folds
CREATE VIEW TargetFolds AS SELECT TargetFold 
FROM (SELECT m.TargetFold FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids < 2 GROUP BY m.TargetFold);

-- make a view for the SCOP TargetSuperfamily and ditch those pdbs that belongs to more than one Superfamily
CREATE VIEW TargetSuperfamilies AS SELECT TargetSuperfamily 
FROM (SELECT m.TargetSuperfamily FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids < 2 GROUP BY m.TargetSuperfamily);

-- make a view for the SCOP TargetFamily and ditch those pdbs that belongs to more than one Family
CREATE VIEW TargetFamilies AS SELECT TargetFamily 
FROM (SELECT m.TargetFamily FROM UniqueMRresultsTbl m where m.NumberofUniqueSCOPids < 2 GROUP BY m.TargetFamily);

-- make a view for targetpdbs in unique folds as above
CREATE VIEW TargetPDBperFold AS SELECT m.TargetFold, COUNT(DISTINCT m.TargetPDBid) AS nTargetPDBs 
FROM UniqueMRresultsTbl m, TargetFolds t WHERE t.TargetFold = m.TargetFold GROUP BY m.TargetFold;


-- make a view for all MR cases
CREATE VIEW AllProteins AS SELECT * FROM UniqueMRresultsTbl m 
WHERE m.Comment NOT LIKE "unused%" AND m.Comment NOT LIKE "twinned%" AND m.twinLtest < 3.5;

-- make a view for solved MR cases
CREATE VIEW AllProteinsSolved AS SELECT *, 
CASE WHEN ( SELECT MAX(u.TFZ) 
FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id) = ( SELECT u.TFZ FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id AND u.Tstar = 1 ) OR m.NumberofSolutions = 1 THEN 1 ELSE 0 END AS CorrectTFZtopmost, 
CASE WHEN ( SELECT MAX(u.LLG) 
FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id) = ( SELECT u.LLG FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id AND u.Tstar = 1 ) OR m.NumberofSolutions = 1 THEN 1 ELSE 0 END AS CorrectLLGtopmost, 
CASE WHEN ( SELECT MAX(u.LLGvrms) 
FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id) = ( SELECT u.LLGvrms FROM AnnotationTbl u WHERE u.MRModel_id = m.p_id AND u.Tstar = 1 ) OR m.NumberofSolutions = 1 THEN 1 ELSE 0 END AS CorrectLLGvrmstopmost 
FROM AllProteins m WHERE m.CorrectSolution > 0;

-- make a view for unsolved MR cases
CREATE VIEW AllProteinsUnSolved AS SELECT * FROM AllProteins m WHERE m.CorrectSolution < 1;

-- make a view for unsolved MR cases, targets belonging to a unique SCOPid
CREATE VIEW ProteinsUnSolved AS SELECT * FROM AllProteins m 
WHERE m.CorrectSolution<1 
AND m.Comment NOT LIKE "solved%" 
AND m.TargetClass IN TargetClasses;

-- make a view for solved MR cases, targets belonging to a unique SCOPid
CREATE VIEW ProteinsSolved AS SELECT * FROM AllProteins m 
WHERE m.CorrectSolution > 0 
AND m.TargetClass IN TargetClasses;

CREATE VIEW Proteins AS SELECT * FROM AllProteins m 
WHERE m.TargetClass IN TargetClasses;

-- define borderline cases to have LLG of template solutions between 20 and 90 and CCglobal > 0.2
CREATE VIEW BorderlineCases AS SELECT * FROM AllProteins 
WHERE (CCglobalTemplate > 0.2 AND iLLGtemplate > 20) AND iLLGtemplate < 90;

CREATE VIEW BorderlineSolved AS SELECT * FROM AllProteins 
WHERE (CCglobalTemplate > 0.2 AND iLLGtemplate > 20) AND iLLGtemplate < 90 AND CorrectSolution > 0;



CREATE VIEW TwoComponentRealMR AS SELECT m.MRTarget_id, m.TargetPDBid AS TargetPDBid, m.Fullmodel AS Fullmodel, 
m.PartialModel, t.RefinedModel, m.CCglobal AS CCglobal, t.CCmap0 AS CC0, m.CCmap1 AS CC1, 
t2.CCmap0 AS CC0comb, t2.CCmap1 AS CC1comb, 
t.LLGvrms AS LLG0, (m.LLGvrms - t.LLGvrms) AS LLG1, m.Bfac0, m.Bfac1, m.MLAD0, m.MLAD1, 
m.TFZ0 AS TFZ1, m.LLGvrms AS LLGtotal, (m.Fracvarvrms1 + m.Fracvarvrms0) AS FracvarVrmsSum, 
m.LLGreflvrms AS LLG1refl, m.FullModelCompleteness AS FullModelCompleteness, m.PAK0 as PAK0,
m.Resolution, m.IsPolarSpacegroup, m.SolutionExists AS SolutionExists,
m.PartialSolutionExists AS PartialSolutionExists, m.NumberofResiduesinTarget, m.NumberofResiduesinModel
FROM AllProteins m, TemplateRMSTbl t , TemplateRMSTbl t2 
WHERE m.PartialModel != t.RefinedModel 
AND m.FullModel != t.RefinedModel 
AND instr(m.Fullmodel, t.RefinedModel) > 0 -- conditions for RNP of template for first component alone
AND m.FullModel = t2.RefinedModel  -- condition for RNP of template for both components combined
AND t.MRTarget_id = m.MRTarget_id 
AND t2.MRTarget_id = m.MRTarget_id 
AND m.modelorder = 2 
AND m.CCmap1 IS NOT NULL 
AND t.CCmap0 IS NOT NULL 
AND m.LLGvrms IS NOT NULL
AND t.LLGvrms IS NOT NULL
AND FullModelCompleteness < 1
AND FracvarVrmsSum < 1
;

CREATE VIEW TwoComponentMROCC AS SELECT m.TargetPDBid AS TargetPDBid, m.Fullmodel AS Fullmodel, 
m.PartialModel, t.RefinedModel, m.CCglobal AS CCglobal, t.CCmap0 AS CC0, m.CCmap1 AS CC1, 
t.LLGvrms AS LLG0, (m.LLGvrms - t.LLGvrms) AS LLG1, m.Bfac0, m.Bfac1, m.MLAD0, m.MLAD1, 
m.TFZ0 AS TFZ1, m.LLGvrms AS LLGtotal, m.Resolution, m.IsPolarSpacegroup, m.SolutionExists AS OccSolutionExists,
m.PartialSolutionExists AS PartialOccSolutionExists, m.NumberofResiduesinTarget, m.NumberofResiduesinModel, 
(m.LLGocc - t.LLGocc) AS LLGocc1, t.LLGocc AS LLGocc0, m.CCoccglobal AS CCocc01, t.CCoccglobal AS CCocc0
FROM AllProteins m, TemplateRMSTbl t 
WHERE m.PartialModel != t.RefinedModel 
AND m.FullModel != t.RefinedModel 
AND instr(m.Fullmodel, t.RefinedModel) > 0
AND t.MRTarget_id = m.MRTarget_id 
AND m.modelorder = 2 
AND m.CCmap1 IS NOT NULL 
AND t.CCmap0 IS NOT NULL 
AND m.LLGvrms IS NOT NULL
AND t.LLGvrms IS NOT NULL
AND t.LLGocc > t.LLGvrms
;




CREATE VIEW TwoComponentContrivedMR AS SELECT f.TargetPDBid AS TargetPDBid, f.Fullmodel AS Fullmodel, 
f.CCglobal AS CCglobal, f.CCmap1 AS CC1, f.CCmap0 AS CC0, f.LLGvrms AS LLGtotal, 
 (f.LLGvrms - f.LLGensmbl0) AS LLG1, f.LLGensmbl0 AS LLG0,
 f.TFZ0, f.TFZ1, f.Bfac0, f.Bfac1, f.Resolution, f.MLAD0, f.MLAD1 
FROM AllProteins f
WHERE f.CCmap1 IS NOT NULL
AND f.CCmap0 IS NOT NULL
AND f.LLGensmbl0 IS NOT NULL
AND f.LLGensmbl1 IS NOT NULL
AND f.LLGvrms IS NOT NULL
AND f.Modelorder = 2 
;



-- create view for ELLGs of OCCrefinement windows using window sizes 1,3,5,7,9,13
CREATE VIEW ELlgOccTbl AS 
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd, Nresoccwnd, CCoccglobal, CCglobal, (CCoccglobal-CCglobal) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd IS NOT NULL AND SolutionExists = 1

SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd1 AS ELLGoccwnd, Nresoccwnd1 AS Nresoccwnd, CCoccglobal1 AS CCoccglobal, CCmap0, (CCoccglobal1-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd1 IS NOT NULL
UNION ALL
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd3 AS ELLGoccwnd, Nresoccwnd3 AS Nresoccwnd, CCoccglobal3 AS CCoccglobal, CCmap0, (CCoccglobal3-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd3 IS NOT NULL

UNION ALL
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd5 AS ELLGoccwnd, Nresoccwnd5 AS Nresoccwnd, CCoccglobal5 AS CCoccglobal, CCmap0, (CCoccglobal5-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd5 IS NOT NULL
UNION ALL
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd7 AS ELLGoccwnd, Nresoccwnd7 AS Nresoccwnd, CCoccglobal7 AS CCoccglobal, CCmap0, (CCoccglobal7-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd7 IS NOT NULL
UNION ALL
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd9 AS ELLGoccwnd, Nresoccwnd9 AS Nresoccwnd, CCoccglobal9 AS CCoccglobal, CCmap0, (CCoccglobal9-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd9 IS NOT NULL
UNION ALL
SELECT TargetPDBid, Fullmodel, LLG0, LLGocc, (LLGocc - LLG0) AS dLLG, ELLG0, ELLGoccwnd13 AS ELLGoccwnd, Nresoccwnd13 AS Nresoccwnd, CCoccglobal13 AS CCoccglobal, CCmap0, (CCoccglobal13-CCmap0) AS dCC 
FROM AllPRoteins WHERE ELLGoccwnd13 IS NOT NULL
;


CREATE VIEW ELlgOccTbl AS 
SELECT TargetPDBid, Fullmodel, LLGvrmstmpl, LLGocctmpl, (LLGocctmpl - LLGvrmstmpl) AS dLLG, 
LLGoccfracmergedtmpl, (LLGoccfracmergedtmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGoccfracmerged,

(LLGocctmpl - (LLGvrmstmpl + NrOccRefParamtmpl)) AS dLLGaicrefparm, NrOccRefParamtmpl, 
(LLGocctmpl - (LLGvrmstmpl + NrOccThresholdstmpl)) AS dLLGaicthrshld, NrOccThresholdstmpl, 
ELLG0, ELLGoccwndtmpl, Nresoccwndtmpl, CCoccglobaltmpl, CCglobaltmpl, (CCoccglobaltmpl-CCglobaltmpl) AS dCC 
FROM AllPRoteins WHERE ELLGoccwndtmpl IS NOT NULL AND SolutionExists = 1
;



-- Comment on those special cases that are outliers
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but high seqid yields a too small RMS. LLG consequently became negative" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LV7" AND m.Fullmodel="2R62_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"                       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NN7" AND m.Fullmodel="2R9R_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T80" AND m.Fullmodel="1KPR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1A32" AND m.Fullmodel="2QOU_O");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XHD" AND m.Fullmodel="1OCX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1U5P" AND m.Fullmodel="1CUN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NWM" AND m.Fullmodel="1YD8_G");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1H9J" AND m.Fullmodel="1B9N_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"                       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1E2S" AND m.Fullmodel="6RLX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2GKE" AND m.Fullmodel="2OTN_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"                       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PIE" AND m.Fullmodel="1PM3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NE4" AND m.Fullmodel="3CL1_A");
UPDATE MRModelTbl SET Comment="possibly solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BFN" AND m.Fullmodel="1CQW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3DEO" AND m.Fullmodel="2QYJ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2I9I" AND m.Fullmodel="3BGH_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LI4" AND m.Fullmodel="3GVP_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, modelcompleteness is very small making it difficult ever matching the target" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MMN" AND m.Fullmodel="1MVF_D");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B22" AND m.Fullmodel="2R32_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CW1" AND m.Fullmodel="1WPW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1FHG" AND m.Fullmodel="2A38_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved when using PACK SELECT ALLOW"   WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1G97" AND m.Fullmodel="3FWW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZJC" AND m.Fullmodel="2AYI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R6N" AND m.Fullmodel="1TUE_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CW1" AND m.Fullmodel="1V94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved possible twinning, Z score L-test: 3.994 although no twin law exist"       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1G95" AND m.Fullmodel="2V0H_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1USE" AND m.Fullmodel="1USD_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2NQI" AND m.Fullmodel="2ASS_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PUC" AND m.Fullmodel="2ASS_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AZZ" AND m.Fullmodel="1OYF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1H76" AND m.Fullmodel="2PMS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XHB" AND m.Fullmodel="2D7I_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CW1" AND m.Fullmodel="2DHT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2NQI" AND m.Fullmodel="1ZIV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HUP" AND m.Fullmodel="1KX0_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CW1" AND m.Fullmodel="2IV0_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KBL" AND m.Fullmodel="1VBG_A");


UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BCY" AND m.Fullmodel="1AOW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template misaligned by one helix turn, modelmap-target correlation = 0.71, templatemap-target correlation = 0.12"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2Z5Q" AND m.Fullmodel="1BG7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong templatemap-target correlation = 0.01 modelmap-target correlation = 0.25"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O3F" AND m.Fullmodel="2HNT_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved with modelmap-target correlation = 0.25"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NM9" AND m.Fullmodel="1BAG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved with modelmap-target correlation = 0.13"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2GUF" AND m.Fullmodel="1FEP_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved but template was wrong modelmap-target correlation = 0.24"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O2R" AND m.Fullmodel="2HNT_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong templatemap-target correlation = 0.03 modelmap-target correlation = 0.24"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2NPO" AND m.Fullmodel="1V3W_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation= 0.19 modelmap-target correlation = 0.26"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R3C" AND m.Fullmodel="3FI3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved with modelmap-target correlation = 0.52"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UCQ" AND m.Fullmodel="1H2S_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong modelmap-target correlation = 0.35"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DVB" AND m.Fullmodel="1NNQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong modelmap-target correlation = 0.3"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="1MAH_F");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.31"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AK2" AND m.Fullmodel="2RGX_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved due to out of memory"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T2D" AND m.Fullmodel="3FI9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.17, modelmap-target correlation = 0.23"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2C77" AND m.Fullmodel="1D2E_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.01, modelmap-target correlation = 0.25"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7V" AND m.Fullmodel="1KPR_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved with modelmap-target correlation = 0.01"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1H68" AND m.Fullmodel="1S6C_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong templatemap-target correlation = 0.01 modelmap-target correlation = 0.26"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y3W" AND m.Fullmodel="2HNT_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong templatemap-target correlation = 0.01 modelmap-target correlation = 0.26"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y3X" AND m.Fullmodel="2HNT_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.33, modelmap-target correlation = 0.27"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2C6M" AND m.Fullmodel="3COI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrongly aligned possibly against gene duplication with modelmap-target correlation = 0.42"  WHERE  ROWID = (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BIK" AND m.Fullmodel="3BYB_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong with modelmap-target correlation = 0.58"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K33" AND m.Fullmodel="1JPX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved after solvent flattening, modelmap-target correlation = 0.27, templatemap-target correlation = 0.27"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2D5B" AND m.Fullmodel="2CSX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.28, modelmap-target correlation = 0.34"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BCY" AND m.Fullmodel="1AXN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.39, modelmap-target correlation = 0.36"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZGT" AND m.Fullmodel="1OKI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but target is a helix and template is slightly better aligned, modelmap-target correlation = 0.38"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1X8Y" AND m.Fullmodel="1GK4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrong with modelmap-target correlation = 0.49"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="1HC9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.32, modelmap-target correlation = 0.37"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R3C" AND m.Fullmodel="1PME_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template equally valid, modelmap-target correlation = 0.41"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QYJ" AND m.Fullmodel="2ZGD_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.32"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3D7Z" AND m.Fullmodel="1PME_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.36"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZJK" AND m.Fullmodel="1GPZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrongly aligned possibly against gene duplication with modelmap-target correlation = 0.44"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BIK" AND m.Fullmodel="1FAK_I");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, possibly twinned, modelmap-target correlation = 0.54, templatemap-target correlation = 0.09"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MUL" AND m.Fullmodel="1B8Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrongly aligned possibly against gene duplication with modelmap-target correlation = 0.45"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BIK" AND m.Fullmodel="1AAP_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, templatemap-target correlation = 0.32, modelmap-target correlation = 0.33"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WOY" AND m.Fullmodel="2CSX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.54"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1GU8" AND m.Fullmodel="1C8S_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template misaligned by half a helix turn, modelmap-target correlation = 0.63"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CYO" AND m.Fullmodel="1JPX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.35"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1P8F" AND m.Fullmodel="2IV0_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template misaligned by one helix turn, modelmap-target correlation = 0.65"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CP1" AND m.Fullmodel="1JPX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.45"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BUU" AND m.Fullmodel="3G84_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.46"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BUU" AND m.Fullmodel="1PW9_A");


-- for data worse than 2.5A resolution
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AMN" AND m.Fullmodel="2I0X_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CKO" AND m.Fullmodel="1P16_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1ZS8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1RGV" AND m.Fullmodel="1G6B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JZB" AND m.Fullmodel="2ASC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1ZHL_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K1S" AND m.Fullmodel="2BQ3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OLU" AND m.Fullmodel="3FWM_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved and template is wrong anyway"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2CFQ" AND m.Fullmodel="3CBX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OLU" AND m.Fullmodel="3FWM_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved and template is wrong anyway"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T5S" AND m.Fullmodel="2V3B_B");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved and template is wrong anyway"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T5S" AND m.Fullmodel="2V3B_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZYU" AND m.Fullmodel="1E6C_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template was wrongly selected to being the second domain. MR solution is the third domain."    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KT0" AND m.Fullmodel="1R9H_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved helix bundle low model completeness"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SJ8" AND m.Fullmodel="1U6H_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OKQ" AND m.Fullmodel="2JD4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1KPR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1Q94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template is different, modelmap-target correlation = 0.35"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1M9I" AND m.Fullmodel="1ALA_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BWZ" AND m.Fullmodel="3EJX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KT0" AND m.Fullmodel="1Q1C_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but template is bad (twinning?)"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1M9S" AND m.Fullmodel="2OMZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, modelmap-target correlation = 0.3"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CK7" AND m.Fullmodel="1L6J_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, (twinning?)"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MT0" AND m.Fullmodel="2HYD_A");
UPDATE MRModelTbl SET Comment="twinned,  Multivariate Z score L-test: 6.090"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1M9I");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved but SSM got template misaligned by one helix turn"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1RGV" AND m.Fullmodel="2FGO_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IY2" AND m.Fullmodel="1XWI_A");

UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain, template is wrong after VRMS refinement"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L6J" AND m.Fullmodel="1CK7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong after VRMS refinement"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HUP" AND m.Fullmodel="2KMB_1");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong after VRMS refinement"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HUP" AND m.Fullmodel="1KWZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template has a slightly higher LLG but modelmap-target correlation = 0.3"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2VA7" AND m.Fullmodel="1WKR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template has the same LLG but modelmap-target correlation = 0.36"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QY2" AND m.Fullmodel="1BSO_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong after VRMS refinement"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MDX" AND m.Fullmodel="2C7T_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong after VRMS refinement"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3D7Z" AND m.Fullmodel="1GOL_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, modelmap-target correlation = 0.26, templatemap-target correlation = 0.01"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IZ2" AND m.Fullmodel="2HI9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, modelmap-target correlation = 0.33, templatemap-target correlation = 0.01"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AK2" AND m.Fullmodel="3AKY_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, modelmap-target correlation = 0.35, templatemap-target correlation = 0.02"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AK2" AND m.Fullmodel="1AKY_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is slightly worse, modelmap-target correlation = 0.26, templatemap-target correlation = 0.23"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FK7" AND m.Fullmodel="1KPI_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template is also wrong, modelmap-target correlation = 0.01, templatemap-target correlation = 0.02"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2CFQ" AND m.Fullmodel="2REY_A");

UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B22" AND m.Fullmodel="2NPS_D");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B22" AND m.Fullmodel="1FAV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix hairpin"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OT5" AND m.Fullmodel="1C94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is totally wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VFY" AND m.Fullmodel="1DVP_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BFP" AND m.Fullmodel="1V3W_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B22" AND m.Fullmodel="1EBO_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BFP" AND m.Fullmodel="3FS8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, long helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SZT" AND m.Fullmodel="1C94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2NPO" AND m.Fullmodel="3FS8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix hairpin"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OT5" AND m.Fullmodel="1FAV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, long helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SZT" AND m.Fullmodel="1FAV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is out of register by half a helix turn"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2D8E" AND m.Fullmodel="1ECM_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved possible twinning, Z score L-test: 3.994 although no twin law exist"       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1G95" AND m.Fullmodel="3FS8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong, long helix hairpin"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K34" AND m.Fullmodel="2ZFC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QKE" AND m.Fullmodel="1HC9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix, template is totally wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2F3L" AND m.Fullmodel="2O6W_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, long helix but correct solution is ambiguous"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SZT" AND m.Fullmodel="2ZFC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="1TGX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template almost correct"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ERW" AND m.Fullmodel="1TBR_R");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JZB" AND m.Fullmodel="1T7E_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template out of register by a helix turn"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WVN" AND m.Fullmodel="1EC6_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0263"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K34" AND m.Fullmodel="1C94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2F3L" AND m.Fullmodel="2J8I_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template is wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OO2" AND m.Fullmodel="2PMR_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0206"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K33" AND m.Fullmodel="1ZW0_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0247"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L2P" AND m.Fullmodel="1CII_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WQF" AND m.Fullmodel="1EK8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template almost correct"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2GKR" AND m.Fullmodel="1TGS_I");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix with decorations mismatching template solution"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BFP" AND m.Fullmodel="3EG4_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0228"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ERL" AND m.Fullmodel="1B8A_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K33" AND m.Fullmodel="2ZFC_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0171"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZIA" AND m.Fullmodel="1TEF_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, in spite of LLG/refl=0.0211"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K33" AND m.Fullmodel="1C94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, beta helix with decorations mismatching template solution"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2NPO" AND m.Fullmodel="3EG4_A");

-- outliers apparently not solved but with correlation coefficients > 0.1
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template on wrong hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NTN" AND m.Fullmodel="2H7Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template on wrong hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NTN" AND m.Fullmodel="1KBA_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, bad model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NWM" AND m.Fullmodel="1WR6_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matching gene duplicate" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BIK" AND m.Fullmodel="1Y62_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1F94" AND m.Fullmodel="1ONJ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BED" AND m.Fullmodel="1UN2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="1CDT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, P 1 space group" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OSA" AND m.Fullmodel="1AVS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template aligned out of register on rows of helices" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1N0R" AND m.Fullmodel="2B0O_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BXE" AND m.Fullmodel="2GYA_Q");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JYB" AND m.Fullmodel="1NNQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KF0" AND m.Fullmodel="1LTK_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, bad model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="2H7Z_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matching gene duplicate" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="6FDR" AND m.Fullmodel="1FCA_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2C6M" AND m.Fullmodel="1UNG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matching gene duplicate" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OUV" AND m.Fullmodel="1KLX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="2H7Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OWJ" AND m.Fullmodel="2PR5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AD2" AND m.Fullmodel="3BBO_D");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BCY" AND m.Fullmodel="1AOW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CBS" AND m.Fullmodel="2RCQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QKE" AND m.Fullmodel="2H7Z_B");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CM3" AND m.Fullmodel="1Y50_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matching gene duplicate" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1A45" AND m.Fullmodel="2BV2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1GVH" AND m.Fullmodel="1CQX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IN5" AND m.Fullmodel="1IXS_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong as the rmsd was estimated too low" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TXD" AND m.Fullmodel="1XCG_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1QVC_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1KAW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IMX" AND m.Fullmodel="1LPH_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong " WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1Z9F_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CP1" AND m.Fullmodel="2ZFC_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V67" AND m.Fullmodel="3BSS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KF0" AND m.Fullmodel="1VPE_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1A5X" AND m.Fullmodel="1K6Y_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VFD" AND m.Fullmodel="1AOV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UV0" AND m.Fullmodel="1OZ7_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1RGV" AND m.Fullmodel="1G3O_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IS1" AND m.Fullmodel="1WQF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2DPE" AND m.Fullmodel="3CZ8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XDU" AND m.Fullmodel="1TW2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XDU" AND m.Fullmodel="1TW2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y8Y" AND m.Fullmodel="3COI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QY2" AND m.Fullmodel="1PBO_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VI7" AND m.Fullmodel="2CVE_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T45" AND m.Fullmodel="2GS7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IS1" AND m.Fullmodel="1DD5_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UHA" AND m.Fullmodel="2WGC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IMX" AND m.Fullmodel="3INS_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KF0" AND m.Fullmodel="13PK_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1X42" AND m.Fullmodel="2HOQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JO8" AND m.Fullmodel="1VYV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, solution out of register with row of helices" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1N0R" AND m.Fullmodel="2RFA_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, long helix bundle" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CYO" AND m.Fullmodel="2ZFC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1W8A" AND m.Fullmodel="2O6S_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KC7" AND m.Fullmodel="1VBG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R00" AND m.Fullmodel="1TW2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CKO" AND m.Fullmodel="1P16_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VFD" AND m.Fullmodel="1IQ7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y8Y" AND m.Fullmodel="3FI3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AK2" AND m.Fullmodel="3GMT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PIO" AND m.Fullmodel="1NHZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QVI" AND m.Fullmodel="2A4E_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, solution out of register with row of helices" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MJ0" AND m.Fullmodel="2FO1_E");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R3C" AND m.Fullmodel="2WEI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1EIL" AND m.Fullmodel="3B59_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AK2" AND m.Fullmodel="3BE4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1ZS8_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XTE" AND m.Fullmodel="2V14_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, solution out of register along beta-helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O6T" AND m.Fullmodel="1D0B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, solution out of register along beta-helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O6T" AND m.Fullmodel="1D0B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DRJ" AND m.Fullmodel="2FN9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BRG" AND m.Fullmodel="2JC6_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2IXR" AND m.Fullmodel="2J0N_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2I5D" AND m.Fullmodel="1K7K_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TXD" AND m.Fullmodel="3GF9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="3ULL_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1X3E_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, misaligned beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V67" AND m.Fullmodel="3ECT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OPK" AND m.Fullmodel="1AD5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1SYV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7X" AND m.Fullmodel="1Q94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AK2" AND m.Fullmodel="2AR7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WE2" AND m.Fullmodel="1E6C_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WKA" AND m.Fullmodel="1UDZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template upside down" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BR1" AND m.Fullmodel="2BDW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UV0" AND m.Fullmodel="1EGG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2EFK" AND m.Fullmodel="2EFL_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7V" AND m.Fullmodel="1Q94_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="1S1I_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2C6M" AND m.Fullmodel="2HOG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PMN" AND m.Fullmodel="1OVE_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UOU" AND m.Fullmodel="1AZY_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NPH" AND m.Fullmodel="1SVY_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K95" AND m.Fullmodel="1U5I_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1P3J" AND m.Fullmodel="1ZD8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template misaligned on beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O6T" AND m.Fullmodel="1H6U_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BRG" AND m.Fullmodel="3DAK_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1P7S" AND m.Fullmodel="2Z6B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QVI" AND m.Fullmodel="2A62_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OMY" AND m.Fullmodel="2I61_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1W8A" AND m.Fullmodel="2Z7X_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KBL" AND m.Fullmodel="1H6Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1H76" AND m.Fullmodel="1AOV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K1S" AND m.Fullmodel="2RDI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1S9J" AND m.Fullmodel="3C4X_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2J0W" AND m.Fullmodel="2CDQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MW7" AND m.Fullmodel="1LFP_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AK2" AND m.Fullmodel="3GMT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BC1" AND m.Fullmodel="1YCN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KR5" AND m.Fullmodel="1DL5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MC0" AND m.Fullmodel="2ZMF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matches gene duplication" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OKQ" AND m.Fullmodel="2H0B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T2D" AND m.Fullmodel="3FI9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1EXU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UBE" AND m.Fullmodel="1T4G_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KC7" AND m.Fullmodel="1H6Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1W93" AND m.Fullmodel="2DZD_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HYU" AND m.Fullmodel="2A87_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HYU" AND m.Fullmodel="3CTY_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CHU" AND m.Fullmodel="1YQ3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BC3" AND m.Fullmodel="1YCN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PMN" AND m.Fullmodel="3GBZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Z6F" AND m.Fullmodel="1XP4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model misaligned 1/3 turn along polar axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OI1" AND m.Fullmodel="3CEY_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register by one turn along beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1W8A" AND m.Fullmodel="1M0Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register by 1/3 turn along beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BSW" AND m.Fullmodel="3EG4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CHU" AND m.Fullmodel="1NEK_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MT0" AND m.Fullmodel="1G29_1");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1I2A" AND m.Fullmodel="1ZHO_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matches gene duplication" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KT0" AND m.Fullmodel="1YAT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1X42" AND m.Fullmodel="2NO4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1X42" AND m.Fullmodel="2NO5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template out of register along row of helices" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DCQ" AND m.Fullmodel="2QYJ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2I5D" AND m.Fullmodel="2PYU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B06" AND m.Fullmodel="3FK9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register by one helix turn" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VJM" AND m.Fullmodel="3DDL_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register by 1/3 turn along beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V67" AND m.Fullmodel="3CJ8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2G6G" AND m.Fullmodel="2PNW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OXS" AND m.Fullmodel="1G29_1");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2EXA" AND m.Fullmodel="1W5D_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1N42" AND m.Fullmodel="1YCN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7V" AND m.Fullmodel="1FRT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7X" AND m.Fullmodel="1EXU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JMS" AND m.Fullmodel="2BPF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register on target's 7-fold pseudo rotational axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NR0" AND m.Fullmodel="2G99_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BIB" AND m.Fullmodel="2EJ9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PBT" AND m.Fullmodel="1HOR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1RN7" AND m.Fullmodel="2CH9_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ICN" AND m.Fullmodel="1KQW_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LR7" AND m.Fullmodel="1TBR_R");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matching hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1A8F" AND m.Fullmodel="1IQ7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register on target's 7-fold pseudo rotational axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NR0" AND m.Fullmodel="3FRX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UOU" AND m.Fullmodel="2DSJ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AG8" AND m.Fullmodel="2IZZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TXD" AND m.Fullmodel="3EO2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LJ5" AND m.Fullmodel="1JJO_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MJ0" AND m.Fullmodel="1S70_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor solution" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ISE" AND m.Fullmodel="1DD5_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1E5W" AND m.Fullmodel="1MIZ_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y57" AND m.Fullmodel="2J0J_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V5G" AND m.Fullmodel="3D7K_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1U2K" AND m.Fullmodel="2CL4_X");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register on target's 7-fold pseudo rotational axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NR0" AND m.Fullmodel="2GNQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ILV" AND m.Fullmodel="2ZWI_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K1S" AND m.Fullmodel="3GQC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1U5P" AND m.Fullmodel="3FB2_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1MHC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1FQE" AND m.Fullmodel="1AOV_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register on target's 6-fold pseudo rotational axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CRZ" AND m.Fullmodel="2OJH_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QYJ" AND m.Fullmodel="1S70_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1EOV" AND m.Fullmodel="1BBU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OK5" AND m.Fullmodel="3ERB_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IPA" AND m.Fullmodel="3GYQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UOU" AND m.Fullmodel="3H5Q_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1X42" AND m.Fullmodel="2W4M_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2HS3" AND m.Fullmodel="1T3T_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K9Z" AND m.Fullmodel="2WEF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JR4" AND m.Fullmodel="3DUL_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OD7" AND m.Fullmodel="1O7V_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BFP" AND m.Fullmodel="3F1X_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QVL" AND m.Fullmodel="2BON_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1H5D" AND m.Fullmodel="1KXN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YS9" AND m.Fullmodel="2HO4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JP4" AND m.Fullmodel="2PCR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BED" AND m.Fullmodel="3FEU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BSW" AND m.Fullmodel="3F1X_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Y57" AND m.Fullmodel="3C9W_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R3C" AND m.Fullmodel="3H4J_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TXD" AND m.Fullmodel="2Z0Q_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2EWB" AND m.Fullmodel="2HB6_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Z8O" AND m.Fullmodel="3E5L_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TF2" AND m.Fullmodel="3DIN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XBA" AND m.Fullmodel="2J90_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template better" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3BZP" AND m.Fullmodel="2W0R_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, model out of register on target's 3-fold pseudo rotational axis" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2J5W" AND m.Fullmodel="3CDZ_B");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VZO" AND m.Fullmodel="2JAM_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UI0" AND m.Fullmodel="2D3Y_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, misaligned helix bundle" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2CJ6" AND m.Fullmodel="1X90_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B5E" AND m.Fullmodel="2YZU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OYW" AND m.Fullmodel="2V1X_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QO4" AND m.Fullmodel="2BOQ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WOU" AND m.Fullmodel="2E0Q_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, long helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1GK7" AND m.Fullmodel="3H80_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OXS" AND m.Fullmodel="1Q1B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DAY" AND m.Fullmodel="2W96_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1QMR" AND m.Fullmodel="1QYS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1M04" AND m.Fullmodel="1YHT_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1TF5" AND m.Fullmodel="3DIN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1M9S" AND m.Fullmodel="2O6S_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, matched hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VIS" AND m.Fullmodel="1WUU_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2QV7" AND m.Fullmodel="2BON_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1I2K" AND m.Fullmodel="3CSW_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="2J01_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="3FIN_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="unsolved, model upside down" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1KR1" AND m.Fullmodel="3EBV_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MIX" AND m.Fullmodel="1EF1_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YVM" AND m.Fullmodel="2BWS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D5C" AND m.Fullmodel="1ZD9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, out of register by a beta strand" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VCY" AND m.Fullmodel="2RCI_A");


UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1KAW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SE8" AND m.Fullmodel="1QVC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1U9P" AND m.Fullmodel="2BEC_B");

UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, model poor" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YZM" AND m.Fullmodel="1IW7_D");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, model poor" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YZM" AND m.Fullmodel="2DFS_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, model poor" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YZM" AND m.Fullmodel="3EZU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, model poor" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ERL" AND m.Fullmodel="2R37_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, out of register by a helix turn" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2KNT" AND m.Fullmodel="1NAG_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2B22" AND m.Fullmodel="2B9B_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZVA" AND m.Fullmodel="2IEQ_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZVA" AND m.Fullmodel="1WDG_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZVA" AND m.Fullmodel="1WDF_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZVA" AND m.Fullmodel="1ZV8_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1P9I" AND m.Fullmodel="2WG5_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1O06" AND m.Fullmodel="1UKL_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FD9" AND m.Fullmodel="2G50_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FD9" AND m.Fullmodel="2RCM_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FD9" AND m.Fullmodel="1KK3_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FD9" AND m.Fullmodel="3DDN_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JZB" AND m.Fullmodel="3GQT_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JZB" AND m.Fullmodel="1W07_A");


UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, poor model" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AU5" AND m.Fullmodel="2Z5H_I");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OXS" AND m.Fullmodel="2IT1_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1EOV" AND m.Fullmodel="1LYL_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MIX" AND m.Fullmodel="1EF1_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PQY" AND m.Fullmodel="1XA3_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Z05" AND m.Fullmodel="2AA4_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ML4" AND m.Fullmodel="2RGW_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, correct solution is not topmost" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V5G" AND m.Fullmodel="3EY9_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BED" AND m.Fullmodel="3C7M_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CM3" AND m.Fullmodel="1Y50_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UEK" AND m.Fullmodel="2V2Q_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BIB" AND m.Fullmodel="2BML_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="2J01_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="3FIN_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CJS" AND m.Fullmodel="2ZKR_5");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1EPU" AND m.Fullmodel="1MQS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AF3" AND m.Fullmodel="2NL9_B");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L2P" AND m.Fullmodel="1CII_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DCQ" AND m.Fullmodel="2JAB_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UHA" AND m.Fullmodel="2WGC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1IMX" AND m.Fullmodel="2INS_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BC3" AND m.Fullmodel="1M9I_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, wrong cell dimension in targetpdb" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PIR" AND m.Fullmodel="1NHZ_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OSA" AND m.Fullmodel="4TNC_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ZAW" AND m.Fullmodel="3BUJ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="9CGT" AND m.Fullmodel="3DHU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1V67" AND m.Fullmodel="3BSS_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2CA8" AND m.Fullmodel="2ON5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CR2" AND m.Fullmodel="2R6A_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DB3" AND m.Fullmodel="1Z45_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VFV" AND m.Fullmodel="3BFN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7V" AND m.Fullmodel="1A6Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R3C" AND m.Fullmodel="2B1P_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1PMN" AND m.Fullmodel="1W98_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ESP" AND m.Fullmodel="3E95_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UV0" AND m.Fullmodel="1SB2_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1UV0" AND m.Fullmodel="3BX4_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slighty wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ESU" AND m.Fullmodel="1KGG_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1P3J" AND m.Fullmodel="1ZD8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1C2A" AND m.Fullmodel="2QN5_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved for one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ULN" AND m.Fullmodel="1WGT_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LR7" AND m.Fullmodel="1TBR_R");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LR7" AND m.Fullmodel="1Z7K_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3AKY" AND m.Fullmodel="1ZD8_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1F94" AND m.Fullmodel="1V6P_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ONJ" AND m.Fullmodel="2H7Z_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AK2" AND m.Fullmodel="2AR7_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AK2" AND m.Fullmodel="3BE4_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1K33" AND m.Fullmodel="1C94_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1MJ0" AND m.Fullmodel="1S70_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T7Z" AND m.Fullmodel="1A6Z_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OD7" AND m.Fullmodel="1O7V_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2J5W" AND m.Fullmodel="1SDD_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2J5W" AND m.Fullmodel="3CDZ_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1NPH" AND m.Fullmodel="1SVY_A");


UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template slightly wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1W9I" AND m.Fullmodel="1QVI_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2J0J" AND m.Fullmodel="1QCF_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZIW" AND m.Fullmodel="1P8T_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template out of register along beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1ZIW" AND m.Fullmodel="2O6S_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, packing reveals high number of clashes" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3POY" AND m.Fullmodel="1EDM_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, two domains with a hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3JUX" AND m.Fullmodel="1NKT_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong, gene duplication" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BLF" AND m.Fullmodel="1OVB_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, hinge motion" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BLF" AND m.Fullmodel="1H76_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong, gene duplication" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BLF" AND m.Fullmodel="1LGB_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2GJK" AND m.Fullmodel="2IYB_E");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2HPI" AND m.Fullmodel="3FN5_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2HPI" AND m.Fullmodel="1I94_G");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, hinge motion" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OLS" AND m.Fullmodel="2HRO_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3PSI" AND m.Fullmodel="1R1P_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3PSI" AND m.Fullmodel="3GXW_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3RZF" AND m.Fullmodel="2JC6_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3AV4" AND m.Fullmodel="3EPZ_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved and template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3EFM" AND m.Fullmodel="2MSJ_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3AGP" AND m.Fullmodel="1OB2_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3KSY" AND m.Fullmodel="2B9D_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="TNCS in dataset MR" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2E26" AND m.Fullmodel="2DDU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PUS" AND m.Fullmodel="3FB9_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PUS" AND m.Fullmodel="2H3L_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PUS" AND m.Fullmodel="1MFG_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2PUS" AND m.Fullmodel="1IKT_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3A0F" AND m.Fullmodel="1MO1_B");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3A0F" AND m.Fullmodel="1QAV_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FYO" AND m.Fullmodel="2Q7E_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FYO" AND m.Fullmodel="1U4P_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3F94" AND m.Fullmodel="1E6K_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2JHP" AND m.Fullmodel="1UZH_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LCF" AND m.Fullmodel="2D3I_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LCF" AND m.Fullmodel="1H76_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LCF" AND m.Fullmodel="1BP5_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2V0C" AND m.Fullmodel="2BAY_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2JKB" AND m.Fullmodel="1IFR_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ZXQ" AND m.Fullmodel="2J6F_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2Y3U" AND m.Fullmodel="1F02_T");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template wrong by 1/7th turn around beta helix" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3SFZ" AND m.Fullmodel="1VYH_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="2JGN_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="3CEC_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="2HJV_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="2O49_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3PS9" AND m.Fullmodel="2NMS_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2YD0" AND m.Fullmodel="2ORM_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3LVT" AND m.Fullmodel="1HTL_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3LVT" AND m.Fullmodel="1LTI_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3LVT" AND m.Fullmodel="1LTB_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3LVT" AND m.Fullmodel="1A2X_B");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2D3I" AND m.Fullmodel="1B1X_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2D3I" AND m.Fullmodel="1BIY_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2WW8" AND m.Fullmodel="1QMO_E");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2WW8" AND m.Fullmodel="3FFY_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2WTB" AND m.Fullmodel="3H0U_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Z3I" AND m.Fullmodel="1Y9L_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1Z3I" AND m.Fullmodel="1IBT_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3AF5" AND m.Fullmodel="2CV8_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2C9K" AND m.Fullmodel="2B22_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="1JLK_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="1JK4_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="2HNW_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="1CE2_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="1F9B_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1JNF" AND m.Fullmodel="1BP5_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ET6" AND m.Fullmodel="3CXR_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ET6" AND m.Fullmodel="1RWB_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ET6" AND m.Fullmodel="2PH3_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2DW4" AND m.Fullmodel="1UKU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2DW4" AND m.Fullmodel="2FQ3_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2DW4" AND m.Fullmodel="2C9N_Y");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WYG" AND m.Fullmodel="1FFU_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1WYG" AND m.Fullmodel="1FIQ_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CE2" AND m.Fullmodel="2H4O_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CE2" AND m.Fullmodel="3BUK_C");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AVC" AND m.Fullmodel="3BRX_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AVC" AND m.Fullmodel="1N00_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, template matched gene duplicated domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AVC" AND m.Fullmodel="1YCN_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CR9" AND m.Fullmodel="1H76_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3S1S" AND m.Fullmodel="2UUS_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3S1S" AND m.Fullmodel="2B2A_A");
UPDATE MRModelTbl SET CorrectSolution=1, Comment="solved, domain with hinge" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3B34" AND m.Fullmodel="1GW6_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3L4P" AND m.Fullmodel="2PJW_H");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3L4P" AND m.Fullmodel="1QAD_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3L4P" AND m.Fullmodel="1N5W_A");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YO8" AND m.Fullmodel="3C9A_C");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YO8" AND m.Fullmodel="3GCW_E");
UPDATE MRModelTbl SET CorrectSolution=0, Comment="unsolved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1YO8" AND m.Fullmodel="1EDM_B");


UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved, template wrong" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1CHU" AND m.Fullmodel="1NEK_A");
UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved, template matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1VFD" AND m.Fullmodel="1AOV_A");
UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1T45" AND m.Fullmodel="2GS7_A");
UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved, matching one hinge domain" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1OX3" AND m.Fullmodel="1AA0_A");
UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AEM" AND m.Fullmodel="2VKA_A");
UPDATE MRModelTbl SET CorrectSolutionNewRMS=1, Comment="solved, template out of register along row of helices" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1DCQ" AND m.Fullmodel="2QYJ_A");

-- for data with template solution not being topmost
UPDATE MRModelTbl SET Comment="unused, MTZ data possible twinned"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1AP9");
UPDATE MRModelTbl SET Comment="unused, MTZ data has issues"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1R2J");
UPDATE MRModelTbl SET Comment="unused, MTZ data has issues"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2AVP");
UPDATE MRModelTbl SET Comment="unused, MTZ data possible twinned"  WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1J2Z");
UPDATE MRModelTbl SET Comment="unused, possibly misindexed MTZ file" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2OVO");


UPDATE MRModelTbl SET Comment="unused, a.s.u. is overfilled"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m WHERE m.ModelCompleteness > 1.1);

UPDATE MRModelTbl SET Comment="unused, problematic MR" WHERE ROWID IN (SELECT m.ROWID FROM UniqueMRresultsTbl m WHERE m.TargetPDBid="3PMQ");
UPDATE MRModelTbl SET Comment="unused, problematic MR, high TFZ, low VRMS" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="2NMV_B");
UPDATE MRModelTbl SET Comment="unused, problematic MR, high TFZ" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1D9X" AND m.Fullmodel="2D7D_B");
UPDATE MRModelTbl SET Comment="unused, problematic MR, high LLG" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3CIG" AND m.Fullmodel="2O6S_A");

UPDATE MRModelTbl SET Comment="unused"                       WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1XCD" AND m.Fullmodel="2O6S_A");
UPDATE MRModelTbl SET Comment="unused, MTZ data is corrupt"            WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2BFN");
UPDATE MRModelTbl SET Comment="unused, MTZ file wrongly indexed upside down"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1BXV");
UPDATE MRModelTbl SET Comment="unused, memory allocation error" WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2FCP");

UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L02");
UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LMP");
UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L31");
UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L36" AND m.Fullmodel="2Z6B_A");
UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1LMP" AND m.Fullmodel="1HFZ_A");
UPDATE MRModelTbl SET Comment="unused, twinned, solved with modelmap-target correlation = 0.3"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="3B65" AND m.Fullmodel="1NHZ_A");
UPDATE MRModelTbl SET Comment="unused, Target structure has many clashes"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2TMN");
UPDATE MRModelTbl SET Comment="unused, phaser imposed a strange rotation of the unit cell"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1L45");
UPDATE MRModelTbl SET Comment="unused, MTZ data has problems"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1HJ6");
UPDATE MRModelTbl SET Comment="unused, TNCS in MTZ data"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2E26");

-- HeteroDimers
UPDATE MRModelTbl SET Comment="unused, MTZ data appears incomplete"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="2ADG");
UPDATE MRModelTbl SET Comment="unused, MTZ data appears incomplete"    WHERE ROWID IN (SELECT m.ROWID FROM MRModelTbl m, MRTargetTbl t WHERE t.p_id = m.MRTarget_id AND t.TargetPDBid="1SB2");





-- create histogram bins
CREATE TABLE TFZsmalliter(i int);
INSERT INTO TFZsmalliter values (0);
INSERT INTO TFZsmalliter values (0.5);
INSERT INTO TFZsmalliter values (1);
INSERT INTO TFZsmalliter values (1.5);
INSERT INTO TFZsmalliter values (2);
INSERT INTO TFZsmalliter values (2.5);
INSERT INTO TFZsmalliter values (3);
INSERT INTO TFZsmalliter values (3.5);
INSERT INTO TFZsmalliter values (4);
INSERT INTO TFZsmalliter values (4.5);
INSERT INTO TFZsmalliter values (5);
INSERT INTO TFZsmalliter values (5.5);
INSERT INTO TFZsmalliter values (6);
INSERT INTO TFZsmalliter values (6.5);
INSERT INTO TFZsmalliter values (7);
INSERT INTO TFZsmalliter values (7.5);
INSERT INTO TFZsmalliter values (8);
INSERT INTO TFZsmalliter values (8.5);
INSERT INTO TFZsmalliter values (9);
INSERT INTO TFZsmalliter values (9.5);
INSERT INTO TFZsmalliter values (10);
INSERT INTO TFZsmalliter values (10.5);
INSERT INTO TFZsmalliter values (11);
INSERT INTO TFZsmalliter values (11.5);
INSERT INTO TFZsmalliter values (12);
CREATE INDEX smiter on TFZsmalliter(i);

CREATE TABLE TFZiter1(i int);
INSERT INTO TFZiter1 values (0);
INSERT INTO TFZiter1 values (1);
INSERT INTO TFZiter1 values (2);
INSERT INTO TFZiter1 values (3);
INSERT INTO TFZiter1 values (4);
INSERT INTO TFZiter1 values (5);
INSERT INTO TFZiter1 values (6);
INSERT INTO TFZiter1 values (7);
INSERT INTO TFZiter1 values (8);
INSERT INTO TFZiter1 values (9);
INSERT INTO TFZiter1 values (10);
INSERT INTO TFZiter1 values (11);
INSERT INTO TFZiter1 values (12);
INSERT INTO TFZiter1 values (13);
INSERT INTO TFZiter1 values (14);
INSERT INTO TFZiter1 values (15);
INSERT INTO TFZiter1 values (16);
INSERT INTO TFZiter1 values (17);
INSERT INTO TFZiter1 values (18);
INSERT INTO TFZiter1 values (19);
INSERT INTO TFZiter1 values (20);
INSERT INTO TFZiter1 values (21);
INSERT INTO TFZiter1 values (22);
INSERT INTO TFZiter1 values (23);
INSERT INTO TFZiter1 values (24);
INSERT INTO TFZiter1 values (25);
INSERT INTO TFZiter1 values (26);
INSERT INTO TFZiter1 values (27);
INSERT INTO TFZiter1 values (28);
INSERT INTO TFZiter1 values (29);
INSERT INTO TFZiter1 values (30);
INSERT INTO TFZiter1 values (31);
INSERT INTO TFZiter1 values (32);
INSERT INTO TFZiter1 values (33);
INSERT INTO TFZiter1 values (34);
INSERT INTO TFZiter1 values (35);
INSERT INTO TFZiter1 values (36);
INSERT INTO TFZiter1 values (37);
INSERT INTO TFZiter1 values (38);
INSERT INTO TFZiter1 values (39);
INSERT INTO TFZiter1 values (40);
INSERT INTO TFZiter1 values (41);
INSERT INTO TFZiter1 values (42);
INSERT INTO TFZiter1 values (43);
INSERT INTO TFZiter1 values (44);
INSERT INTO TFZiter1 values (45);
INSERT INTO TFZiter1 values (46);
INSERT INTO TFZiter1 values (47);
INSERT INTO TFZiter1 values (48);
INSERT INTO TFZiter1 values (49);
INSERT INTO TFZiter1 values (50);
INSERT INTO TFZiter1 values (51);
INSERT INTO TFZiter1 values (52);
INSERT INTO TFZiter1 values (53);
INSERT INTO TFZiter1 values (54);
INSERT INTO TFZiter1 values (55);
INSERT INTO TFZiter1 values (56);
INSERT INTO TFZiter1 values (57);
INSERT INTO TFZiter1 values (58);
INSERT INTO TFZiter1 values (59);
INSERT INTO TFZiter1 values (60);
CREATE INDEX iter1 on TFZiter1(i);

-- generate table below with python code like:
-- >>> lstr = "INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( %2.1f, %d, %2.2f, %2.2f );"
-- >>> v = 0.0
-- >>> count = 0
-- >>> while v < 60:
-- ...   v += 0.5
-- ...   upper = v+0.25
-- ...   lower = v-0.25
-- ...   count += 1
-- ...   print lstr %(v, count, upper, lower)
-- 
CREATE TABLE TFZiter(i real, n int, deltaup real, deltalow real);
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 0.0, 0, 0.25, -1000.0 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 0.5, 1, 0.75, 0.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 1.0, 2, 1.25, 0.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 1.5, 3, 1.75, 1.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 2.0, 4, 2.25, 1.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 2.5, 5, 2.75, 2.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 3.0, 6, 3.25, 2.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 3.5, 7, 3.75, 3.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 4.0, 8, 4.25, 3.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 4.5, 9, 4.75, 4.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 5.0, 10, 5.25, 4.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 5.5, 11, 5.75, 5.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 6.0, 12, 6.25, 5.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 6.5, 13, 6.75, 6.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 7.0, 14, 7.25, 6.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 7.5, 15, 7.75, 7.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 8.0, 16, 8.25, 7.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 8.5, 17, 8.75, 8.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 9.0, 18, 9.25, 8.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 9.5, 19, 9.75, 9.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 10.0, 20, 10.25, 9.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 10.5, 21, 10.75, 10.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 11.0, 22, 11.25, 10.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 11.5, 23, 11.75, 11.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 12.0, 24, 12.25, 11.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 12.5, 25, 12.75, 12.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 13.0, 26, 13.25, 12.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 13.5, 27, 13.75, 13.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 14.0, 28, 14.25, 13.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 14.5, 29, 14.75, 14.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 15.0, 30, 15.25, 14.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 15.5, 31, 15.75, 15.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 16.0, 32, 16.25, 15.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 16.5, 33, 16.75, 16.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 17.0, 34, 17.25, 16.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 17.5, 35, 17.75, 17.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 18.0, 36, 18.25, 17.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 18.5, 37, 18.75, 18.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 19.0, 38, 19.25, 18.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 19.5, 39, 19.75, 19.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 20.0, 40, 20.25, 19.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 20.5, 41, 20.75, 20.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 21.0, 42, 21.25, 20.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 21.5, 43, 21.75, 21.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 22.0, 44, 22.25, 21.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 22.5, 45, 22.75, 22.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 23.0, 46, 23.25, 22.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 23.5, 47, 23.75, 23.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 24.0, 48, 24.25, 23.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 24.5, 49, 24.75, 24.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 25.0, 50, 25.25, 24.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 25.5, 51, 25.75, 25.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 26.0, 52, 26.25, 25.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 26.5, 53, 26.75, 26.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 27.0, 54, 27.25, 26.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 27.5, 55, 27.75, 27.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 28.0, 56, 28.25, 27.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 28.5, 57, 28.75, 28.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 29.0, 58, 29.25, 28.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 29.5, 59, 29.75, 29.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 30.0, 60, 30.25, 29.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 30.5, 61, 30.75, 30.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 31.0, 62, 31.25, 30.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 31.5, 63, 31.75, 31.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 32.0, 64, 32.25, 31.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 32.5, 65, 32.75, 32.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 33.0, 66, 33.25, 32.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 33.5, 67, 33.75, 33.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 34.0, 68, 34.25, 33.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 34.5, 69, 34.75, 34.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 35.0, 70, 35.25, 34.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 35.5, 71, 35.75, 35.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 36.0, 72, 36.25, 35.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 36.5, 73, 36.75, 36.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 37.0, 74, 37.25, 36.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 37.5, 75, 37.75, 37.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 38.0, 76, 38.25, 37.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 38.5, 77, 38.75, 38.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 39.0, 78, 39.25, 38.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 39.5, 79, 39.75, 39.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 40.0, 80, 40.25, 39.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 40.5, 81, 40.75, 40.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 41.0, 82, 41.25, 40.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 41.5, 83, 41.75, 41.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 42.0, 84, 42.25, 41.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 42.5, 85, 42.75, 42.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 43.0, 86, 43.25, 42.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 43.5, 87, 43.75, 43.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 44.0, 88, 44.25, 43.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 44.5, 89, 44.75, 44.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 45.0, 90, 45.25, 44.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 45.5, 91, 45.75, 45.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 46.0, 92, 46.25, 45.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 46.5, 93, 46.75, 46.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 47.0, 94, 47.25, 46.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 47.5, 95, 47.75, 47.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 48.0, 96, 48.25, 47.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 48.5, 97, 48.75, 48.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 49.0, 98, 49.25, 48.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 49.5, 99, 49.75, 49.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 50.0, 100, 50.25, 49.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 50.5, 101, 50.75, 50.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 51.0, 102, 51.25, 50.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 51.5, 103, 51.75, 51.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 52.0, 104, 52.25, 51.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 52.5, 105, 52.75, 52.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 53.0, 106, 53.25, 52.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 53.5, 107, 53.75, 53.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 54.0, 108, 54.25, 53.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 54.5, 109, 54.75, 54.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 55.0, 110, 55.25, 54.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 55.5, 111, 55.75, 55.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 56.0, 112, 56.25, 55.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 56.5, 113, 56.75, 56.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 57.0, 114, 57.25, 56.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 57.5, 115, 57.75, 57.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 58.0, 116, 58.25, 57.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 58.5, 117, 58.75, 58.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 59.0, 118, 59.25, 58.75 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 59.5, 119, 59.75, 59.25 );
INSERT INTO TFZiter (i, n, deltaup, deltalow) VALUES( 60.0, 120, 1000.0, 59.75 );

CREATE INDEX iter on TFZiter(i);

CREATE TABLE TFZbigiter(i int);
INSERT INTO TFZbigiter values (0);
INSERT INTO TFZbigiter values (2.5);
INSERT INTO TFZbigiter values (5);
INSERT INTO TFZbigiter values (7.5);
INSERT INTO TFZbigiter values (10);
INSERT INTO TFZbigiter values (12.5);
INSERT INTO TFZbigiter values (15);
INSERT INTO TFZbigiter values (17.5);
INSERT INTO TFZbigiter values (20);
INSERT INTO TFZbigiter values (22.5);
INSERT INTO TFZbigiter values (25);
INSERT INTO TFZbigiter values (27.5);
INSERT INTO TFZbigiter values (30);
INSERT INTO TFZbigiter values (32.5);
INSERT INTO TFZbigiter values (35);
INSERT INTO TFZbigiter values (37.5);
INSERT INTO TFZbigiter values (40);
INSERT INTO TFZbigiter values (42.5);
INSERT INTO TFZbigiter values (45);
INSERT INTO TFZbigiter values (47.5);
INSERT INTO TFZbigiter values (50);
INSERT INTO TFZbigiter values (52.5);
INSERT INTO TFZbigiter values (55);
INSERT INTO TFZbigiter values (57.5);
INSERT INTO TFZbigiter values (60);
CREATE INDEX biter on TFZbigiter(i);

CREATE TABLE FracVarbigiter(i real);
INSERT INTO FracVarbigiter values(0.0);
INSERT INTO FracVarbigiter values(0.025);
INSERT INTO FracVarbigiter values(0.05);
INSERT INTO FracVarbigiter values(0.075);
INSERT INTO FracVarbigiter values(0.1);
INSERT INTO FracVarbigiter values(0.125);
INSERT INTO FracVarbigiter values(0.15);
INSERT INTO FracVarbigiter values(0.175);
INSERT INTO FracVarbigiter values(0.2);
INSERT INTO FracVarbigiter values(0.225);
INSERT INTO FracVarbigiter values(0.25);
INSERT INTO FracVarbigiter values(0.275);
INSERT INTO FracVarbigiter values(0.3);
INSERT INTO FracVarbigiter values(0.325);
INSERT INTO FracVarbigiter values(0.35);
INSERT INTO FracVarbigiter values(0.375);
INSERT INTO FracVarbigiter values(0.4);
CREATE INDEX fbiter on FracVarbigiter(i);

CREATE TABLE FracVariter(i real);
INSERT INTO FracVariter values(0.0);
INSERT INTO FracVariter values(0.01);
INSERT INTO FracVariter values(0.02);
INSERT INTO FracVariter values(0.03);
INSERT INTO FracVariter values(0.04);
INSERT INTO FracVariter values(0.05);
INSERT INTO FracVariter values(0.06);
INSERT INTO FracVariter values(0.07);
INSERT INTO FracVariter values(0.08);
INSERT INTO FracVariter values(0.09);
INSERT INTO FracVariter values(0.1);
INSERT INTO FracVariter values(0.11);
INSERT INTO FracVariter values(0.12);
INSERT INTO FracVariter values(0.13);
INSERT INTO FracVariter values(0.14);
INSERT INTO FracVariter values(0.15);
INSERT INTO FracVariter values(0.16);
INSERT INTO FracVariter values(0.17);
INSERT INTO FracVariter values(0.18);
INSERT INTO FracVariter values(0.19);
INSERT INTO FracVariter values(0.2);
INSERT INTO FracVariter values(0.21);
INSERT INTO FracVariter values(0.22);
INSERT INTO FracVariter values(0.23);
INSERT INTO FracVariter values(0.24);
INSERT INTO FracVariter values(0.25);
INSERT INTO FracVariter values(0.26);
INSERT INTO FracVariter values(0.27);
INSERT INTO FracVariter values(0.28);
INSERT INTO FracVariter values(0.29);
INSERT INTO FracVariter values(0.3);
INSERT INTO FracVariter values(0.31);
INSERT INTO FracVariter values(0.32);
INSERT INTO FracVariter values(0.33);
INSERT INTO FracVariter values(0.34);
INSERT INTO FracVariter values(0.35);
INSERT INTO FracVariter values(0.36);
INSERT INTO FracVariter values(0.37);
INSERT INTO FracVariter values(0.38);
INSERT INTO FracVariter values(0.39);
INSERT INTO FracVariter values(0.4);
CREATE INDEX fiter on FracVariter(i);


CREATE TABLE LLG_vrmsbiggeriter(i real);
INSERT INTO LLG_vrmsbiggeriter VALUES(-0.02);
INSERT INTO LLG_vrmsbiggeriter VALUES(-0.01);
INSERT INTO LLG_vrmsbiggeriter VALUES(0);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.01);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.02);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.03);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.04);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.05);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.06);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.07);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.08);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.09);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.1);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.11);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.12);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.13);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.14);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.15);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.16);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.17);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.18);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.19);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.2);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.21);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.22);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.23);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.24);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.25);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.26);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.27);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.28);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.29);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.3);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.31);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.32);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.33);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.34);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.35);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.36);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.37);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.38);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.39);
INSERT INTO LLG_vrmsbiggeriter VALUES(0.4);
CREATE INDEX lbbiter on LLG_vrmsbiggeriter(i);


CREATE TABLE LLG_vrmsbigiter(i real);
INSERT INTO LLG_vrmsbigiter VALUES(-0.02);
INSERT INTO LLG_vrmsbigiter VALUES(-0.015);
INSERT INTO LLG_vrmsbigiter VALUES(-0.01);
INSERT INTO LLG_vrmsbigiter VALUES(-0.005);
INSERT INTO LLG_vrmsbigiter VALUES(0);
INSERT INTO LLG_vrmsbigiter VALUES(0.005);
INSERT INTO LLG_vrmsbigiter VALUES(0.01);
INSERT INTO LLG_vrmsbigiter VALUES(0.015);
INSERT INTO LLG_vrmsbigiter VALUES(0.02);
INSERT INTO LLG_vrmsbigiter VALUES(0.025);
INSERT INTO LLG_vrmsbigiter VALUES(0.03);
INSERT INTO LLG_vrmsbigiter VALUES(0.035);
INSERT INTO LLG_vrmsbigiter VALUES(0.04);
INSERT INTO LLG_vrmsbigiter VALUES(0.045);
INSERT INTO LLG_vrmsbigiter VALUES(0.05);
INSERT INTO LLG_vrmsbigiter VALUES(0.055);
INSERT INTO LLG_vrmsbigiter VALUES(0.06);
INSERT INTO LLG_vrmsbigiter VALUES(0.065);
INSERT INTO LLG_vrmsbigiter VALUES(0.07);
INSERT INTO LLG_vrmsbigiter VALUES(0.075);
INSERT INTO LLG_vrmsbigiter VALUES(0.08);
INSERT INTO LLG_vrmsbigiter VALUES(0.085);
INSERT INTO LLG_vrmsbigiter VALUES(0.09);
INSERT INTO LLG_vrmsbigiter VALUES(0.095);
INSERT INTO LLG_vrmsbigiter VALUES(0.1);
INSERT INTO LLG_vrmsbigiter VALUES(0.105);
INSERT INTO LLG_vrmsbigiter VALUES(0.11);
INSERT INTO LLG_vrmsbigiter VALUES(0.115);
INSERT INTO LLG_vrmsbigiter VALUES(0.12);
INSERT INTO LLG_vrmsbigiter VALUES(0.125);
INSERT INTO LLG_vrmsbigiter VALUES(0.13);
INSERT INTO LLG_vrmsbigiter VALUES(0.135);
INSERT INTO LLG_vrmsbigiter VALUES(0.14);
INSERT INTO LLG_vrmsbigiter VALUES(0.145);
INSERT INTO LLG_vrmsbigiter VALUES(0.15);
INSERT INTO LLG_vrmsbigiter VALUES(0.155);
INSERT INTO LLG_vrmsbigiter VALUES(0.16);
INSERT INTO LLG_vrmsbigiter VALUES(0.165);
INSERT INTO LLG_vrmsbigiter VALUES(0.17);
INSERT INTO LLG_vrmsbigiter VALUES(0.175);
INSERT INTO LLG_vrmsbigiter VALUES(0.18);
INSERT INTO LLG_vrmsbigiter VALUES(0.185);
INSERT INTO LLG_vrmsbigiter VALUES(0.19);
INSERT INTO LLG_vrmsbigiter VALUES(0.195);
INSERT INTO LLG_vrmsbigiter VALUES(0.2);
CREATE INDEX lbiter on LLG_vrmsbigiter(i);


CREATE TABLE LLG_vrmsiter(i real);
INSERT INTO LLG_vrmsiter VALUES(-0.02);
INSERT INTO LLG_vrmsiter VALUES(-0.0195);
INSERT INTO LLG_vrmsiter VALUES(-0.019);
INSERT INTO LLG_vrmsiter VALUES(-0.0185);
INSERT INTO LLG_vrmsiter VALUES(-0.018);
INSERT INTO LLG_vrmsiter VALUES(-0.0175);
INSERT INTO LLG_vrmsiter VALUES(-0.017);
INSERT INTO LLG_vrmsiter VALUES(-0.0165);
INSERT INTO LLG_vrmsiter VALUES(-0.016);
INSERT INTO LLG_vrmsiter VALUES(-0.0155);
INSERT INTO LLG_vrmsiter VALUES(-0.015);
INSERT INTO LLG_vrmsiter VALUES(-0.0145);
INSERT INTO LLG_vrmsiter VALUES(-0.014);
INSERT INTO LLG_vrmsiter VALUES(-0.0135);
INSERT INTO LLG_vrmsiter VALUES(-0.013);
INSERT INTO LLG_vrmsiter VALUES(-0.0125);
INSERT INTO LLG_vrmsiter VALUES(-0.012);
INSERT INTO LLG_vrmsiter VALUES(-0.0115);
INSERT INTO LLG_vrmsiter VALUES(-0.011);
INSERT INTO LLG_vrmsiter VALUES(-0.0105);
INSERT INTO LLG_vrmsiter VALUES(-0.01);
INSERT INTO LLG_vrmsiter VALUES(-0.0095);
INSERT INTO LLG_vrmsiter VALUES(-0.009);
INSERT INTO LLG_vrmsiter VALUES(-0.0085);
INSERT INTO LLG_vrmsiter VALUES(-0.008);
INSERT INTO LLG_vrmsiter VALUES(-0.0075);
INSERT INTO LLG_vrmsiter VALUES(-0.007);
INSERT INTO LLG_vrmsiter VALUES(-0.0065);
INSERT INTO LLG_vrmsiter VALUES(-0.006);
INSERT INTO LLG_vrmsiter VALUES(-0.0055);
INSERT INTO LLG_vrmsiter VALUES(-0.005);
INSERT INTO LLG_vrmsiter VALUES(-0.0045);
INSERT INTO LLG_vrmsiter VALUES(-0.004);
INSERT INTO LLG_vrmsiter VALUES(-0.0035);
INSERT INTO LLG_vrmsiter VALUES(-0.003);
INSERT INTO LLG_vrmsiter VALUES(-0.0025);
INSERT INTO LLG_vrmsiter VALUES(-0.002);
INSERT INTO LLG_vrmsiter VALUES(-0.0015);
INSERT INTO LLG_vrmsiter VALUES(-0.001);
INSERT INTO LLG_vrmsiter VALUES(-0.0005);
INSERT INTO LLG_vrmsiter VALUES(0.0);
INSERT INTO LLG_vrmsiter VALUES(0.0005);
INSERT INTO LLG_vrmsiter VALUES(0.001);
INSERT INTO LLG_vrmsiter VALUES(0.0015);
INSERT INTO LLG_vrmsiter VALUES(0.002);
INSERT INTO LLG_vrmsiter VALUES(0.0025);
INSERT INTO LLG_vrmsiter VALUES(0.003);
INSERT INTO LLG_vrmsiter VALUES(0.0035);
INSERT INTO LLG_vrmsiter VALUES(0.004);
INSERT INTO LLG_vrmsiter VALUES(0.0045);
INSERT INTO LLG_vrmsiter VALUES(0.005);
INSERT INTO LLG_vrmsiter VALUES(0.0055);
INSERT INTO LLG_vrmsiter VALUES(0.006);
INSERT INTO LLG_vrmsiter VALUES(0.0065);
INSERT INTO LLG_vrmsiter VALUES(0.007);
INSERT INTO LLG_vrmsiter VALUES(0.0075);
INSERT INTO LLG_vrmsiter VALUES(0.008);
INSERT INTO LLG_vrmsiter VALUES(0.0085);
INSERT INTO LLG_vrmsiter VALUES(0.009);
INSERT INTO LLG_vrmsiter VALUES(0.0095);
INSERT INTO LLG_vrmsiter VALUES(0.01);
INSERT INTO LLG_vrmsiter VALUES(0.0105);
INSERT INTO LLG_vrmsiter VALUES(0.011);
INSERT INTO LLG_vrmsiter VALUES(0.0115);
INSERT INTO LLG_vrmsiter VALUES(0.012);
INSERT INTO LLG_vrmsiter VALUES(0.0125);
INSERT INTO LLG_vrmsiter VALUES(0.013);
INSERT INTO LLG_vrmsiter VALUES(0.0135);
INSERT INTO LLG_vrmsiter VALUES(0.014);
INSERT INTO LLG_vrmsiter VALUES(0.0145);
INSERT INTO LLG_vrmsiter VALUES(0.015);
INSERT INTO LLG_vrmsiter VALUES(0.0155);
INSERT INTO LLG_vrmsiter VALUES(0.016);
INSERT INTO LLG_vrmsiter VALUES(0.0165);
INSERT INTO LLG_vrmsiter VALUES(0.017);
INSERT INTO LLG_vrmsiter VALUES(0.0175);
INSERT INTO LLG_vrmsiter VALUES(0.018);
INSERT INTO LLG_vrmsiter VALUES(0.0185);
INSERT INTO LLG_vrmsiter VALUES(0.019);
INSERT INTO LLG_vrmsiter VALUES(0.0195);
INSERT INTO LLG_vrmsiter VALUES(0.02);
CREATE INDEX liter on LLG_vrmsiter(i);


CREATE TABLE LLG_vrmsiter2(i real);
INSERT INTO LLG_vrmsiter2 VALUES(-0.02);
INSERT INTO LLG_vrmsiter2 VALUES(-0.019);
INSERT INTO LLG_vrmsiter2 VALUES(-0.018);
INSERT INTO LLG_vrmsiter2 VALUES(-0.017);
INSERT INTO LLG_vrmsiter2 VALUES(-0.016);
INSERT INTO LLG_vrmsiter2 VALUES(-0.015);
INSERT INTO LLG_vrmsiter2 VALUES(-0.014);
INSERT INTO LLG_vrmsiter2 VALUES(-0.013);
INSERT INTO LLG_vrmsiter2 VALUES(-0.012);
INSERT INTO LLG_vrmsiter2 VALUES(-0.011);
INSERT INTO LLG_vrmsiter2 VALUES(-0.01);
INSERT INTO LLG_vrmsiter2 VALUES(-0.009);
INSERT INTO LLG_vrmsiter2 VALUES(-0.008);
INSERT INTO LLG_vrmsiter2 VALUES(-0.007);
INSERT INTO LLG_vrmsiter2 VALUES(-0.006);
INSERT INTO LLG_vrmsiter2 VALUES(-0.005);
INSERT INTO LLG_vrmsiter2 VALUES(-0.004);
INSERT INTO LLG_vrmsiter2 VALUES(-0.003);
INSERT INTO LLG_vrmsiter2 VALUES(-0.002);
INSERT INTO LLG_vrmsiter2 VALUES(-0.001);
INSERT INTO LLG_vrmsiter2 VALUES(0.0);
INSERT INTO LLG_vrmsiter2 VALUES(0.001);
INSERT INTO LLG_vrmsiter2 VALUES(0.002);
INSERT INTO LLG_vrmsiter2 VALUES(0.003);
INSERT INTO LLG_vrmsiter2 VALUES(0.004);
INSERT INTO LLG_vrmsiter2 VALUES(0.005);
INSERT INTO LLG_vrmsiter2 VALUES(0.006);
INSERT INTO LLG_vrmsiter2 VALUES(0.007);
INSERT INTO LLG_vrmsiter2 VALUES(0.008);
INSERT INTO LLG_vrmsiter2 VALUES(0.009);
INSERT INTO LLG_vrmsiter2 VALUES(0.01);
INSERT INTO LLG_vrmsiter2 VALUES(0.011);
INSERT INTO LLG_vrmsiter2 VALUES(0.012);
INSERT INTO LLG_vrmsiter2 VALUES(0.013);
INSERT INTO LLG_vrmsiter2 VALUES(0.014);
INSERT INTO LLG_vrmsiter2 VALUES(0.015);
INSERT INTO LLG_vrmsiter2 VALUES(0.016);
INSERT INTO LLG_vrmsiter2 VALUES(0.017);
INSERT INTO LLG_vrmsiter2 VALUES(0.018);
INSERT INTO LLG_vrmsiter2 VALUES(0.019);
INSERT INTO LLG_vrmsiter2 VALUES(0.02);
CREATE INDEX liter2 on LLG_vrmsiter2(i);


CREATE TABLE LLG_vrmsiter3(i real);
INSERT INTO LLG_vrmsiter3 VALUES(0.0);
INSERT INTO LLG_vrmsiter3 VALUES(0.0025);
INSERT INTO LLG_vrmsiter3 VALUES(0.005);
INSERT INTO LLG_vrmsiter3 VALUES(0.0075);
INSERT INTO LLG_vrmsiter3 VALUES(0.01);
INSERT INTO LLG_vrmsiter3 VALUES(0.0125);
INSERT INTO LLG_vrmsiter3 VALUES(0.015);
INSERT INTO LLG_vrmsiter3 VALUES(0.0175);
INSERT INTO LLG_vrmsiter3 VALUES(0.02);
INSERT INTO LLG_vrmsiter3 VALUES(0.0225);
INSERT INTO LLG_vrmsiter3 VALUES(0.025);
INSERT INTO LLG_vrmsiter3 VALUES(0.0275);
INSERT INTO LLG_vrmsiter3 VALUES(0.03);
INSERT INTO LLG_vrmsiter3 VALUES(0.0325);
INSERT INTO LLG_vrmsiter3 VALUES(0.035);
INSERT INTO LLG_vrmsiter3 VALUES(0.0375);
INSERT INTO LLG_vrmsiter3 VALUES(0.04);
INSERT INTO LLG_vrmsiter3 VALUES(0.0425);
INSERT INTO LLG_vrmsiter3 VALUES(0.045);
INSERT INTO LLG_vrmsiter3 VALUES(0.0475);
INSERT INTO LLG_vrmsiter3 VALUES(0.05);
INSERT INTO LLG_vrmsiter3 VALUES(0.0525);
INSERT INTO LLG_vrmsiter3 VALUES(0.055);
INSERT INTO LLG_vrmsiter3 VALUES(0.0575);
INSERT INTO LLG_vrmsiter3 VALUES(0.06);
CREATE INDEX liter3 on LLG_vrmsiter3(i);



CREATE TABLE LLGiter(i real);
INSERT INTO LLGiter VALUES(0.0);
INSERT INTO LLGiter VALUES(10.0);
INSERT INTO LLGiter VALUES(20.0);
INSERT INTO LLGiter VALUES(30.0);
INSERT INTO LLGiter VALUES(40.0);
INSERT INTO LLGiter VALUES(50.0);
INSERT INTO LLGiter VALUES(60.0);
INSERT INTO LLGiter VALUES(70.0);
INSERT INTO LLGiter VALUES(80.0);
INSERT INTO LLGiter VALUES(90.0);
INSERT INTO LLGiter VALUES(100.0);
INSERT INTO LLGiter VALUES(110.0);
INSERT INTO LLGiter VALUES(120.0);
INSERT INTO LLGiter VALUES(130.0);
INSERT INTO LLGiter VALUES(140.0);
INSERT INTO LLGiter VALUES(150.0);
INSERT INTO LLGiter VALUES(160.0);
INSERT INTO LLGiter VALUES(170.0);
INSERT INTO LLGiter VALUES(180.0);
INSERT INTO LLGiter VALUES(190.0);
INSERT INTO LLGiter VALUES(200.0);
INSERT INTO LLGiter VALUES(210.0);
INSERT INTO LLGiter VALUES(220.0);
INSERT INTO LLGiter VALUES(230.0);
INSERT INTO LLGiter VALUES(240.0);
INSERT INTO LLGiter VALUES(250.0);
INSERT INTO LLGiter VALUES(260.0);
INSERT INTO LLGiter VALUES(270.0);
INSERT INTO LLGiter VALUES(280.0);
INSERT INTO LLGiter VALUES(290.0);
INSERT INTO LLGiter VALUES(300.0);
INSERT INTO LLGiter VALUES(310.0);
INSERT INTO LLGiter VALUES(320.0);
INSERT INTO LLGiter VALUES(330.0);
INSERT INTO LLGiter VALUES(340.0);
INSERT INTO LLGiter VALUES(350.0);
INSERT INTO LLGiter VALUES(360.0);
INSERT INTO LLGiter VALUES(370.0);
INSERT INTO LLGiter VALUES(380.0);
INSERT INTO LLGiter VALUES(390.0);
INSERT INTO LLGiter VALUES(400.0);
INSERT INTO LLGiter VALUES(510.0);
INSERT INTO LLGiter VALUES(520.0);
INSERT INTO LLGiter VALUES(530.0);
INSERT INTO LLGiter VALUES(540.0);
INSERT INTO LLGiter VALUES(550.0);
INSERT INTO LLGiter VALUES(560.0);
INSERT INTO LLGiter VALUES(570.0);
INSERT INTO LLGiter VALUES(580.0);
INSERT INTO LLGiter VALUES(590.0);
INSERT INTO LLGiter VALUES(600.0);
INSERT INTO LLGiter VALUES(610.0);
INSERT INTO LLGiter VALUES(620.0);
INSERT INTO LLGiter VALUES(630.0);
INSERT INTO LLGiter VALUES(640.0);
INSERT INTO LLGiter VALUES(650.0);
INSERT INTO LLGiter VALUES(660.0);
INSERT INTO LLGiter VALUES(670.0);
INSERT INTO LLGiter VALUES(680.0);
INSERT INTO LLGiter VALUES(690.0);
INSERT INTO LLGiter VALUES(700.0);
INSERT INTO LLGiter VALUES(710.0);
INSERT INTO LLGiter VALUES(720.0);
INSERT INTO LLGiter VALUES(730.0);
INSERT INTO LLGiter VALUES(740.0);
INSERT INTO LLGiter VALUES(750.0);
INSERT INTO LLGiter VALUES(760.0);
INSERT INTO LLGiter VALUES(770.0);
INSERT INTO LLGiter VALUES(780.0);
INSERT INTO LLGiter VALUES(790.0);
INSERT INTO LLGiter VALUES(800.0);
INSERT INTO LLGiter VALUES(810.0);
INSERT INTO LLGiter VALUES(820.0);
INSERT INTO LLGiter VALUES(830.0);
INSERT INTO LLGiter VALUES(840.0);
INSERT INTO LLGiter VALUES(850.0);
INSERT INTO LLGiter VALUES(860.0);
INSERT INTO LLGiter VALUES(870.0);
INSERT INTO LLGiter VALUES(880.0);
INSERT INTO LLGiter VALUES(890.0);
INSERT INTO LLGiter VALUES(900.0);
INSERT INTO LLGiter VALUES(910.0);
INSERT INTO LLGiter VALUES(920.0);
INSERT INTO LLGiter VALUES(930.0);
INSERT INTO LLGiter VALUES(940.0);
INSERT INTO LLGiter VALUES(950.0);
INSERT INTO LLGiter VALUES(960.0);
INSERT INTO LLGiter VALUES(970.0);
INSERT INTO LLGiter VALUES(980.0);
INSERT INTO LLGiter VALUES(990.0);
INSERT INTO LLGiter VALUES(1000.0);
CREATE INDEX iter on LLGiter(i);



CREATE TABLE LLGiter2(i real);
INSERT INTO LLGiter2 VALUES(0.0);
INSERT INTO LLGiter2 VALUES(20.0);
INSERT INTO LLGiter2 VALUES(40.0);
INSERT INTO LLGiter2 VALUES(60.0);
INSERT INTO LLGiter2 VALUES(80.0);
INSERT INTO LLGiter2 VALUES(100.0);
INSERT INTO LLGiter2 VALUES(120.0);
INSERT INTO LLGiter2 VALUES(140.0);
INSERT INTO LLGiter2 VALUES(160.0);
INSERT INTO LLGiter2 VALUES(180.0);
INSERT INTO LLGiter2 VALUES(200.0);
INSERT INTO LLGiter2 VALUES(220.0);
INSERT INTO LLGiter2 VALUES(240.0);
INSERT INTO LLGiter2 VALUES(260.0);
INSERT INTO LLGiter2 VALUES(280.0);
INSERT INTO LLGiter2 VALUES(300.0);
INSERT INTO LLGiter2 VALUES(320.0);
INSERT INTO LLGiter2 VALUES(340.0);
INSERT INTO LLGiter2 VALUES(360.0);
INSERT INTO LLGiter2 VALUES(380.0);
INSERT INTO LLGiter2 VALUES(400.0);
INSERT INTO LLGiter2 VALUES(420.0);
INSERT INTO LLGiter2 VALUES(440.0);
INSERT INTO LLGiter2 VALUES(460.0);
INSERT INTO LLGiter2 VALUES(480.0);
INSERT INTO LLGiter2 VALUES(500.0);
INSERT INTO LLGiter2 VALUES(520.0);
INSERT INTO LLGiter2 VALUES(540.0);
INSERT INTO LLGiter2 VALUES(560.0);
INSERT INTO LLGiter2 VALUES(580.0);
INSERT INTO LLGiter2 VALUES(600.0);
INSERT INTO LLGiter2 VALUES(620.0);
INSERT INTO LLGiter2 VALUES(640.0);
INSERT INTO LLGiter2 VALUES(660.0);
INSERT INTO LLGiter2 VALUES(680.0);
INSERT INTO LLGiter2 VALUES(700.0);
INSERT INTO LLGiter2 VALUES(720.0);
INSERT INTO LLGiter2 VALUES(740.0);
INSERT INTO LLGiter2 VALUES(760.0);
INSERT INTO LLGiter2 VALUES(780.0);
INSERT INTO LLGiter2 VALUES(800.0);
INSERT INTO LLGiter2 VALUES(820.0);
INSERT INTO LLGiter2 VALUES(840.0);
INSERT INTO LLGiter2 VALUES(860.0);
INSERT INTO LLGiter2 VALUES(880.0);
INSERT INTO LLGiter2 VALUES(900.0);
INSERT INTO LLGiter2 VALUES(920.0);
INSERT INTO LLGiter2 VALUES(940.0);
INSERT INTO LLGiter2 VALUES(960.0);
INSERT INTO LLGiter2 VALUES(980.0);
INSERT INTO LLGiter2 VALUES(1000.0);
CREATE INDEX iter2 on LLGiter2(i);



CREATE TABLE LLGiter3(i real, n int, deltaup real, deltalow real);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(0.0, 0, 2.5, -99999.0);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(5.0, 1, 7.5, 2.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(10.0, 2, 12.5, 7.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(15.0, 3, 17.5, 12.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(20.0, 4, 22.5, 17.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(25.0, 5,  27.5, 22.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(30.0, 6, 32.5, 27.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(35.0, 7, 37.5, 32.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(40.0, 8, 42.5, 37.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(45.0, 9, 47.5, 42.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(50.0, 10, 52.5, 47.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(55.0, 11,  57.5, 52.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(60.0, 12, 62.5, 57.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(65.0, 13, 67.5, 62.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(70.0, 14, 72.5, 67.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(75.0, 15, 77.5, 72.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(80.0, 16, 82.5, 77.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(85.0, 17, 87.5, 82.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(90.0, 18, 92.5, 87.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(95.0, 19, 97.5, 92.5);
INSERT INTO LLGiter3 (i, n, deltaup, deltalow) VALUES(100.0, 20, 99999.0, 97.5);
CREATE INDEX iter3 on LLGiter3(i);


CREATE TABLE LLGiter4(i real);
INSERT INTO LLGiter4 VALUES(0.0);
INSERT INTO LLGiter4 VALUES(10.0);
INSERT INTO LLGiter4 VALUES(20.0);
INSERT INTO LLGiter4 VALUES(30.0);
INSERT INTO LLGiter4 VALUES(40.0);
INSERT INTO LLGiter4 VALUES(50.0);
INSERT INTO LLGiter4 VALUES(60.0);
INSERT INTO LLGiter4 VALUES(70.0);
INSERT INTO LLGiter4 VALUES(80.0);
INSERT INTO LLGiter4 VALUES(90.0);
INSERT INTO LLGiter4 VALUES(100.0);
INSERT INTO LLGiter4 VALUES(110.0);
INSERT INTO LLGiter4 VALUES(120.0);
INSERT INTO LLGiter4 VALUES(130.0);
INSERT INTO LLGiter4 VALUES(140.0);
INSERT INTO LLGiter4 VALUES(150.0);
CREATE INDEX iter4 on LLGiter4(i);

CREATE TABLE LLGiter5(i real);
INSERT INTO LLGiter5 VALUES(0.0);
INSERT INTO LLGiter5 VALUES(5.0);
INSERT INTO LLGiter5 VALUES(10.0);
INSERT INTO LLGiter5 VALUES(15.0);
INSERT INTO LLGiter5 VALUES(20.0);
INSERT INTO LLGiter5 VALUES(25.0);
INSERT INTO LLGiter5 VALUES(30.0);
INSERT INTO LLGiter5 VALUES(35.0);
INSERT INTO LLGiter5 VALUES(40.0);
INSERT INTO LLGiter5 VALUES(45.0);
INSERT INTO LLGiter5 VALUES(50.0);
INSERT INTO LLGiter5 VALUES(55.0);
INSERT INTO LLGiter5 VALUES(60.0);
INSERT INTO LLGiter5 VALUES(65.0);
INSERT INTO LLGiter5 VALUES(70.0);
INSERT INTO LLGiter5 VALUES(75.0);
INSERT INTO LLGiter5 VALUES(80.0);
INSERT INTO LLGiter5 VALUES(85.0);
INSERT INTO LLGiter5 VALUES(90.0);
INSERT INTO LLGiter5 VALUES(95.0);
INSERT INTO LLGiter5 VALUES(100.0);
INSERT INTO LLGiter5 VALUES(105.0);
INSERT INTO LLGiter5 VALUES(110.0);
INSERT INTO LLGiter5 VALUES(115.0);
INSERT INTO LLGiter5 VALUES(120.0);
INSERT INTO LLGiter5 VALUES(125.0);
INSERT INTO LLGiter5 VALUES(130.0);
INSERT INTO LLGiter5 VALUES(135.0);
INSERT INTO LLGiter5 VALUES(140.0);
INSERT INTO LLGiter5 VALUES(145.0);
INSERT INTO LLGiter5 VALUES(150.0);
CREATE INDEX iter3 on LLGiter5(i);


CREATE TABLE LLGiter6(i real);
INSERT INTO LLGiter6 VALUES(0.0);
INSERT INTO LLGiter6 VALUES(2.5);
INSERT INTO LLGiter6 VALUES(5);
INSERT INTO LLGiter6 VALUES(7.5);
INSERT INTO LLGiter6 VALUES(10);
INSERT INTO LLGiter6 VALUES(12.5);
INSERT INTO LLGiter6 VALUES(15);
INSERT INTO LLGiter6 VALUES(17.5);
INSERT INTO LLGiter6 VALUES(20);
INSERT INTO LLGiter6 VALUES(22.5);
INSERT INTO LLGiter6 VALUES(25);
INSERT INTO LLGiter6 VALUES(27.5);
INSERT INTO LLGiter6 VALUES(30);
INSERT INTO LLGiter6 VALUES(32.5);
INSERT INTO LLGiter6 VALUES(35);
INSERT INTO LLGiter6 VALUES(37.5);
INSERT INTO LLGiter6 VALUES(40);
INSERT INTO LLGiter6 VALUES(42.5);
INSERT INTO LLGiter6 VALUES(45);
INSERT INTO LLGiter6 VALUES(47.5);
INSERT INTO LLGiter6 VALUES(50);
INSERT INTO LLGiter6 VALUES(52.5);
INSERT INTO LLGiter6 VALUES(55);
INSERT INTO LLGiter6 VALUES(57.5);
INSERT INTO LLGiter6 VALUES(60);
INSERT INTO LLGiter6 VALUES(62.5);
INSERT INTO LLGiter6 VALUES(65);
INSERT INTO LLGiter6 VALUES(67.5);
INSERT INTO LLGiter6 VALUES(70);
INSERT INTO LLGiter6 VALUES(72.5);
INSERT INTO LLGiter6 VALUES(75);
INSERT INTO LLGiter6 VALUES(77.5);
INSERT INTO LLGiter6 VALUES(80);
CREATE INDEX llgindx6 on LLGiter6(i);


CREATE TABLE LLGiter7(i real);
INSERT INTO LLGiter7 VALUES(0.0);
INSERT INTO LLGiter7 VALUES(1);
INSERT INTO LLGiter7 VALUES(2);
INSERT INTO LLGiter7 VALUES(3);
INSERT INTO LLGiter7 VALUES(4);
INSERT INTO LLGiter7 VALUES(5);
INSERT INTO LLGiter7 VALUES(6);
INSERT INTO LLGiter7 VALUES(7);
INSERT INTO LLGiter7 VALUES(8);
INSERT INTO LLGiter7 VALUES(9);
INSERT INTO LLGiter7 VALUES(10);
INSERT INTO LLGiter7 VALUES(11);
INSERT INTO LLGiter7 VALUES(12);
INSERT INTO LLGiter7 VALUES(13);
INSERT INTO LLGiter7 VALUES(14);
INSERT INTO LLGiter7 VALUES(15);
INSERT INTO LLGiter7 VALUES(16);
INSERT INTO LLGiter7 VALUES(17);
INSERT INTO LLGiter7 VALUES(18);
INSERT INTO LLGiter7 VALUES(19);
INSERT INTO LLGiter7 VALUES(20);
INSERT INTO LLGiter7 VALUES(21);
INSERT INTO LLGiter7 VALUES(22);
INSERT INTO LLGiter7 VALUES(23);
INSERT INTO LLGiter7 VALUES(24);
INSERT INTO LLGiter7 VALUES(25);
INSERT INTO LLGiter7 VALUES(26);
INSERT INTO LLGiter7 VALUES(27);
INSERT INTO LLGiter7 VALUES(28);
INSERT INTO LLGiter7 VALUES(29);
INSERT INTO LLGiter7 VALUES(30);
INSERT INTO LLGiter7 VALUES(31);
INSERT INTO LLGiter7 VALUES(32);
INSERT INTO LLGiter7 VALUES(33);
INSERT INTO LLGiter7 VALUES(34);
INSERT INTO LLGiter7 VALUES(35);
INSERT INTO LLGiter7 VALUES(36);
INSERT INTO LLGiter7 VALUES(37);
INSERT INTO LLGiter7 VALUES(38);
INSERT INTO LLGiter7 VALUES(39);
INSERT INTO LLGiter7 VALUES(40);
CREATE INDEX llgindx7 on LLGiter7(i);

CREATE TABLE dLLGiter(i real);
INSERT INTO dLLGiter VALUES(-100.0);
INSERT INTO dLLGiter VALUES(-95.0);
INSERT INTO dLLGiter VALUES(-90.0);
INSERT INTO dLLGiter VALUES(-85.0);
INSERT INTO dLLGiter VALUES(-80.0);
INSERT INTO dLLGiter VALUES(-75.0);
INSERT INTO dLLGiter VALUES(-70.0);
INSERT INTO dLLGiter VALUES(-65.0);
INSERT INTO dLLGiter VALUES(-60.0);
INSERT INTO dLLGiter VALUES(-55.0);
INSERT INTO dLLGiter VALUES(-50.0);
INSERT INTO dLLGiter VALUES(-45.0);
INSERT INTO dLLGiter VALUES(-40.0);
INSERT INTO dLLGiter VALUES(-35.0);
INSERT INTO dLLGiter VALUES(-30.0);
INSERT INTO dLLGiter VALUES(-25.0);
INSERT INTO dLLGiter VALUES(-20.0);
INSERT INTO dLLGiter VALUES(-15.0);
INSERT INTO dLLGiter VALUES(-10.0);
INSERT INTO dLLGiter VALUES(-5.0);
INSERT INTO dLLGiter VALUES(0.0);
INSERT INTO dLLGiter VALUES(5.0);
INSERT INTO dLLGiter VALUES(10.0);
INSERT INTO dLLGiter VALUES(15.0);
INSERT INTO dLLGiter VALUES(20.0);
INSERT INTO dLLGiter VALUES(25.0);
INSERT INTO dLLGiter VALUES(30.0);
INSERT INTO dLLGiter VALUES(35.0);
INSERT INTO dLLGiter VALUES(40.0);
INSERT INTO dLLGiter VALUES(45.0);
INSERT INTO dLLGiter VALUES(50.0);
INSERT INTO dLLGiter VALUES(55.0);
INSERT INTO dLLGiter VALUES(60.0);
INSERT INTO dLLGiter VALUES(65.0);
INSERT INTO dLLGiter VALUES(70.0);
INSERT INTO dLLGiter VALUES(75.0);
INSERT INTO dLLGiter VALUES(80.0);
INSERT INTO dLLGiter VALUES(85.0);
INSERT INTO dLLGiter VALUES(90.0);
INSERT INTO dLLGiter VALUES(95.0);
INSERT INTO dLLGiter VALUES(100.0);
CREATE INDEX dllgndx on dLLGiter(i);


CREATE TABLE XtalSysTbl(name text);
INSERT INTO XtalSysTbl VALUES("Trigonal");
INSERT INTO XtalSysTbl VALUES("Orthorhombic");
INSERT INTO XtalSysTbl VALUES("Monoclinic");
INSERT INTO XtalSysTbl VALUES("Triclinic");
INSERT INTO XtalSysTbl VALUES("Cubic");
INSERT INTO XtalSysTbl VALUES("Hexagonal");
INSERT INTO XtalSysTbl VALUES("Tetragonal");
CREATE INDEX xtalsysindx on XtalSysTbl(name);


CREATE TABLE PointgroupTbl(pg int);
INSERT INTO PointgroupTbl VALUES(2);
INSERT INTO PointgroupTbl VALUES(3);
INSERT INTO PointgroupTbl VALUES(4);
INSERT INTO PointgroupTbl VALUES(6);
INSERT INTO PointgroupTbl VALUES(8);
INSERT INTO PointgroupTbl VALUES(12);
CREATE INDEX Pointgroupindx on PointgroupTbl(pg);


CREATE TABLE IsPolarTbl(ispolar int);
INSERT INTO IsPolarTbl VALUES(0);
INSERT INTO IsPolarTbl VALUES(1);
INSERT INTO IsPolarTbl VALUES(2);



CREATE TABLE SEQiditer(i real);
INSERT INTO SEQiditer VALUES(15);
INSERT INTO SEQiditer VALUES(18);
INSERT INTO SEQiditer VALUES(21);
INSERT INTO SEQiditer VALUES(24);
INSERT INTO SEQiditer VALUES(27);
INSERT INTO SEQiditer VALUES(30);
INSERT INTO SEQiditer VALUES(33);
INSERT INTO SEQiditer VALUES(36);
INSERT INTO SEQiditer VALUES(39);
INSERT INTO SEQiditer VALUES(42);
INSERT INTO SEQiditer VALUES(45);
INSERT INTO SEQiditer VALUES(48);
INSERT INTO SEQiditer VALUES(51);
INSERT INTO SEQiditer VALUES(54);
INSERT INTO SEQiditer VALUES(57);
INSERT INTO SEQiditer VALUES(60);
INSERT INTO SEQiditer VALUES(63);
INSERT INTO SEQiditer VALUES(66);
INSERT INTO SEQiditer VALUES(69);
INSERT INTO SEQiditer VALUES(72);
INSERT INTO SEQiditer VALUES(75);
CREATE INDEX siter on SEQiditer(i);

CREATE TABLE SEQidbigiter(i real);
INSERT INTO SEQidbigiter VALUES(5);
INSERT INTO SEQidbigiter VALUES(10);
INSERT INTO SEQidbigiter VALUES(15);
INSERT INTO SEQidbigiter VALUES(20);
INSERT INTO SEQidbigiter VALUES(25);
INSERT INTO SEQidbigiter VALUES(30);
INSERT INTO SEQidbigiter VALUES(35);
INSERT INTO SEQidbigiter VALUES(40);
INSERT INTO SEQidbigiter VALUES(45);
INSERT INTO SEQidbigiter VALUES(50);
INSERT INTO SEQidbigiter VALUES(55);
INSERT INTO SEQidbigiter VALUES(60);
INSERT INTO SEQidbigiter VALUES(65);
INSERT INTO SEQidbigiter VALUES(70);
INSERT INTO SEQidbigiter VALUES(75);
INSERT INTO SEQidbigiter VALUES(80);
INSERT INTO SEQidbigiter VALUES(85);
INSERT INTO SEQidbigiter VALUES(90);
INSERT INTO SEQidbigiter VALUES(95);
INSERT INTO SEQidbigiter VALUES(100);
CREATE INDEX sbiter on SEQidbigiter(i);


CREATE TABLE Resiter(i real);
INSERT INTO Resiter VALUES(0.5);
INSERT INTO Resiter VALUES(0.7);
INSERT INTO Resiter VALUES(0.9);
INSERT INTO Resiter VALUES(1.1);
INSERT INTO Resiter VALUES(1.3);
INSERT INTO Resiter VALUES(1.5);
INSERT INTO Resiter VALUES(1.7);
INSERT INTO Resiter VALUES(1.9);
INSERT INTO Resiter VALUES(2.1);
INSERT INTO Resiter VALUES(2.3);
INSERT INTO Resiter VALUES(2.5);
INSERT INTO Resiter VALUES(2.7);
INSERT INTO Resiter VALUES(2.9);
INSERT INTO Resiter VALUES(3.1);
INSERT INTO Resiter VALUES(3.3);
INSERT INTO Resiter VALUES(3.5);
INSERT INTO Resiter VALUES(3.7);
INSERT INTO Resiter VALUES(3.9);
INSERT INTO Resiter VALUES(4.1);
INSERT INTO Resiter VALUES(4.3);
INSERT INTO Resiter VALUES(4.5);
INSERT INTO Resiter VALUES(4.7);
INSERT INTO Resiter VALUES(4.9);
INSERT INTO Resiter VALUES(5.1);
CREATE INDEX resindx on Resiter(i);

CREATE TABLE ModelCompliter(i real);
INSERT INTO ModelCompliter VALUES(0.1);
INSERT INTO ModelCompliter VALUES(0.2);
INSERT INTO ModelCompliter VALUES(0.3);
INSERT INTO ModelCompliter VALUES(0.4);
INSERT INTO ModelCompliter VALUES(0.5);
INSERT INTO ModelCompliter VALUES(0.6);
INSERT INTO ModelCompliter VALUES(0.7);
INSERT INTO ModelCompliter VALUES(0.8);
INSERT INTO ModelCompliter VALUES(0.9);
INSERT INTO ModelCompliter VALUES(1.0);
CREATE INDEX mdlcmplindx on ModelCompliter(i);

CREATE TABLE Residuesiter(i real);
INSERT INTO Residuesiter VALUES(0);
INSERT INTO Residuesiter VALUES(100);
INSERT INTO Residuesiter VALUES(200);
INSERT INTO Residuesiter VALUES(300);
INSERT INTO Residuesiter VALUES(400);
INSERT INTO Residuesiter VALUES(500);
INSERT INTO Residuesiter VALUES(600);
INSERT INTO Residuesiter VALUES(700);
INSERT INTO Residuesiter VALUES(800);
INSERT INTO Residuesiter VALUES(900);
INSERT INTO Residuesiter VALUES(1000);
INSERT INTO Residuesiter VALUES(1100);
INSERT INTO Residuesiter VALUES(1200);
INSERT INTO Residuesiter VALUES(1300);
INSERT INTO Residuesiter VALUES(1400);
INSERT INTO Residuesiter VALUES(1500);
INSERT INTO Residuesiter VALUES(1600);
INSERT INTO Residuesiter VALUES(1700);
INSERT INTO Residuesiter VALUES(1800);
CREATE INDEX residueindx on Residuesiter(i);

CREATE TABLE Residuesiter2(i real);
INSERT INTO Residuesiter2 VALUES(0);
INSERT INTO Residuesiter2 VALUES(50);
INSERT INTO Residuesiter2 VALUES(100);
INSERT INTO Residuesiter2 VALUES(150);
INSERT INTO Residuesiter2 VALUES(200);
INSERT INTO Residuesiter2 VALUES(250);
INSERT INTO Residuesiter2 VALUES(300);
INSERT INTO Residuesiter2 VALUES(350);
INSERT INTO Residuesiter2 VALUES(400);
INSERT INTO Residuesiter2 VALUES(450);
INSERT INTO Residuesiter2 VALUES(500);
INSERT INTO Residuesiter2 VALUES(550);
INSERT INTO Residuesiter2 VALUES(600);
INSERT INTO Residuesiter2 VALUES(650);
INSERT INTO Residuesiter2 VALUES(700);
INSERT INTO Residuesiter2 VALUES(750);
INSERT INTO Residuesiter2 VALUES(800);
INSERT INTO Residuesiter2 VALUES(850);
INSERT INTO Residuesiter2 VALUES(900);
INSERT INTO Residuesiter2 VALUES(950);
INSERT INTO Residuesiter2 VALUES(1000);
INSERT INTO Residuesiter2 VALUES(1050);
INSERT INTO Residuesiter2 VALUES(1100);
INSERT INTO Residuesiter2 VALUES(1150);
INSERT INTO Residuesiter2 VALUES(1200);
INSERT INTO Residuesiter2 VALUES(1250);
INSERT INTO Residuesiter2 VALUES(1300);
INSERT INTO Residuesiter2 VALUES(1350);
INSERT INTO Residuesiter2 VALUES(1400);
INSERT INTO Residuesiter2 VALUES(1450);
INSERT INTO Residuesiter2 VALUES(1500);
INSERT INTO Residuesiter2 VALUES(1550);
INSERT INTO Residuesiter2 VALUES(1600);
INSERT INTO Residuesiter2 VALUES(1650);
INSERT INTO Residuesiter2 VALUES(1700);
INSERT INTO Residuesiter2 VALUES(1750);
INSERT INTO Residuesiter2 VALUES(1800);
INSERT INTO Residuesiter2 VALUES(1850);
CREATE INDEX residue2indx on Residuesiter2(i);


CREATE TABLE Residuesiter1(i real);
INSERT INTO Residuesiter1 VALUES(0);
INSERT INTO Residuesiter1 VALUES(200);
INSERT INTO Residuesiter1 VALUES(400);
INSERT INTO Residuesiter1 VALUES(600);
INSERT INTO Residuesiter1 VALUES(800);
INSERT INTO Residuesiter1 VALUES(1000);
INSERT INTO Residuesiter1 VALUES(1200);
INSERT INTO Residuesiter1 VALUES(1400);
INSERT INTO Residuesiter1 VALUES(1600);
INSERT INTO Residuesiter1 VALUES(1800);
CREATE INDEX residue1indx on Residuesiter1(i);


CREATE TABLE Residuesiter3(i real);
INSERT INTO Residuesiter3 VALUES(0);
INSERT INTO Residuesiter3 VALUES(200);
INSERT INTO Residuesiter3 VALUES(2000);

CREATE TABLE Residuesiter4(i real);
INSERT INTO Residuesiter4 VALUES(0);
INSERT INTO Residuesiter4 VALUES(50);
INSERT INTO Residuesiter4 VALUES(100);
INSERT INTO Residuesiter4 VALUES(150);
INSERT INTO Residuesiter4 VALUES(200);
INSERT INTO Residuesiter4 VALUES(250);
INSERT INTO Residuesiter4 VALUES(300);
INSERT INTO Residuesiter4 VALUES(350);
INSERT INTO Residuesiter4 VALUES(400);
INSERT INTO Residuesiter4 VALUES(450);
INSERT INTO Residuesiter4 VALUES(500);
INSERT INTO Residuesiter4 VALUES(550);
INSERT INTO Residuesiter4 VALUES(600);
INSERT INTO Residuesiter4 VALUES(650);
INSERT INTO Residuesiter4 VALUES(700);
INSERT INTO Residuesiter4 VALUES(750);
INSERT INTO Residuesiter4 VALUES(800);
INSERT INTO Residuesiter4 VALUES(850);
INSERT INTO Residuesiter4 VALUES(900);
INSERT INTO Residuesiter4 VALUES(950);
INSERT INTO Residuesiter4 VALUES(1000);
INSERT INTO Residuesiter4 VALUES(1050);
INSERT INTO Residuesiter4 VALUES(1100);
INSERT INTO Residuesiter4 VALUES(1150);
INSERT INTO Residuesiter4 VALUES(1200);
INSERT INTO Residuesiter4 VALUES(1250);
INSERT INTO Residuesiter4 VALUES(1300);
INSERT INTO Residuesiter4 VALUES(1350);
INSERT INTO Residuesiter4 VALUES(1400);
INSERT INTO Residuesiter4 VALUES(1450);
INSERT INTO Residuesiter4 VALUES(1500);
INSERT INTO Residuesiter4 VALUES(1550);
INSERT INTO Residuesiter4 VALUES(1600);
INSERT INTO Residuesiter4 VALUES(1650);
INSERT INTO Residuesiter4 VALUES(1700);
INSERT INTO Residuesiter4 VALUES(1750);
INSERT INTO Residuesiter4 VALUES(1800);
INSERT INTO Residuesiter4 VALUES(1850);
CREATE INDEX residueindx4 on Residuesiter4(i);


CREATE TABLE SCOPClasses(name text);
INSERT INTO SCOPClasses VALUES("All alpha proteins");
INSERT INTO SCOPClasses VALUES("All beta proteins");
INSERT INTO SCOPClasses VALUES("Alpha and beta proteins (a+b)");
INSERT INTO SCOPClasses VALUES("Alpha and beta proteins (a/b)");
INSERT INTO SCOPClasses VALUES("Coiled coil proteins");
INSERT INTO SCOPClasses VALUES("Designed proteins");
INSERT INTO SCOPClasses VALUES("Membrane and cell surface proteins and peptides");
INSERT INTO SCOPClasses VALUES("Multi-domain proteins (alpha and beta)");
INSERT INTO SCOPClasses VALUES("Small proteins");



CREATE TABLE RMSD_VRMSratioIter(f real);
INSERT INTO RMSD_VRMSratioIter VALUES(0.3);
INSERT INTO RMSD_VRMSratioIter VALUES(0.35);
INSERT INTO RMSD_VRMSratioIter VALUES(0.4);
INSERT INTO RMSD_VRMSratioIter VALUES(0.45);
INSERT INTO RMSD_VRMSratioIter VALUES(0.5);
INSERT INTO RMSD_VRMSratioIter VALUES(0.55);
INSERT INTO RMSD_VRMSratioIter VALUES(0.6);
INSERT INTO RMSD_VRMSratioIter VALUES(0.65);
INSERT INTO RMSD_VRMSratioIter VALUES(0.7);
INSERT INTO RMSD_VRMSratioIter VALUES(0.75);
INSERT INTO RMSD_VRMSratioIter VALUES(0.8);
INSERT INTO RMSD_VRMSratioIter VALUES(0.85);
INSERT INTO RMSD_VRMSratioIter VALUES(0.9);
INSERT INTO RMSD_VRMSratioIter VALUES(0.95);
INSERT INTO RMSD_VRMSratioIter VALUES(1.0);
INSERT INTO RMSD_VRMSratioIter VALUES(1.05);
INSERT INTO RMSD_VRMSratioIter VALUES(1.1);
INSERT INTO RMSD_VRMSratioIter VALUES(1.15);
INSERT INTO RMSD_VRMSratioIter VALUES(1.2);
INSERT INTO RMSD_VRMSratioIter VALUES(1.25);
INSERT INTO RMSD_VRMSratioIter VALUES(1.3);
INSERT INTO RMSD_VRMSratioIter VALUES(1.35);
INSERT INTO RMSD_VRMSratioIter VALUES(1.4);
INSERT INTO RMSD_VRMSratioIter VALUES(1.45);
INSERT INTO RMSD_VRMSratioIter VALUES(1.5);
INSERT INTO RMSD_VRMSratioIter VALUES(1.55);
INSERT INTO RMSD_VRMSratioIter VALUES(1.6);
INSERT INTO RMSD_VRMSratioIter VALUES(1.65);
INSERT INTO RMSD_VRMSratioIter VALUES(1.7);
INSERT INTO RMSD_VRMSratioIter VALUES(1.75);
INSERT INTO RMSD_VRMSratioIter VALUES(1.8);
INSERT INTO RMSD_VRMSratioIter VALUES(1.85);
INSERT INTO RMSD_VRMSratioIter VALUES(1.9);
INSERT INTO RMSD_VRMSratioIter VALUES(1.95);
INSERT INTO RMSD_VRMSratioIter VALUES(2.0);
INSERT INTO RMSD_VRMSratioIter VALUES(2.05);
CREATE INDEX RMSD_VRMSratioidx ON RMSD_VRMSratioIter(f);


CREATE TABLE ASUVoliter(i real);
INSERT INTO ASUVoliter VALUES(0);
INSERT INTO ASUVoliter VALUES(25000);
INSERT INTO ASUVoliter VALUES(50000);
INSERT INTO ASUVoliter VALUES(75000);
INSERT INTO ASUVoliter VALUES(100000);
INSERT INTO ASUVoliter VALUES(125000);
INSERT INTO ASUVoliter VALUES(150000);
INSERT INTO ASUVoliter VALUES(175000);
INSERT INTO ASUVoliter VALUES(200000);
INSERT INTO ASUVoliter VALUES(225000);
INSERT INTO ASUVoliter VALUES(250000);
INSERT INTO ASUVoliter VALUES(275000);
INSERT INTO ASUVoliter VALUES(300000);
INSERT INTO ASUVoliter VALUES(325000);
INSERT INTO ASUVoliter VALUES(350000);
INSERT INTO ASUVoliter VALUES(375000);
INSERT INTO ASUVoliter VALUES(400000);
INSERT INTO ASUVoliter VALUES(425000);
INSERT INTO ASUVoliter VALUES(450000);
INSERT INTO ASUVoliter VALUES(475000);
INSERT INTO ASUVoliter VALUES(500000);
INSERT INTO ASUVoliter VALUES(525000);
INSERT INTO ASUVoliter VALUES(550000);
INSERT INTO ASUVoliter VALUES(575000);
INSERT INTO ASUVoliter VALUES(600000);
INSERT INTO ASUVoliter VALUES(625000);
INSERT INTO ASUVoliter VALUES(650000);
INSERT INTO ASUVoliter VALUES(675000);
INSERT INTO ASUVoliter VALUES(700000);
INSERT INTO ASUVoliter VALUES(825000);
INSERT INTO ASUVoliter VALUES(850000);
INSERT INTO ASUVoliter VALUES(875000);
INSERT INTO ASUVoliter VALUES(800000);
CREATE INDEX ASUVoliterindx on ASUVoliter(i);


CREATE TABLE RMSDiter(i real);
INSERT INTO RMSDiter VALUES(0.0);
INSERT INTO RMSDiter VALUES(0.1);
INSERT INTO RMSDiter VALUES(0.2);
INSERT INTO RMSDiter VALUES(0.3);
INSERT INTO RMSDiter VALUES(0.4);
INSERT INTO RMSDiter VALUES(0.5);
INSERT INTO RMSDiter VALUES(0.6);
INSERT INTO RMSDiter VALUES(0.7);
INSERT INTO RMSDiter VALUES(0.8);
INSERT INTO RMSDiter VALUES(0.9);
INSERT INTO RMSDiter VALUES(1.0);
INSERT INTO RMSDiter VALUES(1.1);
INSERT INTO RMSDiter VALUES(1.2);
INSERT INTO RMSDiter VALUES(1.3);
INSERT INTO RMSDiter VALUES(1.4);
INSERT INTO RMSDiter VALUES(1.5);
INSERT INTO RMSDiter VALUES(1.6);
INSERT INTO RMSDiter VALUES(1.7);
INSERT INTO RMSDiter VALUES(1.8);
INSERT INTO RMSDiter VALUES(1.9);
INSERT INTO RMSDiter VALUES(2.0);
INSERT INTO RMSDiter VALUES(2.1);
INSERT INTO RMSDiter VALUES(2.2);
INSERT INTO RMSDiter VALUES(2.3);
INSERT INTO RMSDiter VALUES(2.4);
INSERT INTO RMSDiter VALUES(2.5);
INSERT INTO RMSDiter VALUES(2.6);
INSERT INTO RMSDiter VALUES(2.7);
INSERT INTO RMSDiter VALUES(2.8);
INSERT INTO RMSDiter VALUES(2.9);
INSERT INTO RMSDiter VALUES(3.0);
INSERT INTO RMSDiter VALUES(3.1);
INSERT INTO RMSDiter VALUES(3.2);
INSERT INTO RMSDiter VALUES(3.3);
INSERT INTO RMSDiter VALUES(3.4);
INSERT INTO RMSDiter VALUES(3.5);
INSERT INTO RMSDiter VALUES(3.6);
INSERT INTO RMSDiter VALUES(3.7);
INSERT INTO RMSDiter VALUES(3.8);
INSERT INTO RMSDiter VALUES(3.9);
INSERT INTO RMSDiter VALUES(4.0);
INSERT INTO RMSDiter VALUES(4.1);
INSERT INTO RMSDiter VALUES(4.2);
INSERT INTO RMSDiter VALUES(4.3);
INSERT INTO RMSDiter VALUES(4.4);
INSERT INTO RMSDiter VALUES(4.5);
INSERT INTO RMSDiter VALUES(4.6);
INSERT INTO RMSDiter VALUES(4.7);
INSERT INTO RMSDiter VALUES(4.8);
INSERT INTO RMSDiter VALUES(4.9);
INSERT INTO RMSDiter VALUES(5.0);
INSERT INTO RMSDiter VALUES(5.1);
INSERT INTO RMSDiter VALUES(5.2);
INSERT INTO RMSDiter VALUES(5.3);
INSERT INTO RMSDiter VALUES(5.4);
INSERT INTO RMSDiter VALUES(5.5);
INSERT INTO RMSDiter VALUES(5.6);
INSERT INTO RMSDiter VALUES(5.7);
INSERT INTO RMSDiter VALUES(5.8);
INSERT INTO RMSDiter VALUES(5.9);
CREATE INDEX RMSDindx on RMSDiter(i);


CREATE TABLE RMSDiter2(i real);
INSERT INTO RMSDiter2 VALUES(0.0);
INSERT INTO RMSDiter2 VALUES(0.2);
INSERT INTO RMSDiter2 VALUES(0.4);
INSERT INTO RMSDiter2 VALUES(0.6);
INSERT INTO RMSDiter2 VALUES(0.8);
INSERT INTO RMSDiter2 VALUES(1.0);
INSERT INTO RMSDiter2 VALUES(1.2);
INSERT INTO RMSDiter2 VALUES(1.4);
INSERT INTO RMSDiter2 VALUES(1.6);
INSERT INTO RMSDiter2 VALUES(1.8);
INSERT INTO RMSDiter2 VALUES(2.0);
INSERT INTO RMSDiter2 VALUES(2.2);
INSERT INTO RMSDiter2 VALUES(2.4);
INSERT INTO RMSDiter2 VALUES(2.6);
INSERT INTO RMSDiter2 VALUES(2.8);
INSERT INTO RMSDiter2 VALUES(2.0);
INSERT INTO RMSDiter2 VALUES(3.2);
INSERT INTO RMSDiter2 VALUES(3.4);
INSERT INTO RMSDiter2 VALUES(3.6);
INSERT INTO RMSDiter2 VALUES(3.8);
INSERT INTO RMSDiter2 VALUES(4.0);
INSERT INTO RMSDiter2 VALUES(4.2);
INSERT INTO RMSDiter2 VALUES(4.4);
INSERT INTO RMSDiter2 VALUES(4.6);
INSERT INTO RMSDiter2 VALUES(4.8);
INSERT INTO RMSDiter2 VALUES(5.0);
INSERT INTO RMSDiter2 VALUES(5.2);
INSERT INTO RMSDiter2 VALUES(5.4);
INSERT INTO RMSDiter2 VALUES(5.6);
INSERT INTO RMSDiter2 VALUES(5.8);
CREATE INDEX RMSDindx2 on RMSDiter2(i);


CREATE TABLE OCCiter(i real);
INSERT INTO OCCiter VALUES(0.0);
INSERT INTO OCCiter VALUES(0.05);
INSERT INTO OCCiter VALUES(0.1);
INSERT INTO OCCiter VALUES(0.15);
INSERT INTO OCCiter VALUES(0.2);
INSERT INTO OCCiter VALUES(0.25);
INSERT INTO OCCiter VALUES(0.3);
INSERT INTO OCCiter VALUES(0.35);
INSERT INTO OCCiter VALUES(0.4);
INSERT INTO OCCiter VALUES(0.45);
INSERT INTO OCCiter VALUES(0.5);
INSERT INTO OCCiter VALUES(0.55);
INSERT INTO OCCiter VALUES(0.6);
INSERT INTO OCCiter VALUES(0.65);
INSERT INTO OCCiter VALUES(0.7);
INSERT INTO OCCiter VALUES(0.75);
INSERT INTO OCCiter VALUES(0.8);
INSERT INTO OCCiter VALUES(0.85);
INSERT INTO OCCiter VALUES(0.9);
INSERT INTO OCCiter VALUES(0.95);
INSERT INTO OCCiter VALUES(1.0);
CREATE INDEX OCCindx on OCCiter(i);

CREATE TABLE dCCiter(i real);
INSERT INTO dCCiter VALUES(-0.1);
INSERT INTO dCCiter VALUES(-0.09);
INSERT INTO dCCiter VALUES(-0.08);
INSERT INTO dCCiter VALUES(-0.07);
INSERT INTO dCCiter VALUES(-0.06);
INSERT INTO dCCiter VALUES(-0.05);
INSERT INTO dCCiter VALUES(-0.04);
INSERT INTO dCCiter VALUES(-0.03);
INSERT INTO dCCiter VALUES(-0.02);
INSERT INTO dCCiter VALUES(-0.01);
INSERT INTO dCCiter VALUES(0.0);
INSERT INTO dCCiter VALUES(0.01);
INSERT INTO dCCiter VALUES(0.02);
INSERT INTO dCCiter VALUES(0.03);
INSERT INTO dCCiter VALUES(0.04);
INSERT INTO dCCiter VALUES(0.05);
INSERT INTO dCCiter VALUES(0.06);
INSERT INTO dCCiter VALUES(0.07);
INSERT INTO dCCiter VALUES(0.08);
INSERT INTO dCCiter VALUES(0.09);
INSERT INTO dCCiter VALUES(0.1);
INSERT INTO dCCiter VALUES(0.11);
INSERT INTO dCCiter VALUES(0.12);
INSERT INTO dCCiter VALUES(0.13);
INSERT INTO dCCiter VALUES(0.14);
INSERT INTO dCCiter VALUES(0.15);
INSERT INTO dCCiter VALUES(0.16);
INSERT INTO dCCiter VALUES(0.17);
INSERT INTO dCCiter VALUES(0.18);
INSERT INTO dCCiter VALUES(0.19);
INSERT INTO dCCiter VALUES(0.2);
CREATE INDEX dCCindx on dCCiter(i);

CREATE TABLE dCCiter2(i real);
INSERT INTO dCCiter2 VALUES(-0.05);
INSERT INTO dCCiter2 VALUES(-0.045);
INSERT INTO dCCiter2 VALUES(-0.04);
INSERT INTO dCCiter2 VALUES(-0.035);
INSERT INTO dCCiter2 VALUES(-0.03);
INSERT INTO dCCiter2 VALUES(-0.025);
INSERT INTO dCCiter2 VALUES(-0.02);
INSERT INTO dCCiter2 VALUES(-0.015);
INSERT INTO dCCiter2 VALUES(-0.01);
INSERT INTO dCCiter2 VALUES(-0.005);
INSERT INTO dCCiter2 VALUES(0.0);
INSERT INTO dCCiter2 VALUES(0.005);
INSERT INTO dCCiter2 VALUES(0.01);
INSERT INTO dCCiter2 VALUES(0.015);
INSERT INTO dCCiter2 VALUES(0.02);
INSERT INTO dCCiter2 VALUES(0.025);
INSERT INTO dCCiter2 VALUES(0.03);
INSERT INTO dCCiter2 VALUES(0.035);
INSERT INTO dCCiter2 VALUES(0.04);
INSERT INTO dCCiter2 VALUES(0.045);
INSERT INTO dCCiter2 VALUES(0.05);
INSERT INTO dCCiter2 VALUES(0.055);
INSERT INTO dCCiter2 VALUES(0.06);
INSERT INTO dCCiter2 VALUES(0.065);
INSERT INTO dCCiter2 VALUES(0.07);
INSERT INTO dCCiter2 VALUES(0.075);
INSERT INTO dCCiter2 VALUES(0.08);
INSERT INTO dCCiter2 VALUES(0.085);
INSERT INTO dCCiter2 VALUES(0.09);
INSERT INTO dCCiter2 VALUES(0.095);
INSERT INTO dCCiter2 VALUES(0.1);
INSERT INTO dCCiter2 VALUES(0.105);
INSERT INTO dCCiter2 VALUES(0.11);
INSERT INTO dCCiter2 VALUES(0.115);
INSERT INTO dCCiter2 VALUES(0.12);
INSERT INTO dCCiter2 VALUES(0.125);
INSERT INTO dCCiter2 VALUES(0.13);
INSERT INTO dCCiter2 VALUES(0.135);
INSERT INTO dCCiter2 VALUES(0.14);
INSERT INTO dCCiter2 VALUES(0.145);
INSERT INTO dCCiter2 VALUES(0.15);
CREATE INDEX dCCindx2 on dCCiter2(i);

CREATE TABLE Nresoccwnditer(i real);
INSERT INTO Nresoccwnditer VALUES(1);
INSERT INTO Nresoccwnditer VALUES(3);
INSERT INTO Nresoccwnditer VALUES(5);
INSERT INTO Nresoccwnditer VALUES(7);
INSERT INTO Nresoccwnditer VALUES(9);
INSERT INTO Nresoccwnditer VALUES(11);
INSERT INTO Nresoccwnditer VALUES(13);
INSERT INTO Nresoccwnditer VALUES(15);
INSERT INTO Nresoccwnditer VALUES(15);
INSERT INTO Nresoccwnditer VALUES(17);
INSERT INTO Nresoccwnditer VALUES(19);
INSERT INTO Nresoccwnditer VALUES(21);
INSERT INTO Nresoccwnditer VALUES(23);
INSERT INTO Nresoccwnditer VALUES(25);
INSERT INTO Nresoccwnditer VALUES(25);
INSERT INTO Nresoccwnditer VALUES(27);
INSERT INTO Nresoccwnditer VALUES(29);
INSERT INTO Nresoccwnditer VALUES(31);
INSERT INTO Nresoccwnditer VALUES(33);
INSERT INTO Nresoccwnditer VALUES(35);
INSERT INTO Nresoccwnditer VALUES(37);
INSERT INTO Nresoccwnditer VALUES(39);
INSERT INTO Nresoccwnditer VALUES(41);
INSERT INTO Nresoccwnditer VALUES(43);
INSERT INTO Nresoccwnditer VALUES(45);
INSERT INTO Nresoccwnditer VALUES(47);
INSERT INTO Nresoccwnditer VALUES(49);
INSERT INTO Nresoccwnditer VALUES(51);
INSERT INTO Nresoccwnditer VALUES(53);
INSERT INTO Nresoccwnditer VALUES(55);
INSERT INTO Nresoccwnditer VALUES(57);
INSERT INTO Nresoccwnditer VALUES(59);
INSERT INTO Nresoccwnditer VALUES(61);
INSERT INTO Nresoccwnditer VALUES(63);
INSERT INTO Nresoccwnditer VALUES(65);
INSERT INTO Nresoccwnditer VALUES(67);
INSERT INTO Nresoccwnditer VALUES(69);
INSERT INTO Nresoccwnditer VALUES(71);
INSERT INTO Nresoccwnditer VALUES(73);
INSERT INTO Nresoccwnditer VALUES(75);
INSERT INTO Nresoccwnditer VALUES(77);
INSERT INTO Nresoccwnditer VALUES(79);
INSERT INTO Nresoccwnditer VALUES(81);
INSERT INTO Nresoccwnditer VALUES(83);
INSERT INTO Nresoccwnditer VALUES(85);
INSERT INTO Nresoccwnditer VALUES(87);
INSERT INTO Nresoccwnditer VALUES(89);
CREATE INDEX Nresoccwndidx on Nresoccwnditer(i);

CREATE TABLE CCiter(i real);
INSERT INTO CCiter values(0.0);
INSERT INTO CCiter values(0.025);
INSERT INTO CCiter values(0.05);
INSERT INTO CCiter values(0.075);
INSERT INTO CCiter values(0.1);
INSERT INTO CCiter values(0.125);
INSERT INTO CCiter values(0.15);
INSERT INTO CCiter values(0.175);
INSERT INTO CCiter values(0.2);
INSERT INTO CCiter values(0.225);
INSERT INTO CCiter values(0.25);
INSERT INTO CCiter values(0.275);
INSERT INTO CCiter values(0.3);
INSERT INTO CCiter values(0.325);
INSERT INTO CCiter values(0.35);
INSERT INTO CCiter values(0.375);
INSERT INTO CCiter values(0.4);
INSERT INTO CCiter values(0.425);
INSERT INTO CCiter values(0.45);
INSERT INTO CCiter values(0.475);
INSERT INTO CCiter values(0.5);
INSERT INTO CCiter values(0.525);
INSERT INTO CCiter values(0.55);
INSERT INTO CCiter values(0.575);
INSERT INTO CCiter values(0.6);
INSERT INTO CCiter values(0.625);
INSERT INTO CCiter values(0.65);
INSERT INTO CCiter values(0.675);
INSERT INTO CCiter values(0.7);
INSERT INTO CCiter values(0.725);
INSERT INTO CCiter values(0.75);
INSERT INTO CCiter values(0.775);
INSERT INTO CCiter values(0.8);
INSERT INTO CCiter values(0.825);
INSERT INTO CCiter values(0.85);
INSERT INTO CCiter values(0.875);
INSERT INTO CCiter values(0.9);
INSERT INTO CCiter values(0.925);
INSERT INTO CCiter values(0.95);
INSERT INTO CCiter values(0.975);
INSERT INTO CCiter values(1.0);
CREATE INDEX cciteridx on CCiter(i);


CREATE TABLE Resiter(i real);
INSERT INTO Resiter VALUES(0.0);
INSERT INTO Resiter VALUES(1.0);
INSERT INTO Resiter VALUES(2.0);
INSERT INTO Resiter VALUES(3.0);
INSERT INTO Resiter VALUES(4.0);
INSERT INTO Resiter VALUES(5.0);
INSERT INTO Resiter VALUES(6.0);
INSERT INTO Resiter VALUES(7.0);
INSERT INTO Resiter VALUES(8.0);
INSERT INTO Resiter VALUES(9.0);
INSERT INTO Resiter VALUES(10.0);
INSERT INTO Resiter VALUES(11.0);
INSERT INTO Resiter VALUES(12.0);
INSERT INTO Resiter VALUES(13.0);
INSERT INTO Resiter VALUES(14.0);
INSERT INTO Resiter VALUES(15.0);
INSERT INTO Resiter VALUES(16.0);
INSERT INTO Resiter VALUES(17.0);
INSERT INTO Resiter VALUES(18.0);
INSERT INTO Resiter VALUES(19.0);
INSERT INTO Resiter VALUES(20.0);
INSERT INTO Resiter VALUES(21.0);
INSERT INTO Resiter VALUES(22.0);
INSERT INTO Resiter VALUES(23.0);
INSERT INTO Resiter VALUES(24.0);
INSERT INTO Resiter VALUES(25.0);
INSERT INTO Resiter VALUES(26.0);
INSERT INTO Resiter VALUES(27.0);
INSERT INTO Resiter VALUES(28.0);
INSERT INTO Resiter VALUES(29.0);
INSERT INTO Resiter VALUES(30.0);


CREATE TABLE MassIter(i real);
INSERT INTO MassIter VALUES(0.0);
INSERT INTO MassIter VALUES(250000);
INSERT INTO MassIter VALUES(500000);
INSERT INTO MassIter VALUES(750000);
INSERT INTO MassIter VALUES(1000000);
INSERT INTO MassIter VALUES(1250000);
INSERT INTO MassIter VALUES(1500000);
INSERT INTO MassIter VALUES(1750000);
INSERT INTO MassIter VALUES(2000000);
INSERT INTO MassIter VALUES(2250000);
INSERT INTO MassIter VALUES(2500000);
INSERT INTO MassIter VALUES(2750000);
INSERT INTO MassIter VALUES(3000000);
INSERT INTO MassIter VALUES(3250000);
INSERT INTO MassIter VALUES(3500000);
INSERT INTO MassIter VALUES(3750000);
INSERT INTO MassIter VALUES(4000000);
INSERT INTO MassIter VALUES(4250000);
INSERT INTO MassIter VALUES(4500000);
INSERT INTO MassIter VALUES(4750000);
INSERT INTO MassIter VALUES(5000000);
INSERT INTO MassIter VALUES(5250000);
INSERT INTO MassIter VALUES(5500000);
INSERT INTO MassIter VALUES(5750000);
INSERT INTO MassIter VALUES(6000000);
INSERT INTO MassIter VALUES(6250000);
INSERT INTO MassIter VALUES(6500000);
INSERT INTO MassIter VALUES(6750000);
INSERT INTO MassIter VALUES(7000000);
INSERT INTO MassIter VALUES(7250000);
INSERT INTO MassIter VALUES(7500000);
INSERT INTO MassIter VALUES(7750000);
INSERT INTO MassIter VALUES(8000000);
INSERT INTO MassIter VALUES(8250000);
INSERT INTO MassIter VALUES(8500000);
INSERT INTO MassIter VALUES(8750000);
INSERT INTO MassIter VALUES(9000000);

